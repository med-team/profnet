*----------------------------------------------------------------------*
      SUBROUTINE RS_READHSSPx(IUNIT,FILE_HSSP_LOC,LERROR,
     +     MAXRES,MAXALIGNS_LOC,MAXCORE_LOC,
     +     PDBID,HEADER,COMPOUND,SOURCE,AUTHOR,SEQLENGTH,
     +     NCHAIN,KCHAIN,CHAINREMARK,NALIGN,
     +     EXCLUDEFLAG,EMBLID,STRID,IDE,SIM,
     +     IFIR,ILAS,JFIR,JLAS,LALI,NGAP,LGAP,
     +     LENSEQ,ACCNUM,IPROTNAME,
     +     PDBNO,PDBSEQ,CHAINID,SECSTR,COLS,SHEETLABEL,BP1,BP2,
     +     ACC,NOCC,VAR,ALISEQ,ALIPOINTER,
     +     SEQPROF,NDEL,NINS,ENTROPY,RELENT,CONSWEIGHT,
     +     LCONSERV,LOLDVERSION)

C---- global parameters
      INCLUDE 'phdParameter.f'

C Reinhard Schneider 1989, BIOcomputing EMBL, D-6900 Heidelberg, FRG
C please report any bug, e-mail (INTERNET):
C     schneider@EMBL-Heidelberg.DE 
C or  sander@EMBL-Heidelberg.DE    
C=======================================================================
C  INCREASE THE NUMBER OF FOLLOWING THREE PARAMETER IN THE CALLING 
C  PROGRAM IF NECESSARY
C=======================================================================
C  maxaligns = maximal number of alignments in a HSSP-file
C  maxres= maximal number of residues in a PDB-protein
C  maxcore= maximal space for storing the alignments
C=======================================================================
C  maxaa= 20 amino acids
C  nblocksize= number of alignments in one line
C  pdbid= Brookhaven Data Bank identifier
C  header,compound,source,author= informations about the PDB-protein
C  pdbseq= amino acid sequence of the PDB-protein
C  chainid= chain identifier (chain A etc.)
C  secstr= DSSP secondary structure summary
C  bp1,bp2= beta-bridge partner
C  cols= DSSP hydrogen bonding patterns for turns and helices,
C        geometrical bend, chirality, one character name of beta-ladder
C        and of beta-sheet
C  sheetlabel= chain identifier of beta bridge partner
C  seqlength= number of amino acids in the PDB-protein 
C  pdbno= residue number as in PDB file
C  nchain= number of different chains in pdbid.DSSP data set
C  kchain= number of chains used in HSSP data set
C  nalign= number of alignments
C  acc= solvated residue surface area in A**2 
C  emblid= EMBL/SWISSPROT identifier of the alignend protein
C  strid= if the 3-D structure of this protein is known, then strid 
C         (structure ID)is the Protein Data Bank identifier as taken
C         from the EMBL/SWISSPROT entry
C  iprotname= one line description of alignend protein
C  aliseq= sequential storage for the alignments
C  alipointer= points to the beginning of alignment X ( 1>= X <=nalign )
C  ifir,ilas= first and last position of the alignment in the test
C             protein
C  jfir,jlas= first and last position of the alignment in the alignend
C             protein
C  lali= length of the alignment excluding insertions and deletions
C  ngap= number of insertions and deletions in the alignment
C  lgap= total length of all insertions and deletions
C  lenseq= length of the entire sequence of the alignend protein
C  ide= percentage of residue identity of the alignment
C  var= sequence variability as derived from the nalign alignments
C  seqprof= relative frequency for each of the 20 amino acids
C  nocc= number of alignend sequences spanning this position (including
C        the test sequence
C  ndel= number of sequences with a deletion in the test protein at this
C        position
C  nins= number of sequences with an insertion in the test protein at
C        this position
C  entropy= entropy measure of sequence variability at this position
C  relent= relative entropy (entropy normalized to the range 0-100)
C  consweight= conservation weight
C=======================================================================
C      IMPLICIT        NONE
C      INTEGER         NBLOCKSIZE	
C      PARAMETER      (NBLOCKSIZE=               70)

C      INTEGER         MAXRES,MAXAA
C      INTEGER         MAXALIGNS,MAXCORE
C      PARAMETER      (MAXAA=                    20)
C============================ import ==================================
C      CHARACTER*222   FILE_HSSP_LOC
      CHARACTER*(*)   FILE_HSSP_LOC
      INTEGER         IUNIT
      LOGICAL         LERROR	
C attributes of sequence with known structure
C      CHARACTER*222   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
C      CHARACTER*(*)   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
      CHARACTER*132   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
      CHARACTER       PDBSEQ(NUMRESMAX),CHAINID(NUMRESMAX),
     +                SECSTR(NUMRESMAX)
C.......length*7
      CHARACTER*7     COLS(NUMRESMAX)
      CHARACTER*132   CHAINREMARK
C     br 2003-08-23: save space
C      CHARACTER       SHEETLABEL(NUMRESMAX)
      CHARACTER       SHEETLABEL(1)
      CHARACTER       SHEETLABEL_NULL
C     br 2003-08-23: end save space

      INTEGER         SEQLENGTH,NCHAIN,KCHAIN,NALIGN
      INTEGER         ACC(NUMRESMAX)

C     br 2003-08-23: save space
C      INTEGER         BP1(NUMRESMAX),BP2(NUMRESMAX),PDBNO(NUMRESMAX)
      INTEGER         BP1(1),BP2(1),PDBNO(1)
      INTEGER         PDBNO_NULL,BP1_NULL,BP2_NULL
C     br 2003-08-23: end save space

C attributes of alignend sequences
C      CHARACTER*222   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
C     +                ACCNUM(MAXALIGNS),IPROTNAME(MAXALIGNS),
C     +                EXCLUDEFLAG(MAXALIGNS)
C      CHARACTER*(*)   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
C     +                ACCNUM(MAXALIGNS),IPROTNAME(MAXALIGNS),
C     +                EXCLUDEFLAG(MAXALIGNS)
      CHARACTER*132   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
     +                IPROTNAME(MAXALIGNS),ACCNUM(MAXALIGNS),
     +                EXCLUDEFLAG(MAXALIGNS)

      INTEGER         ALIPOINTER(MAXALIGNS),
     +                IFIR(MAXALIGNS),ILAS(MAXALIGNS),JFIR(MAXALIGNS),
     +                JLAS(MAXALIGNS),LALI(MAXALIGNS),NGAP(MAXALIGNS),
     +                LGAP(MAXALIGNS),LENSEQ(MAXALIGNS)
      REAL            IDE(MAXALIGNS),SIM(MAXALIGNS)
C     br 2003-08-23: save space
C      CHARACTER       ALISEQ(MAXCORE)
      CHARACTER       ALISEQ(1)
      CHARACTER       ALISEQ_NULL
C     br 2003-08-23: end save space

C attributes of profile
      INTEGER         SEQPROF(NUMRESMAX,MAXAA),
     +                NOCC(NUMRESMAX),NDEL(NUMRESMAX),NINS(NUMRESMAX)
      REAL            CONSWEIGHT(NUMRESMAX)
C     br 2003-08-23: save space
C      INTEGER         VAR(NUMRESMAX),RELENT(NUMRESMAX)
C      REAL            ENTROPY(NUMRESMAX)
      INTEGER         VAR(1),RELENT(1)
      INTEGER         VAR_NULL,RELENT_NULL
      REAL            ENTROPY(1)
      REAL            ENTROPY_NULL
C     br 2003-08-23: end save space

C.......
      LOGICAL         LCONSERV,LOLDVERSION
C=======================================================================
C internal	
C      INTEGER         MAXALIGNS_LOC
C      PARAMETER      (MAXALIGNS_LOC=          3000)
C      PARAMETER      (MAXALIGNS_LOC=           MAXALIGNS)
C	character profileseq*(maxaa)
      CHARACTER       CTEMP*(NBLOCKSIZE),TEMPNAME*222
      CHARACTER*222   LINE
C     CHARACTER*20  HSSPRELEASE
      CHARACTER       CHAINSELECT
      LOGICAL         LCHAIN,LONG_ID
      INTEGER         ICHAINBEG,ICHAINEND,NALIGNORG,
     +                I,J,K,IPOS,ILEN,NRES,IRES,
     +                NBLOCK,IALIGN,IBLOCK,IALI,
     +                IBEG,IEND,IPOINTER(MAXALIGNS)

      INTEGER         MAXCORE_LOC,MAXALIGNS_LOC,MAXRES
      INTEGER         ITMP
      LOGICAL         LDEBUG_LOCAL

C order of amino acid symbols in the HSSP sequence profile block
C	profileseq='VLIMFWYGAPSTCHRKQEND'

      LERROR=.FALSE.
C     br 2003-08-23: avoid warnings
      IBEG=0
      IEND=0
      J=   0

C     used to debug
C      LDEBUG_LOCAL=.FALSE.
      LDEBUG_LOCAL=.TRUE.
      

C     BR 2007/08/22: not sure this is right, was not initialized
      LONG_ID=.FALSE.


      NALIGN=0
      CHAINREMARK=' '

      DO I=1,MAXALIGNS
         IPOINTER(I)=0
      ENDDO
      LCHAIN=.FALSE.
      
      TEMPNAME(1:)=FILE_HSSP_LOC
      I=INDEX(TEMPNAME,'_!_')
      IF (I.NE.0) THEN
         TEMPNAME(1:)=FILE_HSSP_LOC(1:I-1)
         LCHAIN=.TRUE.
         READ(FILE_HSSP_LOC(I+3:),'(A1)')CHAINSELECT
         WRITE(6,'(T2,A,T10,A,T50,A)')'---',
     +        '--- RS_READHSSP: extract the chain: ',chainselect
      ENDIF

C      CALL RSLIB_OPEN_FILE(IUNIT,TEMPNAME,'OLD,READONLY',LERROR)
C      OPEN(IUNIT,FILE=TEMPNAME,STATUS='OLD',READONLY,ERR=99)

C      OPEN(IUNIT,FILE=TEMPNAME,STATUS='OLD',ERR=99)
      OPEN(IUNIT,FILE=TEMPNAME,ERR=99)
      IF (LERROR .EQV. .TRUE.) THEN
         WRITE(6,'(A)')'*** ERROR FOR RS_READHSSP: open problem'
         GOTO 99
      ENDIF

      READ(IUNIT,'(A)',ERR=99)LINE
C check if it is a HSSP-file and get the release number for format flags
      IF (LINE(1:4).NE.'HSSP') THEN
         WRITE(6,'(A)')'*** ERROR FOR RS_READHSSP: is not a HSSP-file'
         LERROR=.TRUE.
         RETURN
      ELSE
         I=INDEX(LINE,'VERSION')+7
C     HSSPRELEASE=LINE(I:)
         LOLDVERSION=.FALSE.
c       if (index(hssprelease,'0.9').ne.0)loldversion=.true.
      ENDIF
C read in PDBID etc.
      DO WHILE(LINE(1:6).NE.'PDBID')
         READ(IUNIT,'(A)',ERR=99)LINE
      ENDDO
      READ(LINE,'(11X,A)',ERR=99)PDBID

      DO WHILE(LINE(1:6).NE.'HEADER')
         READ(IUNIT,'(A)',ERR=99)LINE
         IF (LINE(1:23).EQ.'PARAMETER  LONG-ID :YES') THEN
            LONG_ID=.TRUE.
            IF (LDEBUG_LOCAL .EQV. .TRUE.) 
     +           WRITE(6,'(A,A)')'DBG longid LINE=',LINE(1:23)
         ENDIF
      ENDDO
      READ(LINE ,'(11X,A)',ERR=99)HEADER
      READ(IUNIT,'(11X,A)',ERR=99)COMPOUND
      READ(IUNIT,'(11X,A)',ERR=99)SOURCE
      READ(IUNIT,'(11X,A)',ERR=99)AUTHOR
      READ(IUNIT,'(11X,I4)',ERR=99)SEQLENGTH
      READ(IUNIT,'(11X,I4)',ERR=99)NCHAIN
      KCHAIN=NCHAIN
      READ(IUNIT,'(A)',ERR=99)LINE
C      IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
      IF (INDEX(LINE,'KCHAIN').NE.0) THEN
         READ(LINE,'(11X,I4,A)',ERR=99)KCHAIN,CHAINREMARK
         READ(IUNIT,'(11X,I4)',ERR=99)NALIGNORG
      ELSE
         READ(LINE,'(11X,I4)',ERR=99)NALIGNORG
      ENDIF	
C if HSSP-file contains no alignments return
      IF (NALIGNORG.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A)')'---',
     +          '--- HSSP-file contains no alignments ***'
         CLOSE(IUNIT)
         RETURN
      ENDIF

C      write(6,*)'xx before overflow, Nali=',nalignorg,' len=',
C     +     seqlength,' kchain=',kchain,' lchain=',lchain


C     parameter overflow handling
      IF (NALIGNORG.GT.MAXALIGNS) THEN
         WRITE(6,'(A)')'-*- HSSP-file contains too many alignments **'
         WRITE(6,'(A)')'-*-     INCREASE MAXALIGNS in phdParameter.f! '
         WRITE(6,'(A,I8,A,I8)')'-*-   is=',MAXALIGNS,' want>',NALIGNORG
         CLOSE(IUNIT)
         LERROR=.TRUE.
         RETURN
      ENDIF
      ITMP=SEQLENGTH+KCHAIN-1
      IF (ITMP.GT.NUMRESMAX) THEN
         WRITE(6,'(A)')'*** PDB-sequence in HSSP-file too long ***'
         WRITE(6,'(A)')'***  INCREASE NUMRESMAX  in phdParameter.f***'	
         WRITE(6,'(A,I8,A,I8)')'-*-   is=',NUMRESMAX,' want>',ITMP
         CLOSE(IUNIT)
         LERROR=.TRUE.
         RETURN
      ENDIF

C number of sequence positions is number of residues + number of chains
C chain break is indicated by a '!'
      NRES=SEQLENGTH+KCHAIN-1
      ICHAINBEG=1
      ICHAINEND=NRES

      IF (LCHAIN .EQV. .TRUE.) THEN
C search for ALIGNMENT-block
         DO WHILE (LINE(1:13).NE.'## ALIGNMENTS')
            READ(IUNIT,'(A)',ERR=99)LINE
         ENDDO
         READ(IUNIT,'(A)',ERR=99)LINE
         ICHAINBEG=0
         ICHAINEND=0
C read till end ; some PDB-chains have DSSP-chain breaks !!
         DO I=1,NRES	
            READ(IUNIT,'(7X,I4,1X,A1)',ERR=99)PDBNO(I),CHAINID(I)
            IF (CHAINID(I) .EQ. CHAINSELECT) THEN
               IF (ICHAINBEG .EQ. 0) ICHAINBEG=I
               ICHAINEND=I
            ENDIF
         ENDDO
         WRITE(6,'(T2,A,T10,I10,I10)')'---',
     +        ICHAINBEG,ICHAINEND
         REWIND(IUNIT)
      ENDIF
      SEQLENGTH=ICHAINEND-ICHAINBEG+1

C     search for the PROTEINS-block
      LINE=' '
      DO WHILE(LINE(1:11).NE.'## PROTEINS')
         READ(IUNIT,'(A)',ERR=99)LINE
C         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG PROT=',LINE
      ENDDO
      READ(IUNIT,'(A)',ERR=99)LINE
      LCONSERV=.FALSE.
      IF (INDEX(LINE,'%WSIM').NE.0) LCONSERV= .TRUE.



C     READ DATA ABOUT THE ALIGNMENTS
      IALIGN=1
      IF (LDEBUG_LOCAL .EQV. .TRUE.) 
     +     WRITE(6,'(A,I5,A,A)')'DBG NALI=',NALIGNORG,' longid=',LONG_ID
      DO I=1,NALIGNORG
         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,'(A,I5)')'DBG iali=',I

         IF (LONG_ID .EQV. .TRUE.) THEN
C           note: read format specified below (line labelled 101)
            write(6,*)'xxok in long'
            READ(IUNIT,101,ERR=99)
     +           EXCLUDEFLAG(IALIGN),EMBLID(IALIGN)(1:),STRID(IALIGN),
     +           IDE(IALIGN),SIM(IALIGN),IFIR(IALIGN),ILAS(IALIGN),
     +           JFIR(IALIGN),JLAS(IALIGN),LALI(IALIGN),NGAP(IALIGN),
     +           LGAP(IALIGN),LENSEQ(IALIGN),ACCNUM(IALIGN),
     +           IPROTNAME(IALIGN)
            write(6,*)'xxok in long after i=',i
         ELSE
            write(6,*)'xxok in short'
C           note: read format specified below (line labelled 100)
            READ(IUNIT,100,ERR=99)
     +           EXCLUDEFLAG(IALIGN),EMBLID(IALIGN)(1:),STRID(IALIGN),
     +           IDE(IALIGN),SIM(IALIGN),IFIR(IALIGN),ILAS(IALIGN),
     +           JFIR(IALIGN),JLAS(IALIGN),LALI(IALIGN),NGAP(IALIGN),
     +           LGAP(IALIGN),LENSEQ(IALIGN),ACCNUM(IALIGN),
     +           IPROTNAME(IALIGN)
         END IF
         write(6,*)'xxok after endif'
         IF (LDEBUG_LOCAL .EQV. .TRUE.) THEN
            WRITE(6,'(A,I5,A,F5.2,A,A)')'DBG PROTali(',IALIGN,') ide=',
     +           IDE(IALIGN),
     +           ' name=',IPROTNAME(IALIGN)
         END IF
         IF (IFIR(IALIGN) .GE. ICHAINBEG .AND. 
     +	      ILAS(IALIGN) .LE. ICHAINEND) THEN
            IPOINTER(I)=IALIGN
            IALIGN=IALIGN+1
         ENDIF
      ENDDO

      write(6,*)' xxok after loop'

 100  FORMAT(5X,A1,2X,A12,A5,2X,F5.2,1X,F5.2,8(1X,I4),2X,A10,1X,A)
 101  FORMAT(5X,A1,2X,A40,A5,2X,F5.2,1X,F5.2,8(1X,I4),2X,A10,1X,A)
      NALIGN=IALIGN-1
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP PROTEINS-block done'

C init pointer ; aliseq contains the alignments (amino acid symbols)
C stored in the following way ; '/' separates alignments
C alignment(x) is stored from:
C           aliseq(alipointer(x)) to aliseq(ilas(x)-ifir(x))
C  aliseq(1........46/48.........60/62....)
C         |           |             |
C         |           |             |
C         pointer     pointer       pointer 
C         ali 1       ali 2         ali 3
C init pointer
      IPOS=1
      DO I=1,NALIGN

C     br 2003-08-23 fast and slim
C         IF (IPOS.GE.MAXCORE) THEN
C            WRITE(6,'(A,I9,A)')
C     +           ' *** LERROR: INCREASE MAXCORE to >',IPOS,'***'
C            STOP
C         ENDIF
C     end fast and slim br 2003-08-23


         ALIPOINTER(I)=IPOS
         ILEN=ILAS(I)-IFIR(I)+1
         IPOS=IPOS+ILEN
C     br 2003-08-23 fast and slim
C         ALISEQ(IPOS)='/'
         ALISEQ_NULL='/'
C     end fast and slim br 2003-08-23
         IPOS=IPOS+1
      ENDDO
      ALIPOINTER(NALIGN+1)=IPOS+1

C number of ALIGNMENTS-blocks
      IF (MOD(FLOAT(NALIGNORG),FLOAT(NBLOCKSIZE)).EQ. 0.0) THEN
         NBLOCK=NALIGNORG/NBLOCKSIZE
      ELSE
         NBLOCK=NALIGNORG/NBLOCKSIZE+1
      ENDIF
C search for ALIGNMENT-block
      DO WHILE (LINE(1:13).NE.'## ALIGNMENTS')
         READ(IUNIT,'(A)',ERR=99)LINE
         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG alinot',LINE
      ENDDO
      READ(IUNIT,'(A)',ERR=99)LINE
      IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
C loop over ALIGNMENTS-blocks
C ....read in pdbno, chainid, secstr etc.
      IALIGN=0
      IALI=0
      DO IBLOCK=1,NBLOCK
         IRES=1
         DO I=1,NRES	

C     BR: 2003-08-23: save space
C            READ(IUNIT,200,ERR=99)
C     +           PDBNO(IRES),CHAINID(IRES),PDBSEQ(IRES),SECSTR(IRES),
C     +           COLS(IRES),BP1(IRES),BP2(IRES),SHEETLABEL(IRES),
C     +           ACC(IRES),NOCC(IRES),VAR(IRES),CTEMP 

            READ(IUNIT,200,ERR=99)
     +           PDBNO_NULL,CHAINID(IRES),PDBSEQ(IRES),SECSTR(IRES),
     +           COLS(IRES),BP1_NULL,BP2_NULL,SHEETLABEL_NULL,
     +           ACC(IRES),NOCC(IRES),VAR_NULL,CTEMP 
C     end save space br 2003-08-23

 200        FORMAT(7X,I4,2(1X,A1),2X,A1,1X,A7,2(I4),A1,I4,2(1X,I4),2X,A)
C.....fill up aliseq
C            IF (LDEBUG_LOCAL .EQV. .TRUE.) 
C     +           WRITE(6,*)'DBG IBLOCK=',IBLOCK, ' IRES=',I
            IF (I .GE. ICHAINBEG .AND. I .LE. ICHAINEND) THEN
               IRES=IRES+1


C     br 2003-08-23 fast and slim
C               IF (PDBSEQ(I) .NE. '!') THEN
C	          CALL STRPOS(CTEMP,IBEG,IEND)
C                  DO IPOS=MAX(IBEG,1),MIN(NBLOCKSIZE,IEND)
C	             IALI=IALIGN+IPOS
C                     IF (CTEMP(IPOS:IPOS) .NE. ' ') THEN
C                        J=ALIPOINTER(IPOINTER(IALI)) + 
C     +                       (I-IFIR(IPOINTER(IALI)))
C                        ALISEQ(J)=CTEMP(IPOS:IPOS)
C	             ENDIF
C	          ENDDO
C               ENDIF
C     end fast and slim br 2003-08-23

            ENDIF
         ENDDO
         IALIGN=IALIGN+NBLOCKSIZE
         DO K=1,2
            READ(IUNIT,'(A)',ERR=99)LINE
            IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
         ENDDO
      ENDDO
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP ALIGNMENTS-block done'
C read in sequence profile, entropy etc.
      IRES=1
      DO I=1,NRES
C     BR 2003-08-23 save space
C         READ(IUNIT,300,ERR=99)(SEQPROF(IRES,K),K=1,MAXAA),
C     +        NOCC(IRES),NDEL(IRES),NINS(IRES),ENTROPY(IRES),
C     +        RELENT(IRES),CONSWEIGHT(IRES)
         READ(IUNIT,300,ERR=99)(SEQPROF(IRES,K),K=1,MAXAA),
     +        NOCC(IRES),NDEL(IRES),NINS(IRES),ENTROPY_NULL,
     +        RELENT_NULL,CONSWEIGHT(IRES)
C     end save space br 2003-08-23
         IF (I .GE. ICHAINBEG .AND. I .LE. ICHAINEND) THEN
            IRES=IRES+1
         ENDIF
      ENDDO
 300  FORMAT(12X,20(I4),1X,3(1X,I4),1X,F7.3,3X,I4,2X,F4.2)
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP PROFILE-block done'
      
      IF (LCHAIN .EQV. .TRUE.) THEN
         DO I=1,NALIGN
            IFIR(I)=IFIR(I)-ICHAINBEG+1
            ILAS(I)=ILAS(I)-ICHAINBEG+1
         ENDDO
      ENDIF

C check if next line (last line in a HSSP-file) contains a '//'
      READ(IUNIT,'(A)',ERR=99)LINE
      IF ((LINE(1:2).EQ.'//').OR.
     +     (LINE(1:13).EQ.'## INSERTION')) THEN
         WRITE(6,'(T2,A,T10,A,A50)')'---',
     +        '   RS_READHSSP ok(cut 50): ',FILE_HSSP_LOC(1:50)
         GOTO 999
      ELSE
         WRITE(6,'(T2,A,T10,A,A,A,A)')'***',
     +        'ERROR FOR RS_READHSSP: ',FILE_HSSP_LOC,' lastLine=',
     +        LINE
         GOTO 99
      ENDIF
 99   WRITE(6,'(A,A)')'**** ERROR FOR RS_READHSSP: READING: ',
     +     FILE_HSSP_LOC
      LERROR=.TRUE.
      NALIGN=0
      SEQLENGTH=0
 999  CLOSE(IUNIT)
      RETURN
      END
***** end of RS_READHSSP
