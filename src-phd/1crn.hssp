HSSP       HOMOLOGY DERIVED SECONDARY STRUCTURE OF PROTEINS , VERSION 1.0 1991
PDBID      1crn
DATE       file generated on  9-Aug-98
SEQBASE    RELEASE 36.0 OF EMBL/SWISS-PROT WITH  74019 SEQUENCES
PARAMETER  SMIN: -0.5  SMAX:  1.0
PARAMETER  gap-open:  3.0 gap-elongation:  0.1
PARAMETER  conservation weights: YES
PARAMETER  InDels in secondary structure allowed: YES
PARAMETER  alignments sorted according to :DISTANCE
THRESHOLD  according to: t(L)=(290.15 * L ** -0.562) +  5
REFERENCE  Sander C., Schneider R. : Database of homology-derived protein structures. Proteins, 9:56-68 (1991).
CONTACT    e-mail (INTERNET) Schneider@EMBL-Heidelberg.DE or Sander@EMBL-Heidelberg.DE / fax +49-6221-387306
AVAILABLE  Free academic use. Commercial users must apply for license.
AVAILABLE  No inclusion in other databanks without permission.
HEADER     PLANT SEED PROTEIN
COMPND     CRAMBIN
SOURCE     ABYSSINIAN CABBAGE (CRAMBE ABYSSINICA) SEED
AUTHOR     W.A.HENDRICKSON,M.M.TEETER
SEQLENGTH    46
NCHAIN        1 chain(s) in 1crn data set
NALIGN       11
NOTATION : ID: EMBL/SWISSPROT identifier of the aligned (homologous) protein
NOTATION : STRID: if the 3-D structure of the aligned protein is known, then STRID is the Protein Data Bank identifier as taken
NOTATION : from the database reference or DR-line of the EMBL/SWISSPROT entry
NOTATION : %IDE: percentage of residue identity of the alignment
NOTATION : %SIM (%WSIM):  (weighted) similarity of the alignment
NOTATION : IFIR/ILAS: first and last residue of the alignment in the test sequence
NOTATION : JFIR/JLAS: first and last residue of the alignment in the alignend protein
NOTATION : LALI: length of the alignment excluding insertions and deletions
NOTATION : NGAP: number of insertions and deletions in the alignment
NOTATION : LGAP: total length of all insertions and deletions
NOTATION : LSEQ2: length of the entire sequence of the aligned protein
NOTATION : ACCNUM: SwissProt accession number
NOTATION : PROTEIN: one-line description of aligned protein
NOTATION : SeqNo,PDBNo,AA,STRUCTURE,BP1,BP2,ACC: sequential and PDB residue numbers, amino acid (lower case = Cys), secondary
NOTATION : structure, bridge partners, solvent exposure as in DSSP (Kabsch and Sander, Biopolymers 22, 2577-2637(1983)
NOTATION : VAR: sequence variability on a scale of 0-100 as derived from the NALIGN alignments
NOTATION : pair of lower case characters (AvaK) in the alignend sequence bracket a point of insertion in this sequence
NOTATION : dots (....) in the alignend sequence indicate points of deletion in this sequence
NOTATION : SEQUENCE PROFILE: relative frequency of an amino acid type at each position. Asx and Glx are in their
NOTATION : acid/amide form in proportion to their database frequencies
NOTATION : NOCC: number of aligned sequences spanning this position (including the test sequence)
NOTATION : NDEL: number of sequences with a deletion in the test protein at this position
NOTATION : NINS: number of sequences with an insertion in the test protein at this position
NOTATION : ENTROPY: entropy measure of sequence variability at this position
NOTATION : RELENT: relative entropy, i.e.  entropy normalized to the range 0-100
NOTATION : WEIGHT: conservation weight

## PROTEINS : EMBL/SWISSPROT identifier and alignment statistics
  NR.    ID         STRID   %IDE %WSIM IFIR ILAS JFIR JLAS LALI NGAP LGAP LSEQ2 ACCNUM     PROTEIN
    1 : cram_craab  1AB1    1.00  1.00    1   46    1   46   46    0    0   46  P01542     CRAMBIN.
    2 : thn_pyrpu           0.53  0.60    2   46    2   47   45    1    1   47  P07504     THIONIN.
    3 : thn_dencl           0.53  0.69    2   44    2   44   43    0    0   46  P01541     DENCLATOXIN B.
    4 : thn3_visal          0.49  0.65    2   46   28   72   45    0    0  111  P01538     VISCOTOXIN A3 PRECURSOR.
    5 : thn_pholi           0.47  0.61    2   46    2   46   45    0    0   46  P01540     LIGATOXIN A.
    6 : thnb_visal          0.44  0.61    2   46    8   52   45    0    0  103  P08943     VISCOTOXIN B PRECURSOR (FRAGMENT).
    7 : thn5_horvu          0.44  0.61    2   46   30   74   45    0    0  137  P09617     DG3).
    8 : y4ik_rhisn          0.54  0.37    8   46  180  207   28    1   11  232  P55494     HYPOTHETICAL 26.8 KD PROTEIN Y4IK.
    9 : dp87_dicdi          0.47  0.26    9   44  234  314   36    2   45  555  Q04503     PRESPORE PROTEIN DP87 PRECURSOR.
   10 : thn2_visal          0.43  0.61    2   41    2   41   40    0    0   46  P32880     VISCOTOXIN A2.
   11 : thn6_horvu          0.40  0.57    2   46   30   74   45    0    0  137  P09618     LEAF-SPECIFIC THIONIN PRECURSOR (CLONE BT
## ALIGNMENTS    1 -   11
 SeqNo  PDBNo AA STRUCTURE BP1 BP2  ACC NOCC  VAR  ....:....1....:....2....:....3....:....4....:....5....:....6....:....7
     1    1   T              0   0   77    1    0  T
     2    2   T  E     -A   34   0A  21    9   22  TSSSSSS  SS
     3    3   C  E     -A   33   0A   0    9    0  CCCCCCC  CC
     4    4   C        -     0   0    0    9    0  CCCCCCC  CC
     5    5   P  S    S+     0   0   52    9   26  PRPPPPK  PK
     6    6   S  S  > S-     0   0   48    9   34  SNTNSNN  ND
     7    7   I  H  > S+     0   0  123    9   24  ITTTTTT  TT
     8    8   V  H  > S+     0   0   98   10   45  VWATTTTV TL
     9    9   A  H  > S+     0   0    6   11   21  AAAGAGGSAGA
    10   10   R  H  X S+     0   0   55   11    0  RRRRRRRRRRR
    11   11   S  H  X S+     0   0   63   11   39  SNNNNNNLPNN
    12   12   N  H  X S+     0   0   82   11   56  NCQIIICEPIC
    13   13   F  H  X S+     0   0    5   11   24  FYYYYYYMHYY
    14   14   N  H  X S+     0   0   87   11    0  NNNNNNNNNNN
    15   15   V  H >< S+     0   0   99   11   42  VVIATTAPLTT
    16   16   C  H 3<>S+     0   0   18   11   16  CCCCCCCNCCC
    17   17   R  H ><5S+     0   0   94   11    0  RRRRRRRRRRR
    18   18   L  T <<5S+     0   0  144   11   33  LLLLLLFSGFF
    19   19   P  T 3 5S-     0   0  107   11   42  PPPTTGAPFGA
    20   20   G  T < 5 +     0   0   53   11    9  GGGGGGGEGGG
    21   21   T      < -     0   0   37   10   32  TTTATGG.CGG
    22   22   P    >>  -     0   0   81   10   32  PIPPSSS.PSS
    23   23   E  H 3> S+     0   0   70   10   37  EsRRRRR.ERR
    24   24   A  H 3> S+     0   0   63   10   35  AePPPEP.gEP
    25   25   I  H <> S+     0   0   99   10   39  IIVTTRV.nVV
    26   26   C  H  X S+     0   0    0   10    0  CCCCCCC.CCC
    27   27   A  H  X S+     0   0   12   10    8  AAAAAAA.GAA
    28   28   T  H  < S+     0   0  120   10   41  TKAKSST.PSG
    29   29   Y  H  < S+     0   0  176   10   53  YKLLLLA.GLA
    30   30   T  H  < S-     0   0   24   10   38  TCSSSSC.YSC
    31   31   G  S  < S+     0   0   35   10   31  GDGGGGG.IGR
    32   32   C        -     0   0    5   11    0  CCCCCCCCCCC
    33   33   I  E     -A    3   0A  51   11   40  IKKKKKKITKK
    34   34   I  E     -A    2   0A  78   11   16  IIIIIIIRIII
    35   35   I        -     0   0   34   11    0  IIIIIIIIIII
    36   36   P  S    S+     0   0  143   11   29  PSSSSSSPNSS
    37   37   G  S    S-     0   0   41   11   24  GGGGGAGLGAG
    38   38   A  S    S+     0   0   80   11   41  ATTSSSPAhSP
    39   39   T        -     0   0  127   11   24  TTGTTTTTrTK
    40   40   C        -     0   0   46   11    7  CCCCCCCSCCC
    41   41   P    >   -     0   0   53   11    8  PPPPBPPPPPP
    42   42   G  G >  S+     0   0   75   10   41  GSPSSSRDH S
    43   43   D  G 3  S+     0   0  116   10    9  DDGDGDDDD D
    44   44   Y  G <  S+     0   0   66   10    3  YYYYWYYYY Y
    45   45   A    <         0   0   70    8   31  AP PBPPA  P
    46   46   N              0   0   76    8   37  NK KHKKS  K
## SEQUENCE PROFILE AND ENTROPY
 SeqNo PDBNo   V   L   I   M   F   W   Y   G   A   P   S   T   C   H   R   K   Q   E   N   D  NOCC NDEL NINS ENTROPY RELENT WEIGHT
    1    1     0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0   0   0   0     1    0    0   0.000      0  1.00
    2    2     0   0   0   0   0   0   0   0   0   0  89  11   0   0   0   0   0   0   0   0     9    0    0   0.349     16  1.00
    3    3     0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0   0   0     9    0    0   0.000      0  1.34
    4    4     0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0   0   0     9    0    0   0.000      0  1.34
    5    5     0   0   0   0   0   0   0   0   0  67   0   0   0   0  11  22   0   0   0   0     9    0    0   0.849     39  0.82
    6    6     0   0   0   0   0   0   0   0   0   0  22  11   0   0   0   0   0   0  56  11     9    0    0   1.149     52  0.78
    7    7     0   0  11   0   0   0   0   0   0   0   0  89   0   0   0   0   0   0   0   0     9    0    0   0.349     16  0.97
    8    8    20  10   0   0   0  10   0   0  10   0   0  50   0   0   0   0   0   0   0   0    10    0    0   1.359     59  0.57
    9    9     0   0   0   0   0   0   0  36  55   0   9   0   0   0   0   0   0   0   0   0    11    0    0   0.916     38  1.04
   10   10     0   0   0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0    11    0    0   0.000      0  1.37
   11   11     0   9   0   0   0   0   0   0   0   9   9   0   0   0   0   0   0   0  73   0    11    0    0   0.886     37  0.97
   12   12     0   0  36   0   0   0   0   0   0   9   0   0  27   0   0   0   9   9   9   0    11    0    0   1.594     66  0.49
   13   13     0   0   0   9   9   0  73   0   0   0   0   0   0   9   0   0   0   0   0   0    11    0    0   0.886     37  1.26
   14   14     0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0 100   0    11    0    0   0.000      0  1.37
   15   15    18   9   9   0   0   0   0   0  18   9   0  36   0   0   0   0   0   0   0   0    11    0    0   1.642     68  0.68
   16   16     0   0   0   0   0   0   0   0   0   0   0   0  91   0   0   0   0   0   9   0    11    0    0   0.305     13  1.32
   17   17     0   0   0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0    11    0    0   0.000      0  1.37
   18   18     0  55   0   0  27   0   0   9   0   0   9   0   0   0   0   0   0   0   0   0    11    0    0   1.121     47  1.14
   19   19     0   0   0   0   9   0   0  18  18  36   0  18   0   0   0   0   0   0   0   0    11    0    0   1.516     63  0.68
   20   20     0   0   0   0   0   0   0  91   0   0   0   0   0   0   0   0   0   9   0   0    11    0    0   0.305     13  1.34
   21   21     0   0   0   0   0   0   0  40  10   0   0  40  10   0   0   0   0   0   0   0    10    1    0   1.194     52  0.80
   22   22     0   0  10   0   0   0   0   0   0  40  50   0   0   0   0   0   0   0   0   0    10    1    0   0.943     41  0.76
   23   23     0   0   0   0   0   0   0   0   0   0  10   0   0   0  70   0   0  20   0   0    10    1    1   0.802     35  0.76
   24   24     0   0   0   0   0   0   0  10  10  50   0   0   0   0   0   0   0  30   0   0    10    1    1   1.168     51  0.70
   25   25    40   0  20   0   0   0   0   0   0   0   0  20   0   0  10   0   0   0  10   0    10    1    0   1.471     64  0.68
   26   26     0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0   0   0    10    1    0   0.000      0  1.33
   27   27     0   0   0   0   0   0   0  10  90   0   0   0   0   0   0   0   0   0   0   0    10    1    0   0.325     14  1.30
   28   28     0   0   0   0   0   0   0  10  10  10  30  20   0   0   0  20   0   0   0   0    10    1    0   1.696     74  0.60
   29   29     0  50   0   0   0   0  10  10  20   0   0   0   0   0   0  10   0   0   0   0    10    1    0   1.359     59  0.47
   30   30     0   0   0   0   0   0  10   0   0   0  50  10  30   0   0   0   0   0   0   0    10    1    0   1.168     51  0.76
   31   31     0   0  10   0   0   0   0  70   0   0   0   0   0   0  10   0   0   0   0  10    10    1    0   0.940     41  0.91
   32   32     0   0   0   0   0   0   0   0   0   0   0   0 100   0   0   0   0   0   0   0    11    0    0   0.000      0  1.37
   33   33     0   0  18   0   0   0   0   0   0   0   0   9   0   0   0  73   0   0   0   0    11    0    0   0.760     32  0.88
   34   34     0   0  91   0   0   0   0   0   0   0   0   0   0   0   9   0   0   0   0   0    11    0    0   0.305     13  1.32
   35   35     0   0 100   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0    11    0    0   0.000      0  1.37
   36   36     0   0   0   0   0   0   0   0   0  18  73   0   0   0   0   0   0   0   9   0    11    0    0   0.760     32  1.03
   37   37     0   9   0   0   0   0   0  73  18   0   0   0   0   0   0   0   0   0   0   0    11    0    0   0.760     32  1.11
   38   38     0   0   0   0   0   0   0   0  18  18  36  18   0   9   0   0   0   0   0   0    11    0    1   1.516     63  0.69
   39   39     0   0   0   0   0   0   0   9   0   0   0  73   0   0   9   9   0   0   0   0    11    0    0   0.886     37  1.09
   40   40     0   0   0   0   0   0   0   0   0   0   9   0  91   0   0   0   0   0   0   0    11    0    0   0.305     13  1.35
   41   41     0   0   0   0   0   0   0   0   0  91   0   0   0   0   0   0   0   0   0   9    11    0    0   0.305     13  1.15
   42   42     0   0   0   0   0   0   0  10   0  10  50   0   0  10  10   0   0   0   0  10    10    0    0   1.498     65  0.82
   43   43     0   0   0   0   0   0   0  20   0   0   0   0   0   0   0   0   0   0   0  80    10    0    0   0.500     22  1.20
   44   44     0   0   0   0   0  10  90   0   0   0   0   0   0   0   0   0   0   0   0   0    10    0    0   0.325     14  1.30
   45   45     0   0   0   0   0   0   0   0  25  63   0   0   0   0   0   0   0   0   0  13     8    0    0   0.900     43  0.88
   46   46     0   0   0   0   0   0   0   0   0   0  13   0   0  13   0  63   0   0  13   0     8    0    0   1.074     52  0.82
## INSERTION LIST
 AliNo  IPOS  JPOS   Len Sequence
     2    23    24     1 sRe
     9    24   250    31 gSHCEVLEKHPVCVRNHVPPHPPPPPQICGSVn
     9    38   295    14 hPTCIRGDGYLCNQTr
//
