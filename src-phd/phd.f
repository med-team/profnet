*----------------------------------------------------------------------*
*     FORTRAN code for program PHD                                     *
*          (Profile based neural network prediction of secondary       *
*          structure, solvent accessibility, and transmembrane         *
*          helices)                                                    *
*----------------------------------------------------------------------*
*     Burkhard Rost             May,        1998      version 0.1      *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998      version 0.2      *
*----------------------------------------------------------------------*
*                                                                      *
*     General notes:  - The program uses a parameter files (INCLUDE):  *
*                       phdParameter.f.f                               *
*                     - Library routines are taken from:               *
*                       lib-phd.f                                      *
*                                                                      *
*     Organisation:   (1)  main program PHD                            *
*                     (2)  MAIN_STOP_EMPTYHSSP                         *
*                     then all subroutines alphabetically              *
*                                                                      *
*                     parameters/variables defined in:                 *
*                       Doc-code-phd.txt                               *
*                                                                      *
*     To port it:     hard coded is only the path for the *.com file   *
*                     currently: /home/rost/pub                        *
*                            or: /home/phd/net (if arg5=server)        *
*                            or: explicitly given in arg6              *
*                     NOTE:  defined in 'iniphd.f',                    *
*     -->                    grep 'change to port'                     *
*                                                                      *
*     Input arg:      arg1: HSSP file                                  *
*                     arg2: optional flag if = "mach", then the output *
*                           (of secondary structure) comes with "# 1"  *
*                     arg3: optional flag if = "whatif", an output file*
*                           with extension ".whatif" is created, which *
*                           can be read by e.g. KaleidaGraph           *
*                     arg4: optional flag if                           *
*                              = "sec" -> prediction of secondary str. *
*                              = "exp" -> prediction of exposure       *
*                              = "htm" -> prediction of helical trans- *
*                                         membrane regions             *
*                           default: sec                               *
*                     arg5: optional if phd LSERVER=TRUE, default: FALSE
*                     arg6: specifies the Parameter file to be chosen  *
*                     arg7: optional flag: = "rdb", an output file is  *
*                           written (extension ".rdb") with a format   *
*                           for interchange, i.e. merging the results  *
*                           from sec and exp- predictions              *
*                     arg8: if = DEC or ALPHA, LDEC=TRUE               *
*                     arg9: working directory, default=local           *
*                    arg10: name for .pred file                        *
*                    arg11: name for .rdb file                         *
*                    arg12: not used                                   *
*                                                                      *
*----------------------------------------------------------------------*
      PROGRAM PHD

C---- include parameter files
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITER,MUE,NUMARGUMENTS,IHELP
      REAL            TIME0,FRTIME_SECNDS
      LOGICAL         LWRITE

******------------------------------*-----------------------------******
*     LDSSPREAD       is set true, if there is an observed secondary   *
*                     structure in the HSSP file (then Evaldssp is     *
*                     called and the pay-off table written into the out*
*                     put)                                             *
*     LFILTER         controls whether the prediction ought to filterd *
*     LOUTBINPROB     if true, the probabilities for the assignment of *
*                     the secondary structures is computed and written *
*                     into the output.                                 *
*     LSERVER         if true the name of FILEPRED, FILEOUTPUT are     *
*                     chosen equal to the name of the HSSP file, other-*
*                     wise according to the name of the protein in the *
*                     HSSP file.                                       *
*     LWRITE          controls whether calls of time and date are writ-*
*                     ten into the printer                             *
*     SECNDS          external function returning seconds elapsed since*
*                     midnight                                         *
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- assigning parameters                -----
C--------------------------------------------------

C---- note: now determined by arguments:
      LSERVER=        .FALSE.
      LDEC=           .FALSE.
C---- security
      FILE_HSSP=      ' '
      
C---- call SRDTIME(CALL,LWRITE) from personal lib-syst.f
      LWRITE=         .FALSE.
c$$$      CALL SRDTIME(1,LWRITE)
C---- call SCFDATE(1,LWRITE) from personal lib-syst.f
      LWRITE=         .FALSE.
C      CALL SCFDATE(1,LWRITE,STARTDATE)
C---- elapsed time: real: seconds since midnight-supplied arg
      TIME0=          0.0
C      TIMESTART=      SECNDS(TIME0)
C      TIMESTART=      FRTIME_SECNDS(TIME0)

C--------------------------------------------------
C---- requesting HSSP file name and further 
C---- input variables to run program
C--------------------------------------------------
C     -------------------
      CALL GET_ARG_NUMBER(NUMARGUMENTS)
C     -------------------
      IF (NUMARGUMENTS.GT.0) THEN
C        -------------------
         CALL GET_ARGUMENT(1,FILE_HSSP)
C        -------------------
         CHAR_ARG_READ(1)=FILE_HSSP
      ELSE
         WRITE(6,'(T2,A)')'---'
         WRITE(6,'(T2,70A1)')('-',ITER=1,70)
         WRITE(6,'(T2,3A1,T10,A)')('-',ITER=1,3),
     +        'Dear User, Welcome to PredictProtein !'
         WRITE(6,'(T2,A)')'---'
         WRITE(6,'(T2,3A1,T10,A)')('-',ITER=1,3),
     +        'Please select a HSSP file for the prediction:'
         WRITE(6,'(T2,A)')'---'
C        ------------
         CALL GETCHAR(80,FILE_HSSP,' HSSP file ? ')
C        ------------
      END IF
      IF (NUMARGUMENTS.GE.2) THEN
         DO ITER=2,NUMARGUMENTS
C           -----------------
            CALL GET_ARGUMENT(ITER,CHAR_ARG_READ(ITER))
C           -----------------
         END DO
      END IF
C---- initialise according to input
C     ===========
      CALL INIPHD(NUMARGUMENTS)
C     ===========
C--------------------------------------------------
C---- initialise the parameter names for all 
C---- particular jobs (architectures)
C--------------------------------------------------

C     ------------
      CALL READPAR
C     ------------
C---- 21 amino acid names (20+unknown) and solvent into AACODE
C     ------------
      CALL TRANSAA
C     ------------
C--------------------------------------------------
C---- adjust filename for output file         -----
C--------------------------------------------------
C--------------------------------------------------
C---- reading sequence from HSSP file         -----
C--------------------------------------------------
      WRITE(6,'(T2,70A1)')('-',ITER=1,70)
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('-',ITER=1,3),
     +     'Protein from HSSP:',FILE_HSSP
      WRITE(6,'(T2,70A1)')('-',ITER=1,70)
      WRITE(6,'(T2,A)')'---'
C     ---------------
      CALL RS_GETHSSP
C     ---------------

C     stop if no sequence in HSSP
      IF (NUMNALIGN(1).EQ.0) THEN
         WRITE(6,'(T2,A,T10,60A1)')'***',('*',ITER=1,60)
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'ERROR in reading HSSP (?), claimed to be empty (main PHD)'
         WRITE(6,'(T2,A,T10,60A1)')'***',('*',ITER=1,60)
         STOP
      END IF
C--------------------------------------------------
C---- if somehow a fault occurred stop program ----
C--------------------------------------------------

      IF (NUMRES.EQ.0) THEN
C        ------------------------
         CALL MAIN_STOP_EMPTYHSSP
C        ------------------------
C!GOTO!  !!!!!!!!!
C        =========
         GOTO 9999
C        =========
C!GOTO!  !!!!!!!!!
      END IF

C--------------------------------------------------
C---- is there a DSSP assignmnet in the HSSP file?
C--------------------------------------------------

      IHELP=0
      DO MUE=1,NUMRES
         IF ((RESSECSTR(MUE).EQ.'H').OR.
     +        (RESSECSTR(MUE).EQ.'G').OR.
     +        (RESSECSTR(MUE).EQ.'E')) THEN
            IHELP=IHELP+1
         END IF
      END DO
      IF ((IHELP.GT.5).AND.(LSERVER.EQV. .FALSE.)) THEN
         LDSSPREAD=.TRUE.
      ELSE
         LDSSPREAD=.FALSE.
      END IF
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,3A1,T10,A)')('-',ITER=1,3),
     +                 'end of reading protein data'
      WRITE(6,'(T2,70A1)')('-',ITER=1,70)
      WRITE(6,'(T2,A)')'---'
C--------------------------------------------------
C---- loop over all networks                  -----
C--------------------------------------------------
C     ============
      CALL NETWORK
C     ============
      IF (LFILTER .EQV. .TRUE.) THEN
C        ===========
         CALL FILTER
C        ===========
      END IF
C--------------------------------------------------
C---- saving data/output file                 -----
C--------------------------------------------------
C---- convert DSSP (8) -> 3 structure types (lib-prot.f)
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A)')'---'
         LWRITE=.TRUE.
C        =============
         CALL EVALDSSP
C        =============
         WRITE(6,'(T2,A)')'---'
         WRITE(6,'(T2,70A1)')('-',ITER=1,70)
         WRITE(6,'(T2,A)')'---'
      END IF
C     ============
      CALL CONTENT
C     ============
C---- write results into output file
C     -----------
      CALL DATAOT
C     -----------
C---- write results on printer
C     -----------
      CALL TXTRES
C     -----------
C--------------------------------------------------
C---- branch for misread HSSP file            -----
C--------------------------------------------------
C!GOTO!!!!!!!!
C     ========
 9999 CONTINUE
C     ========
C!GOTO!!!!!!!!
      END
***** end of MAIN

***** ------------------------------------------------------------------
***** SUB MAIN_STOP_EMPTYHSSP
***** ------------------------------------------------------------------
C---- 
C---- NAME : MAIN_STOP_EMPTYHSSP
C---- ARG  : 
C---- DES  : For an empty HSSP file blabla written 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        For an empty HSSP file blabla written.           *
*----------------------------------------------------------------------*
      SUBROUTINE MAIN_STOP_EMPTYHSSP

C---- local variables
      INTEGER         ITER
******------------------------------*-----------------------------******
      WRITE(6,'(T2,70A1)')('*',ITER=1,70)
      WRITE(6,'(T2,A3)')'***'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'An error occurred: the adressed HSSP file is empty.'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'Program has stopped consequently !'
      WRITE(6,'(T2,A3)')'***'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'A potential cause is you mistyped the file name.'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'Either start the program with: '
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     '"dir/PredictProtein.MACHINE filename"'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'or check the path of the file you want to be read.'
      WRITE(6,'(T2,A3)')'***'
      WRITE(6,'(T2,3A1,T10,A,T30,A50)')('*',ITER=1,3),
     +     'Sorry               !'
      WRITE(6,'(T2,A3)')'***'
      WRITE(6,'(T2,70A1)')('*',ITER=1,70)
      END
***** end of MAIN_STOP_EMPTYHSSP

***** ------------------------------------------------------------------
***** SUB BINFIL
***** ------------------------------------------------------------------
C---- 
C---- NAME : BINFIL
C---- ARG  : 
C---- DES  : Executes the output decision, i.e., actual network 
C---- DES  : prediction 
C---- DES  : in terms of binary values (for filtered output). 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        This SBR executes the output decision, i.e. the  *
*     --------        actual prediciton of the network in terms of     *
*                     binary output values.                            *
*     input:          OUTFIL, (THREXP10ST, MAXEXP, EXPCODE)            *
*     var. read:      SSCODE, EXPCODE, MAXEXP, THREXP10ST              *
*     output:         OUTEXPFIL, OUTBINCHARFIL                         *
*     called by:      SBR FILTEREXP                                    *
*     calling:        lib-comp.f: SRSTZ1, SRMAX1                       *
*----------------------------------------------------------------------*
      SUBROUTINE BINFIL

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITOUT,IMAX
      REAL            OUTLOC(1:(NUMOUTMAX)),RMAX
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- loop over all residues                  -----
C--------------------------------------------------

      DO MUE=1,NUMRES
C        -----------
         CALL SRSTZ1(OUTLOC,NUMOUTMAX)
C        -----------
         DO ITOUT=1,NUMOUT
            OUTLOC(ITOUT)=OUTFIL(ITOUT,MUE)
         END DO
C----------------------------------------
C------- WTO decision
C----------------------------------------

C        -----------
         CALL SRMAX1(OUTLOC,NUMOUTMAX,RMAX,IMAX)
C        -----------
         IF (RMAX.EQ.0) THEN
            OUTEXPFIL(MUE)=MAXEXP
            OUTBINCHARFIL(MUE)='e'
         ELSE
            OUTEXPFIL(MUE)=THREXP10ST(IMAX)
            OUTBINCHARFIL(MUE)=EXPCODE(IMAX)
         END IF

C----------------------------------------
C------- end of WTO decision
C----------------------------------------
      END DO
C--------------------------------------------------
C---- end of loop over all residues            ----
C--------------------------------------------------
      END
***** end of BINFIL

***** ------------------------------------------------------------------
***** SUB BINOUT
***** ------------------------------------------------------------------
C---- 
C---- NAME : BINOUT
C---- ARG  : 
C---- DES  : Executes the output decision, i.e., actual network 
C---- DES  : prediction 
C---- DES  : in terms of binary values. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        This SBR executes the output decision, i.e. the  *
*     --------        actual prediciton of the network in terms of     *
*                     binary output values.                            *
*     const. passed:  NUMRES, NSECEL, NUMOUT, NUMEXP, MODESECSTRON     *
*     --------------                  NUMOUTMAX,                       *
*     var. passed:    in: OUTPUT; out: OUTBIN, OUTBINCHAR, OUTEXP      *
*     var. read:      SSCODE, EXPCODE, MAXEXP, THREXP10ST              *
*     called by:      SBR NETWORK                                      *
*     calling:        lib-comp.f: SRSTZ1, SRMAX1                       *
*----------------------------------------------------------------------*
      SUBROUTINE BINOUT

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITOUT,IMAX
      REAL            OUTLOC(1:(NUMOUTMAX)),RMAX
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- loop over all residues                  -----
C--------------------------------------------------

      DO MUE=1,NUMRES
C        -----------
         CALL SRSTZ1(OUTLOC,NUMOUTMAX)
C        -----------
         DO ITOUT=1,NUMOUT
            OUTLOC(ITOUT)=OUTPUT(ITOUT,MUE)
         END DO
C----------------------------------------
C------- WTO decision
C----------------------------------------

C        -----------
         CALL SRMAX1(OUTLOC,NUMOUTMAX,RMAX,IMAX)
C        -----------
         DO ITOUT=1,NUMOUT
            OUTBIN(ITOUT,MUE)=0
         END DO
C------------------------------
C------- secondary structure
C------------------------------

         IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
            OUTBIN(IMAX,MUE)=1
            OUTBINCHAR(MUE)=   SSCODE(IMAX)
            OUTBINCHARFIL(MUE)=SSCODE(IMAX)
C------------------------------
C------- exposure
C------------------------------

         ELSE
            IF (RMAX.EQ.0) THEN
               OUTBIN(NUMOUT,MUE)=1
               OUTEXP(MUE)=       MAXEXP
               OUTBINCHAR(MUE)=   'e'
            ELSE
               OUTBIN(IMAX,MUE)=  1
               OUTEXP(MUE)=       THREXP10ST(IMAX)
               OUTBINCHAR(MUE)=   EXPCODE(IMAX)
            END IF

         END IF
C----------------------------------------
C------- end of WTO decision
C----------------------------------------
      END DO
C--------------------------------------------------
C---- end of loop over all residues            ----
C--------------------------------------------------
      END
***** end of BINOUT

***** ------------------------------------------------------------------
***** SUB CODEIN
***** ------------------------------------------------------------------
C---- 
C---- NAME : CODEIN
C---- ARG  : 
C---- DES  : The 20 amino acid one letter names (+solvent) are 
C---- DES  : 'transcribed' into a vector with NCODEUNT components 
C---- DES  : 0,1. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The 20 amino acid one letter names (+solvent) are*
*     --------        'transcribed' into a vector with NCODEUNT        *
*                     components 0,1.                                  *
*     input var.:     MODEASSCAS, PROFACC, NBIOLOBJ                    *
*     -----------     CODEVECPROF, LOGI_CONS, *_INDEL,                 *
*     output var.:    CODEVECTOR                                       *
*     exchange var.:  to ASSCAS: CODEVECTOR                            *
*     --------------  from     : CODEVECPROF                           *
*     called from:    ASSCAS                                           *
*     procedure:      The actual residue ACTRESIDUE taken from SBR     *
*     ---------       ASSCAS is compared with the amino acid code vector
*                     AACODE(i).  In case of being equal the integer   *
*                     number i is transposed into a binary number by   *
*                     NCODEUNT units.                                  *
*                     for: MODECODEIN='PROFILE' the profiles are given *
*                     by CODEVECPROF(i), i=1,21, for each residue from *
*                     all those read from the databank.  In ASSCAS the *
*                     maximum of all these numbers is searched (PROFMAX)
*                     For the binary approach (MODECODEIN='PROFILE-BIN')
*                     the following way is used to transmit the integers
*                     to binaries:                                     *
*                     nprof in first intervall -> 000  (for PROFACC =4)*
*                           in second          -> 100                  *
*                           in third           -> 110                  *
*                           in fourth          -> 111                  *
*----------------------------------------------------------------------*
      SUBROUTINE CODEIN

C---- global parameter                                                 *
      INCLUDE 'phdParameter.f'

C---- local parameter                                                  *
      INTEGER         LOWEST
      PARAMETER      (LOWEST=                    2)

C---- local variables
      INTEGER         CHECKNUM,ITER1,ITER2,ITPROF,ITACC
      LOGICAL         CHECKFLAG(1:NBIOLOBJMAX),LHELP
      CHARACTER*1     CHELP
******------------------------------*-----------------------------******
*     ITER1,2,ITPROF,ITACC  iteration variables                        *
*     ACTRESIDUE      gives the one letter name of the actual residue  *
*                     which by calling of CODEIN ought to be translated*
*                     into a vector with 21 components (one 1, rest 0) *
*                     alternatively such a vector is given by a binary *
*                     vector with NCODEUNITS-1 components:2**unit...   *
*     CODEVECTOR      is this translated coding for every amino acid   *
*                     type (for further information, see SR  CODEIN).  *
*     LOWEST          for MODECODEIN='PROFILE-BIN': the 000.. vector is*
*                     used for RESPROF < lowest, the 111.. for >       *
*                     PROFMAX-LOWEST                                   *
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- for alphabetical coding                 -----
C--------------------------------------------------
      IF (MODEASSCAS(ACTCHI).EQ.'ALPHABET') THEN
C------- all lower case letters -> C
         CHELP=ACTRESIDUE
         IF ((CHELP.EQ.'a').OR.(CHELP.EQ.'b').OR.(CHELP.EQ.'c').OR.
     A        (CHELP.EQ.'d').OR.(CHELP.EQ.'e').OR.(CHELP.EQ.'f').OR.
     B        (CHELP.EQ.'g').OR.(CHELP.EQ.'h').OR.(CHELP.EQ.'i').OR.
     C        (CHELP.EQ.'j').OR.(CHELP.EQ.'k').OR.(CHELP.EQ.'l').OR.
     D        (CHELP.EQ.'m').OR.(CHELP.EQ.'n').OR.(CHELP.EQ.'o').OR.
     E        (CHELP.EQ.'p').OR.(CHELP.EQ.'q').OR.(CHELP.EQ.'r').OR.
     F        (CHELP.EQ.'s').OR.(CHELP.EQ.'t').OR.(CHELP.EQ.'u').OR.
     G        (CHELP.EQ.'v').OR.(CHELP.EQ.'w').OR.(CHELP.EQ.'y').OR.
     H        (CHELP.EQ.'x').OR.(CHELP.EQ.'z').OR.(CHELP.EQ.'X')) THEN
            ACTRESIDUE='C'
C------- Z -> E
         ELSEIF (CHELP.EQ.'Z') THEN
            ACTRESIDUE='E'
C------- B -> D
         ELSEIF (CHELP.EQ.'B') THEN
            ACTRESIDUE='D'
C------- for alignments: . -> E
         ELSEIF (CHELP.EQ.'.') THEN
            ACTRESIDUE='O'
C------- for alignments: . -> E
         ELSEIF (CHELP.EQ.' ') THEN
            ACTRESIDUE='U'
            WRITE(6,'(T2,A,T10,A,T30,I6,A,T40,A1)')
     +           '---','CODEIN for pos:',ACTPOS,'residue =',ACTRESIDUE
C------- for breaks: -> U
         ELSEIF (CHELP.EQ.'!') THEN
            ACTRESIDUE='U'
         END IF
C------- upper case letters
         DO ITER1=1,NBIOLOBJ
            IF (ACTRESIDUE.EQ.AACODE(ITER1)) THEN
               DO ITER2=1,NCODEUNT
                  CODEVECTOR(ITER2)=REAL(AABIT(ITER1,ITER2))
               END DO
               CHECKFLAG(ITER1)=.TRUE.
            ELSE
               CHECKFLAG(ITER1)=.FALSE.
            END IF
         END DO
C------- consistency check
         CHECKNUM=0
         DO ITER1=1,NBIOLOBJ
            IF (CHECKFLAG(ITER1) .EQV. .TRUE.) THEN
               CHECKNUM=CHECKNUM+1
            END IF
         END DO
         IF (CHECKNUM.NE.1) THEN
            WRITE(6,*)' FAULT in CODEIN: checknum = ',CHECKNUM
            WRITE(6,*)' for residue:  ',ACTRESIDUE
            STOP
         END IF
C--------------------------------------------------
C---- for encoding with profiles              -----
C--------------------------------------------------

      ELSEIF (MODEASSCAS(ACTCHI).EQ.'PROFILE-BIN') THEN
C change insertion
C         IF ((ACTRESIDUE.EQ.'!').OR.(ACTRESIDUE.EQ.'.')) THEN
         IF (ACTRESIDUE.EQ.'!') THEN
            DO ITPROF=1,(NBIOLOBJ-1)
               CODEVECPROF(ITPROF)=0
            END DO
            CODEVECPROF(ITPROF)=100
         END IF
         DO ITPROF=1,NBIOLOBJ
            LHELP=.TRUE.
C---------- resprof = 0 (+lowest)?
            IF (CODEVECPROF(ITPROF).GT.LOWEST) THEN
               CODEVECTOR(((ITPROF-1)*(PROFACC-1))+1)=1.
            ELSE
               LHELP=.FALSE.
               DO ITACC=1,(PROFACC-1)
                  CODEVECTOR(((ITPROF-1)*(PROFACC-1))+ITACC)=0.
               END DO
            END IF
C---------- profmax (-lowest)?
            IF (LHELP .EQV. .TRUE.) THEN
               IF (CODEVECPROF(ITPROF).GT.(PROFMAX-LOWEST)) THEN
                  DO ITACC=1,(PROFACC-1)
                     CODEVECTOR(((ITPROF-1)*(PROFACC-1))+ITACC)=1.
                  END DO
                  LHELP=.FALSE.
               ELSE
                  CODEVECTOR(ITPROF*(PROFACC-1))=0.
               END IF
            END IF
C---------- in between: sort into intervalls
            IF (LHELP .EQV. .TRUE.) THEN
               DO ITACC=2,(PROFACC-2)
                  IF (CODEVECPROF(ITPROF).GT.
     +                 ((ITACC-1)*PROFINTERV)) THEN
                     CODEVECTOR((ITPROF-1)*(PROFACC-1)+ITACC)=1.
                  ELSE
                     CODEVECTOR((ITPROF-1)*(PROFACC-1)+ITACC)=0.
                  END IF
               END DO
            END IF
         END DO
C--------------------------------------------------
C---- real profiles                           -----
C--------------------------------------------------

      ELSEIF ( (MODEASSCAS(ACTCHI).EQ.'PROFILE-REAL').OR.
     +         LOGI_CONS ) THEN
         IF ((ACTRESIDUE.EQ.'!').OR.(ACTRESIDUE.EQ.' ')) THEN
            DO ITPROF=1,(NBIOLOBJ-1)
               CODEVECPROF(ITPROF)=0
            END DO
            CODEVECPROF(ITPROF)=100
         END IF
         DO ITPROF=1,NBIOLOBJ
C---------- resprof = 0 (+lowest)?
            IF (CODEVECPROF(ITPROF).GT.LOWEST) THEN
               CODEVECTOR(ITPROF)=
     +              REAL(CODEVECPROF(ITPROF))/REAL(PROFMAX)
            ELSE
               CODEVECTOR(ITPROF)=0.
            END IF
         END DO
      ELSE
         WRITE(6,*)'CODEIN: wrong mode chosen: MODEASSCAS(ACTCHI)=',
     +        MODEASSCAS(ACTCHI)
         STOP
      END IF
C--------------------------------------------------
C---- end of MODEASSCAS                       -----
C--------------------------------------------------
      END
***** end of CODEIN

***** ------------------------------------------------------------------
***** SUB CODESTR
***** ------------------------------------------------------------------
C---- 
C---- NAME : CODESTR
C---- ARG  : 
C---- DES  : The NSECEL secondary structures generated by net1 are 
C---- DES  : 'transcribed' into vectors with NSECEL components 0,1 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The NSECEL secondary structures generated by net1*
*     --------        are 'transcribed' into vectors with NSECEL       *
*                     components 0,1                                   *
*     purpose:        The NSECEL secondary structures generated by net1*
*     --------        are 'transcribed' into vectors with NSECEL       *
*                     components 0,1                                   *
*     input var.:     MODEASSSTR, CASCACC, NSECEL                      *
*     -----------     CODEVECINCASC                                    *
*     output var.:    CODEVECTOR                                       *
*     exchange var.:  to ASSSTR: CODEVECTOR                            *
*     --------------  from     : CODEVECINCASC                         *
*     called from:    ASSSTR                                           *
*     procedure:      The actual residue ACTSTR is compared to SSCODE  *
*     ---------       if ACTSTR.eq.SSCODE(i), CODEVECTOR(i) is set to 1*
*                     CODEVECTOR(j), with j=1,NSECEL except i          *
*----------------------------------------------------------------------*
      SUBROUTINE CODESTR

C---- global parameter                                                 *
      INCLUDE 'phdParameter.f'

C---- local parameter                                                  *
      INTEGER         LOWEST
      PARAMETER      (LOWEST=                    2)
      INTEGER         ITSEC,ITACC,INTERMAX,POSMAX,IHELP,ITOUT
      LOGICAL         INTERFLAG
******------------------------------*-----------------------------******
*     ITER1           iteration variable                               *
*     CODEVECTOR      is this translated coding for every structure    *
*                     type (for further information, see SR  CODEIN).  *
*     LOWEST          for MODECODEIN='PROFILE-BIN': the 000.. vector is*
*                     used for RESPROF < lowest, the 111.. for >       *
*                     PROFMAX-LOWEST                                   *
******------------------------------*-----------------------------******

      POSMAX=         0

C----------------------------------------
C---- real-new 2                 --------
C----------------------------------------

      IF ( (MODEASSSTR(ACTCHI).EQ.'REAL-OCT').OR.
     +     (MODEASSSTR(ACTCHI).EQ.'REAL-CONS').OR.
     +     ((.NOT.LOGI_REALINPUT).AND.LOGI_CONS) ) THEN
         IF (CODEVECINCASC(NSECEL+1).NE.100) THEN
C----------------------------------------
C---------- secondary part
            DO ITSEC=1,NSECEL
C------------- intrj = 0 (+lowest)?
               IF (CODEVECINCASC(ITSEC).GT.LOWEST) THEN
                  CODEVECTOR(((ITSEC-1)*(CASCACC-1))+1)=1.
               ELSE
                  DO ITACC=1,(CASCACC-1)
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=0.
                  END DO
               END IF
C------------- > 100 (-lowest)?
               IF (CODEVECINCASC(ITSEC).GT.(100-LOWEST)) THEN
                  CODEVECTOR(ITSEC*(CASCACC-1))=1.
               ELSE
                  CODEVECTOR(ITSEC*(CASCACC-1))=0.
               END IF
C------------- in between: sort into intervalls
               DO ITACC=2,(CASCACC-2)
                  IF ((CODEVECINCASC(ITSEC).GE.
     +                 ((ITACC-1)*CASCINTERV))) THEN
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=1.
                  ELSE
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=0.
                  END IF
               END DO
            END DO
C----------------------------------------
C---------- no conservation weight
            IF (MODEASSSTR(ACTCHI).EQ.'REAL-OCT') THEN
C------------- spacer
               DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0
               END DO
               IHELP=(NSECEL+1)*(CASCACC-1)
C----------------------------------------
C---------- conservation weight 1992: H-E-L-cons-spacer
C----------                           -----------------
            ELSEIF (MODEASSSTR(ACTCHI).EQ.'REAL-CONS') THEN
C------------- spacer = 0
               DO ITACC=((NSECEL+1)*(CASCACC-1)+1),
     +              ((NSECEL+2)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0.
               END DO
C------------- coding conservation weight with additional
               DO ITACC=1,(CASCACC-1)
                  IF ((ACTCONSWEIGHT.GE.((ITACC-1)*2./(CASCACC-1))).AND.
     +                 (ACTCONSWEIGHT.LT.(ITACC*2./(CASCACC-1)))) THEN
                     CODEVECTOR(NSECEL*(CASCACC-1)+ITACC)=1.
                  ELSE
                     CODEVECTOR(NSECEL*(CASCACC-1)+ITACC)=0.
                  END IF
               END DO
               IHELP=(NSECEL+1)*(CASCACC-1)
C----------------------------------------
C---------- conservation weight 1993: H-E-L-spacer-expos-cons-indel
C----------                           -----------------------------
            ELSEIF (LOGI_CONS.AND.(.NOT.LOGI_INDEL)) THEN
C------------- spacer
               DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0
               END DO
               IHELP=(NSECEL+1)*(CASCACC-1)
C------------- conservation weight new
               DO ITACC=1,(CASCACC-1)
                  IF ((ACTCONSWEIGHT.GE.((ITACC-1)*2.
     +                  /REAL(CASCACC-1))).AND.
     +              (ACTCONSWEIGHT.LT.(ITACC*2./REAL(CASCACC-1)))) THEN
                     CODEVECTOR(IHELP+ITACC)=1.
                  ELSE
                     CODEVECTOR(IHELP+ITACC)=0.
                  END IF
               END DO
               IHELP=(NSECEL+2)*(CASCACC-1)
C----------------------------------------
C---------- insertions and deletions number deletions/insertions
            ELSEIF (LOGI_CONS.AND.LOGI_INDEL) THEN
C------------- spacer
               DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0
               END DO
               IHELP=(NSECEL+1)*(CASCACC-1)
C------------- conservation weight new
               DO ITACC=1,(CASCACC-1)
                  IF (((ACTNDEL/REAL(ACTNALIGN))
     +                 .GE.((ITACC-1)/REAL(CASCACC-1))).AND.
     +                 ((ACTNDEL/REAL(ACTNALIGN))
     +                 .LT.(ITACC*2./REAL(CASCACC-1)))) THEN
                     CODEVECTOR(IHELP+ITACC)=1.
                  ELSE
                     CODEVECTOR(IHELP+ITACC)=0.
                  END IF
               END DO
               IHELP=IHELP+(CASCACC-1)
               DO ITACC=1,(CASCACC-1)
                  IF (((ACTNINS/REAL(ACTNALIGN))
     +                 .GE.((ITACC-1)/REAL(CASCACC-1))).AND.
     +                 ((ACTNINS/REAL(ACTNALIGN))
     +                 .LT.(ITACC*2./REAL(CASCACC-1)))) THEN
                     CODEVECTOR(IHELP+ITACC)=1.
                  ELSE
                     CODEVECTOR(IHELP+ITACC)=0.
                  END IF
               END DO
            END IF
   
C------- spacer ?
         ELSE
            DO ITACC=1,(NSECEL*(CASCACC-1))
               CODEVECTOR(ITACC)=0.
            END DO
            IF (LOGI_CONS .EQV. .TRUE.) THEN
               DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0.
               END DO
               DO ITACC=((NSECEL+1)*(CASCACC-1)+1),NCODEUNT
                  CODEVECTOR(ITACC)=1.
               END DO
            ELSE
               DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
                  CODEVECTOR(ITACC)=0.
               END DO
               DO ITACC=((NSECEL+1)*(CASCACC-1)+1),NCODEUNT
                  CODEVECTOR(ITACC)=1.
               END DO
            END IF
         END IF

C----------------------------------------
C---- binary : winner takes all    ------
C----------------------------------------

      ELSEIF (MODEASSSTR(ACTCHI).EQ.'BIN-EXCL') THEN

C------- code secondary structure
         IF (CODEVECINCASC(NSECEL+1).NE.100) THEN
            INTERMAX=0
            INTERFLAG=.TRUE.
            DO ITSEC=1,NSECEL
               IF (CODEVECINCASC(ITSEC).GT.INTERMAX) THEN
                  INTERFLAG=.FALSE.
                  POSMAX=ITSEC
                  INTERMAX=CODEVECINCASC(ITSEC)
               END IF
            END DO
C---------- consistency check (one found?)
            IF (INTERFLAG .EQV. .TRUE.) THEN
               WRITE(6,*)'fault in CODESTR no example regarded as'
               WRITE(6,*)'"winner". For test and mue =',
     +              ACTPOS,'  and the respective outputs:'
               WRITE(6,*)'stopped 17-12-91-1b'
               STOP
            END IF
C---------- winner takes all
            DO ITSEC=1,(NSECEL+1)
               CODEVECTOR(ITSEC)=0.
            END DO
            CODEVECTOR(POSMAX)=1.
C------- spacer ?
         ELSE
            CODEVECTOR(NSECEL+1)=1.
            DO ITSEC=1,NSECEL
               CODEVECTOR(ITSEC)=0.
            END DO
         END IF

C----------------------------------------
C---- real-new                   --------
C----------------------------------------

      ELSEIF (MODEASSSTR(ACTCHI).EQ.'REAL-NEW') THEN
         IF (CODEVECINCASC(NSECEL+1).NE.100) THEN
            DO ITSEC=1,NSECEL
C------------- intrj = 0 (+lowest)?
               IF (CODEVECINCASC(ITSEC).GT.LOWEST) THEN
                  CODEVECTOR(((ITSEC-1)*(CASCACC-1))+1)=1.
               ELSE
                  DO ITACC=1,(CASCACC-1)
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=0.
                  END DO
               END IF
C------------- > 100 (-lowest)?
               IF (CODEVECINCASC(ITSEC).GT.(100-LOWEST)) THEN
                  CODEVECTOR(ITSEC*(CASCACC-1))=1.
               ELSE
                  CODEVECTOR(ITSEC*(CASCACC-1))=0.
               END IF
C------------- in between: sort into intervalls
               DO ITACC=2,(CASCACC-2)
                  IF ((CODEVECINCASC(ITSEC).GE.
     +                 ((ITACC-1)*CASCINTERV))) THEN
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=1.
                  ELSE
                     CODEVECTOR(((ITSEC-1)*(CASCACC-1))+ITACC)=0.
                  END IF
               END DO
            END DO
C------- spacer ?
         ELSE
            DO ITACC=1,(NSECEL*(CASCACC-1))
               CODEVECTOR(ITACC)=0
            END DO
            DO ITACC=(NSECEL*(CASCACC-1)+1),((NSECEL+1)*(CASCACC-1))
               CODEVECTOR(ITACC)=1
            END DO
         END IF
C----------------------------------------
C---- real real                  --------
C----------------------------------------

      ELSEIF ( (MODEASSSTR(ACTCHI).EQ.'REAL-REAL').OR.
     +         (MODEASSSTR(ACTCHI)(1:3).EQ.'RR-') ) THEN
         IF (CODEVECINCASC(NSECEL+1).NE.100) THEN
            DO ITOUT=1,NUMOUT
C------------- intrj = 0 (+lowest)?
               IF (CODEVECINCASC(ITOUT).GT.LOWEST) THEN
                  CODEVECTOR(ITOUT)=CODEVECINCASC(ITOUT)/100.
               ELSE
                  CODEVECTOR(ITOUT)=0.
               END IF
            END DO

C---------- spacer = 0
            CODEVECTOR(NUMOUT+1)=0.
            IHELP=(NUMOUT+1)

C---------- conservation weight
            CODEVECTOR(IHELP+1)=ACTCONSWEIGHT/2.
            IHELP=(NUMOUT+2)

C---------- number deletions/insertions
            IF (LOGI_INDEL .EQV. .TRUE.) THEN
               CODEVECTOR(IHELP+1)=ACTNDEL/MAX(1.,REAL(ACTNALIGN))
               CODEVECTOR(IHELP+2)=ACTNINS/MAX(1.,REAL(ACTNALIGN))
            END IF

C------- spacer ?
         ELSE
            DO ITOUT=1,NUMOUT
               CODEVECTOR(ITOUT)=0.
            END DO
            CODEVECTOR(NUMOUT+1)=1.
            DO ITOUT=(NUMOUT+2),NCODEUNT
               CODEVECTOR(ITOUT)=0.
            END DO
         END IF

      ELSE
         WRITE(6,'(T2,A,T10,A)')'***','CODESTR: mode strange!'
         WRITE(6,'(T2,A,T10,A)')'***','stopped 6-3-94d'
         STOP
      END IF
C----------------------------------------
C---- end of modeassstr           -------
C----------------------------------------
      END
***** end of CODESTR

***** ------------------------------------------------------------------
***** SUB CONTENT
***** ------------------------------------------------------------------
C---- 
C---- NAME : CONTENT
C---- ARG  : 
C---- DES  : The content in secondary structure, resp. relative 
C---- DES  : percentage of occurrence per amino acid is computed. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The content in secondary structure, resp. relative
*     --------        percentage of occurrence per amino acid is       *
*                     computed.                                        *
*     input variables:OUTBINCHARFIL, OUTBINCHAR, DSSPCHAR, RESNAME     *
*     output variab.: CONTPRED,CONTDSSP,CONTAA                         *
*     called by:      MAIN                                             *
*     calling:        lib-comp.f  SISTZ1                               *
*     procedure:      See SEVALPO, SEVALQUO, STABLEPO                  *
*----------------------------------------------------------------------*
      SUBROUTINE CONTENT

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITAA,COUNT(1:8),ICOUNT,ITSEC
      CHARACTER*24    TXTAA
      CHARACTER*1     INTERCHAR
******------------------------------*-----------------------------******

C---- default
      TXTAA(1:24)='ABCDEFGHIKLMNPQRSTVWXYZU'
C----------------------------------------
C---- computing the content of acids   --
C----------------------------------------
      DO ITAA=1,24
         ICOUNT=0
         DO MUE=1,NUMRES
            IF (TXTAA(ITAA:ITAA).EQ.RESNAME(MUE)) THEN
               ICOUNT=ICOUNT+1
            END IF
         END DO
         CONTAA(ITAA)=100*ICOUNT/REAL(NUMRES)
      END DO

C-------------------------------------------------------------
C---- secondary structure prediction                     -----
C-------------------------------------------------------------

      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
C-------------------------------------------
C------- computing the content of pred SS --
C-------------------------------------------
C------- initially set equal content=prediction
C        ===========
         CALL SISTZ1(COUNT,8)
C        ===========

         DO MUE=1,NUMRES
            IF (LFILTER .EQV. .TRUE.) THEN
               INTERCHAR=OUTBINCHARFIL(MUE)
            ELSE
               INTERCHAR=OUTBINCHAR(MUE)
            END IF
            IF (INTERCHAR.EQ.'H') THEN
               COUNT(1)=COUNT(1)+1
            ELSEIF (INTERCHAR.EQ.'E') THEN
               COUNT(2)=COUNT(2)+1
            ELSEIF ((INTERCHAR.EQ.'L').OR.(INTERCHAR.EQ.' ')) THEN
               COUNT(NSECEL)=COUNT(NSECEL)+1
            ELSEIF (NSECEL.GT.3) THEN
               IF (INTERCHAR.EQ.'T') THEN
                  COUNT(3)=COUNT(3)+1
               END IF
            END IF
         END DO
         DO ITSEC=1,NSECEL
            CONTPRED(ITSEC)=100*COUNT(ITSEC)/REAL(NUMRES)
         END DO
C-------------------------------------------
C------- computing the content of obs SS  --
C-------------------------------------------
         IF (LDSSPREAD .EQV. .TRUE.) THEN
C---------- initially set equal content=prediction
C           ===========
            CALL SISTZ1(COUNT,8)
C           ===========

            DO MUE=1,NUMRES
               INTERCHAR=CONVSECSTR(MUE)
               IF (INTERCHAR.EQ.'H') THEN
                  COUNT(1)=COUNT(1)+1
               ELSEIF (INTERCHAR.EQ.'E') THEN
                  COUNT(2)=COUNT(2)+1
               ELSEIF ((INTERCHAR.EQ.'L').OR.(INTERCHAR.EQ.' ')) THEN
                  COUNT(NSECEL)=COUNT(NSECEL)+1
               ELSEIF (NSECEL.GT.3) THEN
                  IF (INTERCHAR.EQ.'T') THEN
                     COUNT(3)=COUNT(3)+1
                  END IF
               END IF
            END DO
            DO ITSEC=1,NSECEL
               CONTDSSP(ITSEC)=100*COUNT(ITSEC)/REAL(NUMRES)
            END DO
         END IF
      END IF
C---- end of content                          -----
C--------------------------------------------------
      END
***** end of CONTENT

***** ------------------------------------------------------------------
***** SUB DATAOT
***** ------------------------------------------------------------------
C---- 
C---- NAME : DATAOT
C---- ARG  : 
C---- DES  : The prediction is written into FILEPRED. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is written into FILEPRED.         *
*     const. passed:  LWHATIF, LRDB, MODESECSTRON                      *
*     var. read:      FILEPRED, FILEOUTPUT, FILE_WHATIF, FILE_RDB      *
*     ext. SBR:       SFILEOPEN (lib-unix)                             *
*     called by:      MAIN                                             *
*     calling:        SCOUNT_HYDROPHOB, DATAOT_OUTPUT,                 *
*     --------        DATAOT_WHATIF, DATAOT_RDBSEC,DATAOT_RDBEXP,      *
*                     WRTCONTENT, WRTPRED, WRTEXP, WRTE, WRTF          *
*----------------------------------------------------------------------*
      SUBROUTINE DATAOT

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local parameter
      INTEGER         KOUT
      PARAMETER      (KOUT=                     10)

C---- local function
      INTEGER         FILEN_STRING

C---- local variables
      LOGICAL         LWRTOUTPUT
      CHARACTER*222   CHFILE
      INTEGER         IEND
******------------------------------*-----------------------------******

      LWRTOUTPUT=     .FALSE.
C--------------------------------------------------
C---- write prediction for testing set        -----
C--------------------------------------------------
C---- write into fileotpredtest
      IEND=           FILEN_STRING(FILEPRED)
      CHFILE=         ' '
      CHFILE(1:IEND)= FILEPRED(1:IEND)
      CALL SFILEOPEN(KOUT,CHFILE(1:IEND),'UNKNOWN',222,' ')
C      write(6,*)'xy filepred=',CHFILE(1:IEND)

C---- general information
C     ==================
C      CALL WRTHEADER(KOUT)
C     ==================
C---- writing content
C     ===================
      CALL WRTCONTENT(KOUT)
C     ===================
C---- write prediction + header
      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
C        ================
C         CALL WRTPRED(6)
         CALL WRTPRED(KOUT)
C        ================
      ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
C        ===============
         CALL WRTEXP(KOUT)
C        ===============
      END IF
      CALL WRTE(KOUT)
      CALL WRTF(KOUT)
C---- for i/o checks:
      WRITE(KOUT,'(A3)')'END'
C--------------------------------------------------
C---- search for potential trans-membrane regions -
C--------------------------------------------------
C---- count hydrophobic residues
C     =========================
      CALL SCOUNT_HYDROPHOB(KOUT)
C     =========================
      CLOSE(KOUT)
C---- close FILEPRED
C--------------------------------------------------
C---- write real outputs of prediction into file --
C--------------------------------------------------
      IF (LWRTOUTPUT .EQV. .TRUE.) THEN
C------- write into fileotpredtest
         IEND=          FILEN_STRING(FILEOUTPUT)
         CHFILE=        ' '
         CHFILE(1:IEND)=FILEOUTPUT(1:IEND)
         CALL SFILEOPEN(KOUT,CHFILE(1:IEND),'UNKNOWN',222,' ')
C         CALL SFILEOPEN(KOUT,FILEOUTPUT,'UNKNOWN',222,' ')
C        ==================
         CALL DATAOT_OUTPUT(KOUT)
C        ==================
         CLOSE(KOUT)
      END IF
C--------------------------------------------------
C---- write format for interchange
C--------------------------------------------------
      IF (LRDB .EQV. .TRUE.) THEN

C------- write into fileotpredtest
         IEND=          FILEN_STRING(FILE_RDB)
         CHFILE=        ' '
         CHFILE(1:IEND)=FILE_RDB(1:IEND)
         CALL SFILEOPEN(KOUT,CHFILE(1:IEND),'UNKNOWN',222,' ')
C         CALL SFILEOPEN(KOUT,FILE_RDB,'UNKNOWN',222,' ')
         IF (MODESECSTRON.EQ.'SECONDARY') THEN
C           ==================
            CALL DATAOT_RDBSEC(KOUT)
C           ==================
         ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
C           ==================
            CALL DATAOT_RDBEXP(KOUT)
C           ==================
         ELSEIF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
C           ==================
            CALL DATAOT_RDBHTM(KOUT)
C           ==================
         END IF
         CLOSE(KOUT)
      END IF
      END
***** end of DATAOT

***** ------------------------------------------------------------------
***** SUB DATAOT_OUTPUT
***** ------------------------------------------------------------------
C---- 
C---- NAME : DATAOT_OUTPUT
C---- ARG  :  
C---- DES  : The real values of a prediction are written into a file: 
C---- DES  : name.OUTPUT 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The real values of a prediction are written into *
*     --------        a file: name.OUTPUT                              *
*     in variables:   kunit                                            *
*     out variables:  OUTPUT(itsec,mue), OUTBINCHAR(mue)               *
*     called by:      DATAOT                                           *
*----------------------------------------------------------------------*
      SUBROUTINE DATAOT_OUTPUT(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'
C---- local variables                                                  *
      INTEGER         KUNIT,ITSEC,MUE
      CHARACTER*1     INTERCHAR
******------------------------------*-----------------------------******

C---- write real output values
      WRITE(KUNIT,'(T10,A,T40,I5)')'number of residues:',NUMRES
      WRITE(KUNIT,'(T10,A,T40,I5)')'number of sec str:',NSECEL
      IF (NSECEL.EQ.3) THEN
         WRITE(KUNIT,'(T10,3A6,T40,A6)')
     +        'out H ','out E ','out L',' WTO  '
      ELSE
         WRITE(KUNIT,'(T10,4A6,T40,I6)')
     +        'out H ','out E ','out T ','out L ',' WTO  '
      END IF
      DO MUE=1,NUMRES
C------- convert loop from blank to "L"
         IF ((OUTBINCHAR(MUE).NE.'H').AND.(OUTBINCHAR(MUE).NE.'E').AND.
     +        (OUTBINCHAR(MUE).NE.'T')) THEN
            INTERCHAR='L'
         ELSE
            INTERCHAR=OUTBINCHAR(MUE)
         END IF
         IF (NSECEL.EQ.3) THEN
            WRITE(KUNIT,'(T10,3F6.2,T40,A6)')
     +           (OUTPUT(ITSEC,MUE),ITSEC=1,NSECEL),INTERCHAR
         ELSE
            WRITE(KUNIT,'(T10,4F6.2,T40,A6)')
     +           (OUTPUT(ITSEC,MUE),ITSEC=1,NSECEL),INTERCHAR
         END IF
      END DO
      END 
***** end of DATAOT_OUTPUT

***** ------------------------------------------------------------------
***** SUB DATAOT_RDBSEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : DATAOT_RDBSEC
C---- ARG  :  
C---- DES  : The prediction is written into a file to merge secondary 
C---- DES  : structure and exposure prediction by calling perl script.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is written into a file to merge   *
*     --------        secondary structure and exposure prediction  by  *
*                     the perl script calling phd.                     *
*     in variables:   KUNIT                                            *
*     called by:      DATAOT                                           *
*----------------------------------------------------------------------*
      SUBROUTINE DATAOT_RDBSEC(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'
      INTEGER         FILEN_STRING
C---- local variables                                                  *
      INTEGER         KUNIT,ITSEC,MUE,IT
      CHARACTER*1     XC,INTERDSSP,INTERPRED
      CHARACTER*222   CTMP
******------------------------------*-----------------------------******

C---- default
      XC=CHAR(9)

C---- header
      WRITE(KUNIT,'(A)')'# Perl-RDB'
      WRITE(KUNIT,'(A)')'# '
      WRITE(KUNIT,'(A)')'# PHDsec: secondary structure prediction'
      CTMP=PROTNAME(1)
      WRITE(KUNIT,'(A)')'# '
      WRITE(KUNIT,'(A,T19,A)')
     +     '# PDBID         :',CTMP(1:FILEN_STRING(CTMP))
      WRITE(KUNIT,'(A,T19,I5)')'# LENGTH        :',NUMRES
      WRITE(KUNIT,'(A)')'# NOTATION No   : residue number '
      WRITE(KUNIT,'(A)')'# NOTATION AA   : amino acid one letter code'
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A)')
     +        '# NOTATION OHEL : observed secondary structure'
      END IF
      WRITE(KUNIT,'(A)')
     +     '# NOTATION PHEL : predicted secondary structure'
         WRITE(KUNIT,'(A)')'# NOTATION PHEL : '//
     +        'H=helix, E=strand, L=non-regular (taken from DSSP)'
      WRITE(KUNIT,'(A)')'# NOTATION RI_S : '//
     +     'reliability of secondary structure prediction , 0-9'
      WRITE(KUNIT,'(A)')'# NOTATION pH   : '//
     +     'probability of predicting helix, 0-9'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION pE   : probability of predicting strand'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION pL   : probability of predicting loop'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION OtH  : network output for helix, 0-100'
      WRITE(KUNIT,'(A)')'# NOTATION OtE  : network output for strand'
      WRITE(KUNIT,'(A)')'# NOTATION OtL  : network output for loop'
      WRITE(KUNIT,'(A)')'# '

C---- column names and width
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A2,A,A2,A,2(A4,A),A4,3(A,A2),3(A,A3))')
     +        'No',XC,'AA',XC,'OHEL',XC,'PHEL',XC,'RI_S',
     +        XC,'pH',XC,'pE',XC,'pL',XC,'OtH',XC,'OtE',XC,'OtL'
         WRITE(KUNIT,'(A2,3(A,A1),4(A,A2),3(A,A2))')
     +        '4N',(XC,'1',it=1,3),(XC,'1N',IT=1,4),(XC,'3N',IT=1,3)
      ELSE
         WRITE(KUNIT,'(A2,A,A2,A,A4,A,A4,3(A,A2),3(A,A3))')
     +        'No',XC,'AA',XC,'PHEL',XC,'RI_S',
     +        XC,'pH',XC,'pE',XC,'pL',XC,'OtH',XC,'OtE',XC,'OtL'
         WRITE(KUNIT,'(A2,2(A,A1),4(A,A2),3(A,A2))')
     +        '4N',(XC,'1',it=1,2),(XC,'1N',IT=1,4),(XC,'3N',IT=1,3)
      END IF

C---- prediction: number, sequence, DSSP, prediction, Rel, Prob(H,E,L)
      DO MUE=1,NUMRES
         IF (LDSSPREAD .EQV. .TRUE.) THEN
            INTERDSSP=CONVSECSTR(MUE)
         ELSE
            INTERDSSP='U'
         END IF
         IF ( (OUTBINCHARFIL(MUE).NE.'H').AND.
     +        (OUTBINCHARFIL(MUE).NE.'G').AND.
     +        (OUTBINCHARFIL(MUE).NE.'B').AND.
     +        (OUTBINCHARFIL(MUE).NE.'E') ) THEN
            INTERPRED='L'
         ELSE
            INTERPRED=OUTBINCHARFIL(MUE)
         END IF
         IF (LDSSPREAD .EQV. .TRUE.) THEN
            WRITE(KUNIT,'(I4,A,A1,A,2(A1,A),I1,3(A,I1),3(A,I3))')MUE,XC,
     +           RESNAME(MUE),XC,INTERDSSP,XC,INTERPRED,XC,RELIND(MUE),
     +           (XC,OUTBINPROB(ITSEC,MUE),ITSEC=1,NSECEL),
     +           (XC,INT(100*OUTPUT(ITSEC,MUE)),ITSEC=1,NSECEL)
         ELSE
            WRITE(KUNIT,'(I4,A,A1,A,A1,A,I1,3(A,I1),3(A,I3))')MUE,XC,
     +           RESNAME(MUE),XC,INTERPRED,XC,RELIND(MUE),
     +           (XC,OUTBINPROB(ITSEC,MUE),ITSEC=1,NSECEL),
     +           (XC,INT(100*OUTPUT(ITSEC,MUE)),ITSEC=1,NSECEL)
         END IF
      END DO
      END 
***** end of DATAOT_RDBSEC

***** ------------------------------------------------------------------
***** SUB DATAOT_RDBEXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : DATAOT_RDBEXP
C---- ARG  :  
C---- DES  : The prediction is written into a file to merge secondary 
C---- DES  : structure and exposure prediction by calling perl script.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is written into a file to merge   *
*     --------        secondary structure and exposure prediction  by  *
*                     the perl script calling phd.                     *
*     in variables:   KUNIT                                            *
*     called by:      DATAOT                                           *
*----------------------------------------------------------------------*
      SUBROUTINE DATAOT_RDBEXP(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FILEN_STRING
      REAL            FR_REXP_TO_EXP,FR_EXP_TO_REXP
      CHARACTER*1     FC_REXP_TO_3STCHAR,FC_REXPI_TO_3STCHAR

C---- local variables                                                  *
      INTEGER         KUNIT,ITEXP,MUE,
     +     OBSEXP,OBSREXP,PHDEXP,PHDREXP,IT,PHDBINLOC(1:NUMOUTMAX)
      CHARACTER*1     XC,OBSCHAR,PHDCHARLOC
      CHARACTER*222   CTMP
******------------------------------*-----------------------------******

C---- default
      XC=CHAR(9)

C---- header
      WRITE(KUNIT,'(A)')'# Perl-RDB'
      WRITE(KUNIT,'(A)')
     +     '# PHDacc: solvent accessibility prediction'
      WRITE(KUNIT,'(A)')'# '
      CTMP=PROTNAME(1)
      WRITE(KUNIT,'(A,T19,A)')
     +     '# PDBID         :',CTMP(1:FILEN_STRING(CTMP))
      WRITE(KUNIT,'(A,T19,I5)')'# LENGTH        :',NUMRES
      WRITE(KUNIT,'(A)')'# NOTATION No   : residue number '
      WRITE(KUNIT,'(A)')'# NOTATION AA   : amino acid one letter code'
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A)')'# NOTATION OACC : '//
     +        'observed accessible surface area (from DSSP), 0-300 A'
      END IF
      WRITE(KUNIT,'(A)')'# NOTATION PACC : '//
     +     'predicted accessible surface area, 0-300 A'
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A)')'# NOTATION OREL : '//
     +        'observed relative accessible surface area, 0-100'
      END IF
      WRITE(KUNIT,'(A)')'# NOTATION PREL : '//
     +     'predicted relative accessible surface area'
      WRITE(KUNIT,'(A)')
     + '# NOTATION RI_A : reliability of accessibility prediction, 0-9'
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A)')'# NOTATION Obie : observed exposure state'
      END IF
      WRITE(KUNIT,'(A)')'# NOTATION Pbie : predicted exposure state'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION Pbie : b=buried, i=intermediate, e=exposed'
      WRITE(KUNIT,'(A)')'# NOTATION Ot(n): '//
     +     'network output for relative accessibility = n*n%, 0-100'
      WRITE(KUNIT,'(A)')'# '

C---- column names and width
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A2,A,A2,7(A,A4),10(A,A2,I1))')
     +        'No',XC,'AA',XC,'OACC',XC,'PACC',XC,'OREL',XC,'PREL',XC,
     +        'RI_A',XC,'Obie',XC,'Pbie',(XC,'Ot',(IT-1),IT=1,10)
         WRITE(KUNIT,'(A2,A,A1,4(A,A2),A,A2,2(A,A1),10(A,A3))')
     +        '4N',XC,'1',(XC,'3N',IT=1,4),XC,'1N',
     +        (XC,'1',IT=1,2),(XC,'3N',IT=1,10)
      ELSE
         WRITE(KUNIT,'(A2,A,A2,4(A,A4),10(A,A2,I1))')
     +        'No',XC,'AA',XC,'PACC',XC,'PREL',XC,
     +        'RI_A',XC,'Pbie',(XC,'Ot',(IT-1),IT=1,10)
         WRITE(KUNIT,'(A2,A,A1,2(A,A2),A,A2,A,A1,10(A,A3))')
     +        '4N',XC,'1',(XC,'3N',IT=1,2),XC,'1N',
     +        XC,'1',(XC,'3N',IT=1,10)
      END IF
C---- prediction: number, sequence, DSSP, prediction, Rel, Prob(H,E,L)
      DO MUE=1,NUMRES
         IF (LFILTER .EQV. .TRUE.) THEN
            PHDREXP=INT(100*OUTEXPFIL(MUE))
            PHDEXP=INT(FR_REXP_TO_EXP(OUTEXPFIL(MUE),RESNAME(MUE)))
            DO ITEXP=1,NUMOUT
               PHDBINLOC(ITEXP)=INT(100*OUTFIL(ITEXP,MUE))
            END DO
            PHDCHARLOC=OUTBINCHARFIL(MUE)
         ELSE
            PHDREXP=INT(100*OUTEXP(MUE))
            PHDEXP=INT(FR_REXP_TO_EXP(OUTEXP(MUE),RESNAME(MUE)))
            DO ITEXP=1,NUMOUT
               PHDBINLOC(ITEXP)=INT(100*OUTPUT(ITEXP,MUE))
            END DO
            PHDCHARLOC=OUTBINCHAR(MUE)
         END IF

         IF (LDSSPREAD .EQV. .TRUE.) THEN
            OBSEXP=RESACC(MUE)
            OBSREXP=INT(100*FR_EXP_TO_REXP(OBSEXP,RESNAME(MUE)))
C                   ------------------
C           version: real value (0 - 1)
C            OBSCHAR=FC_REXP_TO_3STCHAR(DESEXP(MUE),
C     +                    THREXP3ST(2),THREXP3ST(3))
C           version: integer value (0 - 100)
            OBSCHAR=FC_REXPI_TO_3STCHAR(INT(100*DESEXP(MUE)),
     +                    THREXP3STI(2),THREXP3STI(3))
C                   ------------------
         ELSE
            OBSEXP= 0
            OBSREXP=0
            OBSCHAR='U'
         END IF

         IF (LDSSPREAD .EQV. .TRUE.) THEN
            WRITE(KUNIT,'(I4,A,A1,A,4(I3,A),I1,2(A,A1),10(A,I3))')
     +           MUE,XC,RESNAME(MUE),XC,OBSEXP,XC,PHDEXP,XC,
     +           OBSREXP,XC,PHDREXP,XC,RELIND(MUE),
     +           XC,OBSCHAR,XC,PHDCHARLOC,
     +           (XC,PHDBINLOC(ITEXP),ITEXP=1,NUMOUT)
         ELSE
            WRITE(KUNIT,'(I4,A,A1,A,2(I3,A),I1,A,A1,10(A,I3))')
     +           MUE,XC,RESNAME(MUE),XC,PHDEXP,XC,
     +           PHDREXP,XC,RELIND(MUE),XC,PHDCHARLOC,
     +           (XC,PHDBINLOC(ITEXP),ITEXP=1,NUMOUT)
         END IF
      END DO
      END 
***** end of DATAOT_RDBEXP

***** ------------------------------------------------------------------
***** SUB DATAOT_RDBHTM
***** ------------------------------------------------------------------
C---- 
C---- NAME : DATAOT_RDBHTM
C---- ARG  :  
C---- DES  : The prediction is written into a RDB file.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is written into a RDB file.       *
*     in variables:   KUNIT                                            *
*     called by:      DATAOT                                           *
*----------------------------------------------------------------------*
      SUBROUTINE DATAOT_RDBHTM(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local functions
      INTEGER         FILEN_STRING

C---- local variables                                                  *
      INTEGER         KUNIT,ITSEC,MUE,IT
      CHARACTER*1     XC,INTERDSSP,INTERPRED
      CHARACTER*222   CTMP

******------------------------------*-----------------------------******
C---- default
      XC=CHAR(9)

C---- header
      WRITE(KUNIT,'(A)')'# Perl-RDB'
      WRITE(KUNIT,'(A)')'# '
      WRITE(KUNIT,'(A)')'# PHDhtm: prediction of helical '//
     +     'transmembrane regions'
      WRITE(KUNIT,'(A)')'# '
      CTMP=PROTNAME(1)
      WRITE(KUNIT,'(A,T19,A)')
     +     '# PDBID         :',CTMP(1:FILEN_STRING(CTMP))
      WRITE(KUNIT,'(A,T19,I5)')'# LENGTH        :',NUMRES
      WRITE(KUNIT,'(A)')'# NOTATION No   : residue number '
      WRITE(KUNIT,'(A)')'# NOTATION AA   : amino acid one letter code'
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A)')'# NOTATION OHL  : observed transmembrane '//
     +        'helices'
      END IF
      WRITE(KUNIT,'(A)')
     +     '# NOTATION PHL  : predicted transmembrane helices'
      WRITE(KUNIT,'(A)')'# NOTATION PHL  : '//
     +     'H=helical membrane, L=no helical transmembrane'
      WRITE(KUNIT,'(A)')'# NOTATION RI_S : '//
     +     'reliability of secondary structure prediction , 0-9'
      WRITE(KUNIT,'(A)')'# NOTATION pH   : '//
     +     'probability of predicting helix, 0-9'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION pL   : probability of predicting loop'
      WRITE(KUNIT,'(A)')
     +     '# NOTATION OtH  : network output for helix, 0-100'
      WRITE(KUNIT,'(A)')'# NOTATION OtL  : network output for loop'
      WRITE(KUNIT,'(A)')'# '

C---- column names and width
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A2,A,A2,A,2(A4,A),A4,2(A,A2),2(A,A3))')
     +        'No',XC,'AA',XC,'OHL ',XC,'PHL ',XC,'RI_S',
     +        XC,'pH',XC,'pL',XC,'OtH',XC,'OtL'
         WRITE(KUNIT,'(A2,3(A,A1),3(A,A2),2(A,A2))')
     +        '4N',(XC,'1',it=1,3),(XC,'1N',IT=1,3),(XC,'3N',IT=1,2)
      ELSE
         WRITE(KUNIT,'(A2,A,A2,A,A4,A,A4,2(A,A2),2(A,A3))')
     +        'No',XC,'AA',XC,'PHL ',XC,'RI_S',
     +        XC,'pH',XC,'pL',XC,'OtH',XC,'OtL'
         WRITE(KUNIT,'(A2,2(A,A1),3(A,A2),2(A,A2))')
     +        '4N',(XC,'1',it=1,2),(XC,'1N',IT=1,3),(XC,'3N',IT=1,2)
      END IF

C---- prediction: number, sequence, DSSP, prediction, Rel, Prob(H,E,L)
      DO MUE=1,NUMRES
         IF (LDSSPREAD .EQV. .TRUE.) THEN
            INTERDSSP=CONVSECSTR(MUE)
         ELSE
            INTERDSSP='U'
         END IF
         IF ( (OUTBINCHARFIL(MUE).NE.'H').AND.
     +        (OUTBINCHARFIL(MUE).NE.'G').AND.
     +        (OUTBINCHARFIL(MUE).NE.'B').AND.
     +        (OUTBINCHARFIL(MUE).NE.'E') ) THEN
            INTERPRED='L'
         ELSE
            INTERPRED=OUTBINCHARFIL(MUE)
         END IF
         IF (LDSSPREAD .EQV. .TRUE.) THEN
            WRITE(KUNIT,'(I4,A,3(A1,A),I1,2(A,I1),2(A,I3))')MUE,XC,
     +           RESNAME(MUE),XC,INTERDSSP,XC,INTERPRED,XC,RELIND(MUE),
     +           (XC,OUTBINPROB(ITSEC,MUE),ITSEC=1,NSECEL),
     +           (XC,INT(100*OUTPUT(ITSEC,MUE)),ITSEC=1,NSECEL)
         ELSE
            WRITE(KUNIT,'(I4,A,2(A1,A),I1,2(A,I1),2(A,I3))')MUE,XC,
     +           RESNAME(MUE),XC,INTERPRED,XC,RELIND(MUE),
     +           (XC,OUTBINPROB(ITSEC,MUE),ITSEC=1,NSECEL),
     +           (XC,INT(100*OUTPUT(ITSEC,MUE)),ITSEC=1,NSECEL)
         END IF
      END DO
      END 
***** end of DATAOT_RDBHTM

***** ------------------------------------------------------------------
***** SUB EVALDSSP
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALDSSP
C---- ARG  : 
C---- DES  : The pay-off of the perceptron, i.e. its success is 
C---- DES  : evaluated 
C---- DES  : by computing certain measures of quality of the 
C---- DES  : prediction. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-off of the perceptron, i.e. its success  *
*     --------        is evaluated by computing certain measures of    *
*                     quality of the prediction.                       *
*     called by:      MAIN                                             *
*     calling:        EVALSEC, EVALEXP                                 *
*----------------------------------------------------------------------*
      SUBROUTINE EVALDSSP

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE

******------------------------------*-----------------------------******

C---------------------------------------------------
C---- convert secondary structure 8 -> 3
C---------------------------------------------------

      DO MUE=1,NUMRES
         IF (RESSECSTR(MUE).EQ.'G') THEN
            CONVSECSTR(MUE)='H'
         ELSEIF ((RESSECSTR(MUE).EQ.'I').OR.
     +           (RESSECSTR(MUE).EQ.'S').OR.
     +           (RESSECSTR(MUE).EQ.'B').OR.
     +           (RESSECSTR(MUE).EQ.'T').OR.
     +           (RESSECSTR(MUE).EQ.'L')) THEN
            CONVSECSTR(MUE)=' '
         ELSE
            CONVSECSTR(MUE)=RESSECSTR(MUE)
         END IF
      END DO
C---------------------------------------------------
C---- secondary structure prediction           -----
C---------------------------------------------------

      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
         write(6,*)'--- now entering: EVALSEC'
C        ============
         CALL EVALSEC
C        ============

C---------------------------------------------------
C---- prediction of solvent exposure prediction  ---
C---------------------------------------------------

      ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
         write(6,*)'--- now entering: EVALEXP'
C        ============
         CALL EVALEXP
C        ============
      END IF
      END
***** end of EVALDSSP

***** ------------------------------------------------------------------
***** SUB EVALSEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALSEC
C---- ARG  : 
C---- DES  : Secondary structure prediction accuracy. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-off of the perceptron, i.e. its success  *
*     --------        is evaluated by computing certain measures of    *
*                     quality of the prediction                        *
*     input variables:CONVSECSTR, OUTBINCHAR                           *
*     output variab.: MATNUM, MATLEN, MATLENDIS                        *
*     --------------- RMATQOFDSSP, RMATQOFPRED, Q3, SQ, CORR, INFO     *
*     called by:      EVALDSSP                                         *
*     calling:        lib-prot.f:                                      *
*                     SEVALPO, SEVALQUO,                               *
*     procedure:      See SEVALPO, SEVALQUO,                           *
*----------------------------------------------------------------------*
      SUBROUTINE EVALSEC

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,DEVNOM,NSECEL_LOC
      CHARACTER*3     TXT3
      LOGICAL         LWRITE

******------------------------------*-----------------------------******
*     MUE,NUE,ITSEC,ITSEC2,ITER,ITHISTO,ITFILES  iteration variables   *
*     CORR(i)         correlation for class i, (Mathews)               *
*     MATLEN(i,j)    matrix with the lengths of the elements:          *
*                     i=1,4 => H,E,C,all                               *
*                     j=1   => number of elements DSSP                 *
*                     j=2   => number of elements PRED                 *
*                     j=2   => summed length of all elements for DSSP  *
*                     j=2   => summed length of all elements for PRED  *
*     MATLENDIS(i,j) gives the distribution of the elements            *
*                     i=1,50=> for histogram (lengths 1-50)            *
*                     j=1,3 => H,E,C for DSSP                          *
*                     j=4,6 => H,E,C for PRED                          *
*     MATNUM(i,j)    the number of residues in a certain secondary     *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix,MATNUM(1,2) those DSSP helices predicted *
*                     as strands and MATNUM(1,4) all DSSP helices,resp.*
*                     MATNUM(4,4) all residues predicted.              *
*     RMATQOFDSSP(i,j) stores according to the same scheme as MATNUM   *
*                     the percentages of residues predicted divided by *
*                     the numbers of DSSP (note no element (4,4) )     *
*     RMATQOFPRED(i,j)same as previous but now percentages of prediction
*     Q3              =properly predicted for all classes/all residues *
*     SQ              first divide predicted/DSSP in each class then   *
*                     sum all classes and divide by e.g. 3             *
******------------------------------*-----------------------------******

C---- blow up states for TM prediction
      IF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
         NSECEL_LOC=3
      ELSE
         NSECEL_LOC=NSECEL
      END IF
C---- evaluate filtered version
      IF (.NOT.LFILTER) THEN
         DO MUE=1,NUMRES
            OUTBINCHARFIL(MUE)=OUTBINCHAR(MUE)
         END DO
      END IF
      DO MUE=(NUMRES+1),NUMRESMAX
         OUTBINCHARFIL(MUE)='x'
         CONVSECSTR(MUE)='x'
      END DO
C---- evaluating the pay-off numbers
C     ============
      CALL SEVALPO(NSECEL_LOC,NUMOUTMAX,NUMRES,NUMRESMAX,
     +     CONVSECSTR,OUTBINCHARFIL,
     +     MATNUM,RMATQOFDSSP,RMATQOFPRED,Q3,SQ,CORR)
C     ============
C---- compute lengths of secondary structure elements
C     =============
      CALL SEVALLEN(NSECEL_LOC,NUMOUTMAX,NUMRES,NUMRESMAX,
     +     50,NHISTOMAX,RESSECSTR,OUTBINCHARFIL,MATLEN,MATLENDIS)
C     =============
      TITLE(1:8)='jury on '
C     ----------------
      CALL SINTTOCHAR(NUMNETJURY,TXT3)
C     ----------------
      TITLE(9:11)=TXT3
      TITLE(12:20)='nets.    '
      LWRITE=.FALSE.
      DEVNOM=50
C     ==============
      IF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
         WRITE(6,*)'--- skip SEVALLEN for htm'
C         CALL SEVALSEG(6,LWRITE,PROTNAME(1),
C     +        NSECEL_LOC,NUMOUTMAX,NUMRES,NUMRESMAX,1,
C     +        CONVSECSTR,OUTBINCHARFIL,2,NUMSEGOVERL,
C     +        COUNTSEGMAT,QLOV,QSOV,QFOV,DEVNOM,2)
      ELSE
C        =============
         CALL SEVALSEG(6,LWRITE,PROTNAME(1),
     +        NSECEL_LOC,NUMOUTMAX,NUMRES,NUMRESMAX,1,
     +        CONVSECSTR,OUTBINCHARFIL,2,NUMSEGOVERL,
     +        COUNTSEGMAT,QLOV,QSOV,QFOV,DEVNOM,2)
C        ==============
      END IF
C---- reconvert 'L' -> ' '
      DO MUE=1,NUMRES
         IF (OUTBINCHAR(MUE).EQ.'L') THEN
            OUTBINCHAR(MUE)=' '
         END IF
      END DO
      END
***** end of EVALSEC

***** ------------------------------------------------------------------
***** SUB EVALEXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALEXP
C---- ARG  : 
C---- DES  : Solvent accessibility prediction accuracy. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Evaluates the prediction of solvent exposure.    *
*     const. passed:  NUMRESMAX, NUMRES,                               *
*     var. passed:    in:     RESACC, RESNAME, OUTEXP                  *
*     ------------    -> out: DESEXP, DSSPVEC_I2, PREDVEC_I2,          *
*                             EXP_NOINBIN,                             *
*                             OBS_NO2ST, *_NO3ST, *_NO10ST,            *
*                             EXP_NO2ST, *_NO3ST, *_NO10ST, EXP_CORR   *
*     var. read:      THREXP2ST, THREXP3ST                             *
*     ext. SBR:       lib-prot.f: SEXP_NOINBIN_2VEC, SEXP_NOINSTATES   *
*     ext. function   lib-comp.f: FRCORRVEC_REAL4                      *
*     -------------   lib-prot.f: FR_EXP_TO_REXP                       *
*     called by:      EVALDSSP                                         *
*     calling:        
*----------------------------------------------------------------------*
      SUBROUTINE EVALEXP

C---- parameters/global variables
      INCLUDE 'phdParameter.f'
      
C---- local function
      REAL            FR_EXP_TO_REXP,FRCORRVEC_REAL4

C---- local variables
      INTEGER         MUE

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- convert exposure to bins 0-9
C--------------------------------------------------

      DO MUE=1,NUMRES
C                    --------------
         DESEXP(MUE)=FR_EXP_TO_REXP(RESACC(MUE),RESNAME(MUE))
C                    --------------
         DSSPVEC_I2(MUE)=INT2(MIN(9.,SQRT(100*DESEXP(MUE))))
         IF (LFILTER .EQV. .TRUE.) THEN
            PREDVEC_I2(MUE)=INT2(MIN(9.,SQRT(100*OUTEXPFIL(MUE))))
         ELSE
            PREDVEC_I2(MUE)=INT2(MIN(9.,SQRT(100*OUTEXP(MUE))))
         END IF
      END DO
C--------------------------------------------------
C---- compute matrix of bin identities
C--------------------------------------------------

C     ======================
      CALL SEXP_NOINBIN_2VEC(NUMRESMAX,NUMRES,1,
     +     DSSPVEC_I2,2,PREDVEC_I2,3,EXP_NOINBIN,3)
C     ======================
C--------------------------------------------------
C---- evaluate correct predictions per state
C--------------------------------------------------

C     ====================
      CALL SEXP_NOINSTATES(EXP_NOINBIN,T2,T3A,T3B,1,
     +     OBS_NOIN2ST,OBS_NOIN3ST,OBS_NOIN10ST,2,
     +     EXP_NOIN2ST,EXP_NOIN3ST,EXP_NOIN10ST,2)
C     ====================
C--------------------------------------------------
C---- evaluate correlation
C--------------------------------------------------

C              ---------------
      EXP_CORR=FRCORRVEC_REAL4(NUMRESMAX,NUMRES,DESEXP,OUTEXP,1.)
C              ---------------
      END
***** end of EVALEXP

***** ------------------------------------------------------------------
***** SUB EVALOUTBINPROB
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALOUTBINPROB
C---- ARG  : 
C---- DES  : The real output values are used to compute the 
C---- DES  : probability 
C---- DES  : of the assignment to either secondary structure. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The real output values are used to compute the   *
*     --------        probability of the assignment to either secondary*
*                     structure.                                       *
*     in variables:   OUTPUT                                           *
*     out variables:  OUTBINPROB                                       *
*     called by:      NETWORK                                          *
*     procedure:      The real values are projected onto a grid of 0-9 *
*     ----------      OUTBINPROB(i)=ngrid, if:                         *
*                        OUTPUT(i)                                     *
*                     -----------------   > 0.ngrid and < 0.(ngrid-1)  *
*                     sum/i {output(i)}                                *
*----------------------------------------------------------------------*
      SUBROUTINE EVALOUTBINPROB

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local variables                                                  *
      INTEGER         ITOUT,ITOUT2,MUE,ITGRID
      REAL            SUM
      LOGICAL         LFOUND
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- compute probabilities of assignment     -----
C--------------------------------------------------

      DO MUE=1,NUMRES

C------- compute sum
         SUM=0.
         DO ITOUT=1,NUMOUT
            SUM=SUM+OUTPUT(ITOUT,MUE)
         END DO
C------- mind potential division by zero
         IF (SUM.LT.0.0001) THEN
            SUM=1
            DO ITOUT=1,NUMOUT
               OUTPUT(ITOUT,MUE)=0
            END DO
         END IF

C------- now project onto grid:
         DO ITOUT=1,NUMOUT
            LFOUND=.FALSE.
            DO ITGRID=1,10
               IF ( (LFOUND.EQV. .FALSE.).AND.
     +              ((OUTPUT(ITOUT,MUE)/SUM).GE.((ITGRID-1)/10.)).AND.
     +              ((OUTPUT(ITOUT,MUE)/SUM).LE.(ITGRID/10.)) ) THEN
                  OUTBINPROB(ITOUT,MUE)=ITGRID-1
                  LFOUND=.TRUE.
               END IF
            END DO
            IF (LFOUND.EQV. .FALSE.) THEN
               WRITE(6,'(T2,A,T10,A,T60,I3)')'***',
     +              'ERROR in EVALOUTBINPROB: non found for numout=',
     +              NUMOUT
               WRITE(6,'(T2,A,T10,A,T16,I5,T25,A,T35,3F5.2,
     +              T55,A,T65,F5.2)')'***','mue=',MUE,'OUTPUT:',
     +              (OUTPUT(ITOUT2,MUE),ITOUT2=1,NUMOUT),'sum=',SUM
            END IF
         END DO
      END DO
C---- end of loop over mue=1,numres
      END 
***** end of EVALOUTBINPROB

***** ------------------------------------------------------------------
***** SUB EVALRELIND
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALRELIND
C---- ARG  :  
C---- DES  : Computation of reliability indices.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computation of reliability indices.              *
*     called by:      NETWORK                                          *
*     calling:        EVALRELSEC, EVALRELEXP, EVALRELTM                *
*----------------------------------------------------------------------*
      SUBROUTINE EVALRELIND(LFILTERLOC)

C---- parameters/global variables
      INCLUDE 'phdParameter.f'
C---- local variables
      LOGICAL         LFILTERLOC
******------------------------------*-----------------------------******
******------------------------------*-----------------------------******
C---------------------------------------------------
C---- secondary structure prediction           -----
C---------------------------------------------------

      IF (MODESECSTRON.EQ.'SECONDARY') THEN
C        ===============
         CALL EVALRELSEC
C        ===============

C---------------------------------------------------
C---- prediction of solvent exposure prediction  ---
C---------------------------------------------------

      ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
C        ===============
         CALL EVALRELEXP(LFILTERLOC)
C        ===============

C---------------------------------------------------
C---- prediction of trans-membrane regions       ---
C---------------------------------------------------

      ELSEIF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
C        ===============
         CALL EVALRELHTM(LFILTERLOC)
C        ===============
      END IF
      END
***** end of EVALRELIND

***** ------------------------------------------------------------------
***** SUB EVALRELSEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALRELSEC
C---- ARG  : 
C---- DES  : Computation of the reliability of the assignment of 
C---- DES  : secondary 
C---- DES  : structure according to the real values of the network 
C---- DES  : output. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computation of the reliability of the assignment *
*     --------        of secondary structure according to the real     *
*                     values of the output of a network.               *
*     in parameter:   NSECEL,NUMRESMAX                                 *
*     in variables:   NUMRES, OUTPUT                                   *
*     out variables:  MAXPOS, NUMRELIND, RELIND                        *
*     called from:    NETWORK                                          *
*     SBRs calling:                                                    *
*        from personal libraries:                                      *
*        lib-comp.f:  SRMAX1, SISTZ1, SISTZ2, SRSTZ1,                  *
*        lib-unix.f:  SFILEOPEN                                        *
*----------------------------------------------------------------------*
      SUBROUTINE EVALRELSEC

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITSEC,DIFF
      INTEGER*2       ITER,INTERMAXPOS
C---- *********
      INTEGER*2       MAXPOS(1:NUMRESMAX),SNDMAXPOS
C---- *********
      REAL            INTERMAXVAL,INTERMAX(1:NUMOUTMAX)
      LOGICAL         LHELP
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- compute the reliability index (first: maxima)
C--------------------------------------------------
C----------------------------------------
C---- compute positions of maxima   -----
C----------------------------------------
      DO MUE=1,NUMRES
C        security set zero
         CALL SRSTZ1(INTERMAX,NUMOUTMAX)
C        array -> vector
         DO ITSEC=1,NSECEL
            INTERMAX(ITSEC)=REAL(OUTPUT(ITSEC,MUE))/100.
         END DO
C        get maximum
         CALL SRMAX1(INTERMAX,NUMOUTMAX,INTERMAXVAL,INTERMAXPOS)
         MAXPOS(MUE)=INTERMAXPOS
C        get second largest value
         INTERMAX(INTERMAXPOS)=0
         CALL SRMAX1(INTERMAX,NUMOUTMAX,INTERMAXVAL,INTERMAXPOS)
         SNDMAXPOS=INTERMAXPOS
C----------------------------------------
C------- compute reliability index ------
C----------------------------------------
         DIFF=INT(100*(OUTPUT(MAXPOS(MUE),MUE)-OUTPUT(SNDMAXPOS,MUE)))

C------- consistency
         IF (DIFF.LT.0) THEN
            WRITE(6,'(T2,A,T10,A,T60,I5)')'***',
     +           'ERROR IN EVALRELSEC: difference negative =',DIFF
            WRITE(6,'(T2,A,T10,A,T20,I3,A,T35,I3,T45,3I4)')'***',
     +           'pos max: ',MAXPOS(MUE),'snd max:',SNDMAXPOS,
     +           (OUTPUT(ITSEC,MUE),ITSEC=1,NSECEL)
            WRITE(6,'(T2,A,T10,A,T20,I5,A)')'***',
     +           'for mue =',MUE,'stopped 28-10-92-1'
            STOP
         END IF

C------- search intervalls
         IF (DIFF.EQ.0) THEN
            RELIND(MUE)=0
         ELSEIF (DIFF.GE.90) THEN
            RELIND(MUE)=9
         ELSE
            LHELP=.TRUE.
            DO ITER=1,9
               IF ((LHELP.EQV. .TRUE.) .AND.
     +              (DIFF.GE.((ITER-1)*10)) .AND.
     +              (DIFF.LT.(ITER*10))) THEN
                  RELIND(MUE)=ITER
                  LHELP=.FALSE.
               END IF
            END DO
         END IF
C------- end of computing reliability ---
C----------------------------------------
      END DO
C--------------------------------------------------
C---- end of computation                      -----
C--------------------------------------------------
      END
***** end of EVALRELSEC

***** ------------------------------------------------------------------
***** SUB EVALRELEXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALRELEXP
C---- ARG  :  
C---- DES  : Computes reliability indices for the exposure prediction.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computes the reliability indices for the         *
*     --------        prediction of exposure.                          *
*     called by:      EVALDSSP                                         *
*----------------------------------------------------------------------*
      SUBROUTINE EVALRELEXP(LFILTERLOC)

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FIRELIND_2ND3OFF

C---- local variables
      INTEGER         MUE,ITEXP
      REAL            OUTLOC(1:(NUMOUTMAX))
      LOGICAL         LFILTERLOC
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- loop over all residues                  -----
C--------------------------------------------------

      DO MUE=1,NUMRES

C        -----------
         CALL SRSTZ1(OUTLOC,NUMOUTMAX)
C        -----------

         IF (LFILTERLOC .EQV. .TRUE.) THEN
            DO ITEXP=1,NUMOUT
               OUTLOC(ITEXP)=OUTFIL(ITEXP,MUE)
            END DO
         ELSE
            DO ITEXP=1,NUMOUT
               OUTLOC(ITEXP)=OUTPUT(ITEXP,MUE)
            END DO
         END IF
C----------------------------------------
C------- relind
C----------------------------------------

C                    ----------------
         RELIND(MUE)=INT2(FIRELIND_2ND3OFF(NUMOUTMAX,NUMOUT,OUTLOC))
C                    ----------------
C         write(6,*)'xx mue=',mue,' ri=',relind(mue),' fil=',LFILTERLOC
      END DO
C--------------------------------------------------
C---- end of loop over all residues            ----
C--------------------------------------------------
      END
***** end of EVALRELEXP

***** ------------------------------------------------------------------
***** SUB EVALRELHTM
***** ------------------------------------------------------------------
C---- 
C---- NAME : EVALRELHTM
C---- ARG  :  
C---- DES  : Reliability indices for the transmembrane regions.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computes the reliability indices for the         *
*     --------        prediction of transmembrane regions.             *
*     called by:      EVALDSSP                                         *
*----------------------------------------------------------------------*
      SUBROUTINE EVALRELHTM(LFILTERLOC)

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FIRELIND_2ND

C---- local variables
      INTEGER         MUE,ITEXP
      REAL            OUTLOC(1:(NUMOUTMAX))
      LOGICAL         LFILTERLOC
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- loop over all residues                  -----
C--------------------------------------------------

      DO MUE=1,NUMRES
C        security: set zero
         CALL SRSTZ1(OUTLOC,NUMOUTMAX)

         IF (LFILTERLOC .EQV. .TRUE.) THEN
            DO ITEXP=1,NUMOUT
               OUTLOC(ITEXP)=OUTFIL(ITEXP,MUE)
            END DO
         ELSE
            DO ITEXP=1,NUMOUT
               OUTLOC(ITEXP)=OUTPUT(ITEXP,MUE)
            END DO
         END IF
C----------------------------------------
C------- relind
C----------------------------------------

C                    ------------
         RELIND(MUE)=INT2(FIRELIND_2ND(NUMOUTMAX,NUMOUT,OUTLOC,1.))
C                    ------------
      END DO
C--------------------------------------------------
C---- end of loop over all residues            ----
C--------------------------------------------------
      END
***** end of EVALRELHTM

***** ------------------------------------------------------------------
***** SUB FILTER
***** ------------------------------------------------------------------
C---- 
C---- NAME : FILTER
C---- ARG  : 
C---- DES  : The prediction is filtered (see FILTERSEC and 
C---- DES  : FILTEREXP) 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is filtered according to:         *
*                     SECONDARY STRUCTURE prediction                   *
*                     for helix of length 1,2:                         *
*                        if relindex >=4 -> extend in direction of     *
*                           lowest relindex, until length =3           *
*                        else            -> cut helix (make it 'L')    *
*                     EXPOSURE prediction                              *
*                     averages over groups of 3 output units           *
*     input:          OUTBINCHAR, RELIND, OUTPUT                       *
*     output:         OUTEXPFIL, OUTFIL, OUTBINCHARFIL                 *
*     called by:      MAIN                                             *
*     calling:        FILTERSEC, FILTEREXP                             *
*----------------------------------------------------------------------*
      SUBROUTINE FILTER

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      LOGICAL         LFILTERLOC

******------------------------------*-----------------------------******

C-------------------------------------------------------------
C---- secondary structure prediction                     -----
C-------------------------------------------------------------

      IF (MODESECSTRON.EQ.'SECONDARY') THEN
C        ==============
         CALL FILTERSEC
C        ==============

      ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
C        ==============
         CALL FILTEREXP
C        ==============
         LFILTERLOC=.TRUE.
C        ===============
         CALL EVALRELIND(LFILTERLOC)
C        ===============
      END IF
      END
***** end of FILTER

***** ------------------------------------------------------------------
***** SUB FILTERSEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : FILTERSEC
C---- ARG  : 
C---- DES  : The prediction is filtered according to: for helix of 
C---- DES  : length 1,2: 
C---- DES  : if relindex >=4 -> extend in direction of 
C---- DES  : lowest relindex, until length =3 
C---- DES  : else -> cut helix (make it 'L') 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The prediction is filtered according to:         *
*     --------        for helix of length 1,2:                         *
*                        if relindex >=4 -> extend in direction of     *
*                           lowest relindex, until length =3           *
*                        else            -> cut helix (make it 'L')    *
*     input variables:OUTBINCHAR, RELIND                               *
*     output variab.: OUTBINCHARFIL                                    *
*     called by:      FILTER                                           *
*----------------------------------------------------------------------*
      SUBROUTINE FILTERSEC

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITLEN,COUNTLEN
      LOGICAL         FLAGINTER

******------------------------------*-----------------------------******

C----------------------------------------
C---- applying the filter        --------
C----------------------------------------
C---- initially set equal filter=prediction
      DO MUE=1,NUMRES
         OUTBINCHARFIL(MUE)=OUTBINCHAR(MUE)
      END DO
      MUE=0
      DO WHILE (MUE.LT.NUMRES)
         MUE=MUE+1
C------- boundary check
         IF (MUE.GT.NUMRES) THEN
            WRITE(6,'(T2,A,T10,A)')'***',
     +     'ERROR: FILTERSEC (from phd) max number of samples exceeded!'
            WRITE(6,'(T2,A,T10,A,T20,I4,T28,A,T38,I6)')'***',
     +           'mue=',mue,'numres=',numres
            STOP
         END IF
C------- look for helices
         IF (OUTBINCHAR(MUE).EQ.'H') THEN
C---------- measure lengths of helical segments
            FLAGINTER=.TRUE.
            COUNTLEN=1
            DO ITLEN=1,100
               IF (FLAGINTER .EQV. .TRUE.) THEN
                  IF ((MUE.LE.(NUMRES-ITLEN)).AND.
     +                 (OUTBINCHAR(MUE+ITLEN).EQ.'H')) THEN
                     COUNTLEN=COUNTLEN+1
                  ELSE
                     FLAGINTER=.FALSE.
                  END IF
               END IF
            END DO
C---------- helix of length < 3?
            IF (COUNTLEN.LT.3) THEN
               FLAGINTER=.TRUE.
               DO ITLEN=1,COUNTLEN
                  IF ( (FLAGINTER .EQV. .TRUE.) .AND.
     +                 (RELIND(MUE+ITLEN-1).GE.4)) THEN
                     FLAGINTER=.FALSE.
                  END IF
               END DO
C------------- Relind < 4 for all residues -> cut
               IF (FLAGINTER .EQV. .TRUE.) THEN
                  DO ITLEN=1,COUNTLEN
                     OUTBINCHARFIL(MUE+ITLEN-1)=' '
                  END DO
C------------- Relind >= 4 for one residue -> elongate!
               ELSE
                  DO ITLEN=1,(3-COUNTLEN)
                     IF (MUE.LE.(NUMRES-(COUNTLEN+ITLEN))) THEN
C     br: hack 2000-05-31: avoid going for 0 ..
                        IF ( (MUE.GE.2) .AND. 
     +                       ((MUE-ITLEN).GT.0) ) THEN
                           IF (RELIND(MUE+COUNTLEN-1+ITLEN)
     +                          .LT.RELIND(MUE-ITLEN)) THEN
                              OUTBINCHARFIL(MUE+COUNTLEN-1+ITLEN)='H'
                              COUNTLEN=COUNTLEN+1
                           ELSE
                              OUTBINCHARFIL(MUE-ITLEN)='H'
                           END IF
                        ELSE
                           OUTBINCHARFIL(MUE+COUNTLEN-1+ITLEN)='H'
                           COUNTLEN=COUNTLEN+1
                        END IF
                     ELSE
                        OUTBINCHARFIL(MUE-ITLEN)='H'
                     END IF
                  END DO
               END IF
            END IF
            MUE=MUE+COUNTLEN-1
         END IF
      END DO
C---- end of FILTERSEC                        -----
C--------------------------------------------------
      END
***** end of FILTERSEC

***** ------------------------------------------------------------------
***** SUB FILTEREXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : FILTEREXP
C---- ARG  : 
C---- DES  : The prediction is filtered according to: averages over 
C---- DES  : groups of 3 output units 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Filters the jury decision: averages over 3       *
*     --------        nearest output neighbours.                       *
*     input:          OUTPUT,                                          *
*     in control:     NUMRES, NUMOUT, NUMOUTMAX,                       *
*     output:         OUTFIL, OUTEXPFIL, OUTBINCHARFIL                 *
*     called by:      FILTER                                           *
*     calling:        SR_FILTER_EXP, BINFIL                            *
*                     lib-prot.f:  SRSTZ1                              *
*----------------------------------------------------------------------*
      SUBROUTINE FILTEREXP

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITEXP
      REAL            OUTLOC(1:NUMOUTMAX),OUTLOCFIL(1:NUMOUTMAX)
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- loop over all residues
C--------------------------------------------------

      DO MUE=1,NUMRES

         DO ITEXP=1,NUMOUT
            OUTLOC(ITEXP)=OUTPUT(ITEXP,MUE)
         END DO

C------- shift emphasis away from buried, by subtracting
C------- from most buried state (0.04, 0.02 for 0 and 1, if >0.10)
         IF (LREDUCE_BURRIED .EQV. .TRUE.) THEN
            IF (OUTLOC(1).GT.REDUCE_MINSIZE) THEN
               OUTLOC(1)=OUTLOC(1)-REDUCE_STATE0
            END IF
            IF (OUTLOC(2).GT.REDUCE_MINSIZE) THEN
               OUTLOC(2)=OUTLOC(2)-REDUCE_STATE1
            END IF
         END IF

C        -----------
         CALL SRSTZ1(OUTLOCFIL,NUMOUTMAX)
C        -----------
C------- nearest neighbour average
C        ==================
         CALL SR_FILTER_EXP(NUMOUTMAX,NUMOUT,OUTLOC,OUTLOCFIL)
C        ==================
C------- store filtered output
         DO ITEXP=1,NUMOUT
            OUTFIL(ITEXP,MUE)=OUTLOCFIL(ITEXP)
         END DO
C------- store filtered output
         DO ITEXP=1,NUMOUT
            OUTFIL(ITEXP,MUE)=OUTLOCFIL(ITEXP)
         END DO
      END DO
C---- end loop over all residues
C--------------------------------------------------
C--------------------------------------------------
C---- WTO decision                            -----
C--------------------------------------------------

C     ===========
      CALL BINFIL
C     ===========
      END
***** end of FILTEREXP

***** ------------------------------------------------------------------
***** SUB INIPHD
***** ------------------------------------------------------------------
C---- 
C---- NAME : INIPHD
C---- ARG  :  
C---- DES  : The variables passed when calling phd are interpreted.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The variables passed when calling phd are        *
*     --------        interpreted (many ifs).                          *
*----------------------------------------------------------------------*
      SUBROUTINE INIPHD(NUMARGUMENTS)

C---- include parameter files
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         NUMARGUMENTS,LENTMPDIR,ITER,IHELP,IEND,IDIR
      CHARACTER*222   TMPDIR,TMPLIST,INTERNAME,TXT80
      LOGICAL         LDEFAULT,LHELP,LOK,LFILE_PRED_DONE,LFILE_RDB_DONE

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults
C--------------------------------------------------
      IEND=           0

      LFILE_PRED_DONE=.FALSE.
      LFILE_RDB_DONE= .FALSE.

      FILEPRED=       ' '
      FILE_RDB=       ' '
      FILEOUTPUT=     ' '
      INTERNAME=      ' '
C--------------------------------------------------
C---- arguments passed
C--------------------------------------------------
C      LHELP=.FALSE.
      LHELP=.TRUE.
      IF (LHELP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A3)')'---'
         WRITE(6,'(T2,A3,T10,A)')'---',
     +        'Arguments given for calling FORTRAN program phd:'
         DO ITER=1,NUMARGUMENTS
            WRITE(6,'(A,T10,I3,T15,a1,A,A1)')' --- Arg ',ITER,'|',
     +   CHAR_ARG_READ(ITER)(1:FILEN_STRING(CHAR_ARG_READ(ITER))),'|'
         END DO
      END IF

C--------------------------------------------------
C---- arg 2: read machine readable?
C--------------------------------------------------
      IF ( (NUMARGUMENTS.GE.2).AND.
     +     ((CHAR_ARG_READ(2).EQ.'MACH').OR.
     +     (CHAR_ARG_READ(2).EQ.'mach').OR.
     +     (CHAR_ARG_READ(2).EQ.'machine').OR.
     +     (CHAR_ARG_READ(2).EQ.'MACHINE').OR.
     +     (CHAR_ARG_READ(2).EQ.'MACHINE_READABLE').OR.
     +     (CHAR_ARG_READ(2).EQ.'machine_readable')) ) THEN
         LMACHINE_READABLE=.TRUE.
      ELSE
         LMACHINE_READABLE=.FALSE.
      END IF
C--------------------------------------------------
C---- arg 3: write WhatIf output?
C--------------------------------------------------
      IF ( (NUMARGUMENTS.GE.3).AND.
     +    ((CHAR_ARG_READ(3).EQ.'WHATIF').OR.
     +     (CHAR_ARG_READ(3).EQ.'WhatIf').OR.
     +     (CHAR_ARG_READ(3).EQ.'whatif').OR.
     +     (CHAR_ARG_READ(3).EQ.'Whatif').OR.
     +     (CHAR_ARG_READ(3).EQ.'KG').OR.
     +     (CHAR_ARG_READ(3).EQ.'kg')) ) THEN
         LWHATIF=.TRUE.
      ELSE
         LWHATIF=.FALSE.
      END IF
C--------------------------------------------------
C---- arg 4: prediction of exposure, or secondary structure?
C--------------------------------------------------
      IF (NUMARGUMENTS.GE.4) THEN
         IF ( (CHAR_ARG_READ(4)(1:3).EQ.'exp').OR.
     +        (CHAR_ARG_READ(4)(1:3).EQ.'EXP').OR.
     +        (CHAR_ARG_READ(4)(1:3).EQ.'acc').OR.
     +        (CHAR_ARG_READ(4)(1:3).EQ.'ACC') ) THEN
            MODESECSTRON='EXPOSURE'
            NSECEL=0
         ELSEIF ( (CHAR_ARG_READ(4)(1:3).EQ.'sec').OR.
     +        (CHAR_ARG_READ(4)(1:3).EQ.'SEC') ) THEN
            MODESECSTRON='SECONDARY'
            NSECEL=3
         ELSEIF ( (CHAR_ARG_READ(4)(1:3).EQ.'htm').OR.
     +        (CHAR_ARG_READ(4)(1:3).EQ.'HTM') ) THEN
            MODESECSTRON='SECONDARY_HTM'
            NSECEL=2
         ELSE
            MODESECSTRON='SECONDARY'
            NSECEL=3
         END IF
      ELSE
         MODESECSTRON='SECONDARY'
      END IF
C--------------------------------------------------
C---- arg 5: user phd -> LSERVER = True
C--------------------------------------------------
      IF ( (NUMARGUMENTS.GE.5).AND.
     +    ((CHAR_ARG_READ(5).EQ.'server').OR.
     +     (CHAR_ARG_READ(5).EQ.'SERVER').OR.
     +     (CHAR_ARG_READ(5).EQ.'phd').OR.
     +     (CHAR_ARG_READ(5).EQ.'PHD')) ) THEN
         LSERVER=.TRUE.
      ELSE
         LSERVER=.FALSE.

C------- be nice, say hello!
         WRITE(6,'(T2,A)')'---'
         WRITE(6,'(T2,70A1)')('-',ITER=1,70)
         WRITE(6,'(T2,A3,T10,A)')'---',
     +        'Dear User, Welcome to PredictProtein !'
         WRITE(6,'(T2,A)')'---'
      END IF
C--------------------------------------------------
C---- set environment (change to port)
C--------------------------------------------------
      IF (LSERVER .EQV. .TRUE.) THEN
         PATH_PARACOM='/home/phd/net/'
      ELSE
         PATH_PARACOM='/home/rost/pub/'
      END IF
      LENPATH_PARACOM=FILEN_STRING(PATH_PARACOM)
C--------------------------------------------------
C---- arg 6: arch list -> see below
C----
C---- arg 7: write into interchange formatted file ?
C--------------------------------------------------
      IF ( (NUMARGUMENTS.GE.7).AND.
     +    ((CHAR_ARG_READ(7).EQ.'RDB').OR.
     +     (CHAR_ARG_READ(7).EQ.'rdb').OR.
     +     (CHAR_ARG_READ(7).EQ.'TMPRDB').OR.
     +     (CHAR_ARG_READ(7).EQ.'tmprdb')) ) THEN
         LRDB=.TRUE.
      ELSE
         LRDB=.FALSE.
      END IF
C--------------------------------------------------
C---- arg 8: machine = DEC or ALPHA ?
C--------------------------------------------------
      IF ( (NUMARGUMENTS.GE.8).AND.
     +    ((CHAR_ARG_READ(8).EQ.'DEC').OR.
     +     (CHAR_ARG_READ(8).EQ.'dec').OR.
     +     (CHAR_ARG_READ(8).EQ.'ALPHA').OR.
     +     (CHAR_ARG_READ(8).EQ.'alpha')) ) THEN
         LDEC=.TRUE.
      ELSE
         LDEC=.FALSE.
      END IF
C--------------------------------------------------
C---- arg 6: read list of architectures
C--------------------------------------------------
      FILE_ARCHLIST(1:LENPATH_PARACOM)=PATH_PARACOM(1:LENPATH_PARACOM)
      LDEFAULT=.FALSE.
      LOK=.FALSE.

      IF (NUMARGUMENTS.GE.6) THEN
         IF (CHAR_ARG_READ(6).EQ.'nov93') THEN
            LDEFAULT=.TRUE.
         ELSEIF (CHAR_ARG_READ(6).EQ.'cic') THEN
            TXT80='Para-cic.com'
         ELSEIF (CHAR_ARG_READ(6)(1:7).EQ.'cic-152') THEN
            TMPDIR(1:LENPATH_PARACOM)=PATH_PARACOM
            TMPDIR((LENPATH_PARACOM+1):(LENPATH_PARACOM+5))='cross/'
            LENTMPDIR=LENPATH_PARACOM+5
            TMPLIST( 1:12)='Para-cic-152'
            TMPLIST(13:13)=CHAR_ARG_READ(6)(8:8)
            TMPLIST(14:17)='.com'
            LOK=.TRUE.
            FILE_ARCHLIST(1:LENTMPDIR)=TMPDIR(1:LENTMPDIR)
            FILE_ARCHLIST((LENTMPDIR+1):(LENTMPDIR+17))=TMPLIST(1:17)
         ELSEIF (CHAR_ARG_READ(6).EQ.'ind') THEN
            TXT80='Para-ind.com'
         ELSEIF (CHAR_ARG_READ(6).EQ.'crb') THEN
            TXT80='Para-crb.com'
         ELSEIF (CHAR_ARG_READ(6)(1:4).EQ.'Para') THEN
            FILE_ARCHLIST=CHAR_ARG_READ(6)
            LOK=.TRUE.
         ELSEIF ( (CHAR_ARG_READ(6)(1:5).EQ.'para:').OR.
     +            (CHAR_ARG_READ(6)(1:5).EQ.'para_') ) THEN
            FILE_ARCHLIST=' '
            IEND=FILEN_STRING(CHAR_ARG_READ(6))
            FILE_ARCHLIST=CHAR_ARG_READ(6)(6:IEND)
            LOK=.TRUE.
         ELSEIF ( CHAR_ARG_READ(6)(1:1).EQ.'/' ) THEN
            FILE_ARCHLIST=CHAR_ARG_READ(6)
            LOK=.TRUE.
         ELSE
            LDEFAULT=.TRUE.
         END IF
         IF (LOK .EQV. .FALSE.) THEN
            IHELP=FILEN_STRING(TXT80)
            FILE_ARCHLIST((LENPATH_PARACOM+1):(LENPATH_PARACOM+IHELP))=
     +           TXT80(1:IHELP)
         END IF
      ELSE
         LDEFAULT=.TRUE.
      END IF
C--------------------------------------------------
C---- arg 9-11: working directory and output files
C--------------------------------------------------
      IF (NUMARGUMENTS.GE.9) THEN
         IDIR=FILEN_STRING(CHAR_ARG_READ(9))
         IF (IDIR.GT.3) THEN
            PATH_WORK=CHAR_ARG_READ(9)(1:IDIR)
            WRITE(6,'(A,T30,A1,A,A1)')' --- working directory ',
     +           '"',PATH_WORK(1:FILEN_STRING(PATH_WORK)),'"'
         END IF
      END IF
      IF (NUMARGUMENTS.GE.10) THEN
         IDIR=FILEN_STRING(CHAR_ARG_READ(10))
         IF (IDIR.GT.3) THEN
            FILEPRED=CHAR_ARG_READ(10)(1:IDIR)
            LFILE_PRED_DONE=.TRUE.
            IEND=FILEN_STRING(FILEPRED)
            WRITE(6,'(A,T30,A1,A,A1)')' --- FILEPRED ',
     +           '"',FILEPRED(1:IEND),'"'
         END IF
      END IF
      IF (NUMARGUMENTS.GE.11) THEN
         IDIR=FILEN_STRING(CHAR_ARG_READ(11))
         IF (IDIR.GT.3) THEN
            FILE_RDB=CHAR_ARG_READ(11)(1:IDIR)
            LFILE_RDB_DONE=.TRUE.
            WRITE(6,'(A,T30,A1,A,A1)')' --- FILE_RDB ',
     +           '"',FILE_RDB(1:FILEN_STRING(FILE_RDB)),'"'
         END IF
      END IF

C--------------------------------------------------
C     default archictectures
C--------------------------------------------------
C         VERSION_SEC=' 4.94_252 '
C         VERSION_EXP=' 4.94_252 '
      VERSION_SEC=' 5.94_317 '
      VERSION_EXP=' 4.94_317 '
      VERSION_HTM=' 8.94_69  '

      IF (LDEFAULT .EQV. .TRUE.) THEN
         IF (MODESECSTRON.EQ.'EXPOSURE') THEN
            TXT80='Para-exp-mar94.com'
            TXT80='Para-exp-apr94.com'
            TXT80='Para-exp317-apr94.com'
         ELSEIF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
            TXT80='Para-htm69-aug94.com'
         ELSE
            TXT80='Para-nov93.com'
            TXT80='Para-apr94.com'
            TXT80='Para-sec317-may94.com'
         END IF
         IHELP=FILEN_STRING(TXT80)
         FILE_ARCHLIST((LENPATH_PARACOM+1):(LENPATH_PARACOM+IHELP))=
     +        TXT80(1:IHELP)

         WRITE(6,'(T2,A,T10,A)')'---',
     +      'Read default architecture list for prediction: '
         WRITE(6,'(T2,A,T10,A1,A,A1)')'---','|',
     +        FILE_ARCHLIST(1:FILEN_STRING(FILE_ARCHLIST)),'|'
      ELSEIF ( (FILE_ARCHLIST(1:11).EQ.'Para-exp317').OR.
     +         (FILE_ARCHLIST(1:11).EQ.'Para-sec317') ) THEN
         VERSION_SEC=' 5.94_317 '
         VERSION_EXP=' 5.94_317 '
         VERSION_HTM=' 8.94_69  '
      END IF
C--------------------------------------------------
C---- adjust filename for output file         -----
C--------------------------------------------------

      IF (LSERVER .EQV. .FALSE.) THEN

C------- get out chain identifier
         LHELP=.TRUE.
         DO ITER=1,80
            IF (LHELP .EQV. .TRUE.) THEN
               IF ( FILE_HSSP(ITER:ITER+2).EQ.'_!_' ) THEN
                  LHELP=.FALSE.
                  IEND=ITER-1
               END IF
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            FILE_HSSP_NOCHAIN=FILE_HSSP
         ELSE 
            FILE_HSSP_NOCHAIN(1:IEND)=FILE_HSSP(1:IEND)
         END IF
         
C------- get unique file id
         LHELP=.TRUE.
         DO ITER=1,80
            IF (LHELP .EQV. .TRUE.) THEN
               IF ( (FILE_HSSP(ITER:ITER+3).EQ.'hssp').OR.
     +              (FILE_HSSP(ITER:ITER+3).EQ.'Hssp').OR.
     +              (FILE_HSSP(ITER:ITER+3).EQ.'HSSP') ) THEN
                  LHELP=.FALSE.
                  IEND=ITER-2
               END IF
            END IF
         END DO

C------- pred file
         IF (.NOT.LFILE_PRED_DONE) THEN
            FILEPRED(1:IEND)=FILE_HSSP(1:IEND)
            FILEPRED((IEND+1):(IEND+5))='.pred'
            WRITE(6,'(A,T30,A1,A,A1)')' --- FILEPRED ',
     +           '"',FILEPRED(1:FILEN_STRING(FILEPRED)),'"'
         END IF

C------- *.rdb
         IF (LRDB.AND.(.NOT.LFILE_RDB_DONE)) THEN
            FILE_RDB=FILE_HSSP(1:IEND)
            IF (MODESECSTRON.EQ.'SECONDARY') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbsec'
            ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbexp'
            ELSEIF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbhtm'
            END IF
            WRITE(6,'(A,T30,A1,A,A1)')' --- FILE_RDB ',
     +           '"',FILE_RDB(1:FILEN_STRING(FILE_RDB)),'"'
         END IF

C------- output (into working)
         IDIR=FILEN_STRING(PATH_WORK)
         IF (IDIR.GT.3) THEN
            FILEOUTPUT(1:IDIR)=PATH_WORK(1:IDIR)
         ELSE
            IDIR=0
         END IF
         FILEOUTPUT(IDIR+1:IDIR+IEND)=FILE_HSSP(1:IEND)
         FILEOUTPUT(IDIR+IEND+1:IDIR+IEND+7) ='.output'

C--------------------------------------------------
C---- for phd on server: read name of HSSP file
C--------------------------------------------------
      ELSE

C------- get unique file id
         LHELP=.TRUE.
         DO ITER=1,222
            IF (LHELP .EQV. .TRUE.) THEN
               IF (FILE_HSSP(ITER:ITER).NE.'.') THEN
                  INTERNAME(ITER:ITER)=FILE_HSSP(ITER:ITER)
               ELSE
                  IEND=ITER-1
                  LHELP=.FALSE.
               END IF
            END IF
         END DO
         FILE_HSSP_NOCHAIN=FILE_HSSP

C------- pred file
         IF (LFILE_PRED_DONE .EQV. .FALSE.) THEN
            FILEPRED(1:IEND)=INTERNAME(1:IEND)
            FILEPRED((IEND+1):(IEND+5))='.pred'
         END IF

C------- *.rdb
         IF ((LRDB .EQV. .TRUE.) .AND. (.NOT.LFILE_RDB_DONE)) THEN
            FILE_RDB=FILE_HSSP(1:IEND)
            IF (MODESECSTRON.EQ.'SECONDARY') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbsec'
            ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbexp'
            ELSEIF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
               FILE_RDB(IEND+1:IEND+7)='.rdbhtm'
            END IF
         END IF

C------- output (into working)
         IDIR=FILEN_STRING(PATH_WORK)
         IF (IDIR.GT.3) THEN
            FILEOUTPUT(1:IDIR)=PATH_WORK(1:IDIR)
         ELSE
            IDIR=0
         END IF
         FILEOUTPUT(IDIR+1:IDIR+IEND)=INTERNAME(1:IEND)
         FILEOUTPUT(IDIR+IEND+1:IDIR+IEND+7)='.output'
      END IF
C--------------------------------------------------
C---- filter asf
C--------------------------------------------------

      IF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
         LFILTER=.FALSE.
      ELSE
C         LFILTER=.FALSE.
         LFILTER=.TRUE.
      END IF
      LOUTBINPROB=.TRUE.
C--------------------------------------------------
C---- initialise length intervals
C--------------------------------------------------


C---- note: unit i = 1, if length <= split_length(i)
      IF (NUNITS_LENGTH.NE.4) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in INIPHD: '//
     +        'currently only for NUNITS_LENGTH = 4 (phdParameter.f) !'
         WRITE(6,'(T2,A,T10,A,T40,I5,T50,A)')'***',
     +        'instead it is: ',NUNITS_LENGTH,'stopped 29-12-93-1.'
         STOP
      ELSE
         SPLIT_LENGTH(1)=60
         SPLIT_LENGTH(2)=120
         SPLIT_LENGTH(3)=240
         SPLIT_LENGTH(4)=500
         SPLIT_LENGTH(4)=NUMRESMAX
      END IF
C---- note: unit i = 1, if distcaps <= split_distcaps(i)
      IF (NUNITS_DISTCAPS.NE.4) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in INIPHD: '//
     +   'currently only for NUNITS_DISTCAPS = 4 (phdParameter.f) !'
         WRITE(6,'(T2,A,T10,A,T40,I5,T50,A)')'***',
     +        'instead it is: ',NUNITS_DISTCAPS,'stopped 29-12-93-1.'
         STOP
      ELSE
         SPLIT_DISTCAPS(1)=10
         SPLIT_DISTCAPS(2)=20
         SPLIT_DISTCAPS(3)=30
         SPLIT_DISTCAPS(4)=40
         SPLIT_DISTCAPS(5)=NUMRESMAX
      END IF
C--------------------------------------------------
C---- exposure thresholds and filtering stuff
C--------------------------------------------------

      IF (MODESECSTRON.EQ.'EXPOSURE') THEN
         MAXEXP=1
         DO ITER=1,10
            THREXP10ST(ITER)=(ITER-1)*(ITER-1)*MAXEXP/100.
C           convert to integer
            THREXP10STI(ITER)=INT(100*THREXP10ST(ITER))
         END DO

         THREXP2ST(1)= 0.
         THREXP2ST(2)= (16/100.)*MAXEXP
         THREXP2ST(3)= MAXEXP
C        convert to integer
         DO ITER=1,3
            THREXP2STI(ITER)=INT(100*THREXP2ST(ITER))
         END DO

         THREXP3ST(1)=0.
         THREXP3ST(2)=(  9/100.)*MAXEXP
         THREXP3ST(3)=( 36/100.)*MAXEXP
         THREXP3ST(4)=MAXEXP
C        convert to integer
         DO ITER=1,4
            THREXP3STI(ITER)=INT(100*THREXP3ST(ITER))
         END DO

         T2= 4
         T3A=3
         T3B=6

C------- filter variables (reduce the emphasis on states 0 and 1%)
         LREDUCE_BURRIED=.TRUE.
         LREDUCE_BURRIED=.FALSE.
         REDUCE_MINSIZE=0.1
         REDUCE_STATE0=0.04
         REDUCE_STATE1=0.02

      END IF
      END
***** end of INIPHD

***** ------------------------------------------------------------------
***** SUB NETWORK
***** ------------------------------------------------------------------
C---- 
C---- NAME : NETWORK
C---- ARG  : 
C---- DES  : This SBR first executes the networks trigger function by
C---- DES  : calling SBR TRIGGER for all architectures. Then, the out-
C---- DES  : puts are summed up to finally compute the jury decision.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        This SBR first executes the networks trigger     *
*     --------        function by calling SBR TRIGGER for all archi-   *
*                     tectures.  Then, the outputs are summed up to    *
*                     finally compute the jury decision.               *
*     const. passed:  NUMRES, NUMOUT, NUMNETSND, NUMNETFST             *
*     var. passed:    in: INPUT, FILEARCHFST, FILEARCHSND              *
*     ------------    out: OUTPUT, OUTBIN, OUTBINCHAR                  *
*     var. read:      NUMIN,NUMHID,NUMOUT,JUNCTION1ST,JUNCTION2ND      *
*     ----------      NUMINFST, NUMINSND, NCODEUNTSND, NCODEUNT        *
*                     NUMNEIGH, CASCAC*                                *
*     calling:        SBR: READARCH, WINDIR, TRIGGER, BINOUT,          *
*     --------             EVALRELIND, EVALOUTBINPROB                  *
*                     lib-comp.f: SRSTZ1                               *
*     called by:      main program                                     *
*----------------------------------------------------------------------*
      SUBROUTINE NETWORK

C---- parameters/global variables
      INCLUDE 'phdParameter.f'
C---- local parameters
      LOGICAL         LCHECK_WRITE
      PARAMETER       (LCHECK_WRITE=.FALSE.)
C      PARAMETER       (LCHECK_WRITE=.TRUE.)

C---- local variables
      INTEGER         CHI,MUE,ITOUT,MSTOP
      REAL            INTEROUT(1:(NUMOUTMAX))
C      CHARACTER*24    FDATE
      LOGICAL         LFILTERLOC

******------------------------------*-----------------------------******
*     CHI,MUE,ITSEC,ITOUT: iteration variables                         *
*     CONTROLCHAR     controls how the header is to be read:           *
*        'FST'        -> first network                                 *
*        'SND'        -> second network                                *
*     INTEROUT(itout) used to sum up the outputs over all jury networks*
******------------------------------*-----------------------------******

C      XDTE=FDATE()
C--------------------------------------------------
C---- loop over all first architectures       -----
C--------------------------------------------------
      DO CHI=1,NUMNETFST
         ACTCHI=CHI
         ACTFILE=FILEARCHFST(CHI)
C----------------------------------------
C------- read architecture ACTCHI   -----
C----------------------------------------
         CONTROLCHAR='FST'
C        =============
         CALL READARCH
C        =============
C------- assign input
C        ===========
         CALL WINDIR
C        ===========
C----------------------------------------
C------- compute output of 1st nets -----
C----------------------------------------
C        ============
         CALL TRIGGER
C        ============
C------- store output in OUTFST
         DO MUE=1,NUMRES
            DO ITOUT=1,NUMOUT
               OUTFST(ITOUT,MUE,CHI)=OUTPUT(ITOUT,MUE)
            END DO

C---------- check output
            MSTOP=10
            IF (LCHECK_WRITE .EQV. .TRUE.) THEN
               IF (MUE.LE.MSTOP) THEN
                  WRITE(6,'(I3,A5,10I4,a5,i3)')MUE,' -> ',
     +                 (INT(100*OUTPUT(ITOUT,MUE)),ITOUT=1,NUMOUT),
     +                 ' obs ',RESACC(MUE)
               ELSEIF (MUE.EQ.(MSTOP+1)) THEN
                  WRITE(6,*)'STOPPED IN NETWORK YY'
C                  STOP
               END IF
            END IF
         END DO
      END DO
C--------------------------------------------------
C---- end of loop over all first architectures ----
C--------------------------------------------------
C--------------------------------------------------
C---- loop over all second architectures      -----
C--------------------------------------------------
      DO CHI=1,NUMNETSND
         ACTCHI=CHI
         ACTFILE=FILEARCHSND(CHI)
C----------------------------------------
C------- read architecture ACTCHI   -----
C----------------------------------------
         CONTROLCHAR='SND'
C        =============
         CALL READARCH
C        =============
C----------------------------------------
C------- compute output of 2nd nets -----
C----------------------------------------
C------- assign windows for input
C        ===========
         CALL WINDIR
C        ===========
C------- compute network trigger
C        ============
         CALL TRIGGER
C        ============
C------- store output in OUTSND
         DO MUE=1,NUMRES
            DO ITOUT=1,NUMOUT
               OUTSND(ITOUT,MUE,CHI)=OUTPUT(ITOUT,MUE)
            END DO
         END DO
      END DO
      WRITE(6,'(T2,A,T10,A)')'---','end of READARCH'
      WRITE(6,'(T2,70A1)')('-',ITOUT=1,70)
      WRITE(6,'(T2,A)')'---'
C--------------------------------------------------
C---- end of loop over all second architectures ---
C--------------------------------------------------
C--------------------------------------------------
C---- add up outputs for jury decision        -----
C--------------------------------------------------
C----------------------------------------
C---- loop over all residues        -----
C----------------------------------------
      DO MUE=1,NUMRES

C        -----------
         CALL SRSTZ1(INTEROUT,NUMOUTMAX)
C        -----------

         DO ITOUT=1,NUMOUT
C---------- loop over all jury 2ND nets
            IF (NUMNETSND.NE.0) THEN
               DO CHI=1,NUMNETSND
                  INTEROUT(ITOUT)=INTEROUT(ITOUT)+OUTSND(ITOUT,MUE,CHI)
               END DO
               INTEROUT(ITOUT)=INTEROUT(ITOUT)/REAL(NUMNETSND)
C---------- loop over all jury 1ST nets
            ELSE
               DO CHI=1,NUMNETFST
                  INTEROUT(ITOUT)=INTEROUT(ITOUT)+OUTFST(ITOUT,MUE,CHI)
               END DO
               INTEROUT(ITOUT)=INTEROUT(ITOUT)/REAL(NUMNETFST)
            END IF
            OUTPUT(ITOUT,MUE)=INTEROUT(ITOUT)
         END DO
C         write(6,'(a,t20,10i3)')'xxx network out',
C     +        (int(100*output(itout,mue)),itout=1,numout)
      END DO
C--------------------------------------------------
C---- end of jury decision                    -----
C--------------------------------------------------
C--------------------------------------------------
C------- WTO decision                         -----
C--------------------------------------------------

C     ===========
      CALL BINOUT
C     ===========
C--------------------------------------------------
C---- assigning reliability index             -----
C--------------------------------------------------
cxx check exposure
      LFILTERLOC=.FALSE.
C     ===============
      CALL EVALRELIND(LFILTERLOC)
C     ===============
C--------------------------------------------------
C---- computing probability of assignment     -----
C---- i.e. the 0-9 % values for each outpt unit  
C--------------------------------------------------

      IF (LOUTBINPROB .EQV. .TRUE.) THEN
C        ===================
         CALL EVALOUTBINPROB
C        ===================
      END IF
      END
***** end of NETWORK

***** ------------------------------------------------------------------
***** SUB READARCH
***** ------------------------------------------------------------------
C---- 
C---- NAME : READARCH
C---- ARG  : 
C---- DES  : The data is read from files containing the junctions. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The data is read from files containing the       *
*     --------        junctions.                                       *
*     input var.:     ACTFILE, ACTCHI, CONTROLPAR                      *
*     note:           The parameters and variables are taken from the  *
*     -----           file parsecstron, see there for further details  *
*----------------------------------------------------------------------*
      SUBROUTINE READARCH

C---- global parameter                                                 *
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         ITER,ITER1,IEND
      CHARACTER*20    DUMMY20
      CHARACTER*3     DUMMY3
      LOGICAL         LOK
C      LOGICAL         FL_OTHER
******------------------------------*-----------------------------******

C---- write note into output
      IEND=           FILEN_STRING(ACTFILE)
      WRITE(6,'(T2,A,T10,A,T20,A3,A2,T26,I2,T30,A)')'---',
     +     'READARCH ',CONTROLCHAR(1:3),': ',ACTCHI,ACTFILE(1:IEND)
C---- read file
      CALL SFILEOPEN(10,ACTFILE(1:IEND),'OLD',222,'READONLY')
C---- header = 15 lines
      DO ITER=1,15
         READ(10,*)
      END DO
C---- architecture parameters = 11 lines --> 25 so far
      IF (CONTROLCHAR.EQ.'FST') THEN
         READ(10,'(A20,A20)')DUMMY20,DUMMY20
C         LOK=FL_OTHER(XDTE)
Cxxpass  LOK=FL_OTHER(XDTE)
         LOK=.TRUE.
         IF ((DUMMY20(1:5).NE.'FIRST').OR.(LOK .EQV. .FALSE.)) THEN
            WRITE(6,'(T2,A,T10,A)')'***',
     +         'ERROR in READARCH for reading in an expected first net'
            CLOSE(10)
            STOP
         END IF
         READ(10,*)
         READ(10,'(A20,A20)')DUMMY20,MODEASSCAS(ACTCHI)
         IF (MODEASSCAS(ACTCHI).EQ.'ALPHABET') THEN
            MODEASSCAS(ACTCHI)='PROFILE-REAL'
         END IF
      ELSE
         READ(10,'(A20,A20)')DUMMY20,DUMMY20
         IF (DUMMY20(1:6).NE.'SECOND') THEN
            WRITE(6,'(T2,A,T10,A)')'***',
     +           'ERROR in READARCH for reading in an expected snd net'
            CLOSE(10)
            STOP
         END IF
         READ(10,'(A20,I6)')DUMMY20,CASCACC
         READ(10,'(A20,A20)')DUMMY20,MODEASSSTR(ACTCHI)
      END IF
      READ(10,'(A20,I6)')DUMMY20,NUMIN
      READ(10,'(A20,I6)')DUMMY20,NUMHID
      READ(10,'(A20,I6)')DUMMY20,NUMOUT
      READ(10,'(A20,I6)')DUMMY20,NCODEUNT
      READ(10,'(A20,I6)')DUMMY20,NUMNEIGH
      READ(10,*)
      READ(10,*)
C---- architecture rest
C--------------------------------------------------
C---- unfortunately the new format does not have --
C---- mode DIL, thus the last units (nmach) not  --
C---- there                                      --
C--------------------------------------------------

      READ(10,*)
      DO ITER=1,NUMHID
         READ(10,'(10F10.4)')
     +        (JUNCTION1ST(ITER1,ITER),ITER1=1,(NUMIN+NUMOUT))
C         write(6,'(10F10.4)')
C     +        (JUNCTION1ST(ITER1,ITER),ITER1=1,(NUMIN+NUMOUT))
      END DO
      READ(10,*)
      DO ITER=1,(NUMHID+NUMOUT)
         READ(10,'(10F10.4)')(JUNCTION2ND(ITER,ITER1),ITER1=1,NUMOUT)
C         write(6,'(10F10.4)')(JUNCTION2ND(ITER,ITER1),ITER1=1,NUMOUT)
      END DO
C---- control
      READ(10,'(A3)')dummy3
      CLOSE(10)
      IF (DUMMY3.NE.'END') THEN
         WRITE(6,'(T2,A,T10,A)')'*****',
     +        'ERROR for reading in the architectures, here for:'
         WRITE(6,'(T2,A,T10,A,T25,A,T30,A,T40,I4,T45,A,T52,A)')'*****',
     +        'controlchar=',CONTROLCHAR,'actchi=',ACTCHI,'ACTFILE=',
     +        ACTFILE
         STOP
      END IF
      END
***** end of READARCH

***** ------------------------------------------------------------------
***** SUB READPAR
***** ------------------------------------------------------------------
C---- 
C---- NAME : READPAR
C---- ARG  : 
C---- DES  : Reads in the required parameters from the parameter file: 
C---- DES  : Parameter.com. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        This SBR reads in the required parameters from   *
*     --------        the parameter file: Parameter.com.               *
*     const. passed:  NUMNETSND,NUMNETFST                              *
*     var. read:      MAXVAR, MAXACC, PROFACC, CASCACC                 *
*     ----------      NUMNETFST,NUMNETSND,NUMNETJURY                   *
*                     NCODEUNTFST, NCODEUNTSND(1:NUMNETSND)            *
*                     TRANS2FROM1(1:NUMNETSND),                        *
*                     MODEASSCAS(1:NUMNETFST),MODEASSSTR(1:NUMNETSND)  *
*                     FILEPRED,FILEARCHFST(1:FST)/SND(1:SND)           *
*     ext. SBR:       SFILEOPEN (lib-unix)                             *
*     called by:      main program                                     *
*----------------------------------------------------------------------*
      SUBROUTINE READPAR

C---- include parameter files
      INCLUDE 'phdParameter.f'

C---- local function
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         ITER,INTER,IBEG,IEND
      CHARACTER*3     DUMMY3
      CHARACTER*20    DUMMY20
      CHARACTER*222   HC
******------------------------------*-----------------------------******

C---- read parameters from file Parameter.f
cxx
C      FILE_ARCHLIST='Para-test.com'
C      write(6,*)'xx in READPAR: forced change of parameter file to:'
C      write(6,*)'xx ',FILE_ARCHLIST

      CALL SFILEOPEN(10,FILE_ARCHLIST,'OLD',222,'READONLY')
C---- header = 14 lines
      DO ITER=1,14
         READ(10,*)
      END DO
C---- paths
      READ(10,*)
C      READ(10,'(T21,A)')PATH_PRED
      READ(10,'(T21,A)')PATH_ARCH
C      READ(10,'(T21,A)')PATH_WORK

C---- architecture parameters = 11 lines --> 25 so far
      READ(10,*)
      READ(10,'(A20,I6)')DUMMY20,MAXVAR
      READ(10,'(A20,I6)')DUMMY20,MAXACC
      READ(10,'(A20,I6)')DUMMY20,PROFACC
      READ(10,'(A20,I6)')DUMMY20,CASCACC
      READ(10,'(A20,I6)')DUMMY20,NBIOLOBJ
      READ(10,*)
      READ(10,'(A20,I6)')DUMMY20,NUMNETFST
      READ(10,'(A20,I6)')DUMMY20,NUMNETSND
      READ(10,'(A20,I6)')DUMMY20,NUMNETJURY
C     >*****
C     > TRANS2FROM1(1:NUMNETSND) (20I4)
      IF (NUMNETSND.NE.0) THEN
         READ(10,*)
         READ(10,*)
         READ(10,'(20I4)')(TRANS2FROM1(ITER),ITER=1,NUMNETSND)
      END IF
C     >*****
C     > MODEASSCAS(1:NUMNETFST) (row: no. A25)
      READ(10,*)
      READ(10,*)
      DO ITER=1,NUMNETFST
         READ(10,'(I10,T21,A)')INTER,MODEASSCAS(ITER)
      END DO
C     >*****
C     > MODEASSSTR(1:NUMNETSND) (row: no. A25)
      IF (NUMNETSND.NE.0) THEN
         READ(10,*)
         READ(10,*)
         DO ITER=1,NUMNETSND
            READ(10,'(I10,T21,A)')INTER,MODEASSSTR(ITER)
         END DO
      END IF
C     >*****
C     > FILEARCHFST(1:NUMNETFST) (row: no. A50)
      READ(10,*)
      READ(10,*)
      DO ITER=1,NUMNETFST
         READ(10,'(I10,T21,A)')INTER,FILEARCHFST(ITER)
      END DO
C     >  FILEASND(1:NUMNETSND) (row: no. A50)
      
      IF (NUMNETSND.NE.0) THEN
         READ(10,*)
         DO ITER=1,NUMNETSND
            READ(10,'(I10,T21,A)')INTER,FILEARCHSND(ITER)
         END DO
      END IF
C---- control
      READ(10,'(A3)')DUMMY3
      CLOSE(10)
      IF (DUMMY3.NE.'END') THEN
         WRITE(6,'(T2,A,T10,A,T50,A40)')'*****',
     +        'ERROR for reading in the parameter file ',
     +        FILE_ARCHLIST
         stop
      END IF
C--------------------------------------------------
C---- adding path_arch to file names
C--------------------------------------------------

      IF (FILEN_STRING(PATH_ARCH).GT.1) THEN
         DO ITER=1,NUMNETFST
            HC=PATH_ARCH
            IBEG=FILEN_STRING(PATH_ARCH)+1
            IEND=IBEG+FILEN_STRING(FILEARCHFST(ITER))-1
            HC(IBEG:IEND)=FILEARCHFST(ITER)
            FILEARCHFST(ITER)(1:IEND)=HC(1:IEND)
         END DO

         DO ITER=1,NUMNETSND
            HC=PATH_ARCH
            IBEG=FILEN_STRING(PATH_ARCH)+1
            IEND=IBEG+FILEN_STRING(FILEARCHSND(ITER))-1
            HC(IBEG:IEND)=FILEARCHSND(ITER)
            FILEARCHSND(ITER)(1:IEND)=HC(1:IEND)
         END DO
      END IF
C---- bullshit to avoid warning
      IF (INTER.GT.1) THEN
         CONTINUE
      END IF
      IF (DUMMY20.EQ.'XX') THEN
         CONTINUE
      END IF
      END
***** end of READPAR

***** ------------------------------------------------------------------
***** SUB RS_GETHSSP
***** ------------------------------------------------------------------
C---- 
C---- NAME : RS_GETHSSP
C---- ARG  : 
C---- DES  : Reading the protein from an HSSP file: FILE_HSSP 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*                      changed: Aug,        2003        version 1.2    *
*----------------------------------------------------------------------*
C     purpose:        Reading the protein from an HSSP file: FILE_HSSP *
C     input parameter:NUMPROTMAX,NUMRESMAX                             *
C     input variable: FILE_HSSP                                        *
C     output variable:PROTNAME,RESNAME,NUMRES                          *
C     ----------------POINTBEG,POINTEND,RESSECSTR,RESACC,RESVAR        *
C                     RESPROF                                          *
C     Note:           Program taken from Reinhard Schneider            *
C=======================================================================
C NUMPROT:       number of proteins in compressed database
C POINTBEG:      pointer to begining of protein
C POINTEND:      pointer to end of protein
C PROTNAME       Brookhaven Data Bank identifier
C RESNAME:       sequential storage for the SEQUENCE
C                NOTE: lower case characters are 'C'; lower case of
C                HSSP-files (insertions) are converted in RS_GETHSSPBASE
C RESSECSTR:     sequential storage for the SECONDARY STRUCTURE 
C                NOTE: original DSSP definition
C=======================================================================
C CAUTION:       RESVAR and RESACC are INTEGER*2
C=======================================================================
C RESACC:        solvated residue surface area in A**2 
C RESVAR:        sequence variability as derived from the nalign 
C                alignments
C OFFSET:        offset of aligned sequence (HSSP-alignment) to PDBSEQ
C=======================================================================
C-----------------------------------------------------------------------
C       Reinhard Schneider              December, 1991                 *
C-----------------------------------------------------------------------
*----------------------------------------------------------------------*
      SUBROUTINE RS_GETHSSP

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local parameters and variables
      INTEGER         KDSSP
C      INTEGER         MAXRESBASE
C      PARAMETER      (MAXRESBASE=        NUMRESMAX)
      PARAMETER      (KDSSP=                    12)
C      INTEGER         MAXRES,MAXAA
C
C     now defined in parameter file!!
C     
C      PARAMETER      (MAXALIGNS=              3000)
C      PARAMETER      (MAXALIGNS=              5000)
C      PARAMETER      (MAXCORE=              500000)
C      PARAMETER      (MAXCORE=              100000)
C      PARAMETER      (MAXAA=                    20)

C..   Attributes of individual proteins
      INTEGER         ACCBASE(NUMRESMAX)
      INTEGER         VARIABILITY(NUMRESMAX)
      INTEGER         PROFILE(NUMRESMAX,MAXAA)
      INTEGER         WRT_NINS(NUMRESMAX),WRITENDEL(NUMRESMAX)
C      INTEGER         WRT_NALIGN(NUMPROTMAX)
      INTEGER         WRT_NALIGN(MAXALIGNS)
      REAL*4          WRT_CONSWEIGHT(NUMRESMAX)

C..   attributes of all proteins
      INTEGER         PROTBEGIN(MAXALIGNS),PROTEND(0:MAXALIGNS)
      CHARACTER       CSEQBASE(NUMRESMAX)
      CHARACTER       CSTRBASE(NUMRESMAX)
      CHARACTER*132   CPROTID(MAXALIGNS)    
      INTEGER         NPROT,NCHAINBREAK,NPROTBREAK,
     +                OFFSET(MAXALIGNS)
C     THRESHOLD for HSSP alignments

C..   LOGICAL   
      LOGICAL         LERROR,LOGIPROFILE,LOGICONSWEIGHT
      INTEGER         I,J,K,ISTART,ISTOP,LEN,KRESBASE,IPOS
C======================================================================
C need to read HSSP files 
C======================================================================
C Reinhard Schneider 1989, BIOcomputing EMBL, D-6900 Heidelberg, FRG
C please report any bug, e-mail (INTERNET):
C     schneider@EMBL-Heidelberg.DE 
C or  sander@EMBL-Heidelberg.DE    
C=======================================================================
C  INCREASE THE NUMBER OF FOLLOWING THREE PARAMETER IF NECESSARY
C  Note: increase also in calling program
C=======================================================================
C  maxaligns = maximal number of alignments in a HSSP-file
C  maxres= maximal number of residues in a PDB-protein
C  maxcore= maximal space for storing the alignments
C=======================================================================
C  maxaa= 20 amino acids
C  nblocksize= number of alignments in one line
C  pdbid= Brookhaven Data Bank identifier
C  header,compound,source,author= informations about the PDB-protein
C  pdbseq= amino acid sequence of the PDB-protein
C  chainid= chain identifier (chain A etc.)
C  secstr= DSSP secondary structure summary
C  bp1,bp2= beta-bridge partner
C  cols= DSSP hydrogen bonding patterns for turns and helices,
C        geometrical bend, chirality, one character name of beta-ladder
C        and of beta-sheet
C  sheetlabel= chain identifier of beta bridge partner
C  seqlength= number of amino acids in the PDB-protein 
C  pdbno= residue number as in PDB file
C  nchain= number of different chains in pdbid.DSSP data set
C  kchain= number of chains used in HSSP data set
C  nalign= number of alignments
C  acc= solvated residue surface area in A**2 
C  emblid= EMBL/SWISSPROT identifier of the alignend protein
C  strid= if the 3-D structure of this protein is known, then strid 
C         (structure ID)is the Protein Data Bank identifier as taken
C         from the EMBL/SWISSPROT entry
C  protname= one line description of alignend protein
C  aliseq= sequential storage for the alignments
C  alipointer= points to the beginning of alignment X ( 1>= X <=nalign )
C  ifir,ilas= first and last position of the alignment in the test
C             protein
C  jfir,jlas= first and last position of the alignment in the alignend
C             protein
C  lali= length of the alignment excluding insertions and deletions
C  ngap= number of insertions and deletions in the alignment
C  lgap= total length of all insertions and deletions
C  lenseq= length of the entire sequence of the alignend protein
C  ide= percentage of residue identity of the alignment
C  var= sequence variability as derived from the nalign alignments
C  seqprof= relative frequency for each of the 20 amino acids
C  nocc= number of alignend sequences spanning this position (including
C        the test sequence
C  ndel= number of sequences with a deletion in the test protein at this
C        position
C  nins= number of sequences with an insertion in the test protein at
C        this position
C  LOGIPROFILE if true the profile is written into output file
C  entropy= entropy measure of sequence variability at this position
C  relent= relative entropy (entropy normalized to the range 0-100)
C=======================================================================
C============================ import ==================================
C attributes of sequence with known structure
      CHARACTER*132   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
      CHARACTER       PDBSEQ(NUMRESMAX),CHAINID(NUMRESMAX),
     +                SECSTR(NUMRESMAX)
C.......LENGHT*7
      CHARACTER*7     COLS(NUMRESMAX)
      CHARACTER*132   CHAINREMARK 

      INTEGER         SEQLENGTH,NCHAIN,KCHAIN,NALIGN
      INTEGER         ACC(NUMRESMAX)

C     br 2003-08-23: save space
C      CHARACTER       SHEETLABEL(NUMRESMAX)
C      INTEGER         BP1(NUMRESMAX),BP2(NUMRESMAX),PDBNO(NUMRESMAX)
      CHARACTER       SHEETLABEL(1)
      INTEGER         BP1(1),BP2(1),PDBNO(1)
C     br 2003-08-23: end save space

C ATTRIBUTES OF ALIGNED SEQUENCES
      CHARACTER*132   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
     +                IPROTNAME(MAXALIGNS),ACCNUM(MAXALIGNS),
     +                EXCLUDEFLAG(MAXALIGNS)
C     br 2003-08-23: save space
C      CHARACTER       ALISEQ(MAXCORE)
      CHARACTER       ALISEQ(1)
C     br 2003-08-23: end save space

      INTEGER         ALIPOINTER(MAXALIGNS),
     +                IFIR(MAXALIGNS),ILAS(MAXALIGNS),JFIR(MAXALIGNS),
     +                JLAS(MAXALIGNS),LALI(MAXALIGNS),NGAP(MAXALIGNS),
     +                LGAP(MAXALIGNS),LENSEQ(MAXALIGNS)
      REAL            IDE(MAXALIGNS),SIM(MAXALIGNS)
C ATTRIBUTES OF PROFILE
      INTEGER         SEQPROF(NUMRESMAX,MAXAA),
     +                NOCC(NUMRESMAX),NDEL(NUMRESMAX),NINS(NUMRESMAX)
      REAL            CONSWEIGHT(NUMRESMAX)

C     br 2003-08-23: save space
C      INTEGER         VAR(NUMRESMAX),RELENT(NUMRESMAX)
C      REAL            ENTROPY(NUMRESMAX)
      INTEGER         VAR(1),RELENT(1)
      REAL            ENTROPY(1)
C     br 2003-08-23: end save space


C     br 2003-08-23: save space
C     br 2003-08-23: end save space

C.......
      LOGICAL         LCONSERV,LOLDVERSION
C Compiler bug patch
C write data in loop, because the SUN4 compiler has a limit stacksize of
C 4096 (before it was 8192 MURKS) in a routine(do_u_out, rwrite or w4cp)
C      integer NPACK
C      parameter (NPACK=1000)
C......
      LOGICAL         LSCREEN

      INTEGER         MAXALIGNS_LOC,MAXRES
C=======================================================================

C.... init
      MAXRES=           NUMRESMAX
      MAXALIGNS_LOC=    MAXALIGNS
      NPROT=            0
      NCHAINBREAK=      0
      NPROTBREAK=       0
      DO I=1,MAXALIGNS_LOC
         emblid(i)=     ' '
      ENDDO
      DO I=1,MAXALIGNS
         PROTBEGIN(I)=  0
         PROTEND(I)=    0
         WRT_NALIGN(I)= 0
         CPROTID(I)=    ' '
         OFFSET(I)=     1
      ENDDO
      DO I=1,NUMRESMAX
         CSEQBASE(I)=   ' '
         CSTRBASE(I)=   ' '
         ACCBASE(I)=    0 
         VARIABILITY(I)=0
         WRITENDEL(I)=  0
         WRT_NINS(I)=   0
      ENDDO
      PROTEND(0)=       -1
C defaults
C      FILE_HSSP='/data/hssp/3b5c.hssp'
C      ISAFE=10
C      lformula=.true.
C      lall=.false.
      LOGIPROFILE=    .TRUE.
      LOGICONSWEIGHT= .TRUE.
      LOGI_INDEL=     .TRUE.
      LSCREEN=        .FALSE.
C=====================================================================
C======= AND HERE WE GO ==============================================
C=====================================================================
C       CALL GETCHAR(80,OUTFILE,' output file ? ')             
C       CALL GETINT(1,ISAFE,' HSSP-threshold + x percent ?')
C       CALL ASK(' including profile information?',LOGIPROFILE)
C       CALL ASK(' including conservation weight?',LOGICONSWEIGHT)
CC      CALL GETCHAR(80,PROTFILE,' protname ? ')             
      WRITE(6,'(T2,A,T10,A,T22,A50)')'---','HSSP file',FILE_HSSP

C---- calling SBR for reading
      CALL RS_READHSSP(KDSSP,FILE_HSSP,LERROR,
     +     MAXRES,MAXALIGNS,MAXCORE,
     +     PDBID,HEADER,COMPOUND,SOURCE,AUTHOR,SEQLENGTH,
     +     NCHAIN,KCHAIN,CHAINREMARK,NALIGN,
     +     EXCLUDEFLAG,EMBLID,STRID,IDE,SIM,
     +     IFIR,ILAS,JFIR,JLAS,LALI,NGAP,LGAP,
     +     LENSEQ,ACCNUM,IPROTNAME,
     +     PDBNO,PDBSEQ,CHAINID,SECSTR,COLS,SHEETLABEL,BP1,BP2,
     +     ACC,NOCC,VAR,ALISEQ,ALIPOINTER,
     +     SEQPROF,NDEL,NINS,ENTROPY,RELENT,CONSWEIGHT,
     +     LCONSERV,LOLDVERSION)
C first PDBSEQ
C-----------------------------------------------------------------------
      IF (LERROR .EQV. .TRUE.) THEN
          write(6,*)'xx LERROR after read hssp: ',LERROR
      ENDIF

      KRESBASE=1
      IF (LERROR .EQV. .FALSE.) THEN
         NPROT=NPROT+1
         LEN=SEQLENGTH
         IF (KRESBASE+LEN .GT. NUMRESMAX) THEN
            WRITE(6,'(T2,A,T10,A,T30,I5)')'***',
     +           'NUMRESMAX OVERFLOW ',KRESBASE+LEN
            STOP
         ENDIF
         CPROTID(NPROT)=PDBID
         PROTBEGIN(NPROT)=PROTEND(NPROT-1)+2
         PROTEND(NPROT)=PROTBEGIN(NPROT)-1+LEN
         KRESBASE=PROTBEGIN(NPROT)
         OFFSET(NPROT)=1
         WRT_NALIGN(NPROT)=NALIGN
         DO K=1,SEQLENGTH
            CSEQBASE(KRESBASE)=PDBSEQ(K)
            IF (PDBSEQ(K).EQ.'   !')NCHAINBREAK=NCHAINBREAK+1
            IF (SECSTR(K).EQ. ' ') THEN
               CSTRBASE(KRESBASE)='L'
            ELSE
               CSTRBASE(KRESBASE)=SECSTR(K)
            ENDIF
            ACCBASE(KRESBASE)=ACC(K)
C            VARIABILITY(KRESBASE)=VAR(K)
            IF (LOGIPROFILE .EQV. .TRUE.) THEN
               DO IPOS=1,MAXAA
                  PROFILE(KRESBASE,IPOS)=SEQPROF(K,IPOS)
               ENDDO
            ENDIF
            IF (LOGICONSWEIGHT .EQV. .TRUE.) THEN
               WRT_CONSWEIGHT(KRESBASE)=CONSWEIGHT(K)
            END IF
            IF (LOGI_INDEL .EQV. .TRUE.) THEN
               WRITENDEL(KRESBASE)=NDEL(K)
               WRT_NINS(KRESBASE)=NINS(K)
            END IF
            KRESBASE=KRESBASE+1
         ENDDO
         CALL STRPOS(FILE_HSSP,ISTART,ISTOP)
C set '/' between proteins
         CSEQBASE(KRESBASE)='/'
         CSTRBASE(KRESBASE)='/'
         KRESBASE=KRESBASE+1
         NPROTBREAK=NPROTBREAK+1
      ELSE 
         WRITE(6,'(T2,A,T10,A,T40,A)')'***',
     +        ' ERROR READING FILE_HSSP: ',FILE_HSSP
      ENDIF
      NPROTBREAK=NPROTBREAK-1
      NPROT=NPROT
C.....end.loop over lifi   
C....................................................................
CDEBUG.........                              
C...............................................................
C write data
C...........................
      DO I=1,NPROT
         PROTNAME(I)=CPROTID(I)
         NUMRES=PROTEND(I)-PROTBEGIN(I)+1
         NUMNALIGN(I)=WRT_NALIGN(I)
         DO J=1,PROTEND(I)
            RESNAME(J)=CSEQBASE(J)
            RESSECSTR(J)=CSTRBASE(J)
            RESACC(J)=ACCBASE(J)
C     br 2003-08-23: save space
C            RESVAR(J)=VARIABILITY(J)
C     br 2003-08-23: end save space
            IF (LOGIPROFILE .EQV. .TRUE.) THEN
               DO K=1,MAXAA
                  RESPROF(J,K)=PROFILE(J,K)
               END DO
            END IF
            IF (LOGICONSWEIGHT .EQV. .TRUE.) THEN
               RESCONSWEIGHT(J)=WRT_CONSWEIGHT(J)
            END IF
            IF (LOGI_INDEL .EQV. .TRUE.) THEN
               RESNDEL(J)=WRITENDEL(J)
               RESNINS(J)=WRT_NINS(J)
            END IF
         END DO
      END DO
C     changed 31-1-94 xxzz
      NUMNALIGN(1)=WRT_NALIGN(1)
cxx
      WRITE(6,'(T2,A,T10,A)')'---','   RS_GETHSSP(final words) got:'
      WRITE(6,'(T2,A,T10,A,T25,A50)')'---','protname(50)',CPROTID(1)
      WRITE(6,'(T2,A,T10,A,T25,I4,T33,A,T45,I4)')'---',
     +     'length:',NUMRES,'N alis:',WRT_NALIGN(1)
      WRITE(6,'(T2,A,T10,A,T25,60A1)')'---',
     +     'sequence',(CSEQBASE(K),K=1,PROTEND(NPROT))
      WRITE(6,'(T2,A,T10,A,T25,60A1)')'---',
     +     'structure',(CSTRBASE(K),K=1,PROTEND(NPROT))
      WRITE(6,'(T2,A,T10,A,T25,5I10)')'---',
     +     'OFFSET',(OFFSET(K),K=1,NPROT)
      IF (LOGIPROFILE.AND.LOGICONSWEIGHT.AND.LOGI_INDEL.AND.
     +     LSCREEN) THEN
         WRITE(6,'(T2,A,T10,A,T72,4A4)')'-no seq',
     + '  V  L  I  M  F  W  Y  G  A  P  S  T  C  H  R  K  Q  E  N  D',
     +        'Cons',' Del',' Ins',' ACC'
         DO I=PROTBEGIN(1),PROTEND(1)
            WRITE(6,'(T2,I4,T8,A1,T10,20I3,T72,F4.2,2I4,I4)')
     +           I,CSEQBASE(I),
     +           (PROFILE(I,J),J=1,MAXAA),WRT_CONSWEIGHT(I),
     +           WRITENDEL(I),WRT_NINS(I),RESACC(I)
         enddo
      ENDIF
      WRITE(6,'(T2,A,T10,A)')'---','   RS_GETHSSP END(really)'
      END
***** end of RS_GETHSSP

***** ------------------------------------------------------------------
***** SUB RS_READHSSP
***** ------------------------------------------------------------------
C---- 
C---- NAME : RS_READHSSP
C---- ARG  :  
C---- DES  : does it ..
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE RS_READHSSP(IUNIT,FILE_HSSP_LOC,LERROR,
     +     MAXRES,MAXALIGNS_LOC,MAXCORE_LOC,
     +     PDBID,HEADER,COMPOUND,SOURCE,AUTHOR,SEQLENGTH,
     +     NCHAIN,KCHAIN,CHAINREMARK,NALIGN,
     +     EXCLUDEFLAG,EMBLID,STRID,IDE,SIM,
     +     IFIR,ILAS,JFIR,JLAS,LALI,NGAP,LGAP,
     +     LENSEQ,ACCNUM,IPROTNAME,
     +     PDBNO,PDBSEQ,CHAINID,SECSTR,COLS,SHEETLABEL,BP1,BP2,
     +     ACC,NOCC,VAR,ALISEQ,ALIPOINTER,
     +     SEQPROF,NDEL,NINS,ENTROPY,RELENT,CONSWEIGHT,
     +     LCONSERV,LOLDVERSION)

C---- global parameters
      INCLUDE 'phdParameter.f'

C Reinhard Schneider 1989, BIOcomputing EMBL, D-6900 Heidelberg, FRG
C please report any bug, e-mail (INTERNET):
C     schneider@EMBL-Heidelberg.DE 
C or  sander@EMBL-Heidelberg.DE    
C=======================================================================
C  INCREASE THE NUMBER OF FOLLOWING THREE PARAMETER IN THE CALLING 
C  PROGRAM IF NECESSARY
C=======================================================================
C  maxaligns = maximal number of alignments in a HSSP-file
C  maxres= maximal number of residues in a PDB-protein
C  maxcore= maximal space for storing the alignments
C=======================================================================
C  maxaa= 20 amino acids
C  nblocksize= number of alignments in one line
C  pdbid= Brookhaven Data Bank identifier
C  header,compound,source,author= informations about the PDB-protein
C  pdbseq= amino acid sequence of the PDB-protein
C  chainid= chain identifier (chain A etc.)
C  secstr= DSSP secondary structure summary
C  bp1,bp2= beta-bridge partner
C  cols= DSSP hydrogen bonding patterns for turns and helices,
C        geometrical bend, chirality, one character name of beta-ladder
C        and of beta-sheet
C  sheetlabel= chain identifier of beta bridge partner
C  seqlength= number of amino acids in the PDB-protein 
C  pdbno= residue number as in PDB file
C  nchain= number of different chains in pdbid.DSSP data set
C  kchain= number of chains used in HSSP data set
C  nalign= number of alignments
C  acc= solvated residue surface area in A**2 
C  emblid= EMBL/SWISSPROT identifier of the alignend protein
C  strid= if the 3-D structure of this protein is known, then strid 
C         (structure ID)is the Protein Data Bank identifier as taken
C         from the EMBL/SWISSPROT entry
C  iprotname= one line description of alignend protein
C  aliseq= sequential storage for the alignments
C  alipointer= points to the beginning of alignment X ( 1>= X <=nalign )
C  ifir,ilas= first and last position of the alignment in the test
C             protein
C  jfir,jlas= first and last position of the alignment in the alignend
C             protein
C  lali= length of the alignment excluding insertions and deletions
C  ngap= number of insertions and deletions in the alignment
C  lgap= total length of all insertions and deletions
C  lenseq= length of the entire sequence of the alignend protein
C  ide= percentage of residue identity of the alignment
C  var= sequence variability as derived from the nalign alignments
C  seqprof= relative frequency for each of the 20 amino acids
C  nocc= number of alignend sequences spanning this position (including
C        the test sequence
C  ndel= number of sequences with a deletion in the test protein at this
C        position
C  nins= number of sequences with an insertion in the test protein at
C        this position
C  entropy= entropy measure of sequence variability at this position
C  relent= relative entropy (entropy normalized to the range 0-100)
C  consweight= conservation weight
C=======================================================================
C      IMPLICIT        NONE
C      INTEGER         NBLOCKSIZE
C      PARAMETER      (NBLOCKSIZE=               70)

C      INTEGER         MAXRES,MAXAA
C      INTEGER         MAXALIGNS,MAXCORE
C      PARAMETER      (MAXAA=                    20)
C============================ import ==================================
C      CHARACTER*222   FILE_HSSP_LOC
      CHARACTER*(*)   FILE_HSSP_LOC
      INTEGER         IUNIT
      LOGICAL         LERROR
C attributes of sequence with known structure
C      CHARACTER*222   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
C      CHARACTER*(*)   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
      CHARACTER*132   PDBID,HEADER,COMPOUND,SOURCE,AUTHOR
      CHARACTER       PDBSEQ(NUMRESMAX),CHAINID(NUMRESMAX),
     +                SECSTR(NUMRESMAX)
C.......length*7
      CHARACTER*7     COLS(NUMRESMAX)
      CHARACTER*132   CHAINREMARK
C     br 2003-08-23: save space
C      CHARACTER       SHEETLABEL(NUMRESMAX)
      CHARACTER       SHEETLABEL(1)
      CHARACTER       SHEETLABEL_NULL
C     br 2003-08-23: end save space

      INTEGER         SEQLENGTH,NCHAIN,KCHAIN,NALIGN
      INTEGER         ACC(NUMRESMAX)

C     br 2003-08-23: save space
C      INTEGER         BP1(NUMRESMAX),BP2(NUMRESMAX),PDBNO(NUMRESMAX)
      INTEGER         BP1(1),BP2(1),PDBNO(1)
      INTEGER         PDBNO_NULL,BP1_NULL,BP2_NULL
C     br 2003-08-23: end save space

C attributes of alignend sequences
C      CHARACTER*222   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
C     +                ACCNUM(MAXALIGNS),IPROTNAME(MAXALIGNS),
C     +                EXCLUDEFLAG(MAXALIGNS)
C      CHARACTER*(*)   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
C     +                ACCNUM(MAXALIGNS),IPROTNAME(MAXALIGNS),
C     +                EXCLUDEFLAG(MAXALIGNS)
      CHARACTER*132   EMBLID(MAXALIGNS),STRID(MAXALIGNS),
     +                IPROTNAME(MAXALIGNS),ACCNUM(MAXALIGNS),
     +                EXCLUDEFLAG(MAXALIGNS)

      INTEGER         ALIPOINTER(MAXALIGNS),
     +                IFIR(MAXALIGNS),ILAS(MAXALIGNS),JFIR(MAXALIGNS),
     +                JLAS(MAXALIGNS),LALI(MAXALIGNS),NGAP(MAXALIGNS),
     +                LGAP(MAXALIGNS),LENSEQ(MAXALIGNS)
      REAL            IDE(MAXALIGNS),SIM(MAXALIGNS)
C     br 2003-08-23: save space
C      CHARACTER       ALISEQ(MAXCORE)
      CHARACTER       ALISEQ(1)
      CHARACTER       ALISEQ_NULL
C     br 2003-08-23: end save space

C attributes of profile
      INTEGER         SEQPROF(NUMRESMAX,MAXAA),
     +                NOCC(NUMRESMAX),NDEL(NUMRESMAX),NINS(NUMRESMAX)
      REAL            CONSWEIGHT(NUMRESMAX)
C     br 2003-08-23: save space
C      INTEGER         VAR(NUMRESMAX),RELENT(NUMRESMAX)
C      REAL            ENTROPY(NUMRESMAX)
      INTEGER         VAR(1),RELENT(1)
      INTEGER         VAR_NULL,RELENT_NULL
      REAL            ENTROPY(1)
      REAL            ENTROPY_NULL
C     br 2003-08-23: end save space

C.......
      LOGICAL         LCONSERV,LOLDVERSION
C=======================================================================
C internal
C      INTEGER         MAXALIGNS_LOC
C      PARAMETER      (MAXALIGNS_LOC=          3000)
C      PARAMETER      (MAXALIGNS_LOC=           MAXALIGNS)
C       character profileseq*(maxaa)
      CHARACTER       CTEMP*(NBLOCKSIZE),TEMPNAME*222
      CHARACTER*222   LINE
C     CHARACTER*20  HSSPRELEASE
      CHARACTER       CHAINSELECT
      LOGICAL         LCHAIN,LONG_ID
      INTEGER         ICHAINBEG,ICHAINEND,NALIGNORG,
     +                I,J,K,IPOS,ILEN,NRES,IRES,
     +                NBLOCK,IALIGN,IBLOCK,IALI,
     +                IBEG,IEND,IPOINTER(MAXALIGNS)

      INTEGER         MAXCORE_LOC,MAXALIGNS_LOC,MAXRES
      INTEGER         ITMP
      LOGICAL         LDEBUG_LOCAL

C order of amino acid symbols in the HSSP sequence profile block
C       profileseq='VLIMFWYGAPSTCHRKQEND'

      LONG_ID=.FALSE.
      LERROR=.FALSE.
C     br 2003-08-23: avoid warnings
      IBEG=0
      IEND=0
      J=   0

C     used to debug
      LDEBUG_LOCAL=.FALSE.
C      LDEBUG_LOCAL=.TRUE.
      
      NALIGN=0
      CHAINREMARK=' '

      DO I=1,MAXALIGNS
         IPOINTER(I)=0
      ENDDO
      LCHAIN=.FALSE.
      
      TEMPNAME(1:)=FILE_HSSP_LOC
      I=INDEX(TEMPNAME,'_!_')
      IF (I.NE.0) THEN
         TEMPNAME(1:)=FILE_HSSP_LOC(1:I-1)
         LCHAIN=.TRUE.
         READ(FILE_HSSP_LOC(I+3:),'(A1)')CHAINSELECT
         WRITE(6,'(T2,A,T10,A,T50,A)')'---',
     +        '--- RS_READHSSP: extract the chain: ',chainselect
      ENDIF

C      CALL RSLIB_OPEN_FILE(IUNIT,TEMPNAME,'OLD,READONLY',LERROR)
C      OPEN(IUNIT,FILE=TEMPNAME,STATUS='OLD',READONLY,ERR=99)

C      OPEN(IUNIT,FILE=TEMPNAME,STATUS='OLD',ERR=99)
      OPEN(IUNIT,FILE=TEMPNAME,ERR=99)
      IF (LERROR .EQV. .TRUE.) THEN
         WRITE(6,'(A)')'*** ERROR FOR RS_READHSSP: open problem'
         GOTO 99
      ENDIF

      READ(IUNIT,'(A)',ERR=99)LINE
C check if it is a HSSP-file and get the release number for format flags
      IF (LINE(1:4).NE.'HSSP') THEN
         WRITE(6,'(A)')'*** ERROR FOR RS_READHSSP: is not a HSSP-file'
         LERROR=.TRUE.
         RETURN
      ELSE
         I=INDEX(LINE,'VERSION')+7
C     HSSPRELEASE=LINE(I:)
         LOLDVERSION=.FALSE.
c       if (index(hssprelease,'0.9').ne.0)loldversion=.true.
      ENDIF
C read in PDBID etc.
      DO WHILE(LINE(1:6).NE.'PDBID')
         READ(IUNIT,'(A)',ERR=99)LINE
      ENDDO
      READ(LINE,'(11X,A)',ERR=99)PDBID
      DO WHILE(LINE(1:6).NE.'HEADER')
         READ(IUNIT,'(A)',ERR=99)LINE
         IF (LINE(1:23).EQ.'PARAMETER  LONG-ID :YES') THEN
            LONG_ID=.TRUE.
         ENDIF
      ENDDO
      READ(LINE ,'(11X,A)',ERR=99)HEADER
      READ(IUNIT,'(11X,A)',ERR=99)COMPOUND
      READ(IUNIT,'(11X,A)',ERR=99)SOURCE
      READ(IUNIT,'(11X,A)',ERR=99)AUTHOR
      READ(IUNIT,'(11X,I4)',ERR=99)SEQLENGTH
      READ(IUNIT,'(11X,I4)',ERR=99)NCHAIN
      KCHAIN=NCHAIN
      READ(IUNIT,'(A)',ERR=99)LINE
C      IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
      IF (INDEX(LINE,'KCHAIN').NE.0) THEN
         READ(LINE,'(11X,I4,A)',ERR=99)KCHAIN,CHAINREMARK
         READ(IUNIT,'(11X,I4)',ERR=99)NALIGNORG
      ELSE
         READ(LINE,'(11X,I4)',ERR=99)NALIGNORG
      ENDIF
C if HSSP-file contains no alignments return
      IF (NALIGNORG.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A)')'---',
     +          '--- HSSP-file contains no alignments ***'
         CLOSE(IUNIT)
         RETURN
      ENDIF

C      write(6,*)'xx before overflow, Nali=',nalignorg,' len=',
C     +     seqlength,' kchain=',kchain,' lchain=',lchain

C     parameter overflow handling
      IF (NALIGNORG.GT.MAXALIGNS) THEN
         WRITE(6,'(A)')'-*- HSSP-file contains too many alignments **'
         WRITE(6,'(A)')'-*-     INCREASE MAXALIGNS in phdParameter.f! '
         WRITE(6,'(A,I8,A,I8)')'-*-   is=',MAXALIGNS,' want>',NALIGNORG
         CLOSE(IUNIT)
         LERROR=.TRUE.
         RETURN
      ENDIF
      ITMP=SEQLENGTH+KCHAIN-1
      IF (ITMP.GT.NUMRESMAX) THEN
         WRITE(6,'(A)')'*** PDB-sequence in HSSP-file too long ***'
         WRITE(6,'(A)')'***  INCREASE NUMRESMAX  in phdParameter.f***'
         WRITE(6,'(A,I8,A,I8)')'-*-   is=',NUMRESMAX,' want>',ITMP
         CLOSE(IUNIT)
         LERROR=.TRUE.
         RETURN
      ENDIF

C number of sequence positions is number of residues + number of chains
C chain break is indicated by a '!'
      NRES=SEQLENGTH+KCHAIN-1
      ICHAINBEG=1
      ICHAINEND=NRES

      IF (LCHAIN .EQV. .TRUE.) THEN
C search for ALIGNMENT-block
         DO WHILE (LINE(1:13).NE.'## ALIGNMENTS')
            READ(IUNIT,'(A)',ERR=99)LINE
         ENDDO
         READ(IUNIT,'(A)',ERR=99)LINE
         ICHAINBEG=0
         ICHAINEND=0
C read till end ; some PDB-chains have DSSP-chain breaks !!
         DO I=1,NRES
            READ(IUNIT,'(7X,I4,1X,A1)',ERR=99)PDBNO(I),CHAINID(I)
            IF (CHAINID(I) .EQ. CHAINSELECT) THEN
               IF (ICHAINBEG .EQ. 0) ICHAINBEG=I
               ICHAINEND=I
            ENDIF
         ENDDO
         WRITE(6,'(T2,A,T10,I10,I10)')'---',
     +        ICHAINBEG,ICHAINEND
         REWIND(IUNIT)
      ENDIF
      SEQLENGTH=ICHAINEND-ICHAINBEG+1

C     search for the PROTEINS-block
      LINE=' '
      DO WHILE(LINE(1:11).NE.'## PROTEINS')
         READ(IUNIT,'(A)',ERR=99)LINE
C         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG PROT=',LINE
      ENDDO
      READ(IUNIT,'(A)',ERR=99)LINE
      LCONSERV=.FALSE.
      IF (INDEX(LINE,'%WSIM').NE.0) LCONSERV= .TRUE.

C     READ DATA ABOUT THE ALIGNMENTS
      IALIGN=1
      IF (LDEBUG_LOCAL .EQV. .TRUE.) 
     +     WRITE(6,'(A,I5,A,L)')'DBG NALI=',NALIGNORG,' long=',LONG_ID
      DO I=1,NALIGNORG
C         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,'(A,I5)')'DBG iali=',I

         IF (LONG_ID .EQV. .TRUE.) THEN
C           note: read format specified below (line labelled 101)
            READ(IUNIT,101,ERR=99)
     +           EXCLUDEFLAG(IALIGN),EMBLID(IALIGN)(1:),STRID(IALIGN),
     +           IDE(IALIGN),SIM(IALIGN),IFIR(IALIGN),ILAS(IALIGN),
     +           JFIR(IALIGN),JLAS(IALIGN),LALI(IALIGN),NGAP(IALIGN),
     +           LGAP(IALIGN),LENSEQ(IALIGN),ACCNUM(IALIGN),
     +           IPROTNAME(IALIGN)
         ELSE
C           note: read format specified below (line labelled 100)
            READ(IUNIT,100,ERR=99)
     +           EXCLUDEFLAG(IALIGN),EMBLID(IALIGN)(1:),STRID(IALIGN),
     +           IDE(IALIGN),SIM(IALIGN),IFIR(IALIGN),ILAS(IALIGN),
     +           JFIR(IALIGN),JLAS(IALIGN),LALI(IALIGN),NGAP(IALIGN),
     +           LGAP(IALIGN),LENSEQ(IALIGN),ACCNUM(IALIGN),
     +           IPROTNAME(IALIGN)
         END IF
         IF (LDEBUG_LOCAL .EQV. .TRUE.) THEN
            WRITE(6,'(A,I5,A,F5.2,A,A)')'DBG PROTali(',IALIGN,') ide=',
     +           IDE(IALIGN),
     +           ' name=',IPROTNAME(IALIGN)
         END IF
         IF (IFIR(IALIGN) .GE. ICHAINBEG .AND. 
     +        ILAS(IALIGN) .LE. ICHAINEND) THEN
            IPOINTER(I)=IALIGN
            IALIGN=IALIGN+1
         ENDIF
      ENDDO
 100  FORMAT(5X,A1,2X,A12,A5,2X,F5.2,1X,F5.2,8(1X,I4),2X,A10,1X,A)
 101  FORMAT(5X,A1,2X,A40,A5,2X,F5.2,1X,F5.2,8(1X,I4),2X,A10,1X,A)
      NALIGN=IALIGN-1
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP PROTEINS-block done'

C init pointer ; aliseq contains the alignments (amino acid symbols)
C stored in the following way ; '/' separates alignments
C alignment(x) is stored from:
C           aliseq(alipointer(x)) to aliseq(ilas(x)-ifir(x))
C  aliseq(1........46/48.........60/62....)
C         |           |             |
C         |           |             |
C         pointer     pointer       pointer 
C         ali 1       ali 2         ali 3
C init pointer
      IPOS=1
      DO I=1,NALIGN

C     br 2003-08-23 fast and slim
C         IF (IPOS.GE.MAXCORE) THEN
C            WRITE(6,'(A,I9,A)')
C     +           ' *** LERROR: INCREASE MAXCORE to >',IPOS,'***'
C            STOP
C         ENDIF
C     end fast and slim br 2003-08-23


         ALIPOINTER(I)=IPOS
         ILEN=ILAS(I)-IFIR(I)+1
         IPOS=IPOS+ILEN
C     br 2003-08-23 fast and slim
C         ALISEQ(IPOS)='/'
         ALISEQ_NULL='/'
C     end fast and slim br 2003-08-23
         IPOS=IPOS+1
      ENDDO
      ALIPOINTER(NALIGN+1)=IPOS+1

C number of ALIGNMENTS-blocks
      IF (MOD(FLOAT(NALIGNORG),FLOAT(NBLOCKSIZE)).EQ. 0.0) THEN
         NBLOCK=NALIGNORG/NBLOCKSIZE
      ELSE
         NBLOCK=NALIGNORG/NBLOCKSIZE+1
      ENDIF
C search for ALIGNMENT-block
      DO WHILE (LINE(1:13).NE.'## ALIGNMENTS')
         READ(IUNIT,'(A)',ERR=99)LINE
         IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG alinot',LINE
      ENDDO
      READ(IUNIT,'(A)',ERR=99)LINE
      IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
C loop over ALIGNMENTS-blocks
C ....read in pdbno, chainid, secstr etc.
      IALIGN=0
      IALI=0
      DO IBLOCK=1,NBLOCK
         IRES=1
         DO I=1,NRES

C     BR: 2003-08-23: save space
C            READ(IUNIT,200,ERR=99)
C     +           PDBNO(IRES),CHAINID(IRES),PDBSEQ(IRES),SECSTR(IRES),
C     +           COLS(IRES),BP1(IRES),BP2(IRES),SHEETLABEL(IRES),
C     +           ACC(IRES),NOCC(IRES),VAR(IRES),CTEMP 

            READ(IUNIT,200,ERR=99)
     +           PDBNO_NULL,CHAINID(IRES),PDBSEQ(IRES),SECSTR(IRES),
     +           COLS(IRES),BP1_NULL,BP2_NULL,SHEETLABEL_NULL,
     +           ACC(IRES),NOCC(IRES),VAR_NULL,CTEMP 
C     end save space br 2003-08-23

 200        FORMAT(7X,I4,2(1X,A1),2X,A1,1X,A7,2(I4),A1,I4,2(1X,I4),2X,A)
C.....fill up aliseq
C            IF (LDEBUG_LOCAL .EQV. .TRUE.) 
C     +           WRITE(6,*)'DBG IBLOCK=',IBLOCK, ' IRES=',I
            IF (I .GE. ICHAINBEG .AND. I .LE. ICHAINEND) THEN
               IRES=IRES+1


C     br 2003-08-23 fast and slim
C               IF (PDBSEQ(I) .NE. '!') THEN
C                 CALL STRPOS(CTEMP,IBEG,IEND)
C                  DO IPOS=MAX(IBEG,1),MIN(NBLOCKSIZE,IEND)
C                    IALI=IALIGN+IPOS
C                     IF (CTEMP(IPOS:IPOS) .NE. ' ') THEN
C                        J=ALIPOINTER(IPOINTER(IALI)) + 
C     +                       (I-IFIR(IPOINTER(IALI)))
C                        ALISEQ(J)=CTEMP(IPOS:IPOS)
C                    ENDIF
C                 ENDDO
C               ENDIF
C     end fast and slim br 2003-08-23

            ENDIF
         ENDDO
         IALIGN=IALIGN+NBLOCKSIZE
         DO K=1,2
            READ(IUNIT,'(A)',ERR=99)LINE
            IF (LDEBUG_LOCAL .EQV. .TRUE.) WRITE(6,*)'DBG ',LINE
         ENDDO
      ENDDO
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP ALIGNMENTS-block done'
C read in sequence profile, entropy etc.
      IRES=1
      DO I=1,NRES
C     BR 2003-08-23 save space
C         READ(IUNIT,300,ERR=99)(SEQPROF(IRES,K),K=1,MAXAA),
C     +        NOCC(IRES),NDEL(IRES),NINS(IRES),ENTROPY(IRES),
C     +        RELENT(IRES),CONSWEIGHT(IRES)
         READ(IUNIT,300,ERR=99)(SEQPROF(IRES,K),K=1,MAXAA),
     +        NOCC(IRES),NDEL(IRES),NINS(IRES),ENTROPY_NULL,
     +        RELENT_NULL,CONSWEIGHT(IRES)
C     end save space br 2003-08-23
         IF (I .GE. ICHAINBEG .AND. I .LE. ICHAINEND) THEN
            IRES=IRES+1
         ENDIF
      ENDDO
 300  FORMAT(12X,20(I4),1X,3(1X,I4),1X,F7.3,3X,I4,2X,F4.2)
      WRITE(6,'(T2,A,T10,A)')'---',
     +     '   RS_READHSSP PROFILE-block done'
      
      IF (LCHAIN .EQV. .TRUE.) THEN
         DO I=1,NALIGN
            IFIR(I)=IFIR(I)-ICHAINBEG+1
            ILAS(I)=ILAS(I)-ICHAINBEG+1
         ENDDO
      ENDIF

C check if next line (last line in a HSSP-file) contains a '//'
      READ(IUNIT,'(A)',ERR=99)LINE
      IF ((LINE(1:2).EQ.'//').OR.
     +     (LINE(1:13).EQ.'## INSERTION')) THEN
         WRITE(6,'(T2,A,T10,A,A50)')'---',
     +        '   RS_READHSSP ok(cut 50): ',FILE_HSSP_LOC(1:50)
         GOTO 999
      ELSE
         WRITE(6,'(T2,A,T10,A,A,A,A)')'***',
     +        'ERROR FOR RS_READHSSP: ',FILE_HSSP_LOC,' lastLine=',
     +        LINE
         GOTO 99
      ENDIF
 99   WRITE(6,'(A,A)')'**** ERROR FOR RS_READHSSP: READING: ',
     +     FILE_HSSP_LOC
      LERROR=.TRUE.
      NALIGN=0
      SEQLENGTH=0
 999  CLOSE(IUNIT)
      RETURN
      END
***** end of RS_READHSSP

***** ------------------------------------------------------------------
***** SUB RSLIB_OPEN_FILE
***** ------------------------------------------------------------------
C---- 
C---- NAME : RSLIB_OPEN_FILE
C---- ARG  : 
C---- DES  : opening file
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE RSLIB_OPEN_FILE(IUNIT,FILENAME,CSTRING,LERROR)

C---- global parameters
      INCLUDE 'phdParameter.f'

C      IMPLICIT NONE
C input
C CSTATUS: 'old' or 'new' or 'unknown'
C CACCESS:  'append' 'direct'
C FORM:     'formatted' or 'unformatted'
C IRECLEN: record length
C NOTE: after opening a "old" or "unknown" file (no direct acess): 
C       rewind the file, because some strange compilers put the file 
C       pointer at the end !
      CHARACTER*222   FILENAME,CSTRING
      INTEGER         IUNIT,IRECLEN
C output: lerror is true if open error
      LOGICAL         LERROR
C internal
      CHARACTER*200   TEMPSTRING,CTEMP
      CHARACTER*10    CNUMBER
      LOGICAL         LRECLEN
      LOGICAL         LNEW,LAPPEND,LUNKNOWN,
     +                LUNFORMATTED,LDIRECT,
     +                LOPENDONE,LSILENT
      INTEGER         LENGTH,I,J,K,IEND
C---- local function
C      INTEGER         FILEN_STRING

C---- br 2003-08-23: bullshit to avoid warning
      IEND=0
C init
      TEMPSTRING=' '
      LNEW=        .FALSE.
      LAPPEND=     .FALSE.
      LRECLEN=     .FALSE.
      LERROR=      .FALSE.
      LUNKNOWN=    .FALSE.
      LUNFORMATTED=.FALSE.
      LDIRECT=     .FALSE.
      LOPENDONE=   .FALSE.
      LSILENT=     .FALSE.
C      IRECLEN=137
      TEMPSTRING(1:)=CSTRING(1:)
C      IEND=FILEN_STRING(CSTRING)
C      TEMPSTRING(1:IEND)=CSTRING(1:IEND)
C      LENGTH=IEND
      TEMPSTRING(1:)=CSTRING(1:)
      CNUMBER='0123456789'
      LENGTH=LEN(TEMPSTRING)
      CALL RSLIB_LOWTOUP(TEMPSTRING,LENGTH)
      IF (INDEX(TEMPSTRING,'NEW').NE.0) THEN
         LNEW=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'UNKNOWN').NE.0) THEN
         LUNKNOWN=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'UNFORMATTED').NE.0) THEN
         LUNFORMATTED=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'DIRECT').NE.0) THEN
         LDIRECT=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'APPEND').NE.0) THEN
         LAPPEND=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'SILENT').NE.0) THEN
         LSILENT=.TRUE.
      ENDIF
      IF (INDEX(TEMPSTRING,'RECL=').NE.0) THEN
         CTEMP=' '
         K=INDEX(TEMPSTRING,'RECL=')+5
         CTEMP(1:)=TEMPSTRING(K:)
         CALL STRPOS(CTEMP,I,J)
C     COMMENTED OUT 22-08, searching for problem with ALPHA
C         J=I
C         DO WHILE(INDEX(CNUMBER,CTEMP(J:J)).NE.0 )
C            J=J+1
C         ENDDO
C         J=J-1
         READ(CTEMP(I:J),'(I6)')IRECLEN
         LRECLEN=.TRUE.
      ENDIF
      
C      IF (LNEW .EQV. .TRUE.) THEN
      IF (LNEW) THEN
         CALL RSLIB_DEL_OLDFILE(IUNIT,FILENAME)
      ENDIF
      IF (LNEW .AND. LUNFORMATTED .AND. LDIRECT ) THEN
C      IF ( (LNEW .EQV. .TRUE.) .AND. (LUNFORMATTED .EQV .TRUE.) .AND. 
C     +     (LDIRECT .EQV. .TRUE.) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='NEW',FORM='UNFORMATTED',
     +        ACCESS='DIRECT',RECL=IRECLEN,ERR=999)
         LOPENDONE=.TRUE.
      ELSE IF ((LNEW .EQV. .TRUE.) .AND. 
     +        (LUNFORMATTED .EQV. .TRUE.) ) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='NEW',FORM='UNFORMATTED',
     +        ERR=999)
         LOPENDONE=.TRUE.
      ELSE IF ((LNEW .EQV. .TRUE.) .AND. 
     +        (LUNFORMATTED .EQV. .FALSE.) .AND. 
     +        (LDIRECT .EQV. .TRUE.)) THEN
         OPEN(IUNIT,FILE=FILENAME,ACCESS='DIRECT',STATUS='NEW',
     +        FORM='FORMATTED',RECL=IRECLEN,ERR=999)
         LOPENDONE=.TRUE.
      ELSE IF ((LNEW.EQV. .FALSE.) .AND. 
     +        (LUNFORMATTED .EQV. .TRUE.) .AND. 
     +        (LDIRECT .EQV. .TRUE.)) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD',FORM='UNFORMATTED',
     +        ACCESS='DIRECT',RECL=IRECLEN,ERR=999)
         LOPENDONE=.TRUE.
      ELSE IF ((LNEW .EQV. .FALSE.) .AND.
     +        (LUNFORMATTED .EQV. .FALSE.) .AND. 
     +        (LDIRECT .EQV. .TRUE.)) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD',FORM='FORMATTED',
     +        ACCESS='DIRECT',RECL=IRECLEN,ERR=999)
         LOPENDONE=.TRUE.
      ELSE IF ((LNEW .EQV. .FALSE.) .AND. 
     +        (LUNFORMATTED .EQV. .TRUE.)) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD',FORM='UNFORMATTED',
     +        ERR=999)
         REWIND(IUNIT)
         LOPENDONE=.TRUE.
      ELSE IF (LNEW .EQV. .TRUE.) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='NEW',ERR=999)
         REWIND(IUNIT)
         LOPENDONE=.TRUE.
C     2003-10 hack xxbr problem with IBM
C      ELSE IF ((LUNKNOWN .EQV..TRUE.) .AND. 
C     +        (LAPPEND .EQV. .TRUE.)) THEN
C         OPEN(IUNIT,FILE=FILENAME,STATUS='UNKNOWN',ACCESS='APPEND',
C     +        ERR=999)
C         REWIND(IUNIT)
C         LOPENDONE=.TRUE.
C      ELSE IF ((LNEW .EQV. .FALSE.).AND. (LAPPEND .EQV. .TRUE.)) THEN
C         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD',ACCESS='APPEND',ERR=999)
C         LOPENDONE=.TRUE.
      ELSE IF (LNEW .EQV. .FALSE.) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD',ERR=999)
         REWIND(IUNIT)
         LOPENDONE=.TRUE.
      ELSE
         OPEN(IUNIT,FILE=FILENAME,STATUS='UNKNOWN',ERR=999)
         rewind(iunit)
         LOPENDONE=.TRUE.
      ENDIF
      IF (LOPENDONE .EQV. .FALSE.) THEN
         WRITE(6,*)' ERROR in RSLIB_OPEN_FILE: file not opened'
         WRITE(6,*)'  unknown specifier combination !'
         STOP
      ENDIF
      RETURN
 999  IF (LSILENT .EQV. .FALSE.) THEN
         WRITE(6,*)' ERROR rs_gethssp: open file error for file: '
         WRITE(6,*)FILENAME
         write(6,*)'lnew=',lnew,  ' lunformatted=',lunformatted,
     +         ' ldirect=',ldirect,   ' lunknown=',lunknown,
     +         ' lappend=',lappend
         write(6,*)' len=',LENGTH
      ENDIF
      LERROR=.TRUE.
      RETURN
      END
***** end of RSLIB_OPEN_FILE

***** ------------------------------------------------------------------
***** SUB RSLIB_DEL_OLDFILE
***** ------------------------------------------------------------------
C---- 
C---- NAME : RSLIB_DEL_OLDFILE
C---- ARG  : 
C---- DES  : deleting old file
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE RSLIB_DEL_OLDFILE(IUNIT,FILENAME)
      CHARACTER*222   FILENAME
      INTEGER         IUNIT
      LOGICAL         LEXIST,LOPEN
      
      INQUIRE(FILE=FILENAME,OPENED=LOPEN)
      IF (LOPEN .EQV. .TRUE.) THEN
         CLOSE(IUNIT)
      ENDIF
      INQUIRE(FILE=FILENAME,EXIST=LEXIST)
      IF (LEXIST .EQV. .TRUE.) THEN
         OPEN(IUNIT,FILE=FILENAME,STATUS='OLD')
         CLOSE(IUNIT,STATUS='DELETE')
      ENDIF
      END
***** end of RSLIB_DEL_OLDFILE

***** ------------------------------------------------------------------
***** SUB RSLIB_LOWTOUP
***** ------------------------------------------------------------------
C---- 
C---- NAME : RSLIB_LOWTOUP
C---- ARG  :  
C---- DES  : changing case of string.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE RSLIB_LOWTOUP(STRING,LENGTH)
C changed by RS (speed up)
      CHARACTER*222   STRING*(*)
      INTEGER         LENGTH
      DO I=1,LENGTH
         IF (STRING(I:I).GE.'a' .AND. STRING(I:I).LE.'z') THEN
            STRING(I:I)=CHAR( ICHAR(STRING(I:I))-32 )
         ENDIF
      ENDDO
      RETURN
      END
***** end of RSLIB_LOWTOUP

***** ------------------------------------------------------------------
***** SUB SCOUNT_HYDROPHOB
***** ------------------------------------------------------------------
C---- 
C---- NAME : SCOUNT_HYDROPHOB
C---- ARG  :  
C---- DES  : The number of adjacent hydrophibic residues is counted.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The number of adjacent hydrophibic residues is   *
*     --------        counted.                                         *
*     in variables:   KUNIT,NUMRES, RESNAME                            *
*     out variables:  Potential transmembrane regions: if more than    *
*     --------------  15: A, V, F, P, M, I, L adjacent                 *
*     called by:      DATAOT                                           *
*     SBRs calling:  from lib-comp.f:                                  *
*     --------------     FILENSTRING                                   *
*----------------------------------------------------------------------*
      SUBROUTINE SCOUNT_HYDROPHOB(KUNIT)

C---- include parameter files
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         HCOUNT,MUE,KUNIT,ICOUNT,ITER,
     +     BEGSEG(1:20),ENDSEG(1:20)
      LOGICAL         LHELP,LFOUND
******------------------------------*-----------------------------******

C---- defaults

      LFOUND=.FALSE.
      MUE=0
      ICOUNT=0
      DO WHILE (MUE.LT.NUMRES)
         HCOUNT=0
         LHELP=.TRUE.
         DO WHILE((MUE.LT.NUMRES).AND.LHELP)
            MUE=MUE+1
            IF ((RESNAME(MUE).EQ.'A').OR.(RESNAME(MUE).EQ.'V')
     +           .OR.(RESNAME(MUE).EQ.'F').OR.(RESNAME(MUE).EQ.'W')
     +           .OR.(RESNAME(MUE).EQ.'M').OR.(RESNAME(MUE).EQ.'Y')
     +          .OR.(RESNAME(MUE).EQ.'I').OR.(RESNAME(MUE).EQ.'L')) THEN
               HCOUNT=HCOUNT+1
            ELSE
               LHELP=.FALSE.
            END IF
         END DO
         IF (HCOUNT.GT.15) THEN
            ICOUNT=ICOUNT+1
            LFOUND=.TRUE.
            BEGSEG(ICOUNT)=MUE-HCOUNT
            ENDSEG(ICOUNT)=MUE
         END IF
      END DO
      IF (LFOUND .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
         WRITE(KUNIT,'(T2,A)')'---'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +        'PHD has been trained only on few membrane proteins.'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   'Consequently, the expected accuracy on these is very low.'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   'The sequence you sent, seems to have transmembrane segments'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   '(probably helical) at the residue positions:'
         WRITE(KUNIT,'(T2,A)')'---'
         DO ITER=1,ICOUNT
            WRITE(KUNIT,'(T2,A,T10,A,T20,I3,T25,A,T35,I5,T42,A,
     +           T48,I5)')'---','segment',ITER,
     +           'begin:',BEGSEG(ITER),'end:',ENDSEG(ITER)
         END DO
         WRITE(KUNIT,'(T2,A)')'---'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   'For these positions the prediction of secondary structure as'
         IF (ICOUNT.GT.1) THEN
            WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +           'given above might be misleading.  The segments are '
            WRITE(KUNIT,'(T2,A,T10,A)')'---','probably helices!'
         ELSE
            WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +           'given above is possibly misleading.  The segment is '
            WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +           'probably a helix!'
         END IF
         WRITE(KUNIT,'(T2,A)')'---'
         WRITE(KUNIT,'(T2,A,T10,A)')'---',
     +   '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
         WRITE(KUNIT,*)
         WRITE(KUNIT,*)
      END IF
C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      END 
***** end of SCOUNT_HYDROPHOB

***** ------------------------------------------------------------------
***** SUB SR_FILTER_EXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : SR_FILTER_EXP
C---- ARG  :  
C---- DES  : Computes nearest (3) neighbour averages for real input 
C---- DES  : vector.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:          computes nearest (3) neighbour averages for    *
*     --------          real input vector                              *
*     input:            OUTPUT(1:numout)                               *
*     output:           OUTFIL(1:numout)                               *
*----------------------------------------------------------------------*
      SUBROUTINE SR_FILTER_EXP(NUMOUTMAX,NUMOUT,OUTPUT,OUTFIL)

      INTEGER           ITEXP,NUMOUTMAX,NUMOUT
      REAL              OUTPUT(1:NUMOUTMAX),OUTFIL(1:NUMOUTMAX),SUM

************************************************************************

      DO ITEXP=1,NUMOUT
         IF (ITEXP.EQ.1) THEN
            OUTFIL(ITEXP)=(OUTPUT(1)+OUTPUT(2))/2.
         ELSEIF (ITEXP.EQ.NUMOUT) THEN
            OUTFIL(ITEXP)=(OUTPUT(NUMOUT-1)+OUTPUT(NUMOUT))/2.
         ELSE
            SUM=0
            DO IT=-1,1
               SUM=SUM+OUTPUT(ITEXP+IT)
            END DO
            OUTFIL(ITEXP)=SUM/3.
         END IF
      END DO

      END
***** end of SR_FILTER_EXP

***** ------------------------------------------------------------------
***** SUB TRANSAA
***** ------------------------------------------------------------------
C---- 
C---- NAME : TRANSAA
C---- ARG  : 
C---- DES  : The 20 amino acid one letter names (+unknown+solvent 
C---- DES  : +Asx +Glx) are 'transcribed' into a vector with 
C---- DES  : NCODEUNT components 0,1 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        the 20 amino acid one letter names (+unknown     *
*     --------        +solvent+Asx+Glx) are 'transcribed' into a vector*
*                     with NCODEUNT components 0,1                     *
*     output variable AACODE,AABIT,AAHEX                               *
*     --------------- and: SSCODE                                      *
*     external SBR:   SIBIT1  (HEXNUM,BITPOW,BITVEC)                   *
*             converts the integer hexadecimal number HEXNUM into an   *
*             integer vector BITVEC(1:(BITPOW+1)) containing 1/0 only, *
*             in such a way that:                                      *
*             sum/j=1,(BITPOW+1) [2**(BITVEC(j-1))]=HEXNUM             *
*             note: 2**BITPOW >= HEXNUM!                               *
*                   BITVEC(1) codes 2**0!                              *
*     procedure:      a one at position of CODEVECTOR   means amino    *
*     ---------       acid                                             *
*                             1    -->  V (VAL: Valine)                *
*                             2    -->  L (LEU: Leucine)               *
*                             3    -->  I (ILE: Isoleucine)            *
*                             4    -->  M (MET: Methiomine)            *
*                             5    -->  F (PHE: Phenylalamine)         *
*                             6    -->  W (TRP: Tryptophan)            *
*                             7    -->  Y (TYR: Tyrosine)              *
*                             8    -->  G (GLY: Glycine)               *
*                             9    -->  A (ALA: Alanine)               *
*                            10    -->  P (PRO: Proline)               *
*                            11    -->  S (SER: Serine)                *
*                            12    -->  T (THR: Threonine)             *
*                            13    -->  C (CYS: Cysteine)              *
*                            14    -->  H (HIS: Histidine)             *
*                            15    -->  R (ARG: Arginine)              *
*                            16    -->  K (LYS: Lysine)                *
*                            17    -->  Q (GLN: Glutamine)             *
*                            18    -->  E (GLU: Glutamate)             *
*                            19    -->  N (ASN: Aspargine)             *
*                            20    -->  D (ASP: Aspartate)             *
*                            21    -->  U (   : Solvent 'ulterior')    *
*                            22    -->  X (   : unknown)               *
*                            23    -->  B (Asx: D or N)                *
*                            24    -->  Z (Glx: Q or E)                *
*                     SSCODE(1)    -->  H (helix)     = H,I,G          *
*                     SSCODE(2)    -->  E (sheet)     = E, B           *
*                     SSCODE(3)    -->  C (no pred.)  = T, S, ' '      *
*                        if four classes:                              *
*                     SSCODE(3)    -->  T (turn)      = T              *
*                     SSCODE(4)    -->  C (no pred.)  = S, ' '         *
*                        if five classes:                              *
*                     SSCODE(3)    -->  T (turn)      = T              *
*                     SSCODE(4)    -->  S (bend)      = S              *
*----------------------------------------------------------------------*
      SUBROUTINE TRANSAA

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

      INTEGER         ITER1,ITER2,ITEXP

******------------------------------*-----------------------------******

C      AACODE(1)='A',(2)='R',(3)='N',(4)='D',(5)='C'.(6)='Q',(7)='E'
C      (8)='G',(9)='H',(10)='I',(11)='L',(12)='K',(13)='M',(14)='F'
C      (15)='P',(16)='S',(17)='T',(18)='W',(19)='Y',(20)='V',(21)='X'
C      (22)='U'

C---- succession according to HSSP scheme
      AACODE(1)='V'
      AACODE(2)='L'
      AACODE(3)='I'
      AACODE(4)='M'
      AACODE(5)='F'
      AACODE(6)='W'
      AACODE(7)='Y'
      AACODE(8)='G'
      AACODE(9)='A'
      AACODE(10)='P'
      AACODE(11)='S'
      AACODE(12)='T'
      AACODE(13)='C'
      AACODE(14)='H'
      AACODE(15)='R'
      AACODE(16)='K'
      AACODE(17)='Q'
      AACODE(18)='E'
      AACODE(19)='N'
      AACODE(20)='D'
      AACODE(21)='U'
      AACODE24='VLIMFWYGAPSTCHRKQENDUXBZ'
      AACODE_LOWC='abcdefghijklmnopqrstuvwxyz'

C----
C---- converting to binary vector (profile real)
C----
      DO ITER1=1,NBIOLOBJ
         DO ITER2=1,NBIOLOBJ
            IF (ITER1.EQ.ITER2) THEN
               AABIT(ITER1,ITER2)=1
            ELSE
               AABIT(ITER1,ITER2)=0
            END IF
         END DO
      END DO

C----
C---- encoding the secondary structure elements, i.e. the classes of 
C---- secondary structure to be distinguished
C---- 
      IF (NSECEL.EQ.3) THEN
         SSCODE(1)='H'
         SSCODE(2)='E'
         SSCODE(3)=' '
      ELSEIF (NSECEL.EQ.4) THEN
         SSCODE(1)='H'
         SSCODE(2)='E'
         SSCODE(3)='T'
         SSCODE(4)=' '
      ELSEIF (NSECEL.EQ.2) THEN
         SSCODE(1)='H'
         SSCODE(2)=' '
      ELSEIF (NSECEL.EQ.5) THEN
         SSCODE(1)='H'
         SSCODE(2)='E'
         SSCODE(3)='T'
         SSCODE(4)='S'
         SSCODE(5)=' '
      ELSEIF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
         WRITE(6,*)'    NSECEL not properly chosen: 3<=NSECEL<=5'
         WRITE(6,*)'    stopped in TRANSAA, stop 2'
         STOP
      END IF

C----
C---- coding exposure state names
C----
      DO ITEXP=1,10
C        version real values (0-1)
C         IF (THREXP10ST(ITEXP).LT.THREXP3ST(2)) THEN
C            EXPCODE(ITEXP)='b'
C         ELSEIF (THREXP10ST(ITEXP).GE.THREXP3ST(3)) THEN
C            EXPCODE(ITEXP)='e'
C         ELSE
C            EXPCODE(ITEXP)='i'
C         END IF

C        version integer values (0-100)
         IF (THREXP10STI(ITEXP).LT.THREXP3STI(2)) THEN
            EXPCODE(ITEXP)='b'
         ELSEIF (THREXP10STI(ITEXP).GE.THREXP3STI(3)) THEN
            EXPCODE(ITEXP)='e'
         ELSE
            EXPCODE(ITEXP)='i'
         END IF
      END DO

      END
***** end of TRANSAA

***** ------------------------------------------------------------------
***** SUB TRIGGER
***** ------------------------------------------------------------------
C---- 
C---- NAME : TRIGGER
C---- ARG  : 
C---- DES  : Executes the network trigger function input --> 
C---- DES  : output,for a particular architecture ACTCHI 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        This SBR executes the network trigger function *
*     --------        input --> output,for a particular architecture   *
*                     ACTCHI.                                          *
*     const. passed:  ACTCHI, ACTFILE, CONTROLCHAR, NUMRES, NSECEL     *
*     var. passed:    in: INPUT, out: OUTPUT                           *
*     var. read:      NUMIN,NUMHID,NUMOUT,JUNCTION1ST,JUNCTION2ND      *
*     ext. SBR:       SFILEOPEN (lib-unix)                             *
*     ext. function   EXP (should be available)                        *
*     called by:      SBR NETWORK                                      *
*     calling:        SBR READARCH                                     *
*----------------------------------------------------------------------*
      SUBROUTINE TRIGGER

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITIN,ITHID,ITOUT
      REAL            INVABW,NEGINVABW,INTERHID

******------------------------------*-----------------------------******
*     MUE,ITIN,ITHID,ITOUT: iteration variables                        *
*     INPUT (NUMHID,NUMIN+) input matrix                               *
*                     is used                                          *
*     INVABW          =1/ABW for avoiding too high quantities          *
*     JUNCTION1ST(j,i) connection of the first layer, i.e. between     *
*                     the input unit j and the  hidden one i           *
*     JUNCTION2ND(j,i) connection of the second layer, i.e. between    *
*                     the hidden unit j and the output one i           *
*     LOCFIELD        the local fields (abbr.: h) are defined by:      *
*                     h(I,MUE )= sum(k,{J(k,i)*s(k,mue))+b(i,mue)      *
*     NEGINVABW       =-INVABW                                         *
*     NUMHID          number of hidden units: 2**NUMHID > NUMOUT       *
*     NUMIN           number of input units = (WIDTH + LENGTH)*NCODEUNT*
*     NUMOUT          number of output u.   = WIDTH * LENGTH * BITACC  *
*     OUTPUT(NUMOUT,NUMHID+) output matrix                             *
******------------------------------*-----------------------------******

C---- cutoffs
      INVABW=1./ABW
      NEGINVABW=(-1.)*INVABW
C--------------------------------------------------
C---- loop over all residues                  -----
C--------------------------------------------------
      DO MUE=1,NUMRES
C------- compute local fields for hidden unit
         DO ITHID=1,NUMHID
            LOCFIELD1(ITHID)=0.
            DO ITIN=1,NUMIN
               IF (INPUT(ITIN,MUE).NE.0.) THEN
                  LOCFIELD1(ITHID)=LOCFIELD1(ITHID)
     +                 +JUNCTION1ST(ITIN,ITHID)*INPUT(ITIN,MUE)
               END IF
            END DO
C---------- threshold units
C            DO ITIN=(NUMIN+1),(NUMIN+NSECEL)
            DO ITIN=(NUMIN+1),(NUMIN+NUMOUT)
               LOCFIELD1(ITHID)=LOCFIELD1(ITHID)+JUNCTION1ST(ITIN,ITHID)
            END DO
         END DO

C------- compute output
         DO ITOUT=1,NUMOUT
            LOCFIELD2(ITOUT)=0.

C---------- PARALLEL
C----------         
            DO ITHID=1,NUMHID

C------------- VECTOR
               IF (ABS(LOCFIELD1(ITHID)).LT.INVABW) THEN
                  INTERHID=(1./(1.+EXP (- LOCFIELD1(ITHID) ) ))
                  LOCFIELD2(ITOUT)=LOCFIELD2(ITOUT)+
     +                 (JUNCTION2ND(ITHID,ITOUT)*INTERHID)
               ELSEIF (LOCFIELD1(ITHID).LE.NEGINVABW) THEN
                  INTERHID=0.
               ELSEIF (LOCFIELD1(ITHID).GE.INVABW) THEN
                  LOCFIELD2(ITOUT)=LOCFIELD2(ITOUT)
     +                 +JUNCTION2ND(ITHID,ITOUT)
                  INTERHID=1.
               ELSE
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'ERROR in SBR TRIGGER: wrong assignment for '
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'intermediate output!! Stopped at 12-10-92-1'
                  STOP
               END IF
C------------- END VECTOR

            END DO
C----------          
C---------- END PARALLEL

C---------- threshold units
C            DO ITHID=(NUMHID+1),(NUMHID+NSECEL)
            DO ITHID=(NUMHID+1),(NUMHID+NUMOUT)
               LOCFIELD2(ITOUT)=LOCFIELD2(ITOUT)
     +              +JUNCTION2ND(ITHID,ITOUT)
            END DO
C---------- output
            IF ((ABS(LOCFIELD2(ITOUT)).LT.INVABW).AND.
     +           (LOCFIELD2(ITOUT).GT.NEGINVABW)) THEN
               OUTPUT(ITOUT,MUE)=1./(1.+ EXP (- LOCFIELD2(ITOUT) ))
            ELSEIF (LOCFIELD2(ITOUT).LE.NEGINVABW) THEN
               OUTPUT(ITOUT,MUE)=0.
            ELSEIF (LOCFIELD2(ITOUT).GE.INVABW) THEN
               OUTPUT(ITOUT,MUE)=1.
            ELSE
               WRITE(6,'(T2,A,T10,A)')'***',
     +              'ERROR in SBR TRIGGER: wrong assignment for output!'
               WRITE(6,'(T2,A,T10,A)')'***',
     +              'stopped at 12-10-92-2'
               STOP
            END IF

         END DO
C------- end of loop over output units

      END DO
C--------------------------------------------------
C---- end of loop over all residues            ----
C--------------------------------------------------
      END
***** end of TRIGGER

***** ------------------------------------------------------------------
***** SUB TXTRES
***** ------------------------------------------------------------------
C---- 
C---- NAME : TXTRES
C---- ARG  : 
C---- DES  : The results of the prediction are written. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:          The results of the prediction are written into *
*     --------          the output.                                    *
*     control:          MODESECSTRON                                   *
*     input:            NUMRES, NUMNETJURY                             *
*     ------            RESNAME, OUTBINCHAR, PROTNAME                  *
*     external subroutines: TIME,DATE                                  *
*----------------------------------------------------------------------*
      SUBROUTINE TXTRES

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITER
      LOGICAL         LHELP

******------------------------------*-----------------------------******

      WRITE(6,*)
      WRITE(6,'(T20,A)')'      **************************************'
      WRITE(6,'(T20,A)')'      *****  Data of program secstron  *****'
      WRITE(6,'(T20,A)')'      **************************************'
      WRITE(6,*)
      WRITE(6,*)
      WRITE(6,*)
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,80A1)')('-',ITER=1,80)
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,A)')'---'
C---- call SCFDATE from external lib-syst-unix.f
      LHELP=.TRUE.
C      CALL SCFDATE(2,LHELP,STARTDATE)
C---- CALL SRDTIME from external lib-syst-unix.f
c$$$      CALL SRDTIME(2,LHELP)
C---- writing header
C     =================
C      CALL WRTPHDHEADER(6)
C     =================
C---- writing content of secondary structure and paper to referee
C     ==================
      CALL WRTCONTENT(6)
C     ==================
      WRITE(6,*)
      WRITE(6,*)
      WRITE(6,'(T10,A,T30,A50)')'Prediction for:',FILE_HSSP
      WRITE(6,'(T10,A)')'---------------'
      WRITE(6,*)
      WRITE(6,*)
      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
C        ===============
         CALL WRTPRED(6)
C        ===============
      ELSEIF (MODESECSTRON.EQ.'EXPOSURE') THEN
C        ==============
         CALL WRTEXP(6)
C        ==============
      END IF
      WRITE(6,*)
      WRITE(6,*)
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,A,T10,A)')'---',
     +     'Note: prediction has been written into file:'
      WRITE(6,'(T2,A,T10,A50)')'---',FILEPRED
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,70A1)')('-',ITER=1,70)
      WRITE(6,'(T2,A)')'---'
      WRITE(6,'(T2,A)')'---'

      END
***** end of TXTRES

***** ------------------------------------------------------------------
***** SUB WINDIR
***** ------------------------------------------------------------------
C---- 
C---- NAME : WINDIR
C---- ARG  : 
C---- DES  : The input and the desired output for every case are 
C---- DES  : read and written into INPUT/DESIRED. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The input and the desired output for every case  *
*     --------        are read and written into INPUT/DESIRED.         *
*     control param.: MODECODEIN, FLAGTRAIN, FLAGTESTNN,               *
*     --------------- NUMPROTTRAIN/TEST, NUMRESTRAIN/TEST              *
*                     TRAIN/TESTINGSET                                 *
*     input parameter:CONTROLCHAR,                                     *
*     output param.:                                                   *
*     input variables:RESPROF, POINTBEG, TRANSLISTTRAIN/TEST           *
*     output variab.: INPUT,DESIRED,TSTINPUT,TSTCMPRSN                 *
*     for SBR WINCUT: PROFMAX, PROFINTERV, CASCINTERV, CASCACC         *
*     called from:    NETWORK                                          *
*     SBRs calling:  WINASS, WININCOMPOSITION, WININCOMPOSITION_ALL,  *
*     --------------  WINDIRINI, WININLENGTH, WININDISTCAPS            *
*     hierarchy:      -> WINDIRINI                                     *
*                     -> WINASS    (assign INPUT, DESIRED, + test)     *
*                       -> WININDIR                                    *
*                      a) -> WININAA (INPUT=CODEVECTOR for Seq.-Str.)  *
*                         -> CODEIN                                    *
*                      b) -> WININSTR(INPUT=CODEVECTOR for Str.-Str.)  *
*                         -> CODESTR                                   *
*                     -> WININCOMPOSTION_ALL                           *
*                     -> WININCOMPOSTION                               *
*                     -> WININLENGTH                                   *
*                     -> WININDISTCAPS                                 *
*     procedure:      For each residue R0, resp. for each object being *
*     ---------       composed by LENGTHOBJ, NUMNEIGH neighbouring amino
*                     acids (resp. objects) are written to both sides  *
*                     thus forming a string of 2*NUMNEIGH + 1 residues *
*                     with residue R0 being in the centre. Each of     *
*                     these residues is encoded by a binary vector of  *
*                     dimension NCODEUNT.  This final string of length *
*                     (2*NUMNEIGH+1)*NCODEUNT represents the INPUT vec-*
*                     tor for case R0.  The corresponding output vector*
*                     DESIRED simply contains the properties currently *
*                     to be predicted, e.g. the secondary structure (see
*                     commentary in parameter file for PREDICTIONTYPE) *
*----------------------------------------------------------------------*
      SUBROUTINE WINDIR

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local parameters
      LOGICAL         LCHECK_WRITE
      PARAMETER       (LCHECK_WRITE=.FALSE.)
C      PARAMETER       (LCHECK_WRITE=.TRUE.)

C---- local variables
      INTEGER         MUE,IT

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults                                -----
C--------------------------------------------------

C     ==============
      CALL WINDIRINI
C     ==============
C-----------------------------------------------------------------------
C---- assign INPUT/DESIRED for TESTing set------------------------------
C-----------------------------------------------------------------------

C---- loop over all residues for particular protein
      DO MUE=1,NUMRES
         ACTPOS=MUE

C------- case discrimination according to actual mue, i.e. the
C------- position of the actual central object within the protein

C------- NUMNEIGH < central position <= NUMRES-NUMNEIGH
         IF ((NUMNEIGH.LT.MUE).AND.(MUE.LE.(NUMRES-NUMNEIGH))) THEN
            CASEDISCR='MIDDLE'
            ACTSOLVADDBEG=0
            ACTSOLVADDEND=0
            ACTSOLVADDEND2=0
C------- NUMNEIGH >= central position <= NUMRES-NUMNEIGH
         ELSEIF ((MUE.LE.NUMNEIGH).AND.(MUE.LE.(NUMRES-NUMNEIGH))) THEN
            CASEDISCR='ADDBEG'
            ACTSOLVADDBEG=NUMNEIGH-MUE+1
            ACTSOLVADDEND=0
            ACTSOLVADDEND2=0
C------- NUMRES-NUMNEIGH < central position <= NUMRES 
         ELSEIF ((NUMNEIGH.LT.MUE).AND.(MUE.GT.(NUMRES-NUMNEIGH)).AND.
     +           (MUE.LE.NUMRES)) THEN
            CASEDISCR='ADDEND'
            ACTSOLVADDBEG=0
            ACTSOLVADDEND=NUMNEIGH-(NUMRES-MUE)
            ACTSOLVADDEND2=0
C------- NUMRES < central position <= NUMRES
         ELSEIF ((NUMNEIGH.LT.MUE).AND.(MUE.GT.NUMRES)) THEN
            CASEDISCR='ADDEND2'
            ACTSOLVADDBEG=0
            ACTSOLVADDEND=NUMNEIGH
            ACTSOLVADDEND2=-(NUMRES-MUE)
C------- NUMNEIGH >= central position > NUMRES-NUMNEIGH <= NUMRES 
         ELSEIF ((NUMNEIGH.GE.MUE).AND.(MUE.GT.(NUMRES-NUMNEIGH)).AND.
     +           (MUE.LE.NUMRES)) THEN
            CASEDISCR='ADDBOTH'
            ACTSOLVADDBEG=NUMNEIGH-MUE+1
            ACTSOLVADDEND=NUMNEIGH-(NUMRES-MUE)
            ACTSOLVADDEND2=0
C------- NUMNEIGH >= central position > NUMRES
         ELSEIF ((NUMNEIGH.GE.MUE).AND.(MUE.GT.NUMRES)) THEN
            CASEDISCR='ADDBOTH2'
            ACTSOLVADDBEG=NUMNEIGH-MUE+1
            ACTSOLVADDEND=NUMNEIGH
            ACTSOLVADDEND2=-(NUMRES-MUE)
         ELSE
            WRITE(6,*)'    fault in WINCUT, case discrimination'
            WRITE(6,*)'     name: ',PROTNAME(1)
            WRITE(6,*)'length=',NUMRES,'  mue=',MUE
            STOP
         END IF
C------- end of case discrimination

C------- assign INPUT/DESIRED
         IF (CASEDISCR.NE.'REJECT') THEN
C           ===========
            CALL WINASS
C           ===========
         ELSE
            WRITE(6,*)'   Fault for casediscrimination, WINCUT'
            WRITE(6,*)'   training set, stop 1.2'
            STOP
         END IF
C--------------------------------------------------
C------- add additional unit for amino acid -------
C------- composition                        -------
C--------------------------------------------------

         IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN

C---------- for N-term: compute composition of whole protein
            IF (MUE.EQ.1) THEN
C              =========================
               CALL WININCOMPOSITION_ALL
C              =========================
            END IF

C---------- compute composition for current window
C           =====================
            CALL WININCOMPOSITION
C           =====================

         END IF
C--------------------------------------------------
C------- add additional unit for protein length
C--------------------------------------------------

         IF (LOGI_LENGTH .EQV. .TRUE.) THEN
C           ================
            CALL WININLENGTH
C           ================
         END IF
C--------------------------------------------------
C------- add additional units for distance to caps
C--------------------------------------------------
         IF (LOGI_DISTCAPS .EQV. .TRUE.) THEN
C           ==================
            CALL WININDISTCAPS
C           ==================
         END IF
C--------------------------------------------------
C------- control write out! xx
C--------------------------------------------------
         IF (LCHECK_WRITE .EQV. .TRUE.) THEN
            IF (MUE.GE.17) THEN
               WRITE(6,*)MUE
               WRITE(6,'(24I3)')
     +              (int(100*INPUT(IT,MUE)),IT=1,NUMIN)
            END IF
            IF (MUE.EQ.27) THEN
               WRITE(6,*)'numin=',numin,'  out=',numout
               STOP
            END IF
         END IF
      END DO
C---- end of loop over mue (residues of protein nue)

C-----------------------------------------------------------------------
C---- end of testing set------------------------------------------------
C-----------------------------------------------------------------------
      END
***** end of WINDIR

***** ------------------------------------------------------------------
***** SUB WINDIRINI
***** ------------------------------------------------------------------
C---- 
C---- NAME : WINDIRINI
C---- ARG  : 
C---- DES  : initialises some of the control variables used throughout 
C---- DES  : programs called by WINDIR. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        initialises some of the control variables used   *
*     --------        throughout programs called by WINDIR             *
*     const. passed:  CONTROLCHAR, MODESECSTRON, MODEASSCAS, MODEASSSTR*
*     var. passed:    out: LOGI_REALINPUT,                             *
*     ------------         LOGI_COMPOSITION, *_LENGTH, *_DISTCAPS      *
*                          LOGI_CONS, *_INDEL,                         *
*                          ACTACC, ACTINTERVALL,                       *
*                          BEGUNITS_COMPOSITION, *_LENGTH, *_DISTCAPS  *
*                          PROFMAX, PROFINTERV, CASCINTERV, ACTNALIGN  *
*     var. read:      PROFACC, CASCACC, NBIOLOBJ, NUMNEIGH, NCODEUNT,  *
*     ----------      NUMRES, RESPROF(mue), NUMNALIGN(nue=1)           *
*                     NUNITS_LENGTH, *_DISTCAPS                        *
*     called by:      WINDIR                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WINDIRINI

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ADDGLOBAL,MUE,ITPROF

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- is input real ?
C--------------------------------------------------

      IF ( ((CONTROLCHAR.EQ.'FST').AND.
     +     (MODEASSCAS(ACTCHI).EQ.'PROFILE-BIN')) .OR.
     +     ((CONTROLCHAR.EQ.'SND') .AND. 
     +      (MODEASSSTR(ACTCHI)(1:5).EQ.'REAL-')) ) THEN
         LOGI_REALINPUT=.FALSE.
      ELSE
         LOGI_REALINPUT=.TRUE.
      END IF

C---- security check
      IF ( (MODESECSTRON.EQ.'EXPOSURE').AND.(CONTROLCHAR.EQ.'SND')) THEN
         WRITE(6,*)'***  WINDIRINI: check whether or not it is correct'
         WRITE(6,*)'***      to assume by default that 2nd archis are'
         WRITE(6,*)'***      not coming with real input!'
         STOP
      END IF
C--------------------------------------------------
C---- any global information ?
C--------------------------------------------------

      LOGI_CONS=.FALSE.
      LOGI_INDEL=.FALSE.
      LOGI_COMPOSITION=.FALSE.
      LOGI_LENGTH=.FALSE.
      LOGI_DISTCAPS=.FALSE.
      
      IF (CONTROLCHAR.EQ.'FST') THEN
         IF ( (MODEASSCAS(ACTCHI).EQ.'PROFILE-REAL').OR.
     +        (MODEASSCAS(ACTCHI).EQ.'ALPHABET') ) THEN
            LOGI_CONS=.FALSE.
         ELSEIF ( (MODEASSCAS(ACTCHI).EQ.'PROFILE-REAL-CONS').OR.
     +        (MODEASSCAS(ACTCHI).EQ.'P-REAL-C') ) THEN
            LOGI_CONS=.TRUE.
         ELSEIF (MODEASSCAS(ACTCHI).EQ.'P-REAL-CONS-INDEL') THEN
            LOGI_CONS=.TRUE.
            LOGI_INDEL=.TRUE.
         ELSEIF ( (MODEASSCAS(ACTCHI).EQ.'P-REAL-CONS-INCOM').OR.
     +            (MODEASSCAS(ACTCHI).EQ.'P-REAL-CIC') ) THEN
            LOGI_CONS=.TRUE.
            LOGI_INDEL=.TRUE.
            LOGI_COMPOSITION=.TRUE.
         ELSEIF ( (MODEASSCAS(ACTCHI).EQ.'P-REAL-CONS-COMPO').OR.
     +            (MODEASSCAS(ACTCHI).EQ.'P-REAL-CC') ) THEN
            LOGI_CONS=.TRUE.
            LOGI_COMPOSITION=.TRUE.
         ELSEIF  (MODEASSCAS(ACTCHI).EQ.'P-REAL-CCLD') THEN
            LOGI_CONS=.TRUE.
            LOGI_COMPOSITION=.TRUE.
            LOGI_LENGTH=.TRUE.
            LOGI_DISTCAPS=.TRUE.
         ELSEIF  (MODEASSCAS(ACTCHI).EQ.'P-REAL-CICLD') THEN
            LOGI_CONS=.TRUE.
            LOGI_INDEL=.TRUE.
            LOGI_COMPOSITION=.TRUE.
            LOGI_LENGTH=.TRUE.
            LOGI_DISTCAPS=.TRUE.
         ELSE
            WRITE(6,'(T2,A,T10,A)')'***','WINDIRINI: trouble '//
     +           'initialising LOGI_* !'
            WRITE(6,'(T2,A,T10,A)')'***','stopped 6-3-94b'
            STOP
         END IF
      ELSEIF (CONTROLCHAR.EQ.'SND') THEN
         IF ( (MODEASSSTR(ACTCHI).EQ.'REAL-CONS').OR.
     +        (MODEASSSTR(ACTCHI).EQ.'REAL-CONS93') ) THEN
            LOGI_CONS=.TRUE.
         ELSEIF (MODEASSSTR(ACTCHI).EQ.'REAL-CONS-INDEL') THEN
            LOGI_CONS=.TRUE.
            LOGI_INDEL=.TRUE.
         ELSEIF ( (MODEASSSTR(ACTCHI).EQ.'REAL-CONS-COMPO').OR.
     +        (MODEASSSTR(ACTCHI).EQ.'REAL-CC') ) THEN
            LOGI_CONS=.TRUE.
            LOGI_COMPOSITION=.TRUE.
         ELSEIF (MODEASSSTR(ACTCHI).EQ.'REAL-CCLD') THEN
            LOGI_CONS=.TRUE.
            LOGI_COMPOSITION=.TRUE.
            LOGI_LENGTH=.TRUE.
            LOGI_DISTCAPS=.TRUE.
         ELSEIF (MODEASSSTR(ACTCHI).EQ.'RR-CCLD') THEN
            LOGI_CONS=.TRUE.
            LOGI_COMPOSITION=.TRUE.
            LOGI_LENGTH=.TRUE.
            LOGI_DISTCAPS=.TRUE.
         ELSEIF (MODEASSSTR(ACTCHI).NE.'REAL-OCT') THEN
            WRITE(6,'(T2,A,T10,A)')'***','WINDIRINI: trouble '//
     +           'initialising LOGI_* !'
            WRITE(6,'(T2,A,T10,A,T20,A1,A,A1)')'***',
     +           'MODEASSSTR=','|',MODEASSSTR(ACTCHI),'|'
            WRITE(6,'(T2,A,T10,A)')'***','stopped 6-3-94c'
            STOP
         END IF
      ELSE
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WINDIRINI '//
     +        'CONTROLCHAR (or MODEASSCAS/STR) wrong ! 6-2-94a'
         STOP
      END IF
C--------------------------------------------------
C---- intervalls and accuracy for binary input
C--------------------------------------------------

      IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
         IF (CONTROLCHAR.EQ.'FST') THEN
            ACTACC=PROFACC
         ELSE
            ACTACC=CASCACC
         END IF
         ACTINTERVALL=1/MAX(1.,REAL(ACTACC))
      END IF
C--------------------------------------------------
C---- begin of global units
C--------------------------------------------------

      ADDGLOBAL=0
      IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
         BEGUNITS_COMPOSITION=(2*NUMNEIGH+1)*NCODEUNT+1
         IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
            ADDGLOBAL=NBIOLOBJ*(ACTACC-1)
         ELSE
            ADDGLOBAL=NBIOLOBJ
         END IF
      END IF
      IF (LOGI_LENGTH .EQV. .TRUE.) THEN
         BEGUNITS_LENGTH=(2*NUMNEIGH+1)*NCODEUNT+ADDGLOBAL+1
         IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
            ADDGLOBAL=ADDGLOBAL+(NUNITS_LENGTH*(ACTACC-1))
         ELSE
            ADDGLOBAL=ADDGLOBAL+NUNITS_LENGTH
         END IF
      END IF
      IF (LOGI_DISTCAPS .EQV. .TRUE.) THEN
         BEGUNITS_DISTCAPS=(2*NUMNEIGH+1)*NCODEUNT+ADDGLOBAL+1
         IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
            ADDGLOBAL=ADDGLOBAL+(2*NUNITS_DISTCAPS*(ACTACC-1))
     +           
         ELSE
            ADDGLOBAL=ADDGLOBAL+2*NUNITS_DISTCAPS
         END IF
      END IF
C--------------------------------------------------
C---- for the input being a profile: find out -----
C---- maximum                                 -----
C--------------------------------------------------

      PROFMAX=0
      DO MUE=1,NUMRES
         DO ITPROF=1,(NBIOLOBJ-1)
            IF (RESPROF(MUE,ITPROF).GT.PROFMAX) THEN
               PROFMAX=RESPROF(MUE,ITPROF)
            END IF
         END DO
      END DO
      PROFINTERV=REAL(PROFMAX)/REAL(PROFACC-2)
      CASCINTERV=100./REAL(CASCACC-2)
      ACTNALIGN=NUMNALIGN(1)
      END
***** end of WINDIRINI

***** ------------------------------------------------------------------
***** SUB WINASS
***** ------------------------------------------------------------------
C---- 
C---- NAME : WINASS
C---- ARG  : 
C---- DES  : For a given window (WINCUT) the vectors for the input 
C---- DES  : and desired output are assigned. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        For a given window (WINCUT) the vectors for the  *
*     --------        input and desired output are assigned.           *
*     control param.: PREDMODE, LENGTHOBJ,                             *
*     --------------- NSECEL, NCODEUNT, NUMNEIGH                       *
*     input parameter:ACTPOS, ACTPOS                                   *
*     ----------------ACTSOLVADDBEG, ACTSOLVADDEND2, ACTSOLVADDEND     *
*     output param.:  ACTSTART, ACTRESIDUE, ACTCONSERV,                *
*     --------------  ACTCONSWEIGHT, ACTPOS, ACTREGION, ACTITER,       *
*     input variables:RESNAME, RESVAR, RESCONSWEIGHT,                  *
*     output variab.: INPUT, DESIRED, TSTCMPRSN                        *
*     called from:    WINCUT                                           *
*     SBRs calling:  WININDIR, WINOUT                                 *
*     procedure:      see SBR WINDIR                                   *
*----------------------------------------------------------------------*
      SUBROUTINE WINASS

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITER1,ITAA
      LOGICAL         LHELP

******------------------------------*-----------------------------******
*     MUE,IT*         serve as iteration variables                     *
*     IHELP           intermediately required variable                 *
*     ACTCASE0        used to memorize the actual case if entering into*
*                     the loop over the objects                        *
*     ACTPOS0         same as ACTCASE0 for the position                *
******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults
C--------------------------------------------------

C---- set zero
C     -----------
      CALL SISTZ1(ACTCOMPOSITION,NBIOLOBJMAX)
C     -----------
C--------------------------------------------------
C---- add solvents at begin                   -----
C--------------------------------------------------

      DO ITER1=1,ACTSOLVADDBEG
         ACTRESIDUE='U'
C------- fork to distinguish between first and second net
         ACTREGION='ADDBEG'
         ACTITER=ITER1
         ACTSTART=(ITER1-1)*NCODEUNT
C        =============
         CALL WININDIR
C        =============
      END DO

C---- sum up for input of amino acid composition
      IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
         ACTCOMPOSITION(NBIOLOBJ)=ACTSOLVADDBEG
      END IF
C--------------------------------------------------
C---- read NUMNEIGH amino acids before central one-
C--------------------------------------------------

      DO ITER1=(ACTSOLVADDBEG+1),NUMNEIGH
         ACTRESIDUE=RESNAME(ACTPOS-(NUMNEIGH-ITER1+1))
         ACTCONSWEIGHT=
     +        RESCONSWEIGHT(ACTPOS-(NUMNEIGH-ITER1+1))
         ACTNDEL=RESNDEL(ACTPOS-(NUMNEIGH-ITER1+1))
         ACTNINS=RESNINS(ACTPOS-(NUMNEIGH-ITER1+1))
C------- fork to distinguish between first and second net
         ACTREGION='BEFORE'
         ACTITER=ACTPOS-(NUMNEIGH-ITER1+1)
         ACTSTART=(ITER1-1)*NCODEUNT
C        =============
         CALL WININDIR
C        =============

C----------------------------------------
C------- sum up for input of amino acid -
C------- composition                    -
C----------------------------------------

         IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
            LHELP=.TRUE.
            DO ITAA=1,(NBIOLOBJ-1)
               IF ((LHELP .EQV. .TRUE.) .AND.
     +              (ACTRESIDUE.EQ.AACODE(ITAA))) THEN
                  ACTCOMPOSITION(ITAA)=ACTCOMPOSITION(ITAA)+1
                  LHELP=.FALSE.
               END IF
            END DO
         END IF
      END DO
C---- end of window before central            -----
C--------------------------------------------------
C----------------------------------------------------------------------
C---- central 'object' which will be predicted                     ----
C----------------------------------------------------------------------
      ACTRESIDUE=RESNAME(ACTPOS)
      ACTCONSWEIGHT=RESCONSWEIGHT(ACTPOS)
      ACTNDEL=RESNDEL(ACTPOS)
      ACTNINS=RESNINS(ACTPOS)
C---- fork to distinguish between first and second net
      ACTREGION='CENTRE'
      ACTITER=ACTPOS
      ACTSTART=NUMNEIGH*NCODEUNT
C     =============
      CALL WININDIR
C     =============
C----------------------------------------
C---- sum up for input of amino acid ----
C---- composition                    ----
C----------------------------------------

      IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
         LHELP=.TRUE.
         DO ITAA=1,(NBIOLOBJ-1)
            IF ((LHELP .EQV. .TRUE.) .AND.
     +           (ACTRESIDUE.EQ.AACODE(ITAA))) THEN
               ACTCOMPOSITION(ITAA)=ACTCOMPOSITION(ITAA)+1
               LHELP=.FALSE.
            END IF
         END DO
      END IF

C----------------------------------------------------------------------
C---- end of central objects DESIRED                              -----
C----------------------------------------------------------------------
C--------------------------------------------------
C---- read NUMNEIGH amino acids after central group
C--------------------------------------------------

      DO ITER1=1,(NUMNEIGH-ACTSOLVADDEND)
         ACTRESIDUE=RESNAME(ACTPOS+ITER1)
         ACTCONSWEIGHT=RESCONSWEIGHT(ACTPOS+ITER1)
         ACTNDEL=RESNDEL(ACTPOS+ITER1)
         ACTNINS=RESNINS(ACTPOS+ITER1)
C------- fork to distinguish between first and second net
         ACTREGION='AFTER'
         ACTITER=ACTPOS+ITER1
         ACTSTART=(NUMNEIGH+ITER1)*NCODEUNT
C        =============
         CALL WININDIR
C        =============
C----------------------------------------
C------- sum up for input of amino acid -
C------- composition                    -
C----------------------------------------

         IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
            LHELP=.TRUE.
            DO ITAA=1,(NBIOLOBJ-1)
               IF ((LHELP .EQV. .TRUE.) .AND.
     +              (ACTRESIDUE.EQ.AACODE(ITAA))) THEN
                  ACTCOMPOSITION(ITAA)=ACTCOMPOSITION(ITAA)+1
                  LHELP=.FALSE.
               END IF
            END DO
         END IF
      END DO
C--------------------------------------------------
C---- add solvents at end                     -----
C--------------------------------------------------

      DO ITER1=(NUMNEIGH-ACTSOLVADDEND+1),NUMNEIGH
         ACTRESIDUE='U'
C------- fork to distinguish between first and second net
         ACTREGION='ADDEND'
         ACTITER=(ACTPOS+ITER1)
         ACTSTART=(NUMNEIGH+ITER1)*NCODEUNT
C        =============
         CALL WININDIR
C        =============
      END DO
C---- sum up for input of amino acid composition
      IF (LOGI_COMPOSITION .EQV. .TRUE.) THEN
         ACTCOMPOSITION(NBIOLOBJ)=ACTCOMPOSITION(NBIOLOBJ)
     +        +ACTSOLVADDEND
      END IF
      END
***** end of WINASS

***** ------------------------------------------------------------------
***** SUB WININDIR
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININDIR
C---- ARG  : 
C---- DES  : For a given window position (adding spacer begin, 
C---- DES  : before 
C---- DES  : central, after central, adding spacer end) the input 
C---- DES  : vector 
C---- DES  : is assigned for exactly that region. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        For a given window position (adding spacer begin,*
*     --------        before central, after central, adding spacer end)*
*                     the input vector is assigned for exactly that    *
*                     region.                                          *
*     control param.: MODEASSCAS, MODEMAIN, NCODEUNT                   *
*     --------------- NBIOLOBJ, NTRAINJURY, NSECEL                     *
*     input parameter:CONTROLCHAR, ACTPOS, ACTPOS                      *
*     ----------------ACTREGION, ACTITER, ACTSTART                     *
*     input variables:RESPROF, RESNAME, RESVAR, RESCONSWEIGHT,         *
*     ----------------INTRJ, TSTINTRJ                                  *
*     from CODEIN:    CODEVECTOR                                       *
*          CODESTR:   CODEVECTOR                                       *
*     output variab.: INPUT, TSTINPUT                                  *
*     for SBR CODEIN: ACTRESIDUE, CODEVECPROF, CODEVECINCASc           *
*     called from:    WINASS                                           *
*     SBRs calling:  CODEIN, CODESTR, WININAA, WININSTR               *
*     procedure:      see SBR WINDIR                                   *
*----------------------------------------------------------------------*
      SUBROUTINE WININDIR

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITPROF,ITOUT
      CHARACTER*222   TMPASSCAS

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- for first network                       -----
C--------------------------------------------------
      IF (CONTROLCHAR.EQ.'FST') THEN
C----------------------------------------
C------- add solvents at begin/end ------
C----------------------------------------

         IF ((ACTREGION.EQ.'ADDBEG').OR.(ACTREGION.EQ.'ADDEND')) THEN
C---------- profile input?
            TMPASSCAS=MODEASSCAS(ACTCHI)
            IF ((TMPASSCAS(1:7).EQ.'PROFILE').OR.
     +           (TMPASSCAS(1:7).EQ.'P-REAL-')) THEN
               DO ITPROF=1,(NBIOLOBJ-1)
                  CODEVECPROF(ITPROF)=0
               END DO
               CODEVECPROF(NBIOLOBJ)=PROFMAX
            END IF
C---------- translating amino acid/profile to input vectors
C           ===========
            CALL CODEIN
C           ===========
C---------- INPUT (resp. TSTINPUT) = CODEVECTOR
C           ============
            CALL WININAA
C           ============
C----------------------------------------
C------- residues before/after/ central -
C----------------------------------------
         ELSEIF ((ACTREGION.EQ.'BEFORE').OR.
     +           (ACTREGION.EQ.'CENTRE').OR.
     +           (ACTREGION.EQ.'AFTER')) THEN
C---------- profile input?
            TMPASSCAS=MODEASSCAS(ACTCHI)
            IF ((TMPASSCAS(1:7).EQ.'PROFILE').OR.
     +           (TMPASSCAS(1:7).EQ.'P-REAL-')) THEN
C change: insertion
C               IF ((ACTRESIDUE.EQ.'.').OR.(ACTRESIDUE.EQ.' ')) THEN
               IF (ACTRESIDUE.EQ.' ') THEN
                  DO ITPROF=1,(NBIOLOBJ-1)
                     CODEVECPROF(ITPROF)=0
                  END DO
                  CODEVECPROF(NBIOLOBJ)=PROFMAX
               ELSE
                  DO ITPROF=1,(NBIOLOBJ-1)
                     CODEVECPROF(ITPROF)=RESPROF(ACTITER,ITPROF)
                  END DO
                  CODEVECPROF(NBIOLOBJ)=0
               END IF
            END IF
C---------- translating amino acid/profile to input vectors
C           ===========
            CALL CODEIN
C           ===========
C---------- INPUT (resp. TSTINPUT) = CODEVECTOR
C           ============
            CALL WININAA
C           ============
         ELSE
            WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +         'ERROR WININDIR called with wrong ACTREGION =',ACTREGION
            WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +           'stopped at 23-10-91-1. For CONTROLCHAR=',CONTROLCHAR
            STOP
         END IF
C--------------------------------------------------
C---- second network, only structure to structure -
C--------------------------------------------------

      ELSEIF (CONTROLCHAR.EQ.'SND') THEN
C----------------------------------------
C------- add solvents at begin/end ------
C----------------------------------------

         IF ((ACTREGION.EQ.'ADDBEG').OR.(ACTREGION.EQ.'ADDEND')) THEN
            DO ITOUT=1,NUMOUT
               CODEVECINCASC(ITOUT)=0
            END DO
            CODEVECINCASC(NUMOUT+1)=100
C---------- translating secondary structure into a vector
C           ============
            CALL CODESTR
C           ============
C---------- INPUT (resp. TSTINPUT) = CODEVECTOR
C           =============
            CALL WININSTR
C           =============
C----------------------------------------
C------- residues before/after/central --
C----------------------------------------
         ELSEIF ((ACTREGION.EQ.'BEFORE').OR.(ACTREGION.EQ.'CENTRE').OR.
     +           (ACTREGION.EQ.'AFTER')) THEN

            DO ITOUT=1,NUMOUT
               CODEVECINCASC(ITOUT)=INT(100*OUTFST(ITOUT,ACTITER,
     +              TRANS2FROM1(ACTCHI)))
            END DO
            CODEVECINCASC(NUMOUT+1)=0
            IF (ACTPOS.LT.1) THEN
               WRITE(6,*)'WININDIR tries to access sample:'
               WRITE(6,*)ACTPOS,'  stopped: 15-12-91-1'
               STOP
            END IF
C---------- translating secondary structure into a vector
C           ============
            CALL CODESTR
C           ============
C---------- INPUT (resp. TSTINPUT) = CODEVECTOR
C           =============
            CALL WININSTR
C           =============
         ELSE
            WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +          'ERROR WININDIR called with wrong ACTREGION =',ACTREGION
            WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +           'stopped at 23-10-91-2. For CONTROLCHAR=',CONTROLCHAR
            STOP
         END IF
      ELSE
         WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +      'ERROR WININDIR called with wrong CONTROLCHAR =',CONTROLCHAR
         WRITE(6,'(T2,A,T10,A,T60,A)')'***',
     +        'stopped at 23-10-91-3. For ACTREGION=',ACTREGION
         STOP
      END IF
      END
***** end of WININDIR

***** ------------------------------------------------------------------
***** SUB WININAA
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININAA
C---- ARG  : 
C---- DES  : INPUT/TSTINPUT = CODEVECTOR for NCODEUNT units. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        INPUT/TSTINPUT = CODEVECTOR for NCODEUNT units.  *
*     control param.: MODEMAIN,LOGICONSWEIGHT                          *
*     --------------- NCODEUNT                                         *
*     input parameter:ACTSTART, ACTPOS, ACTCONSWEIGHT                  *
*     input variables:CODEVECTOR                                       *
*     output variab.: INPUT,TSTINPUT                                   *
*     called from:    WINASS                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININAA

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITER,INTERNCODEUNT

******------------------------------*-----------------------------******

C---- NCODEUNT according to MODEASSCAS:
      IF ((LOGI_CONS .EQV. .TRUE.) .AND.
     +     (LOGI_INDEL .EQV. .FALSE.)) THEN
         INTERNCODEUNT=NCODEUNT-1
      ELSEIF ((LOGI_CONS .EQV. .TRUE.) .AND. 
     +        (LOGI_INDEL.EQV. .TRUE.)) THEN
         INTERNCODEUNT=NCODEUNT-3
      ELSE
         INTERNCODEUNT=NCODEUNT
      END IF
C--------------------------------------------------
C---- for binary input                        -----
C--------------------------------------------------

      IF ((MODEASSCAS(ACTCHI).EQ.'ALPHABET').OR.
     +     (MODEASSCAS(ACTCHI).EQ.'PROFILE-BIN')) THEN
         DO ITER=1,INTERNCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=INT(CODEVECTOR(ITER))
         END DO
C--------------------------------------------------
C---- for real input                          -----
C--------------------------------------------------

      ELSEIF (MODEASSCAS(ACTCHI).EQ.'PROFILE-REAL') THEN
         DO ITER=1,INTERNCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=CODEVECTOR(ITER)
         END DO
C--------------------------------------------------
C---- for real input (with conservation weight) ---
C--------------------------------------------------

      ELSEIF ((LOGI_CONS .EQV. .TRUE.) .AND.
     +        (LOGI_INDEL .EQV. .FALSE.)) THEN
         DO ITER=1,INTERNCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=CODEVECTOR(ITER)
         END DO
         IF (ACTCONSWEIGHT.LE.2) THEN
            INPUT((ACTSTART+NCODEUNT),ACTPOS)=ACTCONSWEIGHT/2.
         ELSE
            INPUT((ACTSTART+NCODEUNT),ACTPOS)=0
         END IF
C--------------------------------------------------
C---- for real input + conservation weight + indels
C--------------------------------------------------

      ELSEIF ((LOGI_CONS .EQV. .TRUE.) .AND. 
     +        (LOGI_INDEL.EQV. .TRUE.)) THEN
         DO ITER=1,INTERNCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=CODEVECTOR(ITER)
         END DO
         IF (ACTCONSWEIGHT.LE.2) THEN
            INPUT((ACTSTART+INTERNCODEUNT+1),ACTPOS)=ACTCONSWEIGHT/2.
         ELSE
            INPUT((ACTSTART+INTERNCODEUNT+1),ACTPOS)=0
         END IF
         INPUT((ACTSTART+NCODEUNT-1),ACTPOS)=ACTNDEL/REAL(ACTNALIGN)
         INPUT((ACTSTART+NCODEUNT),ACTPOS)=ACTNINS/REAL(ACTNALIGN)
      ELSE
         WRITE(6,'(T2,A)')'***'
         WRITE(6,'(T2,A,T10,A)')  '***','ERROR:   please ask for help!'
         WRITE(6,'(T2,A,T10,A,A)')'***','WININAA: MODEASSCAS not in '//
     +        'appropriate mode! The current choice is:'
         WRITE(6,'(T2,A,T10,A,T40,A,T70,I3)')'***',MODEASSCAS(ACTCHI),
     +        'for architecture:',ACTCHI
         STOP
      END IF
      END
***** end of WININAA

***** ------------------------------------------------------------------
***** SUB WININSTR
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININSTR
C---- ARG  : 
C---- DES  : INPUT/TSTINPUT = CODEVECTOR for NCODEUNT units. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        INPUT/TSTINPUT = CODEVECTOR for NCODEUNT units.  *
*     control param.: MODEMAIN,LOGICONSWEIGHT                          *
*     --------------- NCODEUNT                                         *
*     input parameter:ACTSTART, ACTPOS, ACTCONSWEIGHT                  *
*     input variables:CODEVECTOR                                       *
*     output variab.: INPUT,TSTINPUT                                   *
*     called from:    WINASS                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININSTR

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         ITER

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- for binary input                        -----
C--------------------------------------------------

      IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
         DO ITER=1,NCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=INT(CODEVECTOR(ITER))
         END DO
      ELSE
         DO ITER=1,NCODEUNT
            INPUT((ACTSTART+ITER),ACTPOS)=CODEVECTOR(ITER)
         END DO
      END IF
      END
***** end of WININSTR

***** ------------------------------------------------------------------
***** SUB WININCOMPOSITION
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININCOMPOSITION
C---- ARG  : 
C---- DES  : assign the values to INPUT/TSTINPUT for the unit 
C---- DES  : coding 
C---- DES  : for the composition of amino acids. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        assign the values to INPUT/TSTINPUT for the unit *
*     --------        coding for the composition of amino acids.       *
*     const. passed:  LOGI_REALINPUT, CONTROLCHAR,                     *
*     var. passed:    out: INPUT                                       *
*     var. read:      NUMIN, NBIOLOBJ, PROFACC, CASCACC, NUMRES,       *
*     ----------      ACTCOMPOSITION, CODECOMPOSTION                   *
*     called by:      WINDIR                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININCOMPOSITION

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         IBEG,IBEG1,ITAA,IHELP,ITACC
      REAL            HDIFF

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults                                -----
C--------------------------------------------------

C---- start of coding unit
      IBEG=BEGUNITS_COMPOSITION-1

C--------------------------------------------------
C---- for binary input                        -----
C--------------------------------------------------

      IF (LOGI_REALINPUT .EQV. .FALSE.) THEN
         DO ITAA=1,NBIOLOBJ
            IBEG1=IBEG+((ITAA-1)*(ACTACC-1))
            IHELP=2*NUMNEIGH+1
            IF (NUMRES.GT.IHELP) THEN
               HDIFF=(CODECOMPOSITION(ITAA)-ACTCOMPOSITION(ITAA))
     +              /REAL(NUMRES-IHELP)
            ELSE
               HDIFF=(CODECOMPOSITION(ITAA)-ACTCOMPOSITION(ITAA))
     +              /REAL(IHELP)
            END IF
            DO ITACC=1,(ACTACC-1)
               IF (HDIFF.GE.(ITACC*ACTINTERVALL)) THEN
                  INPUT((IBEG1+ITACC),ACTPOS)=1
               ELSE
                  INPUT((IBEG1+ITACC),ACTPOS)=0
               END IF
            END DO
         END DO
C--------------------------------------------------
C---- for real input                          -----
C--------------------------------------------------

      ELSEIF (LOGI_REALINPUT .EQV. .TRUE.) THEN
         IHELP=2*NUMNEIGH+1
         DO ITAA=1,NBIOLOBJ
            IF (NUMRES.GT.IHELP) THEN
               HDIFF=(CODECOMPOSITION(ITAA)-ACTCOMPOSITION(ITAA))
     +              /REAL(NUMRES-IHELP)
            ELSE
               HDIFF=(CODECOMPOSITION(ITAA)-ACTCOMPOSITION(ITAA))
     +              /REAL(IHELP)
            END IF
            INPUT((IBEG+ITAA),ACTPOS)=HDIFF
         END DO
      ELSE
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WININCOMPOSITION'//
     +        ' something wrong with input to SBR!'
         STOP
      END IF
      END
***** end of WININCOMPOSITION

***** ------------------------------------------------------------------
***** SUB WININCOMPOSITION_ALL
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININCOMPOSITION_ALL
C---- ARG  : 
C---- DES  : Compute amino acid composittion for a particular 
C---- DES  : protein (nue). 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Compute the amino acid composittion for a par-   *
*     --------        ticular protein (nue).                           *
*     input:          NUMRES, RESNAME, ACTPOS,                         *
*     ------          AACODE24, AACODE_LOWC, NBIOLOBJ                  *
*     output variab.: CODECOMPOSITION                                  *
*     SBRs calling:  SISTZ1 (from lib-comp.f)                         *
*     called by:      WINDIR                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININCOMPOSITION_ALL

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         MUE,ITAA,ITAA2,ITC
      CHARACTER*1     HCHAR
      LOGICAL         LHELP,LHELP2

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults                                -----
C--------------------------------------------------
      ITC=            0

C---- set zero
C     -----------
      CALL SISTZ1(CODECOMPOSITION,NBIOLOBJMAX)
C     -----------
      
C---- loop over whole protein        
      DO MUE=1,NUMRES
         LHELP=.TRUE.
         HCHAR=RESNAME(MUE)

C------- check for usual amino acids
         DO ITAA=1,(NBIOLOBJ-1)
            IF (LHELP .EQV. .TRUE.) THEN
               IF (HCHAR.EQ.AACODE24(ITAA:ITAA)) THEN
                  CODECOMPOSITION(ITAA)=CODECOMPOSITION(ITAA)+1
                  LHELP=.FALSE.
               END IF
            END IF
         END DO

C------- check for ends, chain breaks asf
         IF (LHELP .EQV. .TRUE.) THEN
            IF ((HCHAR.EQ.'!').OR.(HCHAR.EQ.'/').OR.
     +           (HCHAR.EQ.'U').OR.(HCHAR.EQ.'O')) THEN
               CODECOMPOSITION(NBIOLOBJ)=CODECOMPOSITION(NBIOLOBJ)+1
               LHELP=.FALSE.
            END IF
         END IF

C------- check for small caps = cysteine
         IF (LHELP .EQV. .TRUE.) THEN
            LHELP2=.TRUE.
            DO ITAA=1,(NBIOLOBJ-1)
               IF (AACODE24(ITAA:ITAA).EQ.'C') THEN
                  ITC=ITAA
                  LHELP2=.FALSE.
               END IF
            END DO
            IF (LHELP2 .EQV. .TRUE.) THEN
               STOP ' *** WININCOMPOSITION_ALL: NO C'
            END IF
            DO ITAA2=1,26
               IF ((LHELP .EQV. .TRUE.) .AND. 
     +              (HCHAR.EQ.AACODE_LOWC(ITAA2:ITAA2))) THEN
                  CODECOMPOSITION(ITC)=CODECOMPOSITION(ITC)+1
                  LHELP=.FALSE.
               END IF
            END DO
         END IF

C------- consistency: one found?
         IF (LHELP .EQV. .TRUE.) THEN
            IF (HCHAR.NE.'.') THEN
               WRITE(6,'(T2,A,T10,A)')'***','ERROR in WININ'//
     +              'COMPOSITION_ALL of amino acids:'
               WRITE(6,'(T2,A,T10,A,T30,A,T35,A,T45,I4)')
     +              '***','no match for:',HCHAR,'prot',1
            END IF
            CODECOMPOSITION(NBIOLOBJ)=CODECOMPOSITION(NBIOLOBJ)+1
         END IF
      END DO
      END
***** end of WININCOMPOSITION_ALL

***** ------------------------------------------------------------------
***** SUB WININLENGTH
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININLENGTH
C---- ARG  : 
C---- DES  : Assign the values to INPUT/TSTINPUT for the units 
C---- DES  : coding for 
C---- DES  : the length of the protein. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        assign the values to INPUT/TSTINPUT for the units*
*     --------        coding for the length of the protein.            *
*     const. passed:  LOGI_REALINPUT, CONTROLCHAR, ACTPOS              *
*     var. passed:    out: INPUT                                       *
*     var. read:      SPLIT_LENGTH, BEGUNITS_LENGTH,  NUNITS_LENGTH,   *
*     ----------      NUMRES                                           *
*     called by:      WINDIR                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININLENGTH

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         IBEG,IBEG1,ITLEN,ITACC,INTERV2(1:4)
      REAL            HDIFF,HINTER
      LOGICAL         LHELP

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults                                -----
C--------------------------------------------------

      INTERV2(1)=SPLIT_LENGTH(1)
      INTERV2(2)=SPLIT_LENGTH(2)-SPLIT_LENGTH(1)
      INTERV2(3)=SPLIT_LENGTH(3)-SPLIT_LENGTH(2)
      INTERV2(4)=SPLIT_LENGTH(4)-SPLIT_LENGTH(3)

C---- start of coding unit
      IBEG=BEGUNITS_LENGTH-1
C---- checks
      IF (NUNITS_LENGTH.NE.4) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WININLENGTH: '//
     +        'currently only for NUNITS_LENGTH = 4 (phdParameter.f) !'
         WRITE(6,'(T2,A,T10,A,T40,I5,T50,A)')'***',
     +        'instead it is: ',NUNITS_LENGTH,'stopped 29-12-93-1.'
         STOP
      END IF
C--------------------------------------------------
C---- for binary input                        -----
C--------------------------------------------------

      IF (LOGI_REALINPUT .EQV. .FALSE.) THEN

C------------------------------
C------- loop over coding units
C------------------------------

         LHELP=.TRUE.
         DO ITLEN=1,NUNITS_LENGTH
            IBEG1=IBEG+((ITLEN-1)*(ACTACC-1))
            IF (LHELP .EQV. .TRUE.) THEN
               IF (NUMRES.GE.SPLIT_LENGTH(ITLEN)) THEN
                  HDIFF=1.
               ELSE
                  IF (NUMRES.GT.500) THEN
                     HDIFF=1.
                  ELSE
                     HINTER=REAL(INTERV2(ITLEN)
     +                    -(SPLIT_LENGTH(ITLEN)-NUMRES))
                     HDIFF=HINTER/REAL(INTERV2(ITLEN))
                  END IF
                  LHELP=.FALSE.
               END IF
            ELSE
               HDIFF=0.
            END IF
C---------- translate reals to binary grid
            DO ITACC=1,(ACTACC-1)
               IF (HDIFF.GE.(ITACC*ACTINTERVALL)) THEN
                  INPUT((IBEG1+ITACC),ACTPOS)=1
               ELSE
                  INPUT((IBEG1+ITACC),ACTPOS)=0
               END IF
            END DO
C---------- end translate reals to binary grid
         END DO
C------- end loop coding units
C------------------------------
C--------------------------------------------------
C---- for real input                          -----
C--------------------------------------------------

      ELSEIF (LOGI_REALINPUT .EQV. .TRUE.) THEN
         LHELP=.TRUE.
         DO ITLEN=1,NUNITS_LENGTH
            IF (LHELP .EQV. .TRUE.) THEN
               IF (NUMRES.GE.SPLIT_LENGTH(ITLEN)) THEN
                  HDIFF=1.
               ELSE
                  IF (NUMRES.GT.500) THEN
                     HDIFF=1.
                  ELSE
                     HINTER=REAL(INTERV2(ITLEN)
     +                    -(SPLIT_LENGTH(ITLEN)-NUMRES))
                     HDIFF=HINTER/REAL(INTERV2(ITLEN))
                  END IF
                  LHELP=.FALSE.
               END IF
            ELSE
               HDIFF=0
            END IF
            INPUT((IBEG+ITLEN),ACTPOS)=HDIFF
         END DO
      END IF

      END
***** end of WININLENGTH

***** ------------------------------------------------------------------
***** SUB WININDISTCAPS
***** ------------------------------------------------------------------
C---- 
C---- NAME : WININDISTCAPS
C---- ARG  : 
C---- DES  : Assign the values to INPUT/TSTINPUT for the units 
C---- DES  : coding for 
C---- DES  : the distance to the caps of the protein. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Assign the values to INPUT/TSTINPUT for the units*
*     --------        coding for the distance to the caps of the protein
*     const. passed:  LOGI_REALINPUT, CONTROLCHAR, ACTPOS              *
*     var. passed:    out: INPUT                                       *
*     var. read:      SPLIT_DISTCAPS, *_DISTCAPS, *_DISTCAPS,          *
*     ----------      ACTACC, ACTINTERVALL, NUMRES, 
*     called by:      WINDIR                                           *
*----------------------------------------------------------------------*
      SUBROUTINE WININDISTCAPS

C---- global parameters
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         IBEGN,IBEGC,IBEG1N,IBEG1C,ITLEN,ITACC
      REAL            HDIFFN,HDIFFC,HINTER

******------------------------------*-----------------------------******

C--------------------------------------------------
C---- defaults                                -----
C--------------------------------------------------

C---- start of coding unit
      IBEGN=BEGUNITS_DISTCAPS-1
C---- checks
      IF (NUNITS_DISTCAPS.NE.4) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WININDISTCAPS: '//
     +    'currently only for NUNITS_DISTCAPS = 4 (phdParameter.f) !'
         WRITE(6,'(T2,A,T10,A,T40,I5,T50,A)')'***',
     +        'instead it is: ',NUNITS_DISTCAPS,'stopped 29-12-93-1.'
         STOP
      END IF
C--------------------------------------------------
C---- for binary input                        -----
C--------------------------------------------------

      IF (LOGI_REALINPUT .EQV. .FALSE.) THEN

C------------------------------
C------- loop over coding units
C------------------------------

         IBEGC=BEGUNITS_DISTCAPS-1+(NUNITS_DISTCAPS*(ACTACC-1))
         IBEGN=BEGUNITS_DISTCAPS-1
         DO ITLEN=1,NUNITS_DISTCAPS
            IBEG1N=IBEGN+((ITLEN-1)*(ACTACC-1))
            IBEG1C=IBEGC+((ITLEN-1)*(ACTACC-1))

C---------- N-term cap
            IF (ACTPOS.LE.SPLIT_DISTCAPS(ITLEN)) THEN
               HINTER=REAL(SPLIT_DISTCAPS(ITLEN)-ACTPOS+1)
               HDIFFN=MIN(1.,(HINTER/10.))
            ELSE
               HDIFFN=0.
            END IF
C---------- C-term cap
            IF ((NUMRES-ACTPOS).LE.SPLIT_DISTCAPS(ITLEN)) THEN
               HDIFFC=1.
               HINTER=REAL(SPLIT_DISTCAPS(ITLEN)-(NUMRES-ACTPOS))
               HDIFFC=MIN(1.,(HINTER/10.))
            ELSE
               HDIFFC=0.
            END IF

C---------- translate reals to binary grid
            DO ITACC=1,(ACTACC-1)
               IF (HDIFFN.GE.(ITACC*ACTINTERVALL)) THEN
                  INPUT((IBEG1N+ITACC),ACTPOS)=1
               ELSE
                  INPUT((IBEG1N+ITACC),ACTPOS)=0
               END IF
               IF (HDIFFC.GE.(ITACC*ACTINTERVALL)) THEN
                  INPUT((IBEG1C+ITACC),ACTPOS)=1
               ELSE
                  INPUT((IBEG1C+ITACC),ACTPOS)=0
               END IF
            END DO
C---------- end translate reals to binary grid
         END DO
C------- end loop coding units
C------------------------------
C--------------------------------------------------
C---- for real input                          -----
C--------------------------------------------------

      ELSEIF (LOGI_REALINPUT .EQV. .TRUE.) THEN
         IBEGC=(BEGUNITS_DISTCAPS-1)+NUNITS_DISTCAPS
         DO ITLEN=1,NUNITS_DISTCAPS
C---------- N-term cap
            IF (ACTPOS.LE.SPLIT_DISTCAPS(ITLEN)) THEN
               HINTER=REAL(SPLIT_DISTCAPS(ITLEN)-ACTPOS+1)
               HDIFFN=MIN(1.,(HINTER/10.))
            ELSE
               HDIFFN=0.
            END IF
C---------- C-term cap
            IF ((NUMRES-ACTPOS).LE.SPLIT_DISTCAPS(ITLEN)) THEN
               HDIFFC=1.
               HINTER=REAL(SPLIT_DISTCAPS(ITLEN)-(NUMRES-ACTPOS))
               HDIFFC=MIN(1.,(HINTER/10.))
            ELSE
               HDIFFC=0.
            END IF
            INPUT((IBEGN+ITLEN),ACTPOS)=HDIFFN
            INPUT((IBEGC+ITLEN),ACTPOS)=HDIFFC
         END DO
      END IF
      END
***** end of WININDISTCAPS

***** ------------------------------------------------------------------
***** SUB WRTBOTHHEADER
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTBOTHHEADER
C---- ARG  :  
C---- DES  : Write headers for prediction (sec and acc).
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTBOTHHEADER(KUNIT,VERSION,NUMNETJURY,
     +     TXT80,IBEG,IEND)

C---- local variables
      INTEGER         KUNIT,ITER,IEND,IBEG,NUMNETJURY
C      CHARACTER*24    FCTIME_DATE
      CHARACTER*5     SBEG,SEND
      CHARACTER*222   INTERTXT,TXT80(1:30),VERSION

************************************************************************

C---- defaults
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- write header into prediction output     -----
C--------------------------------------------------
      CALL WRTF(KUNIT)
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     +     ' PHD output for your protein:',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     +     ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)
c$$$      WRITE(KUNIT,'(A3,A1,A24,T78,A3)')SBEG,' ',FCTIME_DATE(),SEND
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,A11,T18,I4,T25,A35,A10,A2,T78,A3)')SBEG,
     +        ' Jury on:  ',NUMNETJURY,' different architectures '//
     +        '(version  ',VERSION,').',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' Note: differently trained '//
     +     'architectures, i.e., different versions can',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     +     '      result in different predictions.',SEND
      CALL WRTE(KUNIT)
      CALL WRTF(KUNIT)
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' About the protein:',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' ------------------',SEND
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)
      IF (IBEG.LT.1) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WRTBOTHHEADER'//
     +        ' Hssp file HEADER changed? '
      END IF
      IF (IEND.GT.30) THEN
         WRITE(6,'(T2,A,T10,A)')'***','ERROR in WRTBOTHHEADER'//
     +        ' Hssp file NALIGN changed? '
      END IF
      DO ITER=IBEG,IEND
         INTERTXT=TXT80(ITER)
         WRITE(KUNIT,'(A3,T10,A60,T78,A3)')SBEG,INTERTXT,SEND
         IF (INTERTXT(1:6).EQ.'NALIGN') THEN
            WRITE(KUNIT,'(A3,T21,A,T78,A3)')SBEG,
     +           '(=number of aligned sequences in HSSP file)',SEND
         END IF
      END DO
      CALL WRTE(KUNIT)
      CALL WRTF(KUNIT)
      CALL WRTE(KUNIT)
      END
***** end of WRTBOTHHEADER

***** ------------------------------------------------------------------
***** SUB WRTCONTENT
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTCONTENT
C---- ARG  :  
C---- DES  : The content in secondary structure, resp. relative percent.
C---- DES  : of occurrence per amino acid is written onto unit KUNIT. 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The content in secondary structure, resp. relative
*     --------        percentage of occurrence per amino acid is       *
*                     written onto unit KUNIT.                         *
*     input variables:CONTPRED,CONTDSSP,CONTAA                         *
*     output variab.: CONTPRED,CONTDSSP,CONTAA                         *
*     called by:      TXTRES, DATAOT                                   *
*----------------------------------------------------------------------*
      SUBROUTINE WRTCONTENT(KUNIT)

C---- parameters/global variables
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         KUNIT,MUE,ITAA,ICOUNT,ITSEC,
     +     ISORT(1:24),ITLINES,IBEG,IEND
      REAL            HCONTAA(1:24)
      CHARACTER*24    TXTAA,TXTCONT
      CHARACTER*1     CAA(1:24),TXTSS(1:8)
      CHARACTER*3     SBEG,SEND

******------------------------------*-----------------------------******

C---- default
      TXTAA(1:24)='ABCDEFGHIKLMNPQRSTVWXYZU'
      SBEG='*  '
      SEND='  *'

      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
         TXTSS(1)='H'
         TXTSS(2)='E'
         IF (NSECEL.GT.3) THEN
            TXTSS(3)='T'
         END IF
         TXTSS(NSECEL)='L'
      END IF
C-------------------------------------------------------------
C---- secondary structure prediction                     -----
C-------------------------------------------------------------

C----------------------------------------
C---- header
C----------------------------------------

C---- paper to quote
      WRITE(KUNIT,'(80A)')('*',MUE=1,80)
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Publication to reference in reporting results:',SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      IF (MODESECSTRON.EQ.'SECONDARY') THEN
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Rost, Burkhard; Sander, Chris: Prediction of protein '//
     +     'structure',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   at better than 70% accuracy. J. Mol. Biol., 1993, '//
     +     '232, 584-599.',SEND
      END IF
      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Rost, Burkhard; Sander, Chris: Combining evolutionary '//
     +     'information',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   and neural networks to predict protein secondary '//
     +     'structure. ',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   Proteins, 1994, 19, 55-72.',SEND
      END IF
      IF (MODESECSTRON.EQ.'EXPOSURE') THEN
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Rost, Burkhard; Sander, Chris: Conservation and  '//
     +     'prediction of ',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   solvent accessibility in protein families. Proteins, '//
     +     '1994,',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   20, 216-226.',SEND
      END IF
      IF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Rost, Burkhard; Casadio, Rita; Fariselli, Piero; '//
     +     'Sander, Chris: ',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   Prediction of helical transmembrane segments at 75% ',
     +     SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '   accuracy. Protein Science, 1995, 4, 521-533',
     +     SEND
      END IF
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND

      WRITE(KUNIT,'(80A)')('*',MUE=1,80)
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')
     +     SBEG,'Some statistics:',SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')
     +     SBEG,'~~~~~~~~~~~~~~~~',SEND
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Percentage of amino acids:',SEND
C----------------------------------------
C---- sorting + writing the content of AA
C----------------------------------------
C     ==============
      CALL SRSORTVEC(24,24,1,CONTAA,2,ISORT,2)
C     ==============
      ICOUNT=0
      DO ITAA=1,24
         IF (CONTAA(ISORT(ITAA)).NE.0) THEN
            HCONTAA(ITAA)=CONTAA(ISORT(ITAA))
            CAA(ITAA)=TXTAA(ISORT(ITAA):ISORT(ITAA))
            ICOUNT=ITAA
         END IF
      END DO

      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(A3,T10,A,T26,5A9)')SBEG,'+--------------+',
     +     ('--------+',ITAA=1,MIN(5,ICOUNT))

      DO ITLINES=1,(INT((ICOUNT-0.1)/5.)+1)
         IBEG=(ITLINES-1)*5+1
         IEND=INT(MIN(REAL(IBEG+4),REAL(ICOUNT)))
         WRITE(KUNIT,'(A3,T10,A,T25,A1,5(A5,A4))')SBEG,
     +        '| AA:','|',(CAA(ITAA),'   |',ITAA=IBEG,IEND)
         WRITE(KUNIT,'(A3,T10,A,T25,A1,5(A2,F5.1,A2))')SBEG,
     +        '| % of AA:','|',('  ',HCONTAA(ITAA),' |',ITAA=IBEG,IEND)
     +                     
         WRITE(KUNIT,'(A3,T10,A,T26,5A9)')SBEG,
     +        '+--------------+',('--------+',ITAA=IBEG,IEND)
      END DO
C----------------------------------------
C---- writing the content of pred SS
C----------------------------------------

      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      IF (MODESECSTRON(1:9).EQ.'SECONDARY') THEN
         IF (MODESECSTRON.EQ.'SECONDARY') THEN
            IF (LDSSPREAD .EQV. .FALSE.) THEN
               WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +           'Percentage of secondary structure predicted:',SEND
            ELSE
               WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Percentage of secondary structure predicted/observed:',SEND
            END IF
         ELSE
            IF (LDSSPREAD .EQV. .FALSE.) THEN
               WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +           'Percentage of helical trans-membrane predicted:',SEND
            ELSE
               WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +  'Percentage of helical trans-membrane predicted/observed:',SEND
            END IF
         END IF
         WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
         WRITE(KUNIT,'(A3,T10,A,T26,4A9)')SBEG,
     +        '+--------------+',('--------+',ITSEC=1,NSECEL)
         WRITE(KUNIT,'(A3,T10,A,T25,A1,4(A4,A1,A4))')SBEG,
     +  '| SecStr:','|',('    ',TXTSS(ITSEC),'   |',ITSEC=1,NSECEL)
         WRITE(KUNIT,'(A3,T10,A,T25,A1,4(A2,F5.1,A2))')SBEG,
     +  '| % Predicted:','|',('  ',CONTPRED(ITSEC),' |',ITSEC=1,NSECEL)
      END IF
C----------------------------------------
C---- writing the content of obs SS    --
C----------------------------------------
      IF ((LDSSPREAD .EQV. .TRUE.) .AND.
     +     (MODESECSTRON(1:9).EQ.'SECONDARY')) THEN
         WRITE(KUNIT,'(A3,T10,A,T26,4A9)')SBEG,
     +        '+--------------+',('--------+',ITSEC=1,NSECEL)
         WRITE(KUNIT,'(A3,T10,A,T25,A1,4(A4,A1,A4))')SBEG,
     +    '| SecStr:','|',('    ',TXTSS(ITSEC),'   |',ITSEC=1,NSECEL)
         WRITE(KUNIT,'(A3,T10,A,T25,A1,4(A2,F5.1,A2))')SBEG,
     +   '| % Observed:','|',('  ',CONTDSSP(ITSEC),' |',ITSEC=1,NSECEL)
     +        
      END IF
      WRITE(KUNIT,'(A3,T10,A,T26,4A9)')SBEG,
     +     '+--------------+',('--------+',ITSEC=1,NSECEL)
C----------------------------------------
C---- assigning class to pred SS
C----------------------------------------

      IF (MODESECSTRON.EQ.'SECONDARY') THEN
         IF ((CONTPRED(1).GT.45).AND.(CONTPRED(2).LT.5)) THEN
            TXTCONT='all-alpha  '
         ELSEIF ((CONTPRED(1).LT.5).AND.(CONTPRED(2).GT.45)) THEN
            TXTCONT='all-beta   '
         ELSEIF ((CONTPRED(1).GT.30).AND.(CONTPRED(2).GT.20)) THEN
            TXTCONT='alpha-beta '
         ELSE
            TXTCONT='mixed class'
         END IF
         WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +        'According to the following classes:',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +    'all-alpha:   %H>45 and %E< 5; all-beta : %H<5 and %E>45',SEND
         WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'alpha-beta : %H>30 and %E>20; mixed:    rest,',SEND
         WRITE(KUNIT,'(A3,T10,A,T60,A,T78,A3)')SBEG,
     +   'this means that the predicted class is: ',TXTCONT(1:11),SEND

         IF (LDSSPREAD .EQV. .TRUE.) THEN
            IF ((CONTDSSP(1).GT.45).AND.(CONTDSSP(2).LT.5)) THEN
               TXTCONT='all-alpha  '
            ELSEIF ((CONTDSSP(1).LT.5).AND.(CONTDSSP(2).GT.45)) THEN
               TXTCONT='all-beta   '
            ELSEIF ((CONTDSSP(1).GT.30).AND.(CONTDSSP(2).GT.20)) THEN
               TXTCONT='alpha-beta '
            ELSE
               TXTCONT='mixed class'
            END IF
            WRITE(KUNIT,'(A3,T10,A,T60,A,T78,A3)')SBEG,
     +    'The class of the observed structure is: ',TXTCONT(1:11),SEND
         END IF
      END IF

      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(80A)')('*',MUE=1,80)
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
C---- end of content                          -----
C--------------------------------------------------
      END
***** end of WRTCONTENT

***** ------------------------------------------------------------------
***** SUB WRTD
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTD
C---- ARG  :  
C---- DES  : write ending lines
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTD(K)
      INTEGER           ITER,K

      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      WRITE(K,'(A3,74A1,T78,A3)')'***',('.',ITER=1,74),'***'
      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      END
***** end of WRTD

***** ------------------------------------------------------------------
***** SUB WRTEXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTEXP
C---- ARG  :  
C---- DES  : write solvent accessibility prediction.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTEXP(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local functions
      CHARACTER*222   FWRITE_STRING_NUMBERS
      CHARACTER*1     FC_REXP_TO_3STCHAR
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         KUNIT,ITER,ITLINES,ITMUE,MUE,IHELP,IEND,IBEG,
     +     OUTEXPLOC(1:80),IPOS
      INTEGER*2       DSSPEXPONE(1:80),PREDEXPONE(1:80)
      REAL            REXP
      CHARACTER*5     SBEG,SEND
      CHARACTER*6     CHELP6
      CHARACTER*222   INTERTXT,TXT80(1:30),VERSION,CHFILE
      CHARACTER*1     PREDTXT(1:80),DSSPTXT(1:80)
      LOGICAL         LWRITE_ACC80,LWRT80

************************************************************************
*     SBRs called:   WRTBOTHHEADER, WRTEXPHEADER, WRTWARN_NOALI,     *
*                     WRTPOPRED                                        *
************************************************************************

C---- defaults
      LWRT80=.FALSE.
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- read header of HSSP file                -----
C--------------------------------------------------
      IEND=FILEN_STRING(FILE_HSSP_NOCHAIN)
      CHFILE(1:IEND)=FILE_HSSP_NOCHAIN(1:IEND)
C      CALL SFILEOPEN(15,CHFILE(1:IEND),'UNKNOWN',222,' ')
C      CALL SFILEOPEN(15,FILE_HSSP_NOCHAIN,'OLD',222,'READONLY')
Cyy      OPEN (15,FILE=FILE_HSSP_NOCHAIN,STATUS='OLD',READONLY)
      OPEN (15,FILE=FILE_HSSP_NOCHAIN,STATUS='OLD')
      DO ITER=1,30
         READ(15,'(A80)')TXT80(ITER)
      END DO
      CLOSE(15)
C---- store begin an end of text to be written
      DO ITER=1,30
         INTERTXT=TXT80(ITER)
         IF (INTERTXT(1:6).EQ.'HEADER') THEN
            IBEG=ITER
         ELSEIF (INTERTXT(1:6).EQ.'NALIGN') THEN
            IEND=ITER
         END IF
      END DO
C----------------------------------------
C---- enough residues in intervall >= 4 ?
C----------------------------------------

      IHELP=0
      DO MUE=1,NUMRES
         IF (RELIND(MUE).GE.4) THEN
            IHELP=IHELP+1
         END IF
      END DO
      IF (IHELP.GT.5) THEN
         LWRITE_ACC80=.TRUE.
      ELSE
         LWRITE_ACC80=.FALSE.
      END IF
C--------------------------------------------------
C---- write header into prediction output     -----
C--------------------------------------------------
      VERSION=VERSION_EXP
C     ==================
      CALL WRTBOTHHEADER(KUNIT,VERSION,NUMNETJURY,TXT80,IBEG,IEND)
C     ==================

C     ==================
      CALL WRTEXPHEADER(KUNIT,LDSSPREAD,LWRITE_ACC80)
C     ==================
C--------------------------------------------------
C---- write warning if too few sequences in ali ---
C--------------------------------------------------

      IF (NUMNALIGN(1).LE.5) THEN
C        ==================
         CALL WRTWARN_NOALI(KUNIT,MODESECSTRON)
C        ==================
      END IF
C--------------------------------------------------
C---- now write the prediction for all        -----
C--------------------------------------------------
C---- data
      WRITE(KUNIT,'(A3,T10,A,T25,A7,T40,A,T50,I5,T78,A3)')SBEG,
     +     'protein:',PROTNAME(1),'length',NUMRES,SEND
      CALL WRTE(KUNIT)
      WRITE(KUNIT,*)

C---- strings of 60: for easy evaluation upon look
      DO ITLINES=1,(INT((NUMRES-1)/60.)+1)
         WRITE(KUNIT,'(T15,A60)')FWRITE_STRING_NUMBERS(LWRT80,ITLINES)
         IPOS=(ITLINES-1)*60
         IEND=INT(MIN(60.,REAL(NUMRES-IPOS)))

C----------------------------------------
C------- intermediate strings of lenth 60
         DO ITMUE=1,INT(MIN(60.,REAL(NUMRES-IPOS)))

C---------- for observed exposure
            IF (LDSSPREAD .EQV. .TRUE.) THEN
               REXP=DESEXP(IPOS+ITMUE)
               IF (REXP.GE.0) THEN
                  DSSPEXPONE(ITMUE)=INT2(MIN( 9.,
     +                                   SQRT(100*REXP/REAL(MAXEXP)) ))
C                     ------------------
                  DSSPTXT(ITMUE)=
     +                FC_REXP_TO_3STCHAR(REXP,THREXP3ST(2),THREXP3ST(3))
C                     ------------------
                  IF ( (DSSPTXT(ITMUE).EQ.'I').OR.
     +                 (DSSPTXT(ITMUE).EQ.'i') ) THEN
                     DSSPTXT(ITMUE)=' '
                  END IF
               ELSE
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'ERROR in WRTEXP: desired exposure < 0'
                  WRITE(6,'(T2,A,T10,A,T30,I6,T40,A,T50,I4)')'***',
     +                 'for itmue = ',itmue,'IPOS=',IPOS
               END IF
            END IF

C---------- for predicted exposure
            IF (LFILTER .EQV. .TRUE.) THEN
               OUTEXPLOC(ITMUE)=INT(100*OUTEXPFIL(IPOS+ITMUE))
               PREDTXT(ITMUE)=OUTBINCHARFIL(IPOS+ITMUE)
            ELSE
               OUTEXPLOC(ITMUE)=INT(100*OUTEXP(IPOS+ITMUE))
               PREDTXT(ITMUE)=OUTBINCHAR(IPOS+ITMUE)
            END IF
            IF (OUTEXPLOC(ITMUE).GE.0) THEN
               PREDEXPONE(ITMUE)=INT2( MIN( 9.,
     +                                 SQRT(REAL(OUTEXPLOC(ITMUE))) ) )
               IF ( (PREDTXT(ITMUE).EQ.'I').OR.
     +              (PREDTXT(ITMUE).EQ.'i') ) THEN
                  PREDTXT(ITMUE)=' '
               END IF
            ELSE
               WRITE(6,'(T2,A,T10,A)')'***',
     +              'ERROR in WRTEXP: predicted exposure < 0'
               WRITE(6,'(T2,A,T10,A,T30,I6,T40,A,T50,I4)')'***',
     +              'for itmue = ',itmue,'IPOS=',IPOS
            END IF
         END DO
C------- end of assigning intermediate strings
C----------------------------------------

C------- sequence
         WRITE(KUNIT,'(T10,A5,60A1,A1)')'AA  |',
     +        (RESNAME(ITMUE),ITMUE=(IPOS+1),(IPOS+IEND)),'|'
C------- projection onto 3 states
         IF (LDSSPREAD .EQV. .TRUE.) THEN
C---------- secondary structure
            WRITE(KUNIT,'(T10,A5,60A1,A1)')'SS  |',
     +           (CONVSECSTR(ITMUE),ITMUE=(IPOS+1),(IPOS+IEND)),'|'
C---------- observed relative exposure 3states
            WRITE(KUNIT,'(A,T10,A5,60A1,A1)')' 3st:','O 3 |',
     +           (DSSPTXT(ITMUE),ITMUE=1,IEND),'|'
            CHELP6='      '
         ELSE
            CHELP6=' 3st:'
         END IF
         WRITE(KUNIT,'(A,T10,A5,60A1,A1)')CHELP6,'P 3 |',
     +        (PREDTXT(ITMUE),ITMUE=1,IEND),'|'
C------- detailed 10 states
         IF (LDSSPREAD .EQV. .TRUE.) THEN
C---------- observed relative exposure
            WRITE(KUNIT,'(A,T10,A5,60I1,A1)')' 10st:','OBS |',
     +           (DSSPEXPONE(ITMUE),ITMUE=1,IEND)
            CHELP6='      '
         ELSE
            CHELP6=' 10st:'
         END IF
C------- predicted relative exposure
         WRITE(KUNIT,'(A,T10,A5,60I1,A1)')CHELP6,'PHD |',
     +        (PREDEXPONE(ITMUE),ITMUE=1,IEND)
C------- reliability index
         WRITE(KUNIT,'(T10,A5,60I1,A1)')
     +        'Rel |',(RELIND(ITMUE),ITMUE=(IPOS+1),(IPOS+IEND))
C--------------------------------------------------
C---- now write the prediction for 81.9%      -----
C--------------------------------------------------
         IF (LWRITE_ACC80 .EQV. .TRUE.) THEN
            DO ITMUE=1,IEND
               IF (RELIND(IPOS+ITMUE).GE.4) THEN
                  IF (LFILTER .EQV. .TRUE.) THEN
                     PREDTXT(ITMUE)=OUTBINCHARFIL(IPOS+ITMUE)
                  ELSE
                     PREDTXT(ITMUE)=OUTBINCHAR(IPOS+ITMUE)
                  END IF
               ELSE
                  PREDTXT(ITMUE)='.'
               END IF
            END DO
            WRITE(KUNIT,'(T2,A,T10,A5,60A1,A1)')'subset:',
     +           'SUB |',(PREDTXT(ITMUE),ITMUE=1,IEND),'|'
         END IF
         WRITE(KUNIT,*)
      END DO
C--------------------------------------------------
C---- compute accuracy if DSSP available      -----
C--------------------------------------------------
      IF (LDSSPREAD .EQV. .TRUE.) THEN
C        =============
         CALL WRTPOEXP(KUNIT)
C        =============
      END IF
      END
***** end of WRTEXP

***** ------------------------------------------------------------------
***** SUB WRTEXPHEADER
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTEXPHEADER
C---- ARG  :  
C---- DES  : write header for solvent accessibility prediction.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTEXPHEADER(KUNIT,LDSSPREAD,LWRITE_ACC80)

C---- local variables
      INTEGER         KUNIT
      CHARACTER*5     SBEG,SEND
      LOGICAL         LWRITE_ACC80,LDSSPREAD

************************************************************************

C---- defaults
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- abbreviations used
C--------------------------------------------------

      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' Abbreviations:',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' --------------',SEND
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     +     '                   AA: amino acid sequence',SEND
      IF (LDSSPREAD .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                   SS: H=helix, E=extended (sheet), blank=re'//
     +        'st (loop)',SEND
      END IF
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  3st: relative solvent accessibility (acc)'//
     + ' in 3 states:', SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       b = 0-9%, i = 9-36%, e = 36-100%.',SEND
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  PHD: Profile network prediction HeiDelberg',
     +     SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  Rel: Reliability index of prediction (0-9)',
     +     SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  O 3: observed relative '//
     +     'acc. in 3 states: B, I, E',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' Note:                 '//
     +     'for convenience a blank is used intermediate (i).',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  P 3: predicted rel. acc. in 3 states',SEND
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                 10st: relative solvent acc. in 10 states',
     +     SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       = n corresponds to a relative acc.'//
     +     ' of n*n %',SEND
      WRITE(KUNIT,'(A3,T78,A3)')SBEG,SEND
C--------------------------------------------------
C---- subset
C--------------------------------------------------

      IF (LWRITE_ACC80 .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' Subset:          SUB: a subset of the prediction, for all '//
     +        'residues with',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       an expected correlation > 0.69 (tab'//
     +        'les in header)',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' Note:                 for this subset the following '//
     +        'symbols are used:',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  "I": is intermediate (for which above '//
     +        '" " is used)',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  ".": means that no prediction is made '//
     +        'for this residue,',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       as the reliability is Rel < 4',SEND
         CALL WRTE(KUNIT)
      END IF
      CALL WRTF(KUNIT)
      END
***** end of WRTEXPHEADER

***** ------------------------------------------------------------------
***** SUB WRTE
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTE
C---- ARG  :  
C---- DES  : write empty line
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTE(K)
      INTEGER           K

      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      END
***** end of WRTE

***** ------------------------------------------------------------------
***** SUB WRTF
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTF
C---- ARG  :  
C---- DES  : write final line
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE WRTF(K)
      INTEGER           ITER,K

      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      WRITE(K,'(A3,74A1,T78,A3)')'*****',('*',ITER=1,74),'*****'
      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      WRITE(K,'(A3,T78,A3)')'*  ','  *'
      END
***** end of WRTF

***** ------------------------------------------------------------------
***** SUB WRTPHDHEADER
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTPHDHEADER
C---- ARG  :  
C---- DES  : write overall header.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTPHDHEADER(K)

C---- local variables                                                  *
      INTEGER           K
C      PARAMETER         (KUNIT=6)
      CHARACTER*5       SBEG,SEND

************************************************************************
      SBEG='*  '
      SEND='  *'

************************************************************************

C---- write header
      CALL WRTF(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T10,A,T78,A3)')SBEG,'PredictProtein: ',SEND
      WRITE(K,'(A3,T10,A,T78,A3)')SBEG,'~~~~~~~~~~~~~~~ ',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T10,A,T78,A3)')SBEG,
     +  'Secondary structure prediction by the PHD neural network',SEND
      WRITE(K,'(A3,T10,A,T78,A3)')'***  ',
     +  '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T10,A,T30,A,T78,A3)')SBEG,'Authors:',
     +     'Burkhard Rost & Chris Sander',SEND
      WRITE(K,'(A3,T10,A,T30,A,T78,A3)')SBEG,' ',
     +     'EMBL, Heidelberg, FRG',SEND
      WRITE(K,'(A3,T10,A,T30,A,T78,A3)')SBEG,' ',
     +     'Meyerhofstrasse 1, 69117 Heidelberg',SEND
      WRITE(K,'(A3,T10,A,T30,A,T78,A3)')SBEG,' ',
     +     'Internet: Predict-Help@EMBL-Heidelberg.DE',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T10,A,T30,A,T78,A3)')SBEG,'All rights reserved.',
     +     ' ',SEND
      CALL WRTE(K)
      CALL WRTF(K)
      CALL WRTE(K)
C---- about the prediction method
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'About the input to the network',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')'***  ',
     +    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The prediction is pe',
     +     'rformed by a system of neural networks.',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The input is a multi',
     +     'ple sequence alignment. It is taken from an HSSP   ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'file (produced by th',
     +     'e program MaxHom:                    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'Sander & Schneider (',
     +     '1991) Proteins, Vol.9, pp. 56-68   ',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'For optimal results ',
     +     'the alignment should contain sequences with varying',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'degrees of sequence ',
     +     'similarity relative to the input protein.          ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The following is an ',
     +   'ideal situation:',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    +-----------------+----------------------+',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    |   sequence:     |  sequence identity   |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    +-----------------+----------------------+',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    | target sequence |  100 %               |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    | aligned seq. 1  |   90 %               |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    | aligned seq. 2  |   80 %               |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    |      ...        |   ...                |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    | aligned seq. 7  |   30 %               |',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '    +-----------------+----------------------+',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      CALL WRTE(K)
C---- results: performance accuracy
      CALL WRTE(K)
      CALL WRTF(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'Estimated Accuracy of Prediction',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'A careful seven-fold',
     +     ' cross validation test on some 130 protein chains  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'(about 25,000 residu',
     +     'es) with less than 30% pairwise sequence identity  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'gave the following r',
     +     'esults:',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'   ++================++',
     +     '-----------------------------------------+     ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'   || Qtotal = 70.8% ||',
     +     '      ("overall three state accuracy")   |     ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'   ++================++',
     +     '-----------------------------------------+     ',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,' ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'    +------------------',
     +     '----------+-----------------------------+      ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'    | Qhelix (% of obse',
     +     'rved)=72% | Qhelix (% of predicted)=73% |      ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'    | Qstrand(% of obse',
     +     'rved)=66% | Qstrand(% of predicted)=60% |      ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'    | Qloop  (% of obse',
     +     'rved)=72% | Qloop  (% of predicted)=74% |      ',SEND
      WRITE(K,'(A3,T6,A23,A,T78,A3)')SBEG,'    +------------------',
     +     '----------+-----------------------------+      ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- definitions
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'These percentages are defined by:',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     'number of correctly predicted residues             ',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     'Qtotal =            ',
     +     '---------------------------------------      (*100)',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     '      number of all residues                       ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     'no of res correctly predicted to be in helix       ',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     'Qhelix (% of obs) = ',
     +     '-------------------------------------------- (*100)',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     'no of all res observed to be in helix              ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     'no of res correctly predicted to be in helix       ',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     'Qhelix (% of pred)= ',
     +     '-------------------------------------------- (*100)',SEND
      WRITE(K,'(A3,T6,A20,A,T78,A3)')SBEG,
     +     '                    ',
     +     'no of all residues predicted to be in helix        ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- single protein chains
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'Averaging over single chains',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The most reasonable ',
     +     'way to compute the overall accuracies is the above ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'quoted percentage of',
     +     ' correctly predicted residues.  However, since the ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'user is mainly inter',
     +     'ested in the expected performance of the prediction',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'for a particular pro',
     +     'tein, the mean value when averaging over protein   ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'chains might be of h',
     +     'elp as well.  Computing first the three state      ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'accuracy for each pr',
     +     'otein chain, and then averaging over 130 chains    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'yields the following',
     +     ' average:',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+-------------------------------====--+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| Qtotal/averaged over chains = 71.0% |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+-------------------------------====--+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| standard deviation          =  9.3% |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+-------------------------------------+',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- correlation coefficient
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'Further measures of performance',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'Matthews correlation coefficient:',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------------------------------------------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| Chelix = 0.60, Cstrand = 0.52, Cloop = 0.51 |',
     +     SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------------------------------------------+',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- length distribution
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +    'Average length of predicted secondary structure segments: ',
     +     SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '            +------------+----------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '            |  predicted | observed |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+-----------+------------+----------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| Lhelix  = |     9.3    |    9.1   |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| Lstrand = |     5.0    |    5.1   |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| Lloop   = |     6.0    |    5.8   |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+-----------+------------+----------+',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- accuracy matrix
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The accuracy matrix ',
     +     'in detail:',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------------------------------------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '|    number of residues with H, E, L    |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------+------+------+------+--------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '|         |net H |net E |net L |sum obs |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------+------+------+------+--------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| obs H   | 5344 |  582 | 1480 |   7406 |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| obs E   |  482 | 3265 | 1231 |   4978 |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| obs L   | 1445 | 1549 | 7831 |  10825 |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------+------+------+------+--------+',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '| sum Net | 7271 | 5396 |10542 |  23209 |',SEND
      WRITE(K,'(A3,T15,A,T78,A3)')SBEG,
     +     '+---------+------+------+------+--------+',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'Note: This table is ',
     +     'to be read in the following manner:                ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      5344 of all re',
     +      'sidues predicted to be in helix, were observed to ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      be in helix, 4',
     +     '82 however belong to observed strands, 1445 to     ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      observed loop ',
     +     'regions.  The term "observed" refers to the DSSP   ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      assignment of ',
     +     'secondary structure calculated from 3D coordinates ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      of experimenta',
     +     'lly determined structures (Dictionary of Secondary ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      Structure  of ',
     +     'Proteins: Kabsch & Sander (1983) Biopolymers, 22,  ',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,'      2577-2637). ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
C---- reliability
      CALL WRTE(K)
      CALL WRTF(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     'Position-specific reliability index',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The network predicts',
     +     ' the three secondary structure types using real    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'numbers from the out',
     +     'put units. The prediction is assigned by choosing  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'the maximal unit ("w',
     +     'inner takes all").  However, the real numbers      ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'contain additional i',
     +     'nformation.                                        ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'E.g. the difference ',
     +     'between the maximal and the second largest output  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'unit can be used to ',
     +     'derive a "reliability index".  This index is given ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'for each residue alo',
     +     'ng with the prediction.  The index is scaled to    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'have values between ',
     +    '0 (lowest reliability), and 9 (highest).',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The accuracies (Qtot',
     +     ') to be expected for residues with values above a  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'particular value of ',
     +     'the index are given below as well as the fraction  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'of such residues (%r',
     +     'es).:',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'Note: the data given',
     +     ' in the following two tables relates to a test set ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'----- which included',
     +     ' four chains of the membrane protein 1prc.  The    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      overall accura',
     +     'cy for this set is 70.2% instead of 70.8%.  The    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'      differences to',
     +     ' the set without membrane chains is marginal.      ',SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| index',
     +'|  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| %res ',
     +'|100.0| 99.5| 88.6| 76.9| 65.4| 54.0| 42.1| 29.6| 15.5|  3.3|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| Qtot ',
     +'| 70.2| 70.4| 73.0| 76.0| 78.7| 81.6| 84.6| 87.3| 92.4| 95.5|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| H%obs',
     +'| 69.0| 69.3| 72.2| 76.0| 79.6| 83.4| 87.2| 91.0| 96.1| 99.2|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| E%obs',
     +'| 63.3| 63.5| 66.4| 69.5| 72.4| 75.9| 78.9| 81.9| 85.5| 79.0|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| H%prd',
     +'| 72.0| 72.0| 75.1| 78.3| 80.9| 84.0| 87.1| 90.2| 92.9| 94.9|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| E%prd',
     +'| 57.9| 58.5| 61.8| 65.6| 69.1| 73.7| 78.6| 82.6| 89.1| 98.8|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The above table give',
     +     's the cumulative results, e.g. 65.4% of all        ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'residues have a reli',
     +     'ability of at least 4.  The overall three-state    ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'accuracy for this su',
     +     'bset of about two thirds of all residues is 78.5%. ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'For this subset, e.g',
     +     '., 79.6% of the observed helices are correctly     ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'predicted, and 80.9%',
     +     ' of all residues predicted to be in helix are      ',SEND
      WRITE(K,'(A3,T6,A,T78,A3)')SBEG,'correct.   ',SEND
      CALL WRTE(K)
      CALL WRTD(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'The following table ',
     +     'gives the non-cumulative quantities, i.e. the      ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'values per reliabili',
     +     'ty index range.  These numbers answer the question:',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'how reliable is the ',
     +     'prediction for all residues labeled with the       ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'particular index i.',
     +     ' ',SEND
      CALL WRTE(K)
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| index',
     +'|  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| %res ',
     +'| 10.9| 11.7| 11.5| 11.4| 11.9| 12.4| 14.1| 12.3|  3.2|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| Qtot ',
     +'| 47.0| 51.2| 59.5| 62.3| 70.1| 77.5| 84.0| 91.6| 95.5|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| H%obs',
     +'| 44.3| 44.9| 52.2| 58.4| 67.0| 74.8| 82.3| 94.9| 99.2|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| E%obs',
     +'| 44.1| 48.9| 56.4| 57.9| 66.6| 73.1| 78.6| 86.5| 79.0|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'|      ',
     +'|     |     |     |     |     |     |     |     |     |',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| H%prd',
     +'| 45.3| 50.3| 59.0| 63.0| 69.7| 77.0| 85.2| 92.1| 94.9|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'| E%prd',
     +'| 38.2| 42.5| 50.3| 52.0| 60.0| 70.9| 77.1| 87.8| 98.8|',
     +     SEND
      WRITE(K,'(A3,T6,A7,A,T78,A3)')SBEG,'+------',
     +'+-----+-----+-----+-----+-----+-----+-----+-----+-----+',
     +     SEND
      CALL WRTE(K)
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'For example, for r',
     +     'esidues with Relindex=5 60% of all predicted beta  ',SEND
      WRITE(K,'(A3,T6,A,T26,A,T78,A3)')SBEG,'strand residues are ',
     +     'correctly identified.                            ',SEND
      CALL WRTE(K)
      CALL WRTF(K)
      CALL WRTE(K)
      CALL WRTE(K)

      END
***** end of WRTPHDHEADER

***** ------------------------------------------------------------------
***** SUB WRTPOEXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTPOEXP
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTPOEXP(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         KUNIT
      CHARACTER*5     SBEG,SEND
      CHARACTER*7     INTERPROTNAME
      CHARACTER*222   TXT80
      LOGICAL         LTOPLINE

************************************************************************
*     SBRs called:   from library: lib-prot.f                         *
*                        STABLE_EXPNOINBIN, STABLE_EXPNOINSTATES       *
************************************************************************

C---- defaults
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- read header of HSSP file                -----
C--------------------------------------------------
      INTERPROTNAME(1:7)=PROTNAME(1)
C---- on printer
      CALL WRTF(KUNIT)
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Performance accuracy of the prediction',SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)

C--------------------------------------------------
C---- table for bins
C--------------------------------------------------

      TXT80='full protein'
C     ======================
      CALL STABLE_EXPNOINBIN(KUNIT,1,EXP_NOINBIN,TXT80,1)
C     ======================
      LTOPLINE=.TRUE.
C     ======================
      CALL STABLE_EXPSTATES(KUNIT,LTOPLINE,LTOPLINE,INTERPROTNAME,1,
     +     EXP_NOINBIN,T2,T3A,T3B,2,
     +     EXP_NOIN2ST,EXP_NOIN3ST,3,EXP_NOIN10ST,EXP_CORR,3)
C     ======================
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)
      END
***** end of WRTPOEXP

***** ------------------------------------------------------------------
***** SUB WRTPOPRED
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTPOPRED
C---- ARG  :  
C---- DES  : write prediction accuracy for SEC
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTPOPRED(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         KUNIT,DEVNOM,NSECEL_LOC
      CHARACTER*5     SBEG,SEND
      CHARACTER*7     INTERPROTNAME
      LOGICAL         LWRITE,LTABLE_QILS

************************************************************************
*     SBRs called:   from library: lib-prot.f                         *
*                        STABLEPOLENFILE, SEVALINFOFILE                *
************************************************************************

C---- defaults
      LTABLE_QILS=.TRUE.
      DEVNOM=50
      SBEG='*  '
      SEND='  *'
C---- blow up states for TM prediction
      IF (MODESECSTRON.EQ.'SECONDARY_HTM') THEN
         NSECEL_LOC=3
      ELSE
         NSECEL_LOC=NSECEL
      END IF
C--------------------------------------------------
C---- read header of HSSP file                -----
C--------------------------------------------------
      INTERPROTNAME(1:7)=PROTNAME(1)
C---- on printer
      CALL WRTF(KUNIT)
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     'Performance accuracy of the prediction',SEND
      WRITE(KUNIT,'(A3,T10,A,T78,A3)')SBEG,
     +     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',SEND
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)

C---- give only per-residue comparison
      IF (LTABLE_QILS .EQV. .FALSE.) THEN
C        ====================
         CALL STABLEPOLENFILE(KUNIT,NSECEL_LOC,NUMOUTMAX,INTERPROTNAME,
     +        MATNUM((NSECEL+1),(NSECEL+1)),TITLE,
     +        MATNUM,RMATQOFDSSP,RMATQOFPRED,Q3,SQ,CORR,MATLEN)
C        ====================
C------- computing the information (entropy)
         LWRITE=.TRUE.
C        ==================
         CALL SEVALINFOFILE(KUNIT,NSECEL_LOC,NUMOUTMAX,
     +        MATNUM,INFO,INFO_INV,LWRITE)
C        ==================
C---- give also segment comparison
      ELSE
C------- computing the information (entropy)
         LWRITE=.FALSE.
C        ==================
         CALL SEVALINFOFILE(6,NSECEL_LOC,NUMOUTMAX,
     +        MATNUM,INFO,INFO_INV,LWRITE)
C        ==================
C        ================
        CALL STABLE_QILS(KUNIT,NSECEL_LOC,NUMOUTMAX,NUMRES,1,
     +        INTERPROTNAME,TITLE,2,MATNUM,MATLEN,3,
     +        RMATQOFDSSP,RMATQOFPRED,Q3,SQ,CORR,INFO,INFO_INV,4,
     +        QSOV,QFOV,DEVNOM,4)
C        ================
      END IF
      CALL WRTE(KUNIT)
      CALL WRTE(KUNIT)
      END
***** end of WRTPOPRED

***** ------------------------------------------------------------------
***** SUB WRTPRED
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTPRED
C---- ARG  :  
C---- DES  : write secondary structure prediction.
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE WRTPRED(KUNIT)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local functions
      CHARACTER*222   FWRITE_STRING_NUMBERS
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         KUNIT,ITER,ITLINES,MUE1,MUE2,MUE3,MUE4,IHELP,
     +     IEND,IBEG,ITSEC,MUE5
      CHARACTER*4     CHARMACH1,CHARMACH2
      CHARACTER*5     SBEG,SEND
      CHARACTER*5     TXT(1:8)
      CHARACTER*222   INTERTXT,TXT80(1:30),VERSION,CHFILE,TXT_80
      CHARACTER*1     INTEROUTTXT(1:80),INTERDSSPTXT(1:80)
      LOGICAL         LWRITE_ACC80,LWRT80

************************************************************************
*     SBRs called:   WRTBOTHHEADER, WRTPREDHEADER, WRTWARN_NOALI,     *
*                     WRTPOPRED                                        *
************************************************************************

C---- br 2003-08-23: bullshit to avoid warning
      TXT_80=' '

C---- defaults
      LWRT80=.FALSE.
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- read header of HSSP file                -----
C--------------------------------------------------
      IEND=FILEN_STRING(FILE_HSSP_NOCHAIN)
      CHFILE(1:IEND)=FILE_HSSP_NOCHAIN(1:IEND)
C      CALL SFILEOPEN(15,CHFILE(1:IEND),'UNKNOWN',222,' ')
C      CALL SFILEOPEN(15,FILE_HSSP_NOCHAIN,'OLD',222,'READONLY')
Cyy      OPEN (15,FILE=FILE_HSSP_NOCHAIN,STATUS='OLD',READONLY)
      OPEN (15,FILE=FILE_HSSP_NOCHAIN,STATUS='OLD')
      DO ITER=1,30
         READ(15,'(A80)')TXT80(ITER)
      END DO
      CLOSE(15)
C---- store begin an end of text to be written
      IEND=30
      DO ITER=1,30
         INTERTXT=TXT80(ITER)
         IF (INTERTXT(1:6).EQ.'HEADER') THEN
            IBEG=ITER
         ELSEIF (INTERTXT(1:6).EQ.'NALIGN') THEN
            IEND=ITER
         END IF
      END DO
C----------------------------------------
C---- enough residues in intervall > 5 ?
C----------------------------------------

      IHELP=0
      DO MUE1=1,NUMRES
         IF (RELIND(MUE1).GE.5) THEN
            IHELP=IHELP+1
         END IF
      END DO
      IF ( (IHELP.GT.5).AND.(MODESECSTRON.EQ.'SECONDARY') ) THEN
         LWRITE_ACC80=.TRUE.
      ELSE
         LWRITE_ACC80=.FALSE.
      END IF
C--------------------------------------------------
C---- write header into prediction output     -----
C--------------------------------------------------
      IF (MODESECSTRON.EQ.'SECONDARY') THEN
         VERSION=VERSION_SEC
      ELSE
         VERSION=VERSION_HTM
      END IF
C     ==================
      CALL WRTBOTHHEADER(KUNIT,VERSION,NUMNETJURY,TXT80,IBEG,IEND)
C     ==================

C     ==================
      CALL WRTPREDHEADER(KUNIT,MODESECSTRON,LOUTBINPROB,LWRITE_ACC80)
C     ==================
C--------------------------------------------------
C---- write warning if too few sequences in ali ---
C--------------------------------------------------

      IF (NUMNALIGN(1).LE.5) THEN
C        ===================
         CALL WRTWARN_NOALI(KUNIT,MODESECSTRON)
C        ===================
      END IF
C--------------------------------------------------
C---- now write the prediction for all        -----
C--------------------------------------------------
C---- machine readable version of strips?
      IF (LMACHINE_READABLE .EQV. .TRUE.) THEN
         CHARMACH1='# 1 '
         CHARMACH2='# 2 '
      ELSE
         CHARMACH1='    '
         CHARMACH2='    '
      END IF
      IF (LOUTBINPROB .EQV. .TRUE.) THEN
         TXT(1)='prH-|'
         TXT(2)='prE-|'
         TXT(NSECEL)='prL-|'
         IF (NSECEL.EQ.4) THEN
            TXT(3)='prT-|'
         END IF
      END IF
C---- data
      WRITE(KUNIT,'(A3,T10,A,T25,A7,T40,A,T50,I5,T78,A3)')SBEG,
     +     'protein:',PROTNAME(1),'length',NUMRES,SEND
      CALL WRTE(KUNIT)
      WRITE(KUNIT,*)

C---- strings of 80: for easy evaluation upon look
      MUE1=0
      MUE2=0
      MUE3=0
      MUE4=0

      DO ITLINES=1,(INT(NUMRES/60.)+1)
         WRITE(KUNIT,'(A4,T10,A4,T15,A60)')CHARMACH1,'    ',
     +        FWRITE_STRING_NUMBERS(LWRT80,ITLINES)
         IHELP=(ITLINES-1)*60
         IEND=MIN(60,(NUMRES-IHELP))

         WRITE(KUNIT,'(A4,T10,A5,T15,60A1,A1)')CHARMACH1,'AA  |',
     +        (RESNAME(MUE1),MUE1=(IHELP+1),(IHELP+IEND)),'|'
C----------------------------------------
C------- observed secondary structure ---
C----------------------------------------

         IF (LDSSPREAD .EQV. .TRUE.) THEN
            DO MUE4=1,IEND
               IF ((RESSECSTR(IHELP+MUE4).EQ.'L').OR.
     +              (RESSECSTR(IHELP+MUE4).EQ.'S').OR.
     +              (RESSECSTR(IHELP+MUE4).EQ.'T')) THEN
                  INTERDSSPTXT(MUE4)=' '
               ELSE
                  INTERDSSPTXT(MUE4)=RESSECSTR(IHELP+MUE4)
               END IF
            END DO
            WRITE(KUNIT,'(A4,T10,A5,T15,60A1,A1)')CHARMACH1,'Obs |',
     +           (INTERDSSPTXT(MUE4),MUE4=1,IEND),'|'
         END IF
C----------------------------------------
C------- prediction                 -----
C----------------------------------------

         IF (LFILTER .EQV. .TRUE.) THEN
            DO MUE2=1,IEND
               IF (OUTBINCHARFIL(IHELP+MUE2).EQ.'L') THEN
                  INTEROUTTXT(MUE2)=' '
               ELSE
                  INTEROUTTXT(MUE2)=OUTBINCHARFIL(IHELP+MUE2)
               END IF
            END DO
            WRITE(KUNIT,'(A4,T10,A5,T15,60A1,A1)')CHARMACH1,'PHD |',
     +           (INTEROUTTXT(MUE4),MUE4=1,IEND),'|'
         ELSE
            WRITE(KUNIT,'(A4,T10,A5,T15,60A1,A1)')CHARMACH1,'PHD |',
     +           (OUTBINCHAR(MUE2),MUE2=(IHELP+1),(IHELP+IEND)),'|'
         END IF
C----------------------------------------
C------- reliability index          -----
C----------------------------------------

C------- if because DEC has difficulties with the integer when <60
         IF (IEND.EQ.60) THEN
            WRITE(KUNIT,'(A4,T10,A5,T15,60I1,A1)')CHARMACH1,'Rel |',
     +           (RELIND(MUE3),MUE3=(IHELP+1),(IHELP+IEND)),'|'
         ELSE
C---- br 99.01 changed
C            TXT_80=' '
C 110        FORMAT(I1)
C            DO MUE3=(IHELP+1),(IHELP+IEND)
C               WRITE(TXT_80(MUE3:MUE3),110,ERR=19991)RELIND(MUE3)
C            END DO

C            WRITE(KUNIT,'(A4,T10,A5,T15,A,A1)')CHARMACH1,'Rel |',
C     +           TXT_80((IHELP+1):(IHELP+IEND)),'|'
C---- br 99.03 changed again, SGI hick-up!!!            
            WRITE(KUNIT,'(A4,T10,A5,T15,60I1)')CHARMACH1,'Rel |',
     +           (RELIND(MUE3),MUE3=(IHELP+1),(IHELP+IEND))

19991       CONTINUE
CCC---------- for DEC
CC            IF (LDEC) THEN
CC               WRITE(KUNIT,'(A4,T10,A5,T15,60I1)')CHARMACH1,'Rel |',
CC     +              (RELIND(MUE3),MUE3=(IHELP+1),(IHELP+IEND))
CC            ELSE
CCC---------- for others
CC               WRITE(KUNIT,'(A4,T10,A5,T15,60I1,A1)')CHARMACH1,'Rel |',
CC     +              (RELIND(MUE3),MUE3=(IHELP+1),(IHELP+IEND)),'|'
CC            END IF
         END IF
C----------------------------------------
C------- probabilities              -----
C----------------------------------------

         IF (LOUTBINPROB .EQV. .TRUE.) THEN
            WRITE(KUNIT,'(T2,A)')'detail:'
            DO ITSEC=1,NSECEL
               WRITE(KUNIT,'(A4,T10,A5,T15,60I1)')CHARMACH2,TXT(ITSEC),
     +              (OUTBINPROB(ITSEC,MUE5),MUE5=(IHELP+1),(IHELP+IEND))
            END DO
         END IF
C--------------------------------------------------
C---- now write the prediction for 81.9%      -----
C--------------------------------------------------
         IF (LWRITE_ACC80 .EQV. .TRUE.) THEN
            DO MUE3=1,IEND
               IF (RELIND(IHELP+MUE3).GE.5 .AND.
     +              (MUE3.GT.0)) THEN
                  IF ((OUTBINCHAR(IHELP+MUE3).EQ.'H').OR.
     +                 (OUTBINCHAR(IHELP+MUE3).EQ.'E')) THEN
                     INTEROUTTXT(MUE3)=OUTBINCHAR(IHELP+MUE3)
                  ELSE
                     INTEROUTTXT(MUE3)='L'
                  END IF
               ELSE
                  INTEROUTTXT(MUE3)='.'
               END IF
            END DO
            WRITE(KUNIT,'(T2,A,T10,A5,T15,60A1,A1)')'subset:',
     +           'SUB |',(INTEROUTTXT(MUE2),MUE2=1,IEND),'|'
         END IF
         WRITE(KUNIT,*)
         WRITE(KUNIT,*)
      END DO
C--------------------------------------------------
C---- compute accuracy if DSSP available      -----
C--------------------------------------------------
      IF (LDSSPREAD .EQV. .TRUE.) THEN
C        ==============
         CALL WRTPOPRED(KUNIT)
C        ==============
      END IF
      END
***** end of WRTPRED

***** ------------------------------------------------------------------
***** SUB WRTPREDHEADER
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTPREDHEADER
C---- ARG  :  
C---- DES  : write secondary structure prediction header
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTPREDHEADER(KUNIT,MODE,LOUTBINPROB,LWRITE_ACC80)

C---- local variables
      INTEGER         KUNIT
      CHARACTER*5     SBEG,SEND
      CHARACTER*222   MODE
      LOGICAL         LOUTBINPROB,LWRITE_ACC80

************************************************************************

C---- defaults
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- abbreviations used
C--------------------------------------------------

      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' Abbreviations:',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,' --------------',SEND
      CALL WRTE(KUNIT)
      IF (MODE.EQ.'SECONDARY') THEN 
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' secondary structure : H=helix, E=extended (sheet), blank=re'//
     +     'st (loop)',SEND
      ELSE
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' secondary structure : H=helical trans-membrane regions, L='//
     +     'rest (loop)',SEND
      END IF
      CALL WRTE(KUNIT)
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                   AA: amino acid sequence',SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  PHD: Profile network prediction HeiDelberg',
     +     SEND
      WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  Rel: Reliability index of prediction (0-9)',
     +     SEND
      CALL WRTE(KUNIT)

      IF (LOUTBINPROB .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' detail:          ',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  prH: ''probability'' for assigning helix',SEND
         IF (MODE.EQ.'SECONDARY') THEN 
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  prE: ''probability'' for assigning strand',
     +        SEND
         END IF
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  prL: ''probability'' for assigning loop',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                 note: the ''probabilites'' are scaled to the'//
     +        ' interval 0-9, ',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       i.e. prH=5 means, that the signal at'//
     +        ' the first',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       output node is 0.5-0.6.',SEND
         CALL WRTE(KUNIT)
      END IF

      IF (LWRITE_ACC80 .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + ' subset:',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  SUB: a subset of the prediction, for all '//
     +        'residues with',SEND
         IF (MODE.EQ.'SECONDARY') THEN 
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       an expected accuracy > 82% (see '//
     +        ' tables in header)',SEND
         ELSE
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       an expected accuracy > 95% (see '//
     +        ' tables in header)',SEND
         END IF
         IF (MODE.EQ.'SECONDARY') THEN 
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                 note: for this subset the following '//
     +           'symbols are used:',SEND
         ELSE
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '               note 1: for this subset the following '//
     +           'symbols are used:',SEND
         END IF
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                    L: is loop (for which above " " is used)',
     +        SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                  ".": means that no prediction is made '//
     +        'for this residue,',SEND
         WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       as the reliability is Rel < 5',SEND
         IF (MODE(1:13).EQ.'SECONDARY_HTM') THEN 
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '               note 2: the subset may contain continuous'//
     +           ' helical segments,',SEND
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       where the row ''PHD'' has loop'//
     +           ' loop regions.  The',SEND
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       reason is that the prediction had'//
     +           ' been filtered.  ',SEND
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       In the row ''SUB'' the non-filtered'//
     +           ' prediction is',SEND
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       given to indicate that the loop'//
     +           ' is probably very',SEND
            WRITE(KUNIT,'(A3,A,T78,A3)')SBEG,
     + '                       short!',SEND
         END IF
         CALL WRTE(KUNIT)
      END IF
      CALL WRTF(KUNIT)
      END
***** end of WRTPREDHEADER

***** ------------------------------------------------------------------
***** SUB WRTWARN_NOALI
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRTWARN_NOALI
C---- ARG  :  
C---- DES  : write warning: no ali existing!
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
      SUBROUTINE WRTWARN_NOALI(KUNIT,MODE)

C---- global parameters                                                *
      INCLUDE 'phdParameter.f'

C---- local variables
      INTEGER         KUNIT
      CHARACTER*5     SBEG,SEND
      CHARACTER*222   MODE

************************************************************************

C---- defaults
      SBEG='*  '
      SEND='  *'
C--------------------------------------------------
C---- write warning if too few sequences in ali ---
C--------------------------------------------------

      WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,'WARNING:',SEND
      WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,'========',SEND
      CALL WRTE(KUNIT)
      IF (MODE(1:13).EQ.'SECONDARY_HTM') THEN
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'Expected accuracy is about 94% if, and only if, '//
     +        'the alignment contains',SEND
      ELSEIF (MODE(1:9).EQ.'SECONDARY') THEN
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'Expected accuracy is about 72% if, and only if, '//
     +        'the alignment contains',SEND
      ELSEIF (MODE(1:8).EQ.'EXPOSURE') THEN
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'Expected correlation is about 0.55 if, and only if, '//
     +        'alignment contains',SEND
      END IF
      IF (NUMNALIGN(1).EQ.1) THEN
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'sufficient information.  For your sequence there '//
     +        'was no homologue in',SEND
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'the current version of Swissprot detected.  This '//
     +        'implies that the',SEND
         IF (MODE.EQ.'SECONDARY') THEN
            WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +           'expected accuracy is about 6-10 percentage'//
     +           ' points lower !',SEND
         ELSEIF (MODE.EQ.'SECONDARY_HTM') THEN
            WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +           'expected accuracy is about  2-5 percentage'//
     +           ' points lower !',SEND
         ELSEIF (MODE.EQ.'EXPOSURE') THEN
            WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +           'expected correlation is about 0.1 lower!',SEND
         END IF
      ELSE
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'sufficient information.  For your sequence there '//
     +        'were not many',SEND
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'homologues in the current version of Swissprot '//
     +        'detected.  This ',SEND
         WRITE(KUNIT,'(A3,T5,A,T78,A3)')SBEG,
     +        'implies that the expected accuracy is some '//
     +        'percentage points lower !',SEND
      END IF
      CALL WRTE(KUNIT)
      CALL WRTF(KUNIT)
      CALL WRTE(KUNIT)
      END
***** end of WRTWARN_NOALI

C vim:et:ts=2:
