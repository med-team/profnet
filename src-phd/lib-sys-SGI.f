***** ------------------------------------------------------------------
***** FCT FCTIME_DATE
***** ------------------------------------------------------------------
C---- 
C---- NAME : FCTIME_DATE
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Oct,        2003        version 1.0    *
*     EMBL/LION                 http://www.predictprotein.org/         *
*     D-69012 Heidelberg        rost@columbia.edu                      *
*                      changed: Aug,        2003        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        returns date                                     *
*     note:           machine type dependent:                          *
*                     SGI, UNIX, LINUX: absolute unix time             *
*                     IBM: char*8 YYYYMMDD                             *
*     input :         NOM, DEN                                         *
*----------------------------------------------------------------------*
      CHARACTER*24 FUNCTION FCTIME_DATE()
      IMPLICIT        NONE
******------------------------------*-----------------------------******
*     execution of function                                            *
C      FCTIME_DATE=FDATE()
      FCTIME_DATE=''
      END
***** end of FCTIME_DATE

***** ------------------------------------------------------------------
***** FCT FRTIME_SECNDS
***** ------------------------------------------------------------------
C---- 
C---- NAME : FRTIME_SECNDS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Oct,        2003        version 1.0    *
*     EMBL/LION                 http://www.predictprotein.org/         *
*     D-69012 Heidelberg        rost@columbia.edu                      *
*                      changed: Aug,        2003        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        returns CPU time seconds                         *
*     note:           machine type dependent:                          *
*                     SGI, UNIX, LINUX: absolute unix time             *
*                     IBM: cputime                                     *
*     input :         NOM, DEN                                         *
*----------------------------------------------------------------------*
      REAL FUNCTION FRTIME_SECNDS(T1)
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      REAL            T1
******------------------------------*-----------------------------******
*     execution of function                                            *
      FRTIME_SECNDS=SECNDS(T1)
      END
***** end of FRTIME_SECNDS

***** ------------------------------------------------------------------
***** SUB SCFDATE
***** ------------------------------------------------------------------
C---- 
C---- NAME : SCFDATE
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE SCFDATE(ITERCALL,LOGIWRITE,DATEOLD)

      IMPLICIT         NONE

      CHARACTER*24     ACTDATE,DATEOLD,CTEMP
      INTEGER          ITER,ITERCALL
      LOGICAL          LOGIWRITE

C
C      ACTDATE=FDATE()
      ACTDATE=''
C ibm
C      CTEMP=' '
C      CALL DATE_AND_TIME(CTEMP)
C      ACTDATE=CTEMP

      IF (LOGIWRITE .EQV. .TRUE.) THEN
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
         IF (ITERCALL.EQ.2) THEN
            WRITE(6,'(T10,A11,A24)')'started:   ',DATEOLD
            WRITE(6,'(T10,A11,A24)')'  ended:   ',ACTDATE
         ELSE
            WRITE(6,'(T10,A11,A24)')'   time:   ',ACTDATE
         END IF
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
      END IF
      IF (ITERCALL.EQ.1) THEN
         DATEOLD=ACTDATE
      END IF

      END
***** end of SCFDATE

***** ------------------------------------------------------------------
***** SUB SRDTIME
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRDTIME
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
***                                                                  ***
***                                                                  ***
***   SUBROUTINE SRDTIME                                             ***
***                                                                  ***
***                                                                  ***
*----------------------------------------------------------------------*
      SUBROUTINE SRDTIME(LOGIWRITE)

      IMPLICIT         NONE

Cunix
      REAL             TIMEARRAYM,TIMEDIFF,DTIME,TIME_TMP
Clinux
C      REAL             TIMEARRAYM(1:2),TIMEDIFF,DTIME,TIME_TMP
      INTEGER          ITER
      LOGICAL          LOGIWRITE


C---- br 2003-08-23: bullshit to avoid warning
      TIME_TMP=        0.0
Cunix
      TIMEDIFF=DTIME(TIMEARRAYM,TIME_TMP)
      TIMEDIFF=DTIME(TIMEARRAYM)
Clinux
C      TIMEDIFF=DTIME(TIMEARRAYM)
Cibm
C      TIMEDIFF=DTIME(TIMEARRAYM)
C      CALL CPU_TIME(TIMEDIFF)
      IF (LOGIWRITE .EQV. .TRUE.) THEN
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
         WRITE (6,'(T10,A12,T25,F9.3,A5)')
     +        'total time: ',TIMEDIFF,'  sec'
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
      END IF

      END
***** end of SRDTIME

