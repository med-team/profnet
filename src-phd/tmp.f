      program test
      CHARACTER*24 FCTIME_DATE
      CHARACTER*24 CTEMP
      write(6,*)'xx =',FCTIME_DATE()
      END

      CHARACTER*24 FUNCTION FCTIME_DATE()
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      CHARACTER*24    CTEMP,CTEMP2
******------------------------------*-----------------------------******
*     execution of function                                            *
      CTEMP=' '
      CALL DATE_AND_TIME(CTEMP,CTEMP2)

      FCTIME_DATE=       ' '
      FCTIME_DATE(1:4)=  CTEMP(1:4)
      FCTIME_DATE(5:5)=  '_'
      FCTIME_DATE(6:7)=  CTEMP(5:6)
      FCTIME_DATE(8:8)=  '_'
      FCTIME_DATE(9:10)= CTEMP(7:8)
      FCTIME_DATE(11:13)=' - '
      FCTIME_DATE(14:15)=CTEMP2(1:2)
      FCTIME_DATE(16:16)=':'
      FCTIME_DATE(17:18)=CTEMP2(3:4)
      FCTIME_DATE(19:19)=':'
      FCTIME_DATE(20:21)=CTEMP2(5:6)
      END
