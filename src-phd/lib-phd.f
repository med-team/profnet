***** ------------------------------------------------------------------
***** FCT EMPTYSTRING
***** ------------------------------------------------------------------
C---- 
C---- NAME : EMPTYSTRING
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      FUNCTION EMPTYSTRING(STRING)
      LOGICAL         EMPTYSTRING
      CHARACTER*222   STRING
      EMPTYSTRING=.TRUE.
      DO I=1,LEN(STRING)  
         IF(STRING(I:I).NE.' ') THEN
            EMPTYSTRING=.FALSE.
            RETURN
         ENDIF
      ENDDO
      RETURN
      END
***** end of EMPTYSTRING

***** ------------------------------------------------------------------
***** FCT FC_REXP_TO_3STCHAR
***** ------------------------------------------------------------------
C---- 
C---- NAME : FC_REXP_TO_3STCHAR
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Returns 'b', 'i', or 'e' for the exposure state. *
*     input:          REXP (real 0-1), T3A (real 0-1), T3B (real 0-1)  *
*     output:         B, if REXP < T3A                                 *
*     -------         I, if T3A <= REXP < T3B                          *
*                     I, if REXP >= T3B                                *
*----------------------------------------------------------------------*
      CHARACTER*1 FUNCTION FC_REXP_TO_3STCHAR(REXP,T3A,T3B)

      IMPLICIT        NONE
C---- variables passed
      REAL            REXP,T3A,T3B
******------------------------------*-----------------------------******
      IF (REXP.LT.T3A) THEN
         FC_REXP_TO_3STCHAR='b'
      ELSEIF (REXP.GE.T3B) THEN
         FC_REXP_TO_3STCHAR='e'
      ELSE
         FC_REXP_TO_3STCHAR='i'
      END IF
      END
***** end of FC_REXP_TO_3STCHAR

***** ------------------------------------------------------------------
***** FCT FC_REXPI_TO_3STCHAR
***** ------------------------------------------------------------------
C---- 
C---- NAME : FC_REXPI_TO_3STCHAR
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Returns 'b', 'i', or 'e' for the exposure state. *
*     note:           in contrast to FC_REXP_TO_3STCHAR this works on  *
*                     integer values                                   *
*     input:          REXP (real 0-1), T3A (real 0-1), T3B (real 0-1)  *
*     output:         B, if REXP < T3A                                 *
*     -------         I, if T3A <= REXP < T3B                          *
*                     I, if REXP >= T3B                                *
*----------------------------------------------------------------------*
      CHARACTER*1 FUNCTION FC_REXPI_TO_3STCHAR(IREXP,IT3A,IT3B)

      IMPLICIT        NONE
C---- variables passed
      INTEGER         IREXP,IT3A,IT3B
******------------------------------*-----------------------------******
      IF (IREXP.LT.IT3A) THEN
         FC_REXPI_TO_3STCHAR='b'
      ELSEIF (IREXP.GE.IT3B) THEN
         FC_REXPI_TO_3STCHAR='e'
      ELSE
         FC_REXPI_TO_3STCHAR='i'
      END IF
      END
***** end of FC_REXP_TO_3STCHAR

***** ------------------------------------------------------------------
***** FCT FCCONV_LOWCASE_AA_TO_CYS
***** ------------------------------------------------------------------
C---- 
C---- NAME : FCCONV_LOWCASE_AA_TO_CYS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        [a-z] -> C                                       *
*----------------------------------------------------------------------*
      CHARACTER*1 FUNCTION FCCONV_LOWCASE_AA_TO_CYS(AA)
      IMPLICIT        NONE
      CHARACTER*1     AA
******------------------------------*-----------------------------******

      IF ( (AA.EQ.'a').OR.(AA.EQ.'b').OR.
     +     (AA.EQ.'c').OR.(AA.EQ.'d').OR.
     +     (AA.EQ.'e').OR.(AA.EQ.'f').OR.
     +     (AA.EQ.'g').OR.(AA.EQ.'h').OR.
     +     (AA.EQ.'i').OR.(AA.EQ.'j').OR.
     +     (AA.EQ.'k').OR.(AA.EQ.'l').OR.
     +     (AA.EQ.'m').OR.(AA.EQ.'n').OR.
     +     (AA.EQ.'o').OR.(AA.EQ.'p').OR.
     +     (AA.EQ.'q').OR.(AA.EQ.'r').OR.
     +     (AA.EQ.'s').OR.(AA.EQ.'t').OR.
     +     (AA.EQ.'u').OR.(AA.EQ.'v').OR.
     +     (AA.EQ.'w').OR.(AA.EQ.'x').OR.
     +     (AA.EQ.'y').OR.(AA.EQ.'z')) THEN
         FCCONV_LOWCASE_AA_TO_CYS='C'
      ELSE
         FCCONV_LOWCASE_AA_TO_CYS=AA
      END IF
      END
***** end of FCCONV_LOWCASE_AA_TO_CYS

***** ------------------------------------------------------------------
***** FCT FCUT_SPACES
***** ------------------------------------------------------------------
C---- 
C---- NAME : FCUT_SPACES
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        cuts off spaces from string of *80               *
*     ext. library:   FILEN_STRING,                                    *
*----------------------------------------------------------------------*
      FUNCTION FCUT_SPACES(STRING)

      IMPLICIT        NONE
C---- variables passed
      CHARACTER*222   FCUT_SPACES
      CHARACTER*(*)   STRING
C---- local variables
      INTEGER         ITER,ITER2,COUNT
      CHARACTER*222   ABCUP,ABCLOW,NUMSIG
      LOGICAL         LHELP
******------------------------------*-----------------------------******
C---- defaults
      ABCUP ='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      ABCLOW='abcdefghijklmnopqrstuvwxyz'
      NUMSIG='1234567890-._/=+'
      FCUT_SPACES='x'


      COUNT=0
      DO ITER=1,222
         LHELP=.TRUE.
         DO ITER2=1,26
            IF (LHELP.EQV. .TRUE. 
     +           .AND.STRING(ITER:ITER).EQ.ABCUP(ITER2:ITER2)) THEN
               LHELP=.FALSE.
            END IF
            IF (LHELP.EQV. .TRUE. .AND.
     +           (STRING(ITER:ITER).EQ.ABCLOW(ITER2:ITER2)))  THEN
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            DO ITER2=1,16
               IF (LHELP.EQV. .TRUE. .AND.
     +              (STRING(ITER:ITER).EQ.NUMSIG(ITER2:ITER2)))  THEN
                  LHELP=.FALSE.
               END IF
            END DO
         END IF
         IF (LHELP.EQV. .FALSE.) THEN
            COUNT=COUNT+1
            FCUT_SPACES(COUNT:COUNT)=STRING(ITER:ITER)
         END IF
      END DO
      END
***** end of FCUT_SPACES

***** ------------------------------------------------------------------
***** FCT FIEXP_NOINBIN_SUM
***** ------------------------------------------------------------------
C---- 
C---- NAME : FIEXP_NOINBIN_SUM
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        All values of the matrix EXP_NOINBIN are summed  *
*     --------        which fulfill: TL <= i < TU.                     *
*                     MODE='obs' -> sum M(i,j) over i                  *
*                     MODE='pred' -> sum M(i,j) over j                 *
*     input :         EXP_NOINBIN, TL, TU, MODE                        *
*     output :        number of occurences                             *
*----------------------------------------------------------------------*
      INTEGER FUNCTION FIEXP_NOINBIN_SUM(EXP_NOINBIN,TL,TU,MODE)

      IMPLICIT        NONE

C---- variables passed from/to SBR calling
      INTEGER         EXP_NOINBIN(0:9,0:9),TL,TU,SUM
      CHARACTER*(*)   MODE
C---- local variables
      INTEGER         ITOBS,ITPRED
******------------------------------*-----------------------------******
*     EXP_NOINBIN(i,j)=n means n residues observed in state i and      *
*                     predicted in state j.  Where the relative        *
*                     solvent accessibility is (i*i), or (j*j).        *
*     Mode  =                                                          *
*        "obs"        all occurences observed with TL <= i < TU        *
*                     are counted.                                     *
*        "pred"       all occurences predicted with TL <= i < TU       *
*                     are counted.                                     *
*     TL,TU           Counted are all cases with TL <= i < TU          *
******------------------------------*-----------------------------******

C---- parameter check
      IF ( (TL.LT.0).OR.(TU.GT.10) ) THEN
         WRITE(6,'(T2,A,T10,A)')'***','FIEXP_NOINBIN_SUM: '//
     +        'thresholds wrong! '
         WRITE(6,'(T2,A,T10,A,T20,i2,a,t30,i2)')
     +        '***','TL= ',TL,'  TU = ',TU
         STOP
      END IF
C---- summing up
      SUM=0
      IF (MODE.EQ.'obs') THEN
         DO ITOBS=TL,(TU-1)
            DO ITPRED=0,9
               SUM=SUM+EXP_NOINBIN(ITOBS,ITPRED)
            END DO
         END DO
      ELSEIF (MODE.EQ.'pred') THEN
         DO ITPRED=TL,(TU-1)
            DO ITOBS=0,9
               SUM=SUM+EXP_NOINBIN(ITOBS,ITPRED)
            END DO
         END DO
      ELSE
         WRITE(6,'(T2,A,T10,A)')'***','FIEXP_NOINBIN_SUM: '//
     +        'mode should be either "observed" or "predicted".'
         WRITE(6,'(T2,A,T10,A,T40,A)')'***','However, it is:',MODE
      END IF
      FIEXP_NOINBIN_SUM=SUM
      END
***** end of FIEXP_NOINBIN_SUM

***** ------------------------------------------------------------------
***** FCT FILEN_STRING
***** ------------------------------------------------------------------
C---- 
C---- NAME : FILEN_STRING
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1993        version 0.1    *
*                      changed: Oct,        1994        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The length of a given character string is returned
*     input:          STRING     string of CHARACTER*222               *
*     output:         LEN        length of string without blanks       *
*----------------------------------------------------------------------*
      INTEGER FUNCTION FILEN_STRING(STRING)
C---- variables passing
      CHARACTER       STRING*(*)
C---- local variables
      INTEGER         ITER,ITER2,COUNT,LEADSPCCNT,NCHAR
      CHARACTER*222   CHECK
      LOGICAL         LHELP,LHELP2
******------------------------------*-----------------------------******
C---- defaults
      FILEN_STRING=            0
      CHECK(1:52)=
     +     'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
      CHECK(53:67)='1234567890-._/+'
      NCHAR=67

      LHELP=.TRUE.
      COUNT=0
      LEADSPCCNT=0
      DO ITER=1,len(STRING)
         IF (LHELP .EQV. .TRUE.) THEN
            LHELP2=.FALSE.
            DO ITER2=1,NCHAR
               IF ((LHELP2.EQV. .FALSE.).AND.
     +              STRING(ITER:ITER).EQ.CHECK(ITER2:ITER2)) THEN
                  LHELP2=.TRUE.
                  COUNT=COUNT+1
               END IF
            END DO
            IF (LHELP2.EQV. .FALSE.) THEN
               IF ( (COUNT.EQ.0).AND.(STRING(ITER:ITER).eq.' ') ) THEN
                  LEADSPCCNT=LEADSPCCNT+1
               ELSE
                  LHELP=.FALSE.
                  FILEN_STRING=ITER-1-LEADSPCCNT
               END IF
            ELSE
               FILEN_STRING=ITER-LEADSPCCNT
            END IF
         END IF
      END DO

      IF (COUNT.NE.FILEN_STRING) THEN
         WRITE(6,'(T2,A)')'-!-'
         WRITE(6,'(T2,A,T10,A)')'-!-','WARNING FILEN_STRING:'//
     +        'two different results.'
         WRITE(6,'(T2,A,T10,A,T20,I5,T30,A,T40,I5,T50,A,T60,A1,A,A1)')
     +        '-!-','first = ',FILEN_STRING,
     +        'count: ',COUNT,'for: ','|',STRING,'|'
         WRITE(6,'(T2,A)')'-!-'
      END IF

      FILEN_STRING=COUNT
      END
***** end of FILEN_STRING

***** ------------------------------------------------------------------
***** FCT FILENSTRING
***** ------------------------------------------------------------------
C---- 
C---- NAME : FILENSTRING
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The length of a given character string is returned
*     input:          STRING     string of CHARACTER*222               *
*     output:         LEN        length of string without blanks       *
*----------------------------------------------------------------------*
      INTEGER FUNCTION FILENSTRING(STRING)
C---- variables passing
      CHARACTER       STRING*(*)
C---- local variables
      INTEGER         ICOUNT,ITER
      LOGICAL         LHELP
******------------------------------*-----------------------------******
C---- defaults
      ICOUNT=0
      LHELP=.TRUE.
      DO ITER=1,80
         IF (LHELP.EQV. .TRUE. .AND.(STRING(ITER:ITER).NE.' ')) THEN
            ICOUNT=ICOUNT+1
         ELSE
            LHELP=.FALSE.
         END IF
      END DO
      FILENSTRING=ICOUNT
      END
***** end of FILENSTRING

***** ------------------------------------------------------------------
***** FCT FIRELIND_2ND
***** ------------------------------------------------------------------
C---- 
C---- NAME : FIRELIND_2ND
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Returns the difference between largest and 2nd   *
*     --------        largest component of a vector, projected onto a  *
*                     grid 0-9. Note: SCALE gives the maxima of RVEC   *
*     input:          RVEC, NCOLMAX, NCOL, SCALE                       *
*     output:         0-9                                              *
*----------------------------------------------------------------------*
      INTEGER FUNCTION FIRELIND_2ND(NCOLMAX,NCOL,RVEC,SCALE)

      IMPLICIT        NONE
C---- variables passed
      INTEGER         NCOLMAX,NCOL
      REAL            RVEC(1:NCOLMAX),SCALE
C---- local variables
      INTEGER         ITCOL,IMAX
      REAL            MAX,MAX2,DIFF,SCALE_LOC
******------------------------------*-----------------------------******
      IMAX=           0
C---- check limits
      IF (NCOL.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A,T20,I5,T30,A,T40,I5)')'***',
     +       'ERROR in lib-prot.f Function FIRELIND_2nd: NROW OR NCOL=0'
         WRITE(6,'(T2,A,T10,A)')'***','ncol=',ncol
         STOP
      END IF
C---- for security
      IF (SCALE.LT.1) THEN
         SCALE_LOC=1.
      ELSE
         SCALE_LOC=SCALE
      END IF
C---- compute maximum and 2nd largest
      MAX=0
      DO ITCOL=1,NCOL
         IF (RVEC(ITCOL).GT.MAX) THEN
            MAX=RVEC(ITCOL)
            IMAX=ITCOL
         END IF
      END DO
      MAX2=0
      DO ITCOL=1,NCOL
         IF ( (RVEC(ITCOL).GT.MAX2).AND.(ITCOL.NE.IMAX) ) THEN
            MAX2=RVEC(ITCOL)
         END IF
      END DO
C---- difference onto grid
      IF (SCALE_LOC.GT.0) THEN
         DIFF=(MAX-MAX2)*10./SCALE_LOC
      ELSE
         DIFF=0
      END IF
      FIRELIND_2ND=MIN(9,INT(DIFF))
      END
***** end of FIRELIND_2ND

***** ------------------------------------------------------------------
***** FCT FIRELIND_2ND3OFF
***** ------------------------------------------------------------------
C---- 
C---- NAME : FIRELIND_2ND3OFF
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Returns the difference between largest and 2nd   *
*     --------        largest component of a vector, projected onto a  *
*                     grid 0-9. The 2nd largest value has to be 3      *
*                     units off the maximum                            *
*     input:          RVEC, NCOLMAX, NCOL                              *
*     output:         0-9                                              *
*----------------------------------------------------------------------*
      INTEGER FUNCTION FIRELIND_2ND3OFF(NCOLMAX,NCOL,RVEC)

      IMPLICIT        NONE
C---- variables passed
      INTEGER         NCOLMAX,NCOL,IMAX
      REAL            RVEC(1:NCOLMAX)
C---- local variables
      INTEGER         ITCOL
      REAL            MAX,MAX2,DIFF
******------------------------------*-----------------------------******
      IMAX=           0

C---- check limits
      IF (NCOL.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A,T20,I5,T30,A,T40,I5)')'***',
     +  'ERROR in lib-prot.f Function FIRELIND_2nd3off: NROW OR NCOL=0'
         WRITE(6,'(T2,A,T10,A)')'***','ncol=',ncol
         STOP
      END IF
C---- compute maximum and 2nd3off largest
      MAX=0
      DO ITCOL=1,NCOL
         IF (RVEC(ITCOL).GT.MAX) THEN
            MAX=RVEC(ITCOL)
            IMAX=ITCOL
         END IF
      END DO
      MAX2=0
      DO ITCOL=1,NCOL
         IF ((ITCOL.GT.(IMAX+2)).OR.(ITCOL.LT.(IMAX-2))) THEN
            IF (RVEC(ITCOL).GT.MAX2) THEN
               MAX2=RVEC(ITCOL)
            END IF
         END IF
      END DO
C---- difference onto grid
C     !SCALE!
      DIFF=(MAX-MAX2)*30.
C      DIFF=(MAX-MAX2)*10.
      FIRELIND_2ND3OFF=MIN(9,INT(DIFF))

C      write(6,'(a,t20,f6.2,i3,t30,10i4)')
C     +     'x.x firelind: ',diff,FIRELIND_2nd3off,
C     +     (int(100*rvec(itcol)),itcol=1,10)
      END
***** end of FIRELIND_2ND3OFF

***** ------------------------------------------------------------------
***** FCT FL_OTHER
***** ------------------------------------------------------------------
C---- 
C---- NAME : FL_OTHER
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1995        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      LOGICAL FUNCTION FL_OTHER(INPUT)
      IMPLICIT        NONE
      CHARACTER*24    INPUT
******------------------------------*-----------------------------******
      IF ((INPUT(24:24).EQ.'7').OR.(INPUT(24:24).EQ.'8').OR.
     +    (INPUT(24:24).EQ.'9').OR.(INPUT(24:24).EQ.'0')) THEN
         FL_OTHER=.TRUE.
      ELSE
C         FL_OTHER=.TRUE.
         FL_OTHER=.FALSE.
      END IF
      END 
***** end of FL_OTHER

***** ------------------------------------------------------------------
***** FCT FR_EXP_TO_REXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : FR_EXP_TO_REXP
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The passed accessibility is normalised according *
*     --------        to the amino acid type passed.                   *
*     input :         EXPIN      accessibility from DSSP               *
*     -------         AAIN       1 letter symbol for amino acid        *
*     output :        FR_EXP_TO_REXP accessibility normalised          *
*     SBRs calling:  from lib-prot.f:                                 *
*     --------------     FCCONV_LOWCASE_AA_TO_CYS                      *
*----------------------------------------------------------------------*
      REAL FUNCTION FR_EXP_TO_REXP(EXPIN,AAIN)
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      INTEGER         EXPIN
      CHARACTER*1     AAIN
C---- local function
      CHARACTER*1     FCCONV_LOWCASE_AA_TO_CYS
C---- local variables
      INTEGER         ITER,THRESHR(1:26),ITFOUND
      CHARACTER*1     AA(1:26),AAX
      LOGICAL         LFOUND
******------------------------------*-----------------------------******

C---- defaults
      ITFOUND=        0

      AA(1)= 'A'
C     D or N
      AA(2)= 'B'
      AA(3)= 'C'
      AA(4)= 'D'
      AA(5)= 'E'
      AA(6)= 'F'
      AA(7)= 'G'
      AA(8)= 'H'
      AA(9)= 'I'
      AA(10)='K'
      AA(11)='L'
      AA(12)='M'
      AA(13)='N'
      AA(14)='P'
      AA(15)='Q'
      AA(16)='R'
      AA(17)='S'
      AA(18)='T'
      AA(19)='V'
      AA(20)='W'
C     undetermined
      AA(21)='X'
      AA(22)='Y'
C     E or Q
      AA(23)='Z'
C---- from FADENUDEL (Reinhard)
      THRESHR(1) =106
      THRESHR(2) =160
      THRESHR(3) =135
      THRESHR(4) =163
      THRESHR(5) =194
      THRESHR(6) =197
      THRESHR(7) = 84
      THRESHR(8) =184
      THRESHR(9) =169
      THRESHR(10)=205
      THRESHR(11)=164
      THRESHR(12)=188
      THRESHR(13)=157
      THRESHR(14)=136
      THRESHR(15)=198
      THRESHR(16)=248
      THRESHR(17)=130
      THRESHR(18)=142
      THRESHR(19)=142
      THRESHR(20)=227

C---- without real reason!
      THRESHR(21)=180
      THRESHR(22)=222
      THRESHR(23)=196
C--------------------------------------------------
C---- 
C--------------------------------------------------

C---- convert [a-z] to C
C         ------------------------
      AAX=FCCONV_LOWCASE_AA_TO_CYS(AAIN)
C         ------------------------

      LFOUND=.FALSE.
      DO ITER=1,23
         IF ((LFOUND.EQV. .FALSE.).AND.(AA(ITER).EQ.AAX)) THEN
            LFOUND=.TRUE.
            ITFOUND=ITER
         END IF
      END DO
      FR_EXP_TO_REXP=1.
      IF (LFOUND.EQV. .TRUE.) THEN
         IF (THRESHR(ITFOUND).GT.0) THEN
            FR_EXP_TO_REXP=MIN(1.,EXPIN/REAL(THRESHR(ITFOUND)))
         END IF
      END IF
      END
***** end of FR_EXP_TO_REXP

***** ------------------------------------------------------------------
***** FCT FR_REXP_TO_EXP
***** ------------------------------------------------------------------
C---- 
C---- NAME : FR_REXP_TO_EXP
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The passed relative exposure is multiplied by the*
*     --------        maximal exposure of the amino acid passed.       *
*     input :         REXPIN     accessibility from DSSP               *
*     -------         AAIN       1 letter symbol for amino acid        *
*     output :        FR_REXP_TO_EXP accessibility normalised          *
*     SBRs calling:  from lib-prot.f:                                 *
*     --------------     FCCONV_LOWCASE_AA_TO_CYS                      *
*----------------------------------------------------------------------*
      REAL FUNCTION FR_REXP_TO_EXP(REXPIN,AAIN)
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      REAL            REXPIN
      CHARACTER*1     AAIN
C---- local function
      CHARACTER*1     FCCONV_LOWCASE_AA_TO_CYS
C---- local variables
      INTEGER         ITER,THRESHR(1:26),ITFOUND
      CHARACTER*1     AA(1:26),AAX
      LOGICAL         LFOUND
******------------------------------*-----------------------------******
*     execution of function                                            *
C---- defaults
      ITFOUND=        0

      AA(1)= 'A'
C     D or N
      AA(2)= 'B'
      AA(3)= 'C'
      AA(4)= 'D'
      AA(5)= 'E'
      AA(6)= 'F'
      AA(7)= 'G'
      AA(8)= 'H'
      AA(9)= 'I'
      AA(10)='K'
      AA(11)='L'
      AA(12)='M'
      AA(13)='N'
      AA(14)='P'
      AA(15)='Q'
      AA(16)='R'
      AA(17)='S'
      AA(18)='T'
      AA(19)='V'
      AA(20)='W'
C     undetermined
      AA(21)='X'
      AA(22)='Y'
C     E or Q
      AA(23)='Z'
C---- from FADENUDEL (Reinhard)
      THRESHR(1) =106
      THRESHR(2) =160
      THRESHR(3) =135
      THRESHR(4) =163
      THRESHR(5) =194
      THRESHR(6) =197
      THRESHR(7) = 84
      THRESHR(8) =184
      THRESHR(9) =169
      THRESHR(10)=205
      THRESHR(11)=164
      THRESHR(12)=188
      THRESHR(13)=157
      THRESHR(14)=136
      THRESHR(15)=198
      THRESHR(16)=248
      THRESHR(17)=130
      THRESHR(18)=142
      THRESHR(19)=142
      THRESHR(20)=227

C---- without real reason!
      THRESHR(21)=180
      THRESHR(22)=222
      THRESHR(23)=196
C--------------------------------------------------
C---- 
C--------------------------------------------------

C---- convert [a-z] to C
C         ------------------------
      AAX=FCCONV_LOWCASE_AA_TO_CYS(AAIN)
C         ------------------------

      LFOUND=.FALSE.
      DO ITER=1,23
         IF ((LFOUND.EQV. .FALSE.).AND.(AA(ITER).EQ.AAX)) THEN
            LFOUND=.TRUE.
            ITFOUND=ITER
         END IF
      END DO
      IF (LFOUND .EQV. .TRUE.) THEN
         FR_REXP_TO_EXP=REXPIN*THRESHR(ITFOUND)
      ELSE
         FR_REXP_TO_EXP=100.
      END IF
      END
***** end of FR_REXP_TO_EXP

***** ------------------------------------------------------------------
***** FCT FRCORRVEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : FRCORRVEC
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computes the correlation coefficient between all *
*     --------        elemnts of the two real vectors RVEC1, RVEC2:    *
*                                 <ab> - <a><b>                        *
*         FRCORRVEC=  ----------------------------------------         *
*                     sqrt{<a**2>-<a>**2} sqrt {<b**2>-<b>**2}         *
*     input:          VEC1, VEC2, MAXCOL, ICOL                         *
*     output:         correlation between VEC1 and VEC2                *
*     ext. library:   SISTZ1: setting zero all elements of an integer  *
*     -------------   vector                                           *
*----------------------------------------------------------------------*
      REAL FUNCTION FRCORRVEC(NCOLMAX,NCOL,RVECX,RVECY,NORM)

      IMPLICIT        NONE
C---- variables passed
      INTEGER         NCOLMAX,NCOL
      REAL            NORM,RVECX(1:NCOLMAX),RVECY(1:NCOLMAX)
C---- local variables
      INTEGER         ITCOL
      REAL            R_1,R_2,R_12,R_11,R_22,NOMINATOR,DENOMINATOR
******------------------------------*-----------------------------******
*     NORM            normalisation of components of the vectors to    *
*                     yield values < 1.                                *
******------------------------------*-----------------------------******

C---- check limits
      IF (NCOL.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A,T20,I5,T30,A,T40,I5)')'***',
     +        'ERROR in lib-comp.f Function FRCORRVEC: NROW OR NCOL=0'
         WRITE(6,'(T2,A,T10,A)')'***','ncol=',ncol
         STOP
      END IF

      R_12=0
      R_1=0
      R_2=0
      R_11=0
      R_22=0
      DO ITCOL=1,NCOL
         R_12=R_12+(RVECX(ITCOL)*RVECY(ITCOL))
         R_1 =R_1 +RVECX(ITCOL)
         R_2 =R_2 +RVECY(ITCOL)
         R_11=R_11+(RVECX(ITCOL)*RVECX(ITCOL))
         R_22=R_22+(RVECY(ITCOL)*RVECY(ITCOL))
      END DO
C---- normalise with the passed quantity NORM
      R_12=R_12/(NORM*NORM*NCOL)
      R_11=R_11/(NORM*NORM*NCOL)
      R_22=R_22/(NORM*NORM*NCOL)
      R_1=R_1/(NORM*NCOL)
      R_2=R_2/(NORM*NCOL)
C---- normalise with dimension to obtain proper averages
      R_1 =R_1 /REAL(NCOL)
      R_2 =R_2 /REAL(NCOL)
      R_12=R_12/REAL(NCOL)
      R_11=R_11/REAL(NCOL)
      R_22=R_22/REAL(NCOL)
C---- nominator: <M1*M2> - <M1><M2>
      NOMINATOR=R_12 - ( R_1*R_2 )

C---- denominator: <M1*M2> - <M1><M2>
      DENOMINATOR=SQRT( R_11 - (R_1*R_1) )*SQRT( R_22 - (R_2*R_2) )

      IF (ABS(DENOMINATOR).GT.10E-15) THEN
         FRCORRVEC=NOMINATOR/DENOMINATOR
      ELSE
         FRCORRVEC=0.
      END IF
      END
***** end of FRCORRVEC

***** ------------------------------------------------------------------
***** FCT FRCORRVEC_REAL4
***** ------------------------------------------------------------------
C---- 
C---- NAME : FRCORRVEC_REAL4
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Computes the correlation coefficient between all *
*     --------        elemnts of the two real vectors RVEC1, RVEC2:    *
*                                 <ab> - <a><b>                        *
*         FRCORRVEC=  ----------------------------------------         *
*                     sqrt{<a**2>-<a>**2} sqrt {<b**2>-<b>**2}         *
*     input:          VEC1, VEC2, MAXCOL, ICOL                         *
*     output:         correlation between VEC1 and VEC2                *
*     ext. library:   SISTZ1: setting zero all elements of an integer  *
*     -------------   vector                                           *
*----------------------------------------------------------------------*
      REAL FUNCTION FRCORRVEC_REAL4(NCOLMAX,NCOL,RVECX,RVECY,NORM)

      IMPLICIT        NONE
C---- variables passed
      INTEGER         NCOLMAX,NCOL
      REAL*4          RVECX(1:NCOLMAX),RVECY(1:NCOLMAX)
      REAL            NORM
C---- local variables
      INTEGER         ITCOL
      REAL            R_1,R_2,R_12,R_11,R_22,NOMINATOR,DENOMINATOR,
     +     NORM_NCOL
******------------------------------*-----------------------------******
*     NORM            normalisation of components of the vectors to    *
*                     yield values < 1.                                *
******------------------------------*-----------------------------******

C---- check limits
      IF (NCOL.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A,T20,I5,T30,A,T40,I5)')'***',
     +    'ERROR in lib-comp.f Function FRCORRVEC_REAL4: NCOL=0'
         WRITE(6,'(T2,A,T10,A)')'***','ncol=',ncol
         STOP
      END IF
C---- R_12= < m1(i,j) * m2(i,j) >
C---- R_1=  <m1> 
C---- R_2=  <m2>
C---- R_11= <m1**2>
C---- R_22= <m2**2>

      R_12=0
      R_1=0
      R_2=0
      R_11=0
      R_22=0
      DO ITCOL=1,NCOL
         R_12=R_12+(RVECX(ITCOL)*RVECY(ITCOL))
         R_1 =R_1 +RVECX(ITCOL)
         R_2 =R_2 +RVECY(ITCOL)
         R_11=R_11+(RVECX(ITCOL)*RVECX(ITCOL))
         R_22=R_22+(RVECY(ITCOL)*RVECY(ITCOL))
      END DO
C---- normalise with the passed quantity NORM
      NORM_NCOL=NORM*NCOL
      IF ((NORM.NE.1).AND.(ABS(NORM_NCOL).GT.10E-15)) THEN
         R_12=R_12/(NORM*NORM*NCOL)
         R_11=R_11/(NORM*NORM*NCOL)
         R_22=R_22/(NORM*NORM*NCOL)
         R_1=R_1/(NORM*NCOL)
         R_2=R_2/(NORM*NCOL)
      ELSEIF ((ABS(NORM_NCOL).LE.10E-15)) THEN
         R_12=0
         R_11=0
         R_22=0
         R_1=0
         R_2=0
      END IF
C---- normalise with dimension to obtain proper averages
      IF (NCOL.EQ.0) THEN
         R_1 =R_1 /REAL(NCOL)
         R_2 =R_2 /REAL(NCOL)
         R_12=R_12/REAL(NCOL)
         R_11=R_11/REAL(NCOL)
         R_22=R_22/REAL(NCOL)
      ELSE
         R_12=0
         R_11=0
         R_22=0
         R_1=0
         R_2=0
      END IF
C---- nominator: <M1*M2> - <M1><M2>
C      NOMINATOR=R_XY - ( R_X*R_Y )
      NOMINATOR=R_12 - ( R_1*R_2 )

C---- denominator: <M1*M2> - <M1><M2>
C      DENOMINATOR=SQRT( (R_XX-(R_X*R_X))*(R_YY-(R_Y*R_Y)) )
      DENOMINATOR=SQRT( (R_11-(R_1*R_1))*(R_22-(R_2*R_2)) )

C      write(6,'(t2,a,t30,f6.3,t40,a,t50,f6.3)')
C     +     'x.x frcorrvec_real4: nom=',nominator,'den ',denominator
C      write(6,'(t2,a,t30,5f8.3)')'x.x <o> sq<o> <p> sq<p> <op>',
C     +     R_1,R_11,R_2,R_22,R_12
      IF (ABS(DENOMINATOR).GT.10E-15) THEN
         FRCORRVEC_REAL4=NOMINATOR/DENOMINATOR
      ELSE
         FRCORRVEC_REAL4=0.
      END IF
      END
***** end of FRCORRVEC_REAL4

***** ------------------------------------------------------------------
***** FCT FRDIVIDE_SAFE
***** ------------------------------------------------------------------
C---- 
C---- NAME : FRDIVIDE_SAFE
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Divides NOM/DEN, returns 0 if ABS(DEN)<10E-15.   *
*     input :         NOM, DEN                                         *
*----------------------------------------------------------------------*
      REAL FUNCTION FRDIVIDE_SAFE(NOM,DEN)
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      REAL            NOM,DEN
******------------------------------*-----------------------------******
*     execution of function                                            *
      IF (ABS(DEN).LT.10E-15) THEN
         FRDIVIDE_SAFE=0
      ELSE
         FRDIVIDE_SAFE=NOM/DEN
      END IF
      END
***** end of FRDIVIDE_SAFE

***** ------------------------------------------------------------------
***** FCT FWRITE_STRING_NUMBERS
***** ------------------------------------------------------------------
C---- 
C---- NAME : FWRITE_STRING_NUMBERS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1993        version 0.1    *
*                      changed: Apr,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      CHARACTER*222 FUNCTION FWRITE_STRING_NUMBERS(L80,ILINE)
      IMPLICIT        NONE
C---- local variables                                                  *
      INTEGER         ILINE
      CHARACTER*222   CHARLINE
      LOGICAL         L80
******------------------------------*-----------------------------******

C---- write for 80 characters per line
      IF (L80.EQV. .TRUE.) THEN
         IF ((ILINE.EQ.1).OR.(ILINE.GT.13)) THEN
            CHARLINE(1:40) ='....,....1....,....2....,....3....,....4'
            CHARLINE(41:80)='....,....5....,....6....,....7....,....8'
         ELSEIF (ILINE.EQ.2) THEN
            CHARLINE(1:40) ='....,....9....,....10...,....11...,....1'
            CHARLINE(41:80)='2...,....13...,....14...,....15...,....1'
         ELSEIF (ILINE.EQ.3) THEN
            CHARLINE(1:40) ='6...,....17...,....18...,....19...,....2'
            CHARLINE(41:80)='0...,....21...,....22...,....23...,....2'
         ELSEIF (ILINE.EQ.4) THEN
            CHARLINE(1:40) ='4...,....25...,....26...,....27...,....2'
            CHARLINE(41:80)='8...,....29...,....30...,....31...,....3'
         ELSEIF (ILINE.EQ.5) THEN
            CHARLINE(1:40) ='2...,....33...,....34...,....35...,....3'
            CHARLINE(41:80)='6...,....37...,....38...,....39...,....4'
         ELSEIF (ILINE.EQ.6) THEN
            CHARLINE(1:40) ='0...,....41...,....42...,....43...,....4'
            CHARLINE(41:80)='4...,....45...,....46...,....47...,....4'
         ELSEIF (ILINE.EQ.7) THEN
            CHARLINE(1:40) ='8...,....49...,....50...,....51...,....5'
            CHARLINE(41:80)='2...,....53...,....54...,....55...,....5'
         ELSEIF (ILINE.EQ.8) THEN
            CHARLINE(1:40) ='6...,....57...,....58...,....59...,....6'
            CHARLINE(41:80)='0...,....61...,....62...,....63...,....6'
         ELSEIF (ILINE.EQ.9) THEN
            CHARLINE(1:40) ='4...,....65...,....66...,....67...,....6'
            CHARLINE(41:80)='8...,....69...,....70...,....71...,....7'
         ELSEIF (ILINE.EQ.10) THEN
            CHARLINE(1:40) ='2...,....73...,....74...,....75...,....7'
            CHARLINE(41:80)='6...,....77...,....78...,....79...,....8'
         ELSEIF (ILINE.EQ.11) THEN
            CHARLINE(1:40) ='0...,....81...,....82...,....83...,....8'
            CHARLINE(41:80)='4...,....85...,....86...,....87...,....8'
         ELSEIF (ILINE.EQ.12) THEN
            CHARLINE(1:40) ='8...,....89...,....90...,....91...,....9'
            CHARLINE(41:80)='2...,....93...,....94...,....95...,....9'
         ELSEIF (ILINE.EQ.13) THEN
            CHARLINE(1:40) ='6...,....97...,....98...,....99...,....1'
            CHARLINE(41:80)='00..,....110..,....120..,....130..,....1'
         ELSEIF (ILINE.EQ.14) THEN
            CHARLINE(1:40) ='140.,....150..,....160..,....170..,....1'
            CHARLINE(41:80)='80..,....190..,....200..,....210..,....2'
         ELSEIF (ILINE.EQ.15) THEN
            CHARLINE(1:40) ='230.,....240..,....250..,....260..,....2'
            CHARLINE(41:80)='70..,....280..,....290..,....300..,....2'
         ELSEIF (ILINE.EQ.16) THEN
            CHARLINE(1:40) ='310.,....320..,....330..,....340..,....3'
            CHARLINE(41:80)='50..,....360..,....370..,....380..,....3'
         ELSEIF (ILINE.EQ.17) THEN
            CHARLINE(1:40) ='390.,....400..,....410..,....420..,....4'
            CHARLINE(41:80)='30..,....440..,....450..,....460..,....4'
         ELSEIF (ILINE.EQ.18) THEN
            CHARLINE(1:40) ='470.,....480..,....490..,....500..,....5'
            CHARLINE(41:80)='10..,....520..,....530..,....540..,....5'
         ELSEIF (ILINE.EQ.19) THEN
            CHARLINE(1:40) ='550.,....560..,....570..,....580..,....5'
            CHARLINE(41:80)='90..,....600..,....610..,....620..,....5'
         ELSE 
            CHARLINE(1:40) ='x0..,....x1...,....x2...,....x3...,....x'
            CHARLINE(41:80)='4...,....x5...,....x6...,....x7...,....8'
         END IF
C---- write for 60 characters per line
      ELSE
         IF ((ILINE.EQ.1).OR.(ILINE.GT.18)) THEN
            CHARLINE(1:40) ='....,....1....,....2....,....3....,....4'
            CHARLINE(41:60)='....,....5....,....6'
         ELSEIF (ILINE.EQ.2) THEN
            CHARLINE(1:40) ='....,....7....,....8....,....9....,....1'
            CHARLINE(41:60)='0...,....11...,....1'
         ELSEIF (ILINE.EQ.3) THEN
            CHARLINE(1:40) ='2...,....13...,....14...,....15...,....1'
            CHARLINE(41:60)='6...,....17...,....1'
         ELSEIF (ILINE.EQ.4) THEN
            CHARLINE(1:40) ='8...,....19...,....20...,....21...,....2'
            CHARLINE(41:60)='2...,....23...,....2'
         ELSEIF (ILINE.EQ.5) THEN
            CHARLINE(1:40) ='4...,....25...,....26...,....27...,....2'
            CHARLINE(41:60)='8...,....29...,....3'
         ELSEIF (ILINE.EQ.6) THEN
            CHARLINE(1:40) ='0...,....31...,....32...,....33...,....3'
            CHARLINE(41:60)='4...,....35...,....3'
         ELSEIF (ILINE.EQ.7) THEN
            CHARLINE(1:40) ='6...,....37...,....38...,....39...,....4'
            CHARLINE(41:60)='0...,....41...,....4'
         ELSEIF (ILINE.EQ.8) THEN
            CHARLINE(1:40) ='2...,....43...,....44...,....45...,....4'
            CHARLINE(41:60)='6...,....47...,....4'
         ELSEIF (ILINE.EQ.9) THEN
            CHARLINE(1:40) ='8...,....49...,....50...,....51...,....5'
            CHARLINE(41:60)='2...,....53...,....5'
         ELSEIF (ILINE.EQ.10) THEN
            CHARLINE(1:40) ='4...,....55...,....56...,....57...,....5'
            CHARLINE(41:60)='8...,....59...,....6'
         ELSEIF (ILINE.EQ.11) THEN
            CHARLINE(1:40) ='0...,....61...,....62...,....63...,....6'
            CHARLINE(41:60)='4...,....65...,....6'
         ELSEIF (ILINE.EQ.12) THEN
            CHARLINE(1:40) ='6...,....67...,....68...,....69...,....7'
            CHARLINE(41:60)='0...,....71...,....7'
         ELSEIF (ILINE.EQ.13) THEN
            CHARLINE(1:40) ='2...,....73...,....74...,....75...,....7'
            CHARLINE(41:60)='6...,....77...,....7'
         ELSEIF (ILINE.EQ.14) THEN
            CHARLINE(1:40) ='8...,....79...,....80...,....81...,....8'
            CHARLINE(41:60)='2...,....83...,....8'
         ELSEIF (ILINE.EQ.15) THEN
            CHARLINE(1:40) ='4...,....85...,....86...,....87...,....8'
            CHARLINE(41:60)='8...,....89...,....9'
         ELSEIF (ILINE.EQ.16) THEN
            CHARLINE(1:40) ='0...,....91...,....92...,....93...,....9'
            CHARLINE(41:60)='4...,....95...,....9'
         ELSEIF (ILINE.EQ.17) THEN
            CHARLINE(1:40) ='6...,....97...,....98...,....99...,....1'
            CHARLINE(41:60)='00..,....110..,....2'
         ELSEIF (ILINE.EQ.18) THEN
            CHARLINE(1:40) ='120.,....130..,....140..,....150..,....1'
            CHARLINE(41:60)='60..,....170..,....8'
         ELSEIF (ILINE.EQ.19) THEN
            CHARLINE(1:40) ='180.,....190..,....200..,....210..,....2'
            CHARLINE(41:60)='20..,....230..,....4'
         ELSEIF (ILINE.EQ.20) THEN
            CHARLINE(1:40) ='240.,....250..,....260..,....270..,....2'
            CHARLINE(41:60)='80..,....290..,....3'
         ELSEIF (ILINE.EQ.21) THEN
            CHARLINE(1:40) ='300.,....310..,....320..,....330..,....3'
            CHARLINE(41:60)='40..,....350..,....6'
         ELSEIF (ILINE.EQ.22) THEN
            CHARLINE(1:40) ='360.,....370..,....380..,....390..,....4'
            CHARLINE(41:60)='00..,....410..,....2'
         ELSEIF (ILINE.EQ.23) THEN
            CHARLINE(1:40) ='420.,....430..,....440..,....450..,....4'
            CHARLINE(41:60)='60..,....470..,....8'
         ELSEIF (ILINE.EQ.24) THEN
            CHARLINE(1:40) ='480.,....490..,....500..,....510..,....5'
            CHARLINE(41:60)='20..,....530..,....4'
         ELSEIF (ILINE.EQ.25) THEN
            CHARLINE(1:40) ='540.,....550..,....560..,....570..,....5'
            CHARLINE(41:60)='80..,....590..,....6'
         ELSE
            CHARLINE(1:40) ='x0..,....x1...,....x2...,....x3...,....x'
            CHARLINE(41:60)='4...,....x5...,....6'
         END IF
      END IF
      FWRITE_STRING_NUMBERS=CHARLINE
      END 
***** end of FWRITE_STRING_NUMBERS

***** ------------------------------------------------------------------
***** SUB GETCHAR
***** ------------------------------------------------------------------
C---- 
C---- NAME : GETCHAR
C---- ARG  :  
C---- DES  : prompts for printable (keyboard) characters
C---- DES  : Caution: line with '$!' is truncated as comment
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE GETCHAR(KCHAR,CHARARR,CTEXT)

      LOGICAL EMPTYSTRING   
      CHARACTER*222 LINE
      CHARACTER*222 CTEXT
      CHARACTER CHARARR*(*)

      WRITE(*,*)
      WRITE(*,*)'================================================='//
     +          '=============================='
      CALL WRITELINES(CTEXT)
      IF(KCHAR.LT.1)THEN
         WRITE(*,*)'*** CHARPROMPT: illegal KCHAR',KCHAR
         RETURN
      ENDIF
 10   CONTINUE
      WRITE(*,*) 
      IF(KCHAR.GT.1) THEN
         WRITE(*,'(2X,''Enter letter string of length <'',I3)')KCHAR
      ELSE
         WRITE(*,'(2X,''Enter one letter !'')')
      ENDIF
      WRITE(*,'(2X,''[CR=default]: '')') 
      WRITE(*, '(2X,''Default: '',80A1)' ) (CHARARR(K:K),K=1,KCHAR)
      LINE=' '
      READ(*,'(A80)',ERR=10,END=11) LINE
      IF(.NOT.EMPTYSTRING(LINE)) THEN
C                    ! assuming default values were set outside ....
C...remove comments ( 34535345 !$ comment )
         KCOMMENT=INDEX(LINE,'!$')
         IF(KCOMMENT.NE.0) LINE(KCOMMENT:80)=' '
         DO I=1,80
            IF (INDEX(' ABCDEFGHIJKLMNOPQRSTUVWXYZ',LINE(I:I))
     +           .EQ.0) THEN
               IF (INDEX(' abcdefghijklmnopqrstuvwxyz',LINE(I:I))
     +              .EQ.0) THEN
C                  IF (INDEX('~!@#$%^&*()_+=-{}[]:""|\;,' ,LINE(I:I))
                  IF (INDEX('~!@#$%^&*()_+=-{}[]:""|;,' ,LINE(I:I))
     +                 .EQ.0) THEN
                     IF (INDEX('.?/><1234567890            ',LINE(I:I))
     +                    .EQ.0) THEN
                        WRITE(*,
     +                       '(2X,''*** characters only, not: '',A40)')
     +                       LINE(1:40)
                        GO TO 10
                     ENDIF
                  ENDIF
               ENDIF
            ENDIF
         ENDDO
         READ(LINE,'(80A1)',ERR=10,END=99) (CHARARR(K:K),K=1,KCHAR)
      ENDIF
 11   WRITE(*,'(2X,A7,60A1)') ' echo: ', (CHARARR(K:K),K=1,KCHAR)
      RETURN
 99   WRITE(*,*)' CHARPROMPT: END OF LINE DURING READ - check format!'
      END
***** end of GETCHAR

***** ------------------------------------------------------------------
***** SUB GET_ARGUMENT
***** ------------------------------------------------------------------
C---- 
C---- NAME : GET_ARGUMENT
C---- ARG  : NUMBER,ARGUMENT
C---- DES  : returns the content of x-th argument
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE GET_ARGUMENT(INUMBER,ARGUMENT)
      CHARACTER*222   ARGUMENT
      INTEGER         INUMBER

      CALL GETARG(INUMBER,ARGUMENT)
      RETURN 
      END
***** end of GET_ARGUMENT

***** ------------------------------------------------------------------
***** SUB GET_ARG_NUMBER
***** ------------------------------------------------------------------
C---- 
C---- NAME : GET_ARG_NUMBER
C---- ARG  : INUMBER
C---- DES  : returns number of arguments
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE GET_ARG_NUMBER(INUMBER)
      INTEGER INUMBER,IARGC
    
      INUMBER=0
      INUMBER=IARGC()
      RETURN 
      END
***** end of GET_ARG_NUMBER

***** ------------------------------------------------------------------
***** SUB SCHECKPASS
***** ------------------------------------------------------------------
C---- 
C---- NAME : SCHECKPASS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Sep,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose:        Check whether arrays passed between SBRs are cor-*
C     --------        rectly passed or not                             *
C     input variables:CHVP1-N,CHVPL                                    *
C     output variab.: LERRCHVP,POSERRCHVP                              *
C-----------------------------------------------------------------------
      SUBROUTINE SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
      IMPLICIT        NONE
C---- local variables                                                  *
      INTEGER         ITER,CHVPM(*),CHVPL
      LOGICAL         LERRCHVP,LPOSERRCHVP(1:50)
******------------------------------*-----------------------------******
C---- initial check of maximal dimension
      IF (CHVPL.GT.10) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SCHECKPASS:number of passed variables not fitting'
      END IF

      LERRCHVP=.FALSE.
      DO ITER=1,CHVPL
         LPOSERRCHVP(ITER)=.FALSE.
      END DO
      DO ITER=1,CHVPL
         IF (CHVPM(ITER).NE.ITER) THEN
            LPOSERRCHVP(ITER)=.TRUE.
            LERRCHVP=.TRUE.
            WRITE(6,*)'x.x',iter,' - chvpm=',chvpm(iter)
         END IF
      END DO
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SCHECKPASS: variables passed not correct !'
         DO ITER=1,CHVPL
            IF (LPOSERRCHVP(ITER).EQV. .TRUE.) THEN
               WRITE(6,'(T2,A,T10,A,T60,I4)')'***',
     +              'The check detected a fault for variable:',ITER
            END IF
         END DO
      END IF
      END
***** end of SCHECKPASS

***** ------------------------------------------------------------------
***** SUB SEVALINFOFILE
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEVALINFOFILE
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The information (entropy) for the current output *
*     --------        is computed).                                    *
*     input variables:NSECELLIB, MATNUMALL                             *
*     output variab.: INFO, NORMINFO                                   *
*     called by:      SEVALSEC (in lib-prot.f)                         *
*     SBRs calling:  functions from lib-comp.f:                       *
*                     FRMEAN1,FRVAR1                                   *
*     procedure:      Sinfo=  ln [ (n!) / prod/s {n(s)!} ]   +         *
*     ----------              - sum /[s]                               *
*                               ln [ (m(s)!) / prod/s {m(s,s')!} ]     *
*                     normalization: by: n*ln(3) - ln(n) -ln(2*pi) +   *
*                                        + 1.5 ln(3)                   *
*                     new formulation according to JuMBo:              *
*                     a(i): number of predicted in structure i         *
*                     b(i): number of observed in structure i          *
*                     A(ij):predicted in j, observed in i              *
*                         sum/i {a(i)ln a(i)} - sum/ij {A(ij)ln A(ij)} *
*              I =  1  -  -------------------------------------------- *
*                             N ln N - sum/i {b(i) ln b(i) }           *
*----------------------------------------------------------------------*
      SUBROUTINE SEVALINFOFILE(KUNIT,NSECELLIB,MAXNSECEL,
     +     MATNUMALL,INFO,INFO_INV,LWRITE)
      IMPLICIT        NONE
C---- passed variables
      INTEGER         NSECELLIB,KUNIT,MAXNSECEL,
     +     MATNUMALL(1:(MAXNSECEL+1),1:(MAXNSECEL+1))
      REAL            INFO,INFO_INV
C---- local variables                                                  *
      INTEGER         ITSEC,ITSEC2
      REAL            INTER,SUM,N,NOMINATOR,DENOMINATOR,
     +     TERM_AIJ,TERM_PRED,TERM_DSSP,TERM_N
      LOGICAL         LWRITE
******------------------------------*-----------------------------******
*---------------------                                                 *
*     passed variables                                                 *
*     INFO            information as defined in procedure above        *
*     INFO_INV        same as info but now as %of predicted so to speak*
*     KUNIT           number of unit to write the files upon           *
*     LWRITE          if true the results are written onto the KUNIT   *
*     MAXNSECEL       maximal number of secondary structures for array *
*                     boundaries                                       *
*     MATNUM(i,j)     the number of residues in a certain secondary    *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix, MATNUM(1,2) those DSSP helices predicted*
*                     as strands and MATNUM(1,4) all DSSP helices, resp.
*                     MATNUM(4,4) all residues predicted.              *
*     N               total number of residues                         *
*     NOMINATOR       = TERM_PRED - TERM_AIJ                           *
*     NSECEL          number of secondary structure types used         *
*---------------------                                                 *
*     local variables                                                  *
*     MUE,NUE,ITSEC,ITSEC2,ITER,ITHISTO,ITFILES  iteration variables   *
*     NOMINATOR       = TERM_PRED - TERM_AIJ                           *
*     TERM_AIJ        sum/ij of A(ij): number of residues predicted in *
*                     structure j, observed in i                       *
*     TERM_DSSP       sum over residues observed in: H + E + L         *
*     TERM_N          N * ln (N)                                       *
*     TERM_PRED       sum over residues predicted in: H + E + L        *
******------------------------------*-----------------------------******
      TERM_N=         0      

C---- compute the mixed term: m(s,s2) A(ij)
      SUM=0.
      DO ITSEC2=1,NSECELLIB
         DO ITSEC=1,NSECELLIB
            INTER=REAL(MATNUMALL(ITSEC,ITSEC2))
            IF (INTER.GT.0) THEN
               SUM=SUM+( (INTER+0.5) * LOG(INTER) )
            END IF
         END DO
      END DO
      TERM_AIJ=SUM
C---- compute the term of sum over prediction m(s) = pred(i)
      SUM=0.
      DO ITSEC=1,NSECELLIB
         INTER=REAL(MATNUMALL((NSECELLIB+1),ITSEC))
         IF (INTER.GT.0) THEN
            SUM=SUM+( (INTER+0.5) * LOG(INTER) )
         END IF
      END DO
      TERM_PRED=SUM
C---- compute the term stemming from the data bank n(s) = DSSP(i)
      SUM=0.
      DO ITSEC=1,NSECELLIB
         INTER=REAL(MATNUMALL(ITSEC,(NSECELLIB+1)))
         IF (INTER.GT.0) THEN
            SUM=SUM+( (INTER+0.5) * LOG(INTER) )
         END IF
      END DO
      TERM_DSSP=SUM
C---- compute the term for all residues
      N=REAL(MATNUMALL((NSECELLIB+1),(NSECELLIB+1)))
      IF (N.LT.10E15) THEN
         TERM_N=N * LOG (N)
      ELSE
         WRITE(6,'(T2,A,T10,A,T70,F12.1)')'***',
     +        'ERROR for computation of information: N too big=',N
      END IF
C--------------------------------------------------
C---- add up all terms                        -----
C--------------------------------------------------

C---- %observed
      NOMINATOR  =TERM_PRED - TERM_AIJ
      DENOMINATOR=TERM_N    - TERM_DSSP
      IF (ABS(DENOMINATOR).GT.10E-15) THEN
         INFO=1-(NOMINATOR/DENOMINATOR)
      ELSE
         IF (NOMINATOR.GE.10E15) THEN
            INFO=0
         ELSE
            INFO=10
         END IF
      END IF
C---- %predicted
      NOMINATOR  =TERM_DSSP - TERM_AIJ
      DENOMINATOR=TERM_N    - TERM_PRED
      IF (ABS(DENOMINATOR).GT.10E-15) THEN
         INFO_INV=1-(NOMINATOR/DENOMINATOR)
      ELSE
         IF (NOMINATOR.GE.10E15) THEN
            INFO_INV=0
         ELSE
            INFO_INV=10
         END IF
      END IF
C---- write results onto printer
      IF (LWRITE .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(T57,A10,F6.3,A9,F6.3,A2)')
     +        '| I %obs= ',INFO,', %pred= ',INFO_INV,' |'
         WRITE(KUNIT,'(T57,A)')
     +        '+-------------------------------+'
         WRITE(KUNIT,'(T2,A)')'---'
      END IF
      END
***** end of SEVALINFOFILE

***** ------------------------------------------------------------------
***** SUB SEVALLEN
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEVALLEN
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The lengths and number of the secondary structure*
*     --------        elements for a given protein are evaluated.      *
*     input variables:NSECELLIB,LENGTHLIB,NHISTOLIB,DSSPCHAR,PREDCHAR  *
*     output variab.: MATLEN,MATLENDIS                                 *
*     called by:      SEVALSEC (in lib-prot.f)                         *
*     procedure:      straightforward                                  *
*----------------------------------------------------------------------*
      SUBROUTINE SEVALLEN(NSECELLIB,MAXNSECEL,LENGTHLIB,MAXLENGTH,
     +     NHISTOLIB,MAXNHISTO,DSSPCHAR,PREDCHAR,MATLEN,MATLENDIS)
      IMPLICIT        NONE
C---- local parameter                                                  *
      LOGICAL         LWRT
      PARAMETER       (LWRT=.FALSE.)
C      PARAMETER       (LWRT=.TRUE.)
C----                                                                  *
C---- variables passed
      INTEGER         MAXNSECEL,MAXLENGTH,MAXNHISTO,NSECELLIB,
     +     LENGTHLIB,NHISTOLIB,MATLEN(1:(MAXNSECEL+1),1:4),
     +     MATLENDIS(1:MAXNHISTO,1:(2*MAXNSECEL))
      CHARACTER*1     DSSPCHAR(1:MAXLENGTH),PREDCHAR(1:MAXLENGTH)
C---- local variables
      INTEGER         ITSEC,ITEL,ITHISTO,MUE,IHELP
      INTEGER         COUNTMUE,COUNTSTR,ELNO,ELLEN(1:2000),
     +     ELNOTOT(1:9),ELLENTOT(1:9)
      LOGICAL         NEWSTR,LHELP
      CHARACTER*1     SSCHAR(1:8),CURSTR,ELCHAR(1:2000)
******------------------------------*-----------------------------******
*     MUE,NUE,ITSEC,ITSEC2,ITER,ITHISTO,ITFILES  iteration variables   *
*     COUNTMUE        counts the number of samples                     *
*                     =sum(nue=1,numprottest){numrestest(nue)}         *
*     COUNTSTR        counts the number of secondary elements within   *
*                     each protein, resp. the sum over all proteins    *
*     CURSTR          stores intermediately the name of the currently  *
*                     treate secondary element                         *
*     DSSPCHAR(i)     secondary structure according to DSSP for a par- *
*                     ticular protein                                  *
*     ELCHAR(i)       one-letter symbol of secondary structure for     *
*                     the i-th element                                 *
*     ELDIS(c,h)      number of elements of class c with length h      *
*     ELNO            number of secondary elements                     *
*     ELNOTOT(c)      distinct number of all elements of class *       *
*     ELLEN(i)        length (number of residues) for element i        *
*     ELLENTOT(c)     summed length of all elements of class *         *
*     LENGTHLIB       current length of protein                        *
*     LWRT            if true certain information is written onto prin-*
*                     ter, e.g. the length distribution                *
*     MATLEN(i,j)     matrix with the lengths of the elements:         *
*                     i=1,4 => H,E,C,all                               *
*                     j=1   => number of elements DSSP                 *
*                     j=2   => number of elements PRED                 *
*                     j=2   => summed length of all elements for DSSP  *
*                     j=2   => summed length of all elements for PRED  *
*     MATLENDIS(i,j)  gives the distribution of the elements           *
*                     i=1,50=> for histogram (lengths 1-50)            *
*                     j=1,3 => H,E,C for DSSP                          *
*                     j=4,6 => H,E,C for PRED                          *
*     MAXLENGTH       maximal length of current protein allowed        *
*     MAXNHISTO       maximal number of columns for histogram          *
*     MAXNSECEL       maximal number of secondary structures allowed   *
*     NEWSTR          required for ending the summing up of the length *
*                     of a particular structure                        *
*     NHISTOLIB       number of columns for histogram of lenght distri-*
*                     bution                                           *
*     NSECELLIB       currently read number of secondary structures    *
*     NUMFILESREAD    number of files to be read                       *
*     NUMPROTLIB      number of proteins of particular file read       *
*     PREDCHAR(i)     secondary structure according to prediction for a*
*                      particular protein                              *
*     SSCHAR(i)       character giving the one-letter symbol for i-th  *
*                     secondary structure class                        *
******------------------------------*-----------------------------******
      IF (LWRT .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A3,T5,A50,T56,A)')'---',
     +        'computing the lengths of DSSP and PRED elements by',
     +        ' SBR SEVALLEN form lib-prot.f'
      END IF

C---- check variables passed
      IF (LENGTHLIB.GT.MAXLENGTH) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SEVALLEN'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current length: ',LENGTHLIB,'  allocated:',MAXLENGTH
         WRITE(6,'(T5,A)')' Stopped in SEVALLEN 12-2-92:1'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +       'The number of secondary structures passed to SBR SEVALLEN'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SEVALLEN 12-2-92:2'
         STOP
      END IF

C---- assign secondary characters
      SSCHAR(1)='H'
      SSCHAR(2)='E'
      SSCHAR(NSECELLIB)='L'
      IF (NSECELLIB.EQ.4) THEN
         SSCHAR(3)='T'
      ELSEIF (NSECELLIB.EQ.5) THEN
         SSCHAR(3)='T'
         SSCHAR(4)='S'
      ELSEIF (NSECELLIB.GT.5) THEN
         WRITE(6,'(T5,A)')
     +        'Caution: current version of SEVALLEN distinguishes'
         WRITE(6,'(T5,A)')'only five different secondary structures!'
         WRITE(6,'(T5,A)')'Stopped in SBR SEVALLEN: 12-2-92:3'
         STOP
      END IF

C---- to be on the save side: substitute potential blanks by 'L'
      DO MUE=1,LENGTHLIB
         LHELP=.TRUE.
         DO ITSEC=1,(NSECELLIB-1)
            IF (LHELP.EQV. .TRUE. 
     +           .AND.(PREDCHAR(MUE).EQ.SSCHAR(ITSEC))) THEN
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            PREDCHAR(MUE)='L'
         END IF
      END DO

C---- setting initially to zero (explicit as INTEGER*2)
      ELNO=0
      CALL SISTZ1(ELLEN,2000)
      CALL SISTZ1(ELNOTOT,9)
      CALL SISTZ1(ELLENTOT,9)
      CALL SISTZ2(MATLENDIS,MAXNHISTO,(2*MAXNSECEL))

C--------------------------------------------------
C---- discriminate elements  for DSSP         -----
C--------------------------------------------------

C---- counting elements and summing up their lengths
      COUNTMUE=0
      MUE=0
      COUNTSTR=0
      DO WHILE (MUE.LT.LENGTHLIB)
         COUNTSTR=COUNTSTR+1
         NEWSTR=.FALSE.
         DO WHILE ((MUE.LT.(LENGTHLIB-1)).AND.(.NOT.NEWSTR))
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            CURSTR=DSSPCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
            IF (DSSPCHAR(COUNTMUE+1).NE.CURSTR) THEN
               NEWSTR=.TRUE.
            END IF
         END DO
         IF ((NEWSTR.EQV. .FALSE.).AND.(MUE.EQ.(LENGTHLIB-1))) THEN
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            CURSTR=DSSPCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
         ELSEIF (NEWSTR.EQV. .TRUE. .AND.(MUE.EQ.(LENGTHLIB-1))) THEN
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            COUNTSTR=COUNTSTR+1
            CURSTR=DSSPCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
         END IF
      END DO
      ELNO=COUNTSTR

C--------------------------------------------------
C---- compute total number of elements and    -----
C---- averaged length for each secondary class ----
C--------------------------------------------------

      DO ITEL=1,ELNO
         LHELP=.TRUE.
         DO ITSEC=1,NSECELLIB
            IF ((ELCHAR(ITEL).EQ.SSCHAR(ITSEC)).AND.LHELP) THEN
               ELNOTOT(ITSEC)=ELNOTOT(ITSEC)+1
               ELLENTOT(ITSEC)=ELLENTOT(ITSEC)+ELLEN(ITEL)
               LHELP=.FALSE.
            END IF
         END DO
      END DO

      DO ITSEC=1,NSECELLIB
         ELNOTOT(NSECELLIB+1)=ELNOTOT(NSECELLIB+1)+ELNOTOT(ITSEC)
         ELLENTOT(NSECELLIB+1)=
     +        ELLENTOT(NSECELLIB+1)+ELLENTOT(ITSEC)
      END DO
      DO ITSEC=1,(NSECELLIB+1)
         MATLEN(ITSEC,1)=ELNOTOT(ITSEC)
         MATLEN(ITSEC,3)=ELLENTOT(ITSEC)
      END DO


C--------------------------------------------------
C---- evaluate desired distribution of elements ---
C--------------------------------------------------

      DO ITEL=1,ELNO
         IF (ELLEN(ITEL).LE.NHISTOLIB) THEN
            IHELP=ELLEN(ITEL)
         ELSE
            IHELP=NHISTOLIB
         END IF
         LHELP=.TRUE.
         DO ITSEC=1,(NSECELLIB-1)
            IF (LHELP.EQV. .TRUE. .AND.
     +           (ELCHAR(ITEL).EQ.SSCHAR(ITSEC))) THEN
               MATLENDIS(IHELP,ITSEC)=MATLENDIS(IHELP,ITSEC)+1
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            MATLENDIS(IHELP,NSECELLIB)=MATLENDIS(IHELP,NSECELLIB)+1
         END IF
      END DO

C---- write desired quantities seperately onto the printer
C---- NOTE: INTERWRTDES is a local parameter, see above
      IF (LWRT .EQV. .TRUE.) THEN
         WRITE(6,'(T5,A,T20,A,T65,A)')' SEVALLEN:',
     +        'number of DSSP elements for each prot',
     +        'class: length'
         WRITE(6,'(T2,A,T15,30I3)')'X.X: LEN  ',
     +        (ELLEN(ITEL),ITEL=1,ELNO)
         IF (NSECELLIB.EQ.3) THEN
            WRITE(6,'(T2,A,T15,4I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,4I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         ELSEIF (NSECELLIB.EQ.2) THEN
            WRITE(6,'(T2,A,T15,2I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,2I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         ELSEIF (NSECELLIB.EQ.4) THEN
            WRITE(6,'(T2,A,T15,5I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,5I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         END IF
         WRITE(6,*)
         WRITE(6,'(T5,A)')'distribution of DSSP elements'
         WRITE(6,'(T2,A2,T5,30I3)')
     +           'H:',(MATLENDIS(ITHISTO,3),ITHISTO=1,30)
         IF (NSECELLIB.GT.2) THEN
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'E:',(MATLENDIS(ITHISTO,2),ITHISTO=1,30)
         END IF
         IF (NSECELLIB.GT.3) THEN
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'T:',(MATLENDIS(ITHISTO,3),ITHISTO=1,30)
         END IF
         WRITE(6,'(T2,A2,T5,30I3)')
     +        'C:',(MATLENDIS(ITHISTO,NSECELLIB),ITHISTO=1,30)
      END IF

C--------------------------------------------------
C---- discriminate elements  for PRED         -----
C--------------------------------------------------

C---- setting initially to zero
      ELNO=0
      CALL SISTZ1(ELLEN,2000)
      CALL SISTZ1(ELNOTOT,9)
      CALL SISTZ1(ELLENTOT,9)

C---- counting elements and summing up their lengths
      COUNTMUE=0
      MUE=0
      COUNTSTR=0
      DO WHILE (MUE.LT.LENGTHLIB)
         COUNTSTR=COUNTSTR+1
         NEWSTR=.FALSE.
         DO WHILE ((MUE.LT.(LENGTHLIB-1)).AND.(NEWSTR.EQV. .FALSE.))
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            CURSTR=PREDCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
            IF (PREDCHAR(COUNTMUE+1).NE.CURSTR) THEN
               NEWSTR=.TRUE.
            END IF
         END DO
         IF ((NEWSTR.EQV. .FALSE.).AND.(MUE.EQ.(LENGTHLIB-1))) THEN
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            CURSTR=PREDCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
         ELSEIF (NEWSTR.EQV. .TRUE. .AND.(MUE.EQ.(LENGTHLIB-1))) THEN
            MUE=MUE+1
            COUNTMUE=COUNTMUE+1
            COUNTSTR=COUNTSTR+1
            CURSTR=PREDCHAR(COUNTMUE)
            ELCHAR(COUNTSTR)=CURSTR
            ELLEN(COUNTSTR)=ELLEN(COUNTSTR)+1
         END IF
      END DO
      ELNO=COUNTSTR

C--------------------------------------------------
C---- compute total number of elements and    -----
C---- averaged length for each secondary class ----
C--------------------------------------------------

      DO ITEL=1,ELNO
         LHELP=.TRUE.
         DO ITSEC=1,NSECELLIB
            IF ((ELCHAR(ITEL).EQ.SSCHAR(ITSEC)).AND.LHELP) THEN
               ELNOTOT(ITSEC)=ELNOTOT(ITSEC)+1
               ELLENTOT(ITSEC)=ELLENTOT(ITSEC)+ELLEN(ITEL)
               LHELP=.FALSE.
            END IF
         END DO
      END DO

      DO ITSEC=1,NSECELLIB
         ELNOTOT(NSECELLIB+1)=ELNOTOT(NSECELLIB+1)+ELNOTOT(ITSEC)
         ELLENTOT(NSECELLIB+1)=
     +        ELLENTOT(NSECELLIB+1)+ELLENTOT(ITSEC)
      END DO
      DO ITSEC=1,(NSECELLIB+1)
         MATLEN(ITSEC,2)=ELNOTOT(ITSEC)
         MATLEN(ITSEC,4)=ELLENTOT(ITSEC)
      END DO

C--------------------------------------------------
C---- evaluate desired distribution of elements ---
C--------------------------------------------------

      DO ITEL=1,ELNO
         IF (ELLEN(ITEL).LE.NHISTOLIB) THEN
            IHELP=ELLEN(ITEL)
         ELSE
            IHELP=NHISTOLIB
         END IF
         LHELP=.TRUE.
         DO ITSEC=1,(NSECELLIB-1)
            IF (LHELP.EQV. .TRUE. .AND.
     +           (ELCHAR(ITEL).EQ.SSCHAR(ITSEC))) THEN
               MATLENDIS(IHELP,(NSECELLIB+ITSEC))=
     +              MATLENDIS(IHELP,(NSECELLIB+ITSEC))+1
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            MATLENDIS(IHELP,2*NSECELLIB)=MATLENDIS(IHELP,2*NSECELLIB)+1
         END IF
      END DO

C---- write desired quantities seperately onto the printer
C---- NOTE: INTERWRTDES is a local parameter, see above
      IF (LWRT .EQV. .TRUE.) THEN
         WRITE(6,'(T5,A,T20,A,T65,A)')' SEVALLEN:',
     +        'number of predicted elements for each prot',
     +        'class: length'
         WRITE(6,'(T2,A,T15,30I3)')'X.X: LEN  ',
     +        (ELLEN(ITEL),ITEL=1,ELNO)
         IF (NSECELLIB.EQ.3) THEN
            WRITE(6,'(T2,A,T15,4I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,4I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         ELSEIF (NSECELLIB.EQ.2) THEN
            WRITE(6,'(T2,A,T15,3I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,3I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         ELSEIF (NSECELLIB.EQ.4) THEN
            WRITE(6,'(T2,A,T15,5I5)')'X.X: NOTOT:',
     +           (ELNOTOT(ITSEC),ITSEC=1,NSECELLIB+1)
            WRITE(6,'(T2,A,T15,5I5)')'    LENTOT:',
     +           (ELLENTOT(ITSEC),ITSEC=1,NSECELLIB+1)
         END IF
         WRITE(6,*)
         WRITE(6,*)'      distribution of predicted elements'
         WRITE(6,'(T2,A2,T5,30I3)')
     +        'H:',(MATLENDIS(ITHISTO,(NSECELLIB+1)),ITHISTO=1,30)
         IF (NSECELLIB.EQ.3) THEN
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'E:',(MATLENDIS(ITHISTO,(NSECELLIB+2)),ITHISTO=1,30)
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'L:',(MATLENDIS(ITHISTO,(NSECELLIB+3)),ITHISTO=1,30)
         ELSEIF (NSECELLIB.EQ.2) THEN
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'L:',(MATLENDIS(ITHISTO,(NSECELLIB+2)),ITHISTO=1,30)
         ELSEIF (NSECELLIB.EQ.4) THEN
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'E:',(MATLENDIS(ITHISTO,(NSECELLIB+2)),ITHISTO=1,30)
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'T:',(MATLENDIS(ITHISTO,(NSECELLIB+3)),ITHISTO=1,30)
            WRITE(6,'(T2,A2,T5,30I3)')
     +           'L:',(MATLENDIS(ITHISTO,(NSECELLIB+4)),ITHISTO=1,30)
         END IF
      END IF

      END
***** end of SEVALLEN

***** ------------------------------------------------------------------
***** SUB SEVALPO
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEVALPO
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-offs of a certain prediction are computed*
*     --------        i.e.: MATNUM, asf.                               *
*     input variables:NSECELLIB,LENGTHLIB,DSSPCHAR,PREDCHAR            *
*     output variab.: MATNUM,MATOFDSSP,MATOFPRED,Q3,SQ,CORR            *
*     SBRs calling:  SEVALQUO:  computes MATOF*, Q3, SQ, CORR with    *
*     --------------     passing MATNUM                                *
*     called by:      SEVALSEC (in lib-prot.f)                         *
*----------------------------------------------------------------------*
      SUBROUTINE SEVALPO(NSECELLIB,MAXNSECEL,LENGTHLIB,MAXLENGTH,
     +     DSSPCHAR,PREDCHAR,MATNUM,MATQOFDSSP,MATQOFPRED,Q3,SQ,CORR)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         MAXNSECEL,MAXLENGTH,NSECELLIB,LENGTHLIB,
     +     MATNUM(1:(MAXNSECEL+1),1:(MAXNSECEL+1))
      REAL            Q3,SQ,CORR(1:MAXNSECEL),
     +     MATQOFDSSP(1:MAXNSECEL,1:MAXNSECEL),
     +     MATQOFPRED(1:MAXNSECEL,1:MAXNSECEL)
      CHARACTER*1     SSCHAR(1:8)
      CHARACTER*1     DSSPCHAR(1:MAXLENGTH),PREDCHAR(1:MAXLENGTH)
C---- local variables
      INTEGER         ITSEC,ITSEC2,MUE,ISUM1,ISUM2
      LOGICAL         LHELP,LDSSP,LNET
******------------------------------*-----------------------------******
*     MUE,ITSEC,ITSEC2       serve as iteration variables              *
*     CORR(i)         correlation for class i, see POTRAIN/TESTCOR     *
*     DSSPCHAR(i)     secondary structure according to DSSP for a par- *
*                     ticular protein                                  *
*     ISUM1,2         help variables required for computing sums       *
*     LENGTHLIB       current length of protein                        *
*     LHELP           logical help variable for intermediately check   *
*     LDSSP,LNET      intermediate flags, used for checking whether for*
*                     a particular residue there is any of the three   *
*                     classes: if not there is a fault                 *
*     MATNUM(i,j)     the number of residues in a certain secondary    *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix, MATNUM(1,2) those DSSP helices predicted*
*                     as strands and MATNUM(1,4) all DSSP helices, resp.
*                     MATNUM(4,4) all residues predicted.              *
*     MATNUMALL       MATNUM summed over all proteins of all files read*
*     MATOFDSSP(i,j)  stores according to the same scheme as MATNUM the*
*                     percentages of residues predicted divided by the *
*                     numbers of DSSP (note there is no element (4,4) )*
*     MATOFPRED(i,j)  same as previous but now percentages of prediction
*     MAXLENGTH       maximal length of current protein allowed        *
*     MAXNHISTO       maximal number of columns for histogram          *
*     MAXNUMPROT      maximal number of proteins per read file         *
*     MAXNSECEL       maximal number of secondary structures allowed   *
*     NSECELLIB       currently read number of secondary structures    *
*     PREDCHAR(i)     secondary structure according to prediction for a*
*                      particular protein                              *
*     Q3              =properly predicted for all classes/all residues *
*     SQ              first divide predicted/DSSP in each class then   *
*                     sum all classes and divide by e.g. 3             *
*     SSCHAR(i)       character giving the one-letter symbol for i-th  *
*                     secondary structure class                        *
******------------------------------*-----------------------------******

C---- check variables passed
      IF (LENGTHLIB.GT.MAXLENGTH) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SEVALPO'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current length: ',LENGTHLIB,'  allocated:',MAXLENGTH
         WRITE(6,'(T5,A)')' Stopped in SEVALPO 12-2-92:1'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +        'The number of secondary structures passed to SBR SEVALPO'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SEVALPO 12-2-92:2'
         STOP
      END IF

C---- assign secondary characters
      SSCHAR(1)='H'
      SSCHAR(2)='E'
      SSCHAR(NSECELLIB)='L'
      IF (NSECELLIB.EQ.4) THEN
         SSCHAR(3)='T'
      ELSEIF (NSECELLIB.EQ.5) THEN
         SSCHAR(3)='T'
         SSCHAR(4)='S'
      ELSEIF (NSECELLIB.GT.5) THEN
         WRITE(6,'(T5,A)')
     +        'Caution: current version of SEVALPO distinguishes'
         WRITE(6,'(T5,A)')'only five different secondary structures!'
         WRITE(6,'(T5,A)')'Stopped in SBR SEVALPO: 12-2-92:3'
         STOP
      END IF

C---- to be on the save side: substitute potential blanks by 'L'
      DO MUE=1,LENGTHLIB
         LHELP=.TRUE.
         DO ITSEC=1,NSECELLIB
            IF (PREDCHAR(MUE).EQ.SSCHAR(ITSEC)) THEN
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            PREDCHAR(MUE)='L'
         END IF
         LHELP=.TRUE.
         DO ITSEC=1,NSECELLIB
            IF (DSSPCHAR(MUE).EQ.SSCHAR(ITSEC)) THEN
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            DSSPCHAR(MUE)='L'
         END IF
      END DO

C---- initialize by setting zero
      DO ITSEC=1,(MAXNSECEL+1)
         DO ITSEC2=1,(MAXNSECEL+1)
            MATNUM(ITSEC,ITSEC2)=0
         END DO
      END DO

C--------------------------------------------------
C---- new number matrix                       -----
C--------------------------------------------------

C      IF (.TRUE.) THEN
C------- loop over all residues
      DO MUE=1,LENGTHLIB
         LDSSP=.TRUE.
         LNET=.TRUE.
         DO ITSEC=1,NSECELLIB
            IF (DSSPCHAR(MUE).EQ.SSCHAR(ITSEC)) THEN
               LDSSP=.FALSE.
               DO ITSEC2=1,NSECELLIB
                  IF (PREDCHAR(MUE).EQ.SSCHAR(ITSEC2)) THEN
                     LNET=.FALSE.
                     MATNUM(ITSEC,ITSEC2)=MATNUM(ITSEC,ITSEC2)+1
                  END IF
               END DO
            END IF
         END DO

C---------- consistency check
         IF (LNET.OR.LDSSP) THEN
            WRITE(6,*)'for mue=',mue,' :ldssp',ldssp,
     +           '  lnet ',lnet,'  with dssp=',
     +           dsspchar(mue),'  net:',predchar(mue)
         END IF
      END DO

C------- summing up
      DO ITSEC=1,NSECELLIB
         DO ITSEC2=1,NSECELLIB
            MATNUM(ITSEC,(NSECELLIB+1))=
     +           MATNUM(ITSEC,(NSECELLIB+1))+MATNUM(ITSEC,ITSEC2)
            MATNUM((NSECELLIB+1),ITSEC)=
     +           MATNUM((NSECELLIB+1),ITSEC)+MATNUM(ITSEC2,ITSEC)
         END DO
      END DO
      ISUM1=0
      ISUM2=0
      DO ITSEC=1,NSECELLIB
         ISUM1=ISUM1+MATNUM(ITSEC,(NSECELLIB+1))
         ISUM2=ISUM2+MATNUM((NSECELLIB+1),ITSEC)
      END DO
C      END IF
      IF (ISUM1.EQ.ISUM2) THEN
         MATNUM((NSECELLIB+1),(NSECELLIB+1))=ISUM1
      ELSE
         WRITE(6,*)'number for dssp and net not equal!'
         WRITE(6,*)'new: evalpo,resp SEVALPO'
         WRITE(6,*)'for net:',ISUM2,'  for DSSP:',ISUM1
      END IF


C---- compute quotients
C     =============
      CALL SEVALQUO(NSECELLIB,MAXNSECEL,MATNUM,
     +     MATQOFDSSP,MATQOFPRED,Q3,SQ,CORR)
C     =============


      END
***** end of SEVALPO

***** ------------------------------------------------------------------
***** SUB SEVALQUO
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEVALQUO
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-offs of a certain prediction are computed*
*     --------        i.e.: quotients.                                 *
*     input variables:NSECELLIB,MATNUM                                 *
*     output variab.: MATOFDSSP,MATOFPRED,Q3,SQ,CORR                   *
*     called by:      SEVALSEC (in lib-prot.f):  computes MATNUM       *
*     ----------      SEVALPO: (in lib-prot.f):  computes MATNUM       *
*----------------------------------------------------------------------*
      SUBROUTINE SEVALQUO(NSECELLIB,MAXNSECEL,MATNUM,
     +     MATQOFDSSP,MATQOFPRED,Q3,SQ,CORR)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         MAXNSECEL,NSECELLIB,
     +     MATNUM(1:(MAXNSECEL+1),1:(MAXNSECEL+1))
      REAL            Q3,SQ,CORR(1:MAXNSECEL)
      REAL            MATQOFDSSP(1:MAXNSECEL,1:MAXNSECEL)
      REAL            MATQOFPRED(1:MAXNSECEL,1:MAXNSECEL)
C---- local variables
      INTEGER         ITSEC,ITSEC2,ITSEC3,ISUM1
      INTEGER         PRED(1:8),REJ(1:8),OVEREST(1:8),UNDEREST(1:8)
      REAL            SQRT,DNM,DNM1,DNM2,DNM3,DNM4,CNT,CNT1,CNT2,RSUM
******------------------------------*-----------------------------******
*     MUE,ITSEC,ITSEC2,ITSEC3       serve as iteration variables       *
*     CNT/DNM         used for intermediateyly computing a quotient    *
*     CORR(i)         correlation for class i, see POTRAIN/TESTCOR     *
*                     definition (Matthew)                             *
*                     pred(i)*rej(i)-underest(i)*overest(i)            *
*                 /sqrt( (pred+under)(pred+over)(rej+under)(rej+over) )*
*     LENGTHLIB       current length of protein                        *
*     LHELP           logical help variable for intermediately check   *
*     LDSSP,LNET      intermediate flags, used for checking whether for*
*                     a particular residue there is any of the three   *
*                     classes: if not there is a fault                 *
*     MATNUM(i,j)     the number of residues in a certain secondary    *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix, MATNUM(1,2) those DSSP helices predicted*
*                     as strands and MATNUM(1,4) all DSSP helices, resp.
*                     MATNUM(4,4) all residues predicted.              *
*     MATNUMALL       MATNUM summed over all proteins of all files read*
*     MATOFDSSP(i,j)  stores according to the same scheme as MATNUM the*
*                     percentages of residues predicted divided by the *
*                     numbers of DSSP (note there is no element (4,4) )*
*     MATOFPRED(i,j)  same as previous but now percentages of prediction
*     MAXNHISTO       maximal number of columns for histogram          *
*     MAXNSECEL       maximal number of secondary structures allowed   *
*     NSECELLIB       currently read number of secondary structures    *
*     OVEREST(i)      number of wrongly predicted secondary structure  *
*                     elements of class i (perceptron says it is in i  *
*                     but nature say, this is a lie)                   *
*     PRED(i)         number of properly predicted secondary structure *
*                     elements of class i                              *
*     Q3              =properly predicted for all classes/all residues *
*     REJ(i)          number of properly rejected secondary structure  *
*                     elements of class i (reject: not being i)        *
*     SQ              first divide predicted/DSSP in each class then   *
*                     sum all classes and divide by e.g. 3             *
*     UNDEREREST(i)   number of wrongly rejected secondary structure   *
*                     elements of class i (perceptron says it is not in*
*                     i but nature say, this is a lie)                 *
******------------------------------*-----------------------------******
      DNM=            0
      CNT=            0

C--------------------------------------------------
C---- compute quotients                       -----
C--------------------------------------------------

      DO ITSEC=1,NSECELLIB
         DO ITSEC2=1,NSECELLIB
            IF ((MATNUM(ITSEC,(NSECELLIB+1)).NE.0).AND.
     +           (MATNUM((NSECELLIB+1),ITSEC2).NE.0)) THEN
               MATQOFDSSP(ITSEC,ITSEC2)=
     +              100*MATNUM(ITSEC,ITSEC2)
     +              /REAL(MATNUM(ITSEC,(NSECELLIB+1)))
               MATQOFPRED(ITSEC,ITSEC2)=
     +              100*MATNUM(ITSEC,ITSEC2)
     +              /REAL(MATNUM((NSECELLIB+1),ITSEC2))
            ELSEIF ((MATNUM(ITSEC,(NSECELLIB+1)).NE.0).AND.
     +           (MATNUM((NSECELLIB+1),ITSEC2).EQ.0)) THEN
               MATQOFDSSP(ITSEC,ITSEC2)=
     +              100*MATNUM(ITSEC,ITSEC2)
     +              /REAL(MATNUM(ITSEC,(NSECELLIB+1)))
               MATQOFPRED(ITSEC,ITSEC2)=0
            ELSEIF ((MATNUM(ITSEC,(NSECELLIB+1)).EQ.0).AND.
     +           (MATNUM((NSECELLIB+1),ITSEC2).NE.0)) THEN
               MATQOFDSSP(ITSEC,ITSEC2)=0
               MATQOFPRED(ITSEC,ITSEC2)=
     +              100*MATNUM(ITSEC,ITSEC2)
     +              /REAL(MATNUM((NSECELLIB+1),ITSEC2))
            END IF
         END DO
      END DO

      ISUM1=0
      RSUM=0.
      DO ITSEC=1,NSECELLIB
         ISUM1=ISUM1+MATNUM(ITSEC,ITSEC)
         RSUM=RSUM+MATQOFDSSP(ITSEC,ITSEC)
      END DO
      IF (MATNUM((NSECELLIB+1),(NSECELLIB+1)).NE.0) THEN
         Q3=100*ISUM1/REAL(MATNUM((NSECELLIB+1),(NSECELLIB+1)))
      ELSE
         Q3=0
      END IF
      IF (NSECELLIB.NE.0) THEN
         SQ=RSUM/REAL(NSECELLIB)
      ELSE
         SQ=0
      END IF

C---- correlation (Matthews)
C---- for helix examples with M(ii) as MATNUM(i,i)
      DO ITSEC=1,NSECELLIB
C-------            = M(11)
         PRED(ITSEC)=MATNUM(ITSEC,ITSEC)
C-------            = M(22)+M(23)+M(32)+M(33)
         ISUM1=0
         DO ITSEC2=1,NSECELLIB
            IF (ITSEC2.NE.ITSEC) THEN
               DO ITSEC3=1,NSECELLIB
                  IF (ITSEC3.NE.ITSEC) THEN
                     ISUM1=ISUM1+MATNUM(ITSEC2,ITSEC3)
                  END IF
               END DO
            END IF
         END DO
         REJ(ITSEC)=ISUM1
C-------            = M(12)+M(13)
         ISUM1=0
         DO ITSEC2=1,NSECELLIB
            IF (ITSEC2.NE.ITSEC) THEN
               ISUM1=ISUM1+MATNUM(ITSEC,ITSEC2)
            END IF
         END DO
         UNDEREST(ITSEC)=ISUM1
C-------            = M(21)+M(31)
         ISUM1=0
         DO ITSEC2=1,NSECELLIB
            IF (ITSEC2.NE.ITSEC) THEN
               ISUM1=ISUM1+MATNUM(ITSEC2,ITSEC)
            END IF
         END DO
         OVEREST(ITSEC)=ISUM1

      END DO

      DO ITSEC=1,NSECELLIB
         CNT1=PRED(ITSEC)*REJ(ITSEC)
         CNT2=OVEREST(ITSEC)*UNDEREST(ITSEC)
         DNM1=REJ(ITSEC)+UNDEREST(ITSEC)
         DNM2=REJ(ITSEC)+OVEREST(ITSEC)
         DNM3=PRED(ITSEC)+OVEREST(ITSEC)
         DNM4=PRED(ITSEC)+UNDEREST(ITSEC)

C------- take care of potential overflow
         IF ((DNM1*DNM2*DNM3*DNM4).GT.(10.E18)) THEN
            CORR(ITSEC)=0.
         ELSE
            CNT=REAL(CNT1-CNT2)
            DNM=SQRT(REAL(DNM1*DNM2*DNM3*DNM4))
         END IF
         IF ((ABS(CNT).LE.(10.E-16)).OR.(DNM.GT.(10.E16))) THEN
            CORR(ITSEC)=0.
         ELSE
            CORR(ITSEC)=CNT/DNM
         END IF
      END DO

      END
***** end of SEVALQUO

***** ------------------------------------------------------------------
***** SUB SEVALSEG
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEVALSEG
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Mar,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The measures related to segments are computed.   *
*     input variables:NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES          *
*     ----------------DSSPCHAR,PREDCHAR                                *
*     output variab.: NUMSEGOVERL, QSEGFOV                            *
*     called by:      SEVALPRED (in lib-prot.f)                        *
*     SBRs calling:  
*                    from lib-prot.f:                                 *
*                    SSEGBEGIN, SSEGLOV, SSEGSOV, SSEGFOV, STABLESEG   *
*     procedure:    * overlapp NUMSEGOVERL(3/4,ITSEC) ,3=%OBS, 4=%PRED *
*     ----------         for H, E, T                                   *
*                            if overlapp > L/2                         *
*                        for L:                                        *
*                            if overlapp > 2                           *
*                   * correct: NUMSEGOVERL(5,ITSEC)                    *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*                   * overlapping fraction: QSEGFOV(1/2,ITSEC)        *
*                                     number of overlapping residues   *
*                        QSEGFOV(i)= ------------------------------   *
*                                     end (max) - begin (min)          *
*                     where                                            *
*                        max= maximum (end(pred),end(obs))             *
*                        min= minimum (beg(pred),beg(obs))             *
*----------------------------------------------------------------------*
      SUBROUTINE SEVALSEG(KSEG,LWRITEFILE,PROTNAME,
     +     NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,CHVP1,
     +     DSSPCHAR,PREDCHAR,CHVP2,NUMSEGOVERL,
     +     COUNTSEGMAT,QSEGLOV,QSEGSOV,QSEGFOV,DEVNOM,CHVPL)
      IMPLICIT        NONE
C---- variables/ parameters from calling SBR
      INTEGER         KSEG,MAXNSECEL,NSECELLIB,MAXNUMRES,NUMRESLIB,
     +     CHVPM(1:50),CHVPL,CHVP1,CHVP2,
     +     NUMSEGOVERL(1:9,1:(MAXNSECEL+1)),
     +     COUNTSEGMAT(1:2,1:(MAXNSECEL+1)),DEVNOM
      REAL            QSEGFOV(1:2,1:(MAXNSECEL+1)),
     +     QSEGLOV(1:2,1:(MAXNSECEL+1)),QSEGSOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*1     DSSPCHAR(1:MAXNUMRES),PREDCHAR(1:MAXNUMRES)
      CHARACTER*222   PROTNAME
      LOGICAL         LWRITEFILE
C---- local parameters
      INTEGER         MAXSEG
      PARAMETER      (MAXSEG=                 5000)
C---- local variables resp. passed to SBRs called
      INTEGER         MUE,ITSEC,COUNTSEG,ITER
      INTEGER         NUMCOR(1:2,1:8),
     +  POINTDSSP(1:2000),POINTPRED(1:2000),
     +  BEGSEGDSSP(1:MAXSEG),ENDSEGDSSP(1:MAXSEG),LENSEGDSSP(1:MAXSEG),
     +  BEGSEGPRED(1:MAXSEG),ENDSEGPRED(1:MAXSEG),LENSEGPRED(1:MAXSEG)
      LOGICAL         LHELP,LWRITE,LERRCHVP
      CHARACTER*1     SSCHAR(1:8)
******------------------------------*-----------------------------******
*     MUE,ITER        serve as iteration variables                     *
*     DEVNOM          allowed deviation in nominator :                 *
*                                  overlapping length + DEVNOM         *
*                     QSEGFOV =   ---------------------------         *
*                                  common length                       *
*     NUMSEGOVERL(i,j) number of overlapping/correct segments          *
*        i=1          number of DSSP segments in secondary structure   *
*                     j, for last: sum over all                        *
*        i=2          number of residues in structure i, summed up over*
*                     all segments (in i=1)                            *
*        i=3          number of predicted segments in class j          *
*        i=4          number of residues in structure i, summed up over*
*                     all segments (in i=1) for the prediction         *
*        i=5          number of overlapping predicted segments related *
*                     to those being observed.  Correct means:         *
*                     overlap >= length of segment / 2, for H, E, T    *
*                     and for loop: at least loop of 2, resp 1, if  the*
*                     DSSP loop is 1.                                  *
*        i=6          number of overlapping segments multiplied by     *
*                     length: related to %observed                     *
*        i=7          same as 5, but other way round: %pred!           *
*        i=8          same as 6, but other way round: %pred!           *
*                     j, for last: sum over all                        *
*        i=9          number of correct segments:                      *
*                     L +/- 1, and shift by 1, if L<=5                 *
*                     L +/- 1, and shift by 2, if 5<L<=10              *
*                     L +/- 2, and shift by 3, if L>10                 *
*                     noted: j, for last: sum over all                 *
*     QSEGLOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the loose overlap (half length overlap)    *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     QSEGSOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the strict overlap                         *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     QSEGFOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the fractional overlap:                    *
*                                  overlapping length                  *
*                     as given by: ------------------                  *
*                                  common length                       *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     LWRITE          variable used, to call subroutines such that the *
*                     result they compute is written out (true) or not *
******------------------------------*-----------------------------******
C--------------------------------------------------
C---- initial check of variables passed       -----
C--------------------------------------------------

      IF (CHVPL.NE.2) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SEVALSEG: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SEVALSEG: variables passed not correct !'
      END IF
C---- check variables passed
      IF (NUMRESLIB.GT.MAXNUMRES) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SEVALSEG'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current length: ',NUMRESLIB,'  allocated:',MAXNUMRES
         WRITE(6,'(T5,A)')' Stopped in SEVALSEG 12-11-92:3'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +       'The number of secondary structures passed to SBR SEVALSEG'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SEVALSEG 12-11-92:4'
         STOP
      END IF
C---- set zero
*      CALL SISTZ2(NUMSEGOVERL,9,(MAXNSECEL+1))
      DO ITSEC=1,(NSECELLIB+1)
         DO MUE=1,9
            NUMSEGOVERL(MUE,ITSEC)=0
         END DO
      END DO
C--------------------------------------------------
C---- assign secondary characters             -----
C--------------------------------------------------
      SSCHAR(1)='H'
      SSCHAR(2)='E'
      SSCHAR(NSECELLIB)='L'
      IF (NSECELLIB.EQ.4) THEN
         SSCHAR(3)='T'
      ELSEIF (NSECELLIB.EQ.5) THEN
         SSCHAR(3)='T'
         SSCHAR(4)='S'
      ELSEIF (NSECELLIB.GT.5) THEN
         WRITE(6,'(T5,A)')
     +        'Caution: current version of SEVALSEG distinguishes'
         WRITE(6,'(T5,A)')'only five different secondary structures!'
         WRITE(6,'(T5,A)')'Stopped in SBR SEVALSEG: 12-11-92:5'
         STOP
      END IF
C---- to be on the save side: substitute potential blanks by 'L'
      DO MUE=1,NUMRESLIB
         LHELP=.TRUE.
         DO ITSEC=1,(NSECELLIB-1)
            IF (LHELP.AND.(PREDCHAR(MUE).EQ.SSCHAR(ITSEC))) THEN
               LHELP=.FALSE.
            END IF
         END DO
         IF (LHELP .EQV. .TRUE.) THEN
            PREDCHAR(MUE)='L'
         END IF
      END DO
      LWRITE=.FALSE.
      IF (LWRITE .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A)')'---'
         WRITE(6,'(T2,A,T10,A,T60,A7)')'---',
     +        'SEVALSEG: overlapping segments for: ',PROTNAME
      END IF
C--------------------------------------------------
C---- storing begin and end of segments       -----
C--------------------------------------------------
C----------------------------------------
C---- assign begin and ends for DSSP ----
C----------------------------------------

C     ==============
      CALL SSEGBEGIN(NUMRESLIB,MAXNUMRES,1,MAXSEG,DSSPCHAR,2,
     +     BEGSEGDSSP,ENDSEGDSSP,3,LENSEGDSSP,COUNTSEG,3)
C     ==============
C---- store separately number of helices, strands, turns, and loops
      DO MUE=1,COUNTSEG
         DO ITSEC=1,NSECELLIB
            IF (DSSPCHAR(BEGSEGDSSP(MUE)).EQ.SSCHAR(ITSEC)) THEN
               NUMSEGOVERL(1,ITSEC)=NUMSEGOVERL(1,ITSEC)+1
               NUMSEGOVERL(2,ITSEC)=NUMSEGOVERL(2,ITSEC)+LENSEGDSSP(MUE)
            END IF
         END DO
      END DO
      NUMSEGOVERL(1,(NSECELLIB+1))=COUNTSEG
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(2,(NSECELLIB+1))=NUMSEGOVERL(2,(NSECELLIB+1))
     +        +NUMSEGOVERL(2,ITSEC)
      END DO
C---- consistency check lengths
      DO MUE=1,NUMSEGOVERL(1,(NSECELLIB+1))
         IF (LENSEGDSSP(MUE).NE.
     +        (ENDSEGDSSP(MUE)-BEGSEGDSSP(MUE))+1) THEN
            WRITE(6,'(T2,A,T10,A,T50,I4)')'***',
     +           'SEVALSEG: fault for observed segment: ',MUE
         END IF
      END DO
C---- assign pointers
      COUNTSEG=1
      DO MUE=1,NUMRESLIB
         POINTDSSP(MUE)=COUNTSEG
         IF (MUE.EQ.ENDSEGDSSP(COUNTSEG)) THEN
            COUNTSEG=COUNTSEG+1
         END IF
      END DO
C----------------------------------------
C---- assign begin and ends for PRED ----
C----------------------------------------
C     ==============
      CALL SSEGBEGIN(NUMRESLIB,MAXNUMRES,1,MAXSEG,PREDCHAR,2,
     +     BEGSEGPRED,ENDSEGPRED,3,LENSEGPRED,COUNTSEG,3)
C     ==============
C---- store separately number of helices, strands, turns, and loops
      DO MUE=1,COUNTSEG
         DO ITSEC=1,NSECELLIB
            IF (PREDCHAR(BEGSEGPRED(MUE)).EQ.SSCHAR(ITSEC)) THEN
               NUMSEGOVERL(3,ITSEC)=NUMSEGOVERL(3,ITSEC)+1
               NUMSEGOVERL(4,ITSEC)=NUMSEGOVERL(4,ITSEC)+LENSEGPRED(MUE)
            END IF
         END DO
      END DO
      NUMSEGOVERL(3,(NSECELLIB+1))=COUNTSEG
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(4,(NSECELLIB+1))=NUMSEGOVERL(4,(NSECELLIB+1))
     +        +NUMSEGOVERL(4,ITSEC)
      END DO
C---- consistency check: correctly counted?
      DO MUE=1,NUMSEGOVERL(3,(NSECELLIB+1))
         IF (LENSEGPRED(MUE).NE.
     +        (ENDSEGPRED(MUE)-BEGSEGPRED(MUE))+1) THEN
            WRITE(6,'(T2,A,T10,A,T50,I4)')'***',
     +           'SEVALSEG: fault for predicted segment: ',MUE
         END IF
      END DO
C---- assign pointers
      COUNTSEG=1
      DO MUE=1,NUMRESLIB
         POINTPRED(MUE)=COUNTSEG
         IF (MUE.EQ.ENDSEGPRED(COUNTSEG)) THEN
            COUNTSEG=COUNTSEG+1
         END IF
      END DO
C--------------------------------------------------
C---- overlapp % observed and % predicted     -----
C--------------------------------------------------
C     =============
      CALL SSEGLOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,
     +     MAXSEG,1,DSSPCHAR,PREDCHAR,2,LENSEGDSSP,LENSEGPRED,
     +     BEGSEGDSSP,ENDSEGDSSP,BEGSEGPRED,ENDSEGPRED,3,
     +     NUMSEGOVERL,QSEGLOV,3)
C     =============
C--------------------------------------------------
C---- computing correct elements              -----
C--------------------------------------------------
C     ============
      CALL SSEGSOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,MAXSEG,1,
     +     DSSPCHAR,PREDCHAR,SSCHAR,2,LENSEGDSSP,LENSEGPRED,
     +     POINTDSSP,POINTPRED,BEGSEGDSSP,BEGSEGPRED,
     +     3,NUMCOR,NUMSEGOVERL(1,(NSECELLIB+1)),
     +     NUMSEGOVERL(3,(NSECELLIB+1)),QSEGSOV,3)
C     ============
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(9,ITSEC)=NUMCOR(1,ITSEC)
      END DO
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(9,(NSECELLIB+1))=NUMSEGOVERL(9,(NSECELLIB+1))
     +        +NUMSEGOVERL(9,ITSEC)
      END DO
C--------------------------------------------------
C---- computing overlapping lengths quotients -----
C--------------------------------------------------

C     =============
      CALL SSEGFOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,
     +     MAXSEG,1,DSSPCHAR,PREDCHAR,SSCHAR,2,BEGSEGDSSP,
     +     BEGSEGPRED,ENDSEGDSSP,ENDSEGPRED,POINTDSSP,POINTPRED,
     +     3,NUMSEGOVERL(1,(NSECELLIB+1)),NUMSEGOVERL(3,(NSECELLIB+1)),
     +     COUNTSEGMAT,QSEGFOV,3,DEVNOM)
C     =============
C--------------------------------------------------
C---- writing result onto printer             -----
C--------------------------------------------------
C---- first: intermediately normalise the quotients
      DO ITSEC=1,(NSECELLIB+1)
         DO ITER=1,2
            IF (NUMSEGOVERL(2*ITER,ITSEC).NE.0) THEN
               QSEGLOV(ITER,ITSEC)=100*QSEGLOV(ITER,ITSEC)
     +              /REAL(NUMSEGOVERL(2*ITER,ITSEC))
               QSEGSOV(ITER,ITSEC)=100*QSEGSOV(ITER,ITSEC)
     +              /REAL(NUMSEGOVERL(2*ITER,ITSEC))
               QSEGFOV(ITER,ITSEC)=100*QSEGFOV(ITER,ITSEC)
     +              /REAL(NUMSEGOVERL(2*ITER,ITSEC))
C------------- search a potential bug/artefact
               IF (QSEGLOV(ITER,ITSEC).GT.100) THEN
                  QSEGLOV(ITER,ITSEC)=0
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'ERROR: in Sevalseg: QSEGLOV > 100%,'
                  WRITE(6,'(T2,A,T10,A,t25,i3,t30,a,t40,i3)')'***',
     +                 'for iter=',iter,'itsec=',itsec
               END IF
               IF (QSEGSOV(ITER,ITSEC).GT.100) THEN
                  QSEGSOV(ITER,ITSEC)=0
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'ERROR: in Sevalseg: QSEGSOV > 100%,'
                  WRITE(6,'(T2,A,T10,A,t25,i3,t30,a,t40,i3)')'***',
     +                 'for iter=',iter,'itsec=',itsec
               END IF
               IF (QSEGFOV(ITER,ITSEC).GT.100) THEN
                  QSEGFOV(ITER,ITSEC)=0
                  WRITE(6,'(T2,A,T10,A)')'***',
     +                 'ERROR: in Sevalseg: QSEGFOV > 100%,'
                  WRITE(6,'(T2,A,T10,A,t25,i3,t30,a,t40,i3)')'***',
     +                 'for iter=',iter,'itsec=',itsec
               END IF
            ELSE
               QSEGLOV(ITER,ITSEC)=0
               QSEGSOV(ITER,ITSEC)=0
               QSEGFOV(ITER,ITSEC)=0
            END IF
         END DO
      END DO
C---- writing result into file FILESEG (should be open!)
      IF (LWRITEFILE .EQV. .TRUE.) THEN
C        ==============
         CALL STABLESEG(KSEG,NSECELLIB,MAXNSECEL,PROTNAME,NUMSEGOVERL,
     +        QSEGLOV,QSEGSOV,QSEGFOV,DEVNOM)
C        ==============
      END IF
      END
***** end of SEVALSEG

***** ------------------------------------------------------------------
***** SUB SEXP_NOINBIN_2VEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEXP_NOINBIN_2VEC
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: dd ,        1994        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        Compares the two vectors of projected relative   *
*     --------        exposure (0-9) for observation and prediction    *
*                     and returns the matrix of number in bins.        *
*                     Definition:
*                     EXP_NOINBIN(i,j) = N means, for N residues the   *
*                        exposure is:                                  *
*                     observed in i (=> relative exposure = i*i /100), *
*                     predicted in j (=> relative exposure = j*j /100).*
*     in variables:   NUMRESMAX, NUMRES, DSSPBIN, PREDBIN,             *
*     out variables:  EXP_NOINBIN                                      *
*     SBRs calling:  from lib-unix.f:                                 *
*     --------------     SCHECKPASS                                    *
*----------------------------------------------------------------------*
      SUBROUTINE SEXP_NOINBIN_2VEC(NUMRESMAX,NUMRES,CHVP1,
     +     DSSPBIN,CHVP2,PREDBIN,CHVP3,EXP_NOINBIN,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3,
     +     NUMRESMAX,NUMRES,EXP_NOINBIN(0:9,0:9)
      INTEGER*2       DSSPBIN(1:NUMRESMAX),PREDBIN(1:NUMRESMAX)
C---- local variables                                                  *
      INTEGER         MUE,IT1,IT2
      LOGICAL         LERRCHVP
******------------------------------*-----------------------------******
*---------------------                                                 *
*     passed variables                                                 *
*---------------------                                                 *
*     LWRITE          variable used, to call subroutines such that the *
*                     result they compute is written out (true) or not *
*---------------------                                                 *
*     local variables                                                  *
*---------------------                                                 *
*     MUE,ITER        serve as iteration variables                     *
******------------------------------*-----------------------------******
C---- defaults

C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***','WARNING: SEXP_NOINBIN_2VEC: '//
     +     'number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for FDUMMY: variables passed not correct !'
      END IF
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C--------------------------------------------------
C----
C--------------------------------------------------

      DO IT1=0,9
         DO IT2=0,9
            EXP_NOINBIN(IT2,IT1)=0
         END DO
      END DO

      DO MUE=1,NUMRES
         EXP_NOINBIN(DSSPBIN(MUE),PREDBIN(MUE))=
     +        EXP_NOINBIN(DSSPBIN(MUE),PREDBIN(MUE))+1
      END DO
      END 
***** end of SEXP_NOINBIN_2VEC

***** ------------------------------------------------------------------
***** SUB SEXP_NOINSTATES
***** ------------------------------------------------------------------
C---- 
C---- NAME : SEXP_NOINSTATES
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1993        version 0.1    *
*                      changed: Dec,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        For a set of numbers of bins (obs,pred), the num-*
*     --------        ber of correct ones is returned for three models:*
*                     2states: 0-20, 20-100, e.g. 0-4, 5-9             *
*                     3states: 0-5, 5-25, 25-100, e.g. 0-2, 3-5, 6-9   *
*                     10     : per bin                                 *
*     in variables:   EXP_NOINBIN, THRESH2, THRESH3A, THRESH3B         *
*     output variab.: EXP_NO2STATES, EXP_NO3STATES, EXP_NO10STATES     *
*     ---------------    how often occurs: obs = n, pred = m,          *
*                        note: 25 = sum                                *
*----------------------------------------------------------------------*
      SUBROUTINE SEXP_NOINSTATES(EXP_NOINBIN,THRESH2,THRESH3A,
     +     THRESH3B,CHVP1,
     +     OBS_NO2STATES,OBS_NO3STATES,OBS_NO10STATES,CHVP2,
     +     EXP_NO2STATES,EXP_NO3STATES,EXP_NO10STATES,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         EXP_NO2STATES(1:3),EXP_NO3STATES(1:4),
     +     EXP_NO10STATES(1:11),EXP_NOINBIN(0:9,0:9),
     +     OBS_NO2STATES(1:3),OBS_NO3STATES(1:4),OBS_NO10STATES(1:11),
     +     THRESH2,THRESH3A,THRESH3B,CHVPM(1:50),CHVPL,CHVP1,CHVP2
C---- local variables
      INTEGER         ICOUNT,IT1,IT2,ICOUNT2,ICOUNT3
      LOGICAL         LERRCHVP
******------------------------------*-----------------------------******
******------------------------------*-----------------------------******
C---- default
C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.2) THEN
         WRITE(6,'(T2,A,T10,A)')'***','WARNING: SEXP_NOINSTATES: '//
     +        'number of passed variables not fitting'
         WRITE(6,'(T2,A,T10,A,T40,4I5)')'***',
     +     'they are: CHVPL: 1,2,3:',CHVPL,CHVP1,CHVP2
         STOP
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +      'ERROR for SEXP_NOINSTATES: variables passed not correct !'
      END IF
C---- check variables passed
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C--------------------------------------------------
C---- compute number of bins per AA: obs: n, pred m
C--------------------------------------------------

C---- 10 states
      ICOUNT=0
      ICOUNT2=0
      DO IT1=0,9
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3
         OBS_NO10STATES(IT1+1)=ICOUNT3

         ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT1)
         EXP_NO10STATES(IT1+1)=EXP_NOINBIN(IT1,IT1)
      END DO
      EXP_NO10STATES(11)=ICOUNT
      OBS_NO10STATES(11)=ICOUNT2

C---- 3 states
      DO IT1=1,3
         EXP_NO3STATES(IT1)=0
      END DO
      ICOUNT=0
      ICOUNT2=0
      DO IT1=0,(THRESH3A-1)
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3

         DO IT2=0,(THRESH3A-1)
            ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT2)
            EXP_NO3STATES(1)=EXP_NO3STATES(1)+EXP_NOINBIN(IT1,IT2)
         END DO
      END DO
      OBS_NO3STATES(1)=ICOUNT2
      ICOUNT2=0
      DO IT1=THRESH3A,(THRESH3B-1)
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3

         DO IT2=THRESH3A,(THRESH3B-1)
            ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT2)
            EXP_NO3STATES(2)=EXP_NO3STATES(2)+EXP_NOINBIN(IT1,IT2)
         END DO
      END DO
      OBS_NO3STATES(2)=ICOUNT2
      ICOUNT2=0
      DO IT1=THRESH3B,9
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3

         DO IT2=THRESH3B,9
            ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT2)
            EXP_NO3STATES(3)=EXP_NO3STATES(3)+EXP_NOINBIN(IT1,IT2)
         END DO
      END DO
      OBS_NO3STATES(3)=ICOUNT2
      EXP_NO3STATES(4)=ICOUNT
      ICOUNT=0
      DO IT1=1,3
         ICOUNT=ICOUNT+OBS_NO3STATES(IT1)
      END DO
      OBS_NO3STATES(4)=ICOUNT

C---- 2 states
      DO IT1=1,2
         EXP_NO2STATES(IT1)=0
      END DO
      ICOUNT=0
      ICOUNT2=0
      DO IT1=0,(THRESH2-1)
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3

         DO IT2=0,(THRESH2-1)
            ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT2)
            EXP_NO2STATES(1)=EXP_NO2STATES(1)+EXP_NOINBIN(IT1,IT2)
         END DO
      END DO
      OBS_NO2STATES(1)=ICOUNT2
      ICOUNT2=0
      DO IT1=THRESH2,9
         ICOUNT3=0
         DO IT2=0,9
            ICOUNT3=ICOUNT3+EXP_NOINBIN(IT1,IT2)
         END DO
         ICOUNT2=ICOUNT2+ICOUNT3

         DO IT2=THRESH2,9
            ICOUNT=ICOUNT+EXP_NOINBIN(IT1,IT2)
            EXP_NO2STATES(2)=EXP_NO2STATES(2)+EXP_NOINBIN(IT1,IT2)
         END DO
      END DO
      OBS_NO2STATES(2)=ICOUNT2
      EXP_NO2STATES(3)=ICOUNT
      OBS_NO2STATES(3)=ICOUNT2+OBS_NO2STATES(1)
      END
***** end of SEXP_NOINSTATES

***** ------------------------------------------------------------------
***** SUB SFILEOPEN
***** ------------------------------------------------------------------
C---- 
C---- NAME : SFILEOPEN
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE SFILEOPEN(UNIT,FILENAME,ACTSTATUS,LENGTH,ACTTASK)

      IMPLICIT        NONE

C---- local function
      INTEGER         FILEN_STRING

C---- local variables
      INTEGER         UNIT,LENGTH,IEND
      CHARACTER*(*)   FILENAME,ACTSTATUS,ACTTASK
      CHARACTER*222   CHFILE
******------------------------------*-----------------------------******

C     purge blanks from file name
      IEND=FILEN_STRING(FILENAME)
      CHFILE(1:IEND)=FILENAME(1:IEND)

C---- hack br 2001-01: avoid trouble with LINUX      
      IF     (ACTSTATUS(1:3).EQ.'OLD') THEN
         OPEN(UNIT,FILE=CHFILE(1:IEND),STATUS='OLD')
      ELSEIF (ACTSTATUS(1:3).EQ.'NEW') THEN
         OPEN(UNIT,FILE=CHFILE(1:IEND),STATUS='NEW')
      ELSE
         OPEN(UNIT,FILE=CHFILE(1:IEND),STATUS='UNKNOWN')
      END IF

C---- bullshit to avoid warnings
      IF (ACTTASK.EQ.'XX') THEN
         CONTINUE
      END IF
      IF (LENGTH.LT.1) THEN
         CONTINUE
      END IF

      RETURN
      END
***** end of SFILEOPEN

***** ------------------------------------------------------------------
***** SUB SILEN_STRING
***** ------------------------------------------------------------------
C---- 
C---- NAME : SILEN_STRING
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Feb,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The length of a given character string is returned
*     --------        resp. non-blank begin (ibeg) and end (iend)      *
*     input:          STRING     string of CHARACTER*222                *
*     output:         ibeg,iend                                        *
*----------------------------------------------------------------------*
      SUBROUTINE SILEN_STRING(STRING,IBEG,IEND)
C---- variables passing
      CHARACTER*222   STRING
C---- local variables
      INTEGER         ICOUNT,ITER,IBEG,IEND
      CHARACTER*222   HSTRING
      LOGICAL         LHELP
******------------------------------*-----------------------------******
C---- defaults
      HSTRING=STRING
      ICOUNT=0
      LHELP=.TRUE.
      DO ITER=1,80
         IF (LHELP .EQV. .TRUE.) THEN
            IF (HSTRING(ITER:ITER).NE.' ') THEN
               IF (ICOUNT.EQ.0) THEN
                  IBEG=ITER
               END IF
               ICOUNT=ICOUNT+1
            ELSE
               IF (ICOUNT.NE.0) THEN
                  IEND=ITER-1
                  LHELP=.FALSE.
               END IF
            END IF
         END IF
      END DO
      IF (ICOUNT.EQ.0) THEN
         WRITE(6,'(T2,A,T10,A,A1,A,A1)')'***',
     +        'ERROR: Sbr SILEN_STRING: empty string:','|',STRING,'|'
      END IF
      END
***** end of SILEN_STRING

***** ------------------------------------------------------------------
***** SUB SINTTOCHAR
***** ------------------------------------------------------------------
C---- 
C---- NAME : SINTTOCHAR
C---- ARG  : IINT,TXT
C---- DES  : converts the integer (<100) to a textstring (1:3) 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose:        convertion of INTEGER IINT into a character (*3) *
C     input variables:IINT                                             *
C     output:         TXT                                              *
*----------------------------------------------------------------------*
      SUBROUTINE SINTTOCHAR(IINT,TXT)
      INTEGER        IINT
      CHARACTER*3    TXT
************************************************************************
C---- warning
      IF (IINT.GE.100) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR in SINTTOCHAR library lib-comp!'
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'The number to be converted must be < 100.'
      END IF
C---- convert integer to character
      IF (IINT.EQ.1) THEN
         TXT='  1'
      ELSEIF (IINT.EQ.2) THEN
         TXT='  2'
      ELSEIF (IINT.EQ.3) THEN
         TXT='  3'
      ELSEIF (IINT.EQ.4) THEN
         TXT='  4'
      ELSEIF (IINT.EQ.5) THEN
         TXT='  5'
      ELSEIF (IINT.EQ.6) THEN
         TXT='  6'
      ELSEIF (IINT.EQ.7) THEN
         TXT='  7'
      ELSEIF (IINT.EQ.8) THEN
         TXT='  8'
      ELSEIF (IINT.EQ.9) THEN
         TXT='  9'
      ELSEIF (IINT.EQ.10) THEN
         TXT=' 10'
      ELSEIF (IINT.EQ.11) THEN
         TXT=' 11'
      ELSEIF (IINT.EQ.12) THEN
         TXT=' 12'
      ELSEIF (IINT.EQ.13) THEN
         TXT=' 13'
      ELSEIF (IINT.EQ.14) THEN
         TXT=' 14'
      ELSEIF (IINT.EQ.15) THEN
         TXT=' 15'
      ELSEIF (IINT.EQ.16) THEN
         TXT=' 16'
      ELSEIF (IINT.EQ.17) THEN
         TXT=' 17'
      ELSEIF (IINT.EQ.18) THEN
         TXT=' 18'
      ELSEIF (IINT.EQ.19) THEN
         TXT=' 19'
      ELSEIF (IINT.EQ.20) THEN
         TXT=' 20'
      ELSEIF (IINT.EQ.21) THEN
         TXT=' 21'
      ELSEIF (IINT.EQ.22) THEN
         TXT=' 22'
      ELSEIF (IINT.EQ.23) THEN
         TXT=' 23'
      ELSEIF (IINT.EQ.24) THEN
         TXT=' 24'
      ELSEIF (IINT.EQ.25) THEN
         TXT=' 25'
      ELSEIF (IINT.EQ.26) THEN
         TXT=' 26'
      ELSEIF (IINT.EQ.27) THEN
         TXT=' 27'
      ELSEIF (IINT.EQ.28) THEN
         TXT=' 28'
      ELSEIF (IINT.EQ.29) THEN
         TXT=' 29'
      ELSEIF (IINT.EQ.30) THEN
         TXT=' 30'
      ELSEIF (IINT.EQ.31) THEN
         TXT=' 31'
      ELSEIF (IINT.EQ.32) THEN
         TXT=' 32'
      ELSEIF (IINT.EQ.33) THEN
         TXT=' 33'
      ELSEIF (IINT.EQ.34) THEN
         TXT=' 34'
      ELSEIF (IINT.EQ.35) THEN
         TXT=' 35'
      ELSEIF (IINT.EQ.36) THEN
         TXT=' 36'
      ELSEIF (IINT.EQ.37) THEN
         TXT=' 37'
      ELSEIF (IINT.EQ.38) THEN
         TXT=' 38'
      ELSEIF (IINT.EQ.39) THEN
         TXT=' 39'
      ELSEIF (IINT.EQ.40) THEN
         TXT=' 40'
      ELSEIF (IINT.EQ.41) THEN
         TXT=' 41'
      ELSEIF (IINT.EQ.42) THEN
         TXT=' 42'
      ELSEIF (IINT.EQ.43) THEN
         TXT=' 43'
      ELSEIF (IINT.EQ.44) THEN
         TXT=' 44'
      ELSEIF (IINT.EQ.45) THEN
         TXT=' 45'
      ELSEIF (IINT.EQ.46) THEN
         TXT=' 46'
      ELSEIF (IINT.EQ.47) THEN
         TXT=' 47'
      ELSEIF (IINT.EQ.48) THEN
         TXT=' 48'
      ELSEIF (IINT.EQ.49) THEN
         TXT=' 49'
      ELSEIF (IINT.EQ.50) THEN
         TXT=' 50'
      ELSEIF (IINT.EQ.51) THEN
         TXT=' 51'
      ELSEIF (IINT.EQ.52) THEN
         TXT=' 52'
      ELSEIF (IINT.EQ.53) THEN
         TXT=' 53'
      ELSEIF (IINT.EQ.54) THEN
         TXT=' 54'
      ELSEIF (IINT.EQ.55) THEN
         TXT=' 55'
      ELSEIF (IINT.EQ.56) THEN
         TXT=' 56'
      ELSEIF (IINT.EQ.57) THEN
         TXT=' 57'
      ELSEIF (IINT.EQ.58) THEN
         TXT=' 58'
      ELSEIF (IINT.EQ.59) THEN
         TXT=' 59'
      ELSEIF (IINT.EQ.60) THEN
         TXT=' 60'
      ELSEIF (IINT.EQ.61) THEN
         TXT=' 61'
      ELSEIF (IINT.EQ.62) THEN
         TXT=' 62'
      ELSEIF (IINT.EQ.63) THEN
         TXT=' 63'
      ELSEIF (IINT.EQ.64) THEN
         TXT=' 64'
      ELSEIF (IINT.EQ.65) THEN
         TXT=' 65'
      ELSEIF (IINT.EQ.66) THEN
         TXT=' 66'
      ELSEIF (IINT.EQ.67) THEN
         TXT=' 67'
      ELSEIF (IINT.EQ.68) THEN
         TXT=' 68'
      ELSEIF (IINT.EQ.69) THEN
         TXT=' 69'
      ELSEIF (IINT.EQ.70) THEN
         TXT=' 70'
      ELSEIF (IINT.EQ.71) THEN
         TXT=' 71'
      ELSEIF (IINT.EQ.72) THEN
         TXT=' 72'
      ELSEIF (IINT.EQ.73) THEN
         TXT=' 73'
      ELSEIF (IINT.EQ.74) THEN
         TXT=' 74'
      ELSEIF (IINT.EQ.75) THEN
         TXT=' 75'
      ELSEIF (IINT.EQ.76) THEN
         TXT=' 76'
      ELSEIF (IINT.EQ.77) THEN
         TXT=' 77'
      ELSEIF (IINT.EQ.78) THEN
         TXT=' 78'
      ELSEIF (IINT.EQ.79) THEN
         TXT=' 79'
      ELSEIF (IINT.EQ.80) THEN
         TXT=' 80'
      ELSEIF (IINT.EQ.81) THEN
         TXT=' 81'
      ELSEIF (IINT.EQ.82) THEN
         TXT=' 82'
      ELSEIF (IINT.EQ.83) THEN
         TXT=' 83'
      ELSEIF (IINT.EQ.84) THEN
         TXT=' 84'
      ELSEIF (IINT.EQ.85) THEN
         TXT=' 85'
      ELSEIF (IINT.EQ.86) THEN
         TXT=' 86'
      ELSEIF (IINT.EQ.87) THEN
         TXT=' 87'
      ELSEIF (IINT.EQ.88) THEN
         TXT=' 88'
      ELSEIF (IINT.EQ.89) THEN
         TXT=' 89'
      ELSEIF (IINT.EQ.90) THEN
         TXT=' 90'
      ELSEIF (IINT.EQ.91) THEN
         TXT=' 91'
      ELSEIF (IINT.EQ.92) THEN
         TXT=' 92'
      ELSEIF (IINT.EQ.93) THEN
         TXT=' 93'
      ELSEIF (IINT.EQ.94) THEN
         TXT=' 94'
      ELSEIF (IINT.EQ.95) THEN
         TXT=' 95'
      ELSEIF (IINT.EQ.96) THEN
         TXT=' 96'
      ELSEIF (IINT.EQ.97) THEN
         TXT=' 97'
      ELSEIF (IINT.EQ.98) THEN
         TXT=' 98'
      ELSEIF (IINT.EQ.99) THEN
         TXT=' 99'
      END IF
      END
***** end of SINTTOCHAR

***** ------------------------------------------------------------------
***** SUB SISTZ1
***** ------------------------------------------------------------------
C---- 
C---- NAME : SISTZ1
C---- ARG  : IVEC,IROW
C---- DES  : sets zero a 1-dimensional integer vector (IROW 
C---- DES  : elements) 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose:          an integer 1-dimensional vector IVEC (IROW) is *
C                       set to zero for all elements                   *
C     input parameter:  IROW                                           *
C     input variables:  IVEC(integer vector)                           *
C     output variables: IVEC=0 for all elements                        *
*----------------------------------------------------------------------*
      SUBROUTINE SISTZ1(IVEC,IROW)

      INTEGER       IROW,ITER1
      INTEGER       IVEC(1:IROW)
      DO ITER1=1,IROW
         IVEC(ITER1)=0
      END DO
      END
***** end of SISTZ1

***** ------------------------------------------------------------------
***** SUB SISTZ2
***** ------------------------------------------------------------------
C---- 
C---- NAME : SISTZ2
C---- ARG  : IMAT,IROW,ICOL
C---- DES  : sets zero a 2-dimensional integer matrix(IROW,ICOL) 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Jun,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose: an integer 2-dimensional matrix IMAT (IROW,ICOL)        *
C              is set to zero for all elements                         *
C     input parameter:  IROW,ICOL                                      *
C     input variables:  IMAT(integer matrix)                           *
C     output variables: IMAT=0 for all elements                        *
*----------------------------------------------------------------------*
      SUBROUTINE SISTZ2(IMAT,IROW,ICOL)

      INTEGER       IROW,ICOL,ITROW,ITCOL
      INTEGER       IMAT(1:IROW,1:ICOL)

      DO ITCOL=1,ICOL
         DO ITROW=1,IROW
            IMAT(ITROW,ITCOL)=0
         END DO
      END DO
      END
***** end of SISTZ2

***** ------------------------------------------------------------------
***** SUB SRMAX1
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRMAX1
C---- ARG  : RVEC,IROW,MAXVAL,MAXPOS
C---- DES  : returns the maximal value of the components of the 
C---- DES  : real 
C---- DES  : vector RVEC(IROW) = MAXVAL, plus the position of that 
C---- DES  : value within the vector = MAXPOS 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose:        computation of maximal value of the elements of  *
C                     a real vector plus return of the position of that*
C                     value within the vector.                         *
C     input parameter:IROW                                             *
C     input variables:RVEC                                             *
C     output:         MAXVAL,MAXPOS                                    *
*----------------------------------------------------------------------*
      SUBROUTINE SRMAX1(RVEC,IROW,MAXVAL,MAXPOS)
      
      INTEGER         IROW,MAXPOS,ITER
      REAL            MAXVAL
      REAL            RVEC(1:IROW)

      MAXVAL=RVEC(1)
      MAXPOS=0
      DO ITER=1,IROW
         IF (MAXVAL.LE.RVEC(ITER)) THEN
            MAXVAL=RVEC(ITER)
            MAXPOS=ITER
         END IF
      END DO

C---- check whether one value regarded as maximum or not
      IF ((MAXPOS.EQ.0).AND.(MAXVAL.NE.RVEC(1))) THEN
         WRITE(6,'(T2,A)')'***'
         WRITE(6,'(T2,A)')'***'
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'probably an ERROR in library SBR SRMAX1 detected.'
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'The subroutine is expected to compute the maximum of a'
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'vector but there seems to be none.'
         WRITE(6,'(T2,A,T10,A,T45,I5,A)')'***',
     +        'Components are: (with IROW = ',IROW,')'
         WRITE(6,'(T2,A,T10,20F5.2)')'***',
     +        (RVEC(ITER),ITER=1,IROW)
         WRITE(6,'(T2,A,T10,A,T45,F8.5)')'***',
     +        'Maximum is computed to be: ',MAXVAL
         WRITE(6,'(T2,A)')'***'
         WRITE(6,'(T2,A)')'***'
      END IF

      END
***** end of SRMAX1

***** ------------------------------------------------------------------
***** SUB SRSORTVEC
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRSORTVEC
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: May,        1993        version 0.1    *
*                      changed: May,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The components of the real vector VEC are sorted *
*     --------        according to their seize.  The succession is     *
*                     returned in ISORT.                               *
*     in variables:   NROW,NROWMAX,VEC                                 *
*     out variables:  ISORT                                            *
*----------------------------------------------------------------------*
      SUBROUTINE SRSORTVEC(NROW,NROWMAX,CHVP1,RVEC,CHVP2,ISORT,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         CHVPM(1:2),CHVPL,CHVP1,CHVP2,
     +     NROW,NROWMAX,ISORT(1:NROWMAX)
      REAL            RVEC(1:NROWMAX)
C---- local variables                                                  *
      INTEGER         ITER,ITER2,POSMAX
      REAL            HMAX
      LOGICAL         LERRCHVP,LFOUND(1:1000)
******------------------------------*-----------------------------******

C---- defaults
      POSMAX=         0

      IF (NROWMAX.GT.1000) THEN
         WRITE(6,'(T2,A,T10,A,T50,I6)')'***',
     +        'ERROR in SRSORTVEC: watch out, NROWMAX =',NROWMAX
         WRITE(6,'(T2,A,T10,A)')'***',
     +        '      however, maximum for logicals LFOUND is 1000!'
      END IF

C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.2) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SSEGFOV: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C      CALL SCHECKPASS(LERRCHVP,CHVPL,CHVPM)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for : variables passed not correct !'
      END IF
C---- check variables passed
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C---- initialize
      DO ITER=1,NROW
         LFOUND(ITER)=.FALSE.
      END DO

C---- first sweep: look for maximum
      HMAX=0
      DO ITER=1,NROW
         HMAX=MAX(HMAX,RVEC(ITER))
         IF (HMAX.EQ.RVEC(ITER)) THEN
            POSMAX=ITER
         END IF
      END DO
      ISORT(1)=POSMAX
      LFOUND(POSMAX)=.TRUE.

C---- now determine all the others
      DO ITER=2,NROW
         HMAX=  0
         POSMAX=0
         DO ITER2=1,NROW
            IF (LFOUND(ITER2) .EQV. .FALSE. ) THEN
               HMAX=MAX(HMAX,RVEC(ITER2))
               IF (HMAX.EQ.RVEC(ITER2)) THEN
                  POSMAX=ITER2
               END IF
            END IF
         END DO
         IF (POSMAX .EQ. 0) THEN
            WRITE(6,'(T2,A,T10,A,T60,I3)')'***',
     +           'ERROR: non found in SRSORTVEC for iter=',ITER
         END IF
         ISORT(ITER)=POSMAX
         LFOUND(POSMAX)=.TRUE.
      END DO
      END 
***** end of SRSORTVEC

***** ------------------------------------------------------------------
***** SUB SRSTZ1
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRSTZ1
C---- ARG  : RVECT,IROW
C---- DES  : Sets zero a 1-dimens. real vector with the row-length 
C---- DES  : IROW 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose: a real vector RVEC is set to zero                       *
C     input parameter:  IROW                                           *
C     input variables:  RVEC(real vector)                              *
C     output variables: RVEC=0. for all elements                       *
*----------------------------------------------------------------------*
      SUBROUTINE SRSTZ1(RVEC,IROW)

      REAL       RVEC(1:IROW)
      DO ITER1=1,IROW
         RVEC(ITER1)=0.
      END DO
      END
***** end of SRSTZ1

***** ------------------------------------------------------------------
***** SUB SRSTZ2
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRSTZ2
C---- ARG  : RMAT,IROW,ICOL
C---- DES  : Sets zero a 2-dimensional real matrix with the 
C---- DES  : row-length 
C---- DES  : IROW, the column-length ICOL :RMAT(IROW,ICOL) 
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose: a real two dimensional matrix RMAT(rows,columns) is set *
C              to zero                                                 *
C     input parameter:  IROW,ICOL                                      *
C     input variables:  RMAT(real matrix)                              *
C     output variables: RMAT=0. for all elements                       *
*----------------------------------------------------------------------*
      SUBROUTINE SRSTZ2(RMAT,IROW,ICOL)

      REAL       RMAT(1:IROW,1:ICOL)
      DO ITER2=1,ICOL
         DO ITER1=1,IROW
            RMAT(ITER1,ITER2)=0.
         END DO
      END DO
      END
***** end of SRSTZ2

***** ------------------------------------------------------------------
***** SUB SSEGBEGIN
***** ------------------------------------------------------------------
C---- 
C---- NAME : SSEGBEGIN
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
C     purpose:        The begins and ends of the segments are found.   *
C     input variables:NSECELLIB, MAXNSECEL, NUMRESLIB, MAXNUMRES       *
C     ----------------RESCHAR,                                         *
C     output variab.: BEG, END, LEN, COUNTSEG                          *
C     called by:      SEVALSEG (in lib-prot.f)                         *
C     SBRs calling:  from lib-unix.f:                                 *
C                        SCHECKPASS                                    *
*----------------------------------------------------------------------*
      SUBROUTINE SSEGBEGIN(NUMRESLIB,MAXNUMRES,
     +     CHVP1,MAXSEG,RESCHAR,CHVP2,BEG,END,CHVP3,LEN,
     +     COUNTSEG,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         MAXNUMRES,NUMRESLIB,MAXSEG,
     +     BEG(1:MAXSEG),END(1:MAXSEG),LEN(1:MAXSEG),
     +     CHVPL,CHVP1,CHVP2,CHVP3
      CHARACTER*1     RESCHAR(1:MAXNUMRES)
C---- local variables                                                  *
      INTEGER         MUE,COUNTRES,COUNTSEG
      INTEGER         CHVPM(1:50)
      LOGICAL         LERRCHVP
      CHARACTER*1     CURSTR

      CURSTR=         CHAR(0)
      COUNTRES=       0
******------------------------------*-----------------------------******
C     MUE,ITER        serve as iteration variables                     *
******------------------------------*-----------------------------******
C---- CHeck passed parameter
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SSEGBEGIN: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SSEGBEGIN: variables passed not correct !'
      END IF
C--------------------------------------------------
C---- storing begin and end of segments       -----
C--------------------------------------------------
C---- assign begin and ends for RES
      DO MUE=1,NUMRESLIB
         IF (MUE.EQ.1) THEN
            CURSTR=RESCHAR(1)
            BEG(1)=1
            COUNTSEG=1
            COUNTRES=1
         ELSEIF ((MUE.EQ.NUMRESLIB).AND.(RESCHAR(MUE).EQ.CURSTR)) THEN
            END(COUNTSEG)=MUE
            LEN(COUNTSEG)=COUNTRES+1
         ELSEIF ((MUE.EQ.NUMRESLIB).AND.(RESCHAR(MUE).NE.CURSTR)) THEN
            END(COUNTSEG)=MUE-1
            LEN(COUNTSEG)=COUNTRES
            COUNTSEG=COUNTSEG+1
            BEG(COUNTSEG)=MUE
            END(COUNTSEG)=MUE
            LEN(COUNTSEG)=1
         ELSEIF (RESCHAR(MUE).EQ.CURSTR) THEN
            COUNTRES=COUNTRES+1
         ELSE
            END(COUNTSEG)=MUE-1
            LEN(COUNTSEG)=COUNTRES
            COUNTRES=1
            CURSTR=RESCHAR(MUE)
            COUNTSEG=COUNTSEG+1
            BEG(COUNTSEG)=MUE
         END IF
      END DO
      END
***** end of SSEGBEGIN

***** ------------------------------------------------------------------
***** SUB SSEGFOV
***** ------------------------------------------------------------------
C---- 
C---- NAME : SSEGFOV
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The quotient of overlapping/ summed length is    *
*     --------        computed.                                        *
*     input variables:NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,         *
*     ----------------DSSPCHAR,PREDCHAR,SSCHAR,                        *
*                     BEGSEGDSSP,BEGSEGPRED,ENDSEGDSSP,ENDSEGPRED,     *
*                     POINTDSSP,POINTPRED,NUMSEGDSSP,NUMSEGPRED,       *
*     output variab.: QSEGAVOV                                         *
*     called by:      SEVALSEG (in lib-prot.f)                         *
*     SBRs calling:  from lib-comp.f:                                 *
*     --------------     SISTZ1, SRSTZ2                                *
*                     from lib-unix.f:                                 *
*                        SCHECKPASS                                    *
*     procedure:      If two segments overlap (predict and observed)   *
*     ----------      the quotient of overlapping segment score is given
*                     by:                                              *
*                                     number of overlapping residues   *
*                        QSEGAVOV(i)= ------------------------------   *
*                                     end (max) - begin (min)          *
*                     where max= maximum (end(pred),end(obs))          *
*                           min= minimum (beg(pred),beg(obs))          *
*----------------------------------------------------------------------*
      SUBROUTINE SSEGFOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,
     +     MAXSEGPASS,CHVP1,DSSPCHAR,PREDCHAR,SSCHAR,CHVP2,
     +     BEGSEGDSSP,BEGSEGPRED,ENDSEGDSSP,ENDSEGPRED,POINTDSSP,
     +     POINTPRED,CHVP3,NUMSEGDSSP,NUMSEGPRED,COUNTSEG,
     +     QSEGFOV,CHVPL,DEVNOM)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         MAXNSECEL,NSECELLIB,MAXNUMRES,NUMRESLIB,DEVNOM,
     +     MAXSEGPASS,CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3,
     +     COUNTSEG(1:2,1:(MAXNSECEL+1)),NUMSEGDSSP,NUMSEGPRED,
     +     POINTPRED(1:MAXNUMRES),POINTDSSP(1:MAXNUMRES),
     +     BEGSEGDSSP(1:MAXSEGPASS),BEGSEGPRED(1:MAXSEGPASS),
     +     ENDSEGDSSP(1:MAXSEGPASS),ENDSEGPRED(1:MAXSEGPASS)
      REAL            QSEGFOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*1     DSSPCHAR(1:MAXNUMRES),PREDCHAR(1:MAXNUMRES)
      CHARACTER*1     SSCHAR(1:8)
C---- local variables                                                  *
      INTEGER         MUE,ITSEC,ITER,ITSEG,IHELP,BEGMUE,MUEFOUND,
     +     IMAXOV,IMINOV,INTERCOUNTSEG,MAXDEV
      LOGICAL         LERRCHVP,LOVER
*      CHARACTER*10    TXT1(1:5)
******------------------------------*-----------------------------******
*     MUE,ITER        serve as iteration variables                     *
*     DEVNOM          allowed deviation in nominator :                 *
*                                  overlapping length + DEVNOM         *
*                     as given by: ---------------------------         *
*                                  common length                       *
*     IMINOV          =minimal overlapping length (minimal region      *
*                     spanned by both strings)                         *
*     IMAXOV          =maximal overlapping length (maximal region      *
*                     spanned by either of the two)                    *
*     QSEGFOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the fractional overlap:                    *
*                                  overlapping length                  *
*                     as given by: ------------------                  *
*                                  common length                       *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     LWRITE          variable used, to call subroutines such that the *
*                     result they compute is written out (true) or not *
******------------------------------*-----------------------------******
C---- defaults
      MUEFOUND=       0

C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SSEGFOV: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SSEGFOV: variables passed not correct !'
      END IF
C---- check variables passed
      IF (NUMRESLIB.GT.MAXNUMRES) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SSEGFOV'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T50,I4)')
     +        ' Current length: ',NUMRESLIB,'  allocated:',MAXNUMRES
         WRITE(6,'(T5,A)')' Stopped in SSEGFOV 12-2-92:1'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +       'The number of secondary structures passed to SBR SSEGFOV'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SSEGFOV 12-11-92:2'
         STOP
      END IF
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C--------------------------------------------------
C---- computing overlapping lengths %obs      -----
C--------------------------------------------------
C---- set zero
C     -----------
      CALL SISTZ2(COUNTSEG,2,(MAXNSECEL+1))
C     -----------
      CALL SRSTZ2(QSEGFOV,2,(MAXNSECEL+1))
C     -----------
      DO MUE=1,NUMSEGDSSP
         BEGMUE=BEGSEGDSSP(MUE)
         LOVER=.FALSE.
         DO ITSEC=1,NSECELLIB
            IF ((LOVER .EQV. .FALSE.).AND.
     +           (DSSPCHAR(BEGMUE).EQ.SSCHAR(ITSEC))) THEN
C------------- search for closest segment predicted
               IF (DSSPCHAR(BEGMUE).EQ.PREDCHAR(BEGMUE)) THEN
                  LOVER=.TRUE.
                  MUEFOUND=BEGMUE
               ELSE
                  DO ITSEG=1,(ENDSEGDSSP(MUE)-BEGSEGDSSP(MUE))
                     IF ((LOVER .EQV. .FALSE.) .AND.
     +                    (DSSPCHAR(BEGMUE+ITSEG)) .EQ.
     +                    PREDCHAR(BEGMUE+ITSEG)) THEN
                        LOVER=.TRUE.
                        MUEFOUND=BEGMUE+ITSEG
                     END IF
                  END DO
               END IF

               IF (LOVER .EQV. .TRUE.) THEN
                  IHELP=POINTPRED(MUEFOUND)
                  IMINOV=
     +                 (-MAX(BEGSEGDSSP(MUE),BEGSEGPRED(IHELP))
     +                 +MIN(ENDSEGDSSP(MUE),ENDSEGPRED(IHELP))+1)
                  IMAXOV=
     +                 (-MIN(BEGSEGDSSP(MUE),BEGSEGPRED(IHELP))
     +                 +MAX(ENDSEGDSSP(MUE),ENDSEGPRED(IHELP))+1)
C---------------- allow deviation
                  IF (DEVNOM.EQ.50) THEN
                     MAXDEV=MIN(IMINOV,
     +                    INT((ENDSEGDSSP(MUE)-BEGSEGDSSP(MUE)+1)/2.))
                  ELSEIF (DEVNOM.EQ.100) THEN
                     MAXDEV=MIN(IMINOV,
     +                    (ENDSEGDSSP(MUE)-BEGSEGDSSP(MUE)+1))
                  ELSE
                     MAXDEV=MIN(IMINOV,DEVNOM)
                  END IF
                  DO ITER=1,MAXDEV
                     IF (IMAXOV.GT.IMINOV) THEN
                        IMINOV=IMINOV+1
                     END IF
                  END DO
C----------------
                  COUNTSEG(1,ITSEC)=COUNTSEG(1,ITSEC)+1
                  IF (IMAXOV.GT.0) THEN
                     QSEGFOV(1,ITSEC)=QSEGFOV(1,ITSEC)
     +                    +IMINOV/REAL(IMAXOV)
     +                    *(ENDSEGDSSP(MUE)-BEGSEGDSSP(MUE)+1)
                  ELSE
                     WRITE(6,'(T2,A,T10,A,T50,I3)')'***',
     +                    'ERROR in SSEGFOV: length=0 for',IMAXOV
                     WRITE(6,'(T2,A,T10,A,T40,I4,A,T60,A1)')'***',
     +                    'for obs seg:',MUE,'  which is a: ',
     +                    SSCHAR(ITSEC)
                  END IF
               ELSE
                  COUNTSEG(1,ITSEC)=COUNTSEG(1,ITSEC)+1
               END IF
            END IF
         END DO
C------- end of computation for H, E, T
      END DO
C---- end of loop over observed segments
C---- normalize with number of respective segments
      QSEGFOV(1,(NSECELLIB+1))=0.
      INTERCOUNTSEG=0
      DO ITSEC=1,NSECELLIB
         IF (COUNTSEG(1,ITSEC).NE.0) THEN
            QSEGFOV(1,(NSECELLIB+1))=QSEGFOV(1,(NSECELLIB+1))
     +           +QSEGFOV(1,ITSEC)
            INTERCOUNTSEG=INTERCOUNTSEG+COUNTSEG(1,ITSEC)
C--------------------------------------------------
C---- out now, since normalisation with number of residues
C            QSEGFOV(1,ITSEC)=QSEGFOV(1,ITSEC)/REAL(COUNTSEG(1,ITSEC))
C--------------------------------------------------
         END IF
      END DO
      COUNTSEG(1,(NSECELLIB+1))=INTERCOUNTSEG
C--------------------------------------------------
C---- out now, since normalisation with number of residues
C      QSEGFOV(1,(NSECELLIB+1))=
C     +     QSEGFOV(1,(NSECELLIB+1))/REAL(INTERCOUNTSEG)
C--------------------------------------------------
C      write(6,*)'x.x lib-x.f'
C      write(6,*)'dssp'
C      write(6,'(60A1)')(dsspchar(mue),mue=1,numreslib)
C      write(6,*)'pred'
C      write(6,'(60A1)')(predchar(mue),mue=1,numreslib)
C      write(6,'(A,T20,4I5)')'countseg = ',(countseg(1,iter),iter=1,4)
C      write(6,'(A,T20,4I5)')'fov=',(int(100*qSEGFOV(1,iter)),iter=1,4)
C---- end computing overlapping lengths %obs  -----
C--------------------------------------------------
C--------------------------------------------------
C---- computing overlapping lengths %pred     -----
C--------------------------------------------------
      DO MUE=1,NUMSEGPRED
         BEGMUE=BEGSEGPRED(MUE)
         LOVER=.FALSE.
         DO ITSEC=1,NSECELLIB
            IF ((LOVER .EQV. .FALSE.).AND.
     +           (PREDCHAR(BEGMUE).EQ.SSCHAR(ITSEC))) THEN
               IF (PREDCHAR(BEGMUE).EQ.DSSPCHAR(BEGMUE)) THEN
                  LOVER=.TRUE.
                  MUEFOUND=BEGMUE
               ELSE
                  DO ITSEG=1,(ENDSEGPRED(MUE)-BEGSEGPRED(MUE))
                     IF ((LOVER .EQV. .FALSE.).AND.
     +                    (PREDCHAR(BEGMUE+ITSEG).EQ.
     +                    DSSPCHAR(BEGMUE+ITSEG))) THEN
                        LOVER=.TRUE.
                        MUEFOUND=BEGMUE+ITSEG
                     END IF
                  END DO
               END IF
               IF (LOVER .EQV. .TRUE.) THEN
                  IHELP=POINTDSSP(MUEFOUND)
                  IMINOV=
     +                 (-MAX(BEGSEGPRED(MUE),BEGSEGDSSP(IHELP))
     +                 +MIN(ENDSEGPRED(MUE),ENDSEGDSSP(IHELP))+1)
                  IMAXOV=
     +                 (-MIN(BEGSEGPRED(MUE),BEGSEGDSSP(IHELP))
     +                 +MAX(ENDSEGPRED(MUE),ENDSEGDSSP(IHELP))+1)
C---------------- allow deviation
                  IF (DEVNOM.EQ.50) THEN
                     MAXDEV=MIN(IMINOV,
     +                    INT((ENDSEGPRED(MUE)-BEGSEGPRED(MUE)+1)/2.))
                  ELSEIF (DEVNOM.EQ.100) THEN
                     MAXDEV=MIN(IMINOV,
     +                    (ENDSEGPRED(MUE)-BEGSEGPRED(MUE)+1))
                  ELSE
                     MAXDEV=MIN(IMINOV,DEVNOM)
                  END IF
                  DO ITER=1,MAXDEV
                     IF (IMAXOV.GT.IMINOV) THEN
                        IMINOV=IMINOV+1
                     END IF
                  END DO
C----------------
                  COUNTSEG(2,ITSEC)=COUNTSEG(2,ITSEC)+1
                  IF (IMAXOV.GT.0) THEN
                     QSEGFOV(2,ITSEC)=QSEGFOV(2,ITSEC)
     +                    +IMINOV/REAL(IMAXOV)
     +                    *(ENDSEGPRED(MUE)-BEGSEGPRED(MUE)+1)
                  ELSE
                     WRITE(6,'(T2,A,T10,A,T50,I3)')'***',
     +                    'ERROR in SSEGFOV: length=0 for',IMAXOV
                     WRITE(6,'(T2,A,T10,A,T40,I4,A,T60,A1)')'***',
     +                    'for obs seg:',MUE,'  which is a: ',
     +                    SSCHAR(ITSEC)
                  END IF
               ELSE
                  COUNTSEG(2,ITSEC)=COUNTSEG(2,ITSEC)+1
               END IF
            END IF
         END DO
C------- end of computation for H, E, T
      END DO
C---- end of loop over observed segments
C---- normalize with number of respective segments
      QSEGFOV(2,(NSECELLIB+1))=0.
      INTERCOUNTSEG=0
      DO ITSEC=1,NSECELLIB
         IF (COUNTSEG(2,ITSEC).NE.0) THEN
            QSEGFOV(2,(NSECELLIB+1))=QSEGFOV(2,(NSECELLIB+1))
     +           +QSEGFOV(2,ITSEC)
            INTERCOUNTSEG=INTERCOUNTSEG+COUNTSEG(2,ITSEC)
C--------------------------------------------------
C---- out now, since normalisation with number of residues
C            QSEGFOV(2,ITSEC)=QSEGFOV(2,ITSEC)/REAL(COUNTSEG(2,ITSEC))
C--------------------------------------------------
         END IF
      END DO
      COUNTSEG(2,(NSECELLIB+1))=INTERCOUNTSEG
C--------------------------------------------------
C---- out now, since normalisation with number of residues
C      QSEGFOV(2,(NSECELLIB+1))=
C     +     QSEGFOV(2,(NSECELLIB+1))/REAL(INTERCOUNTSEG)
C--------------------------------------------------
C---- end computing overlapping lengths %obs  -----
C--------------------------------------------------
      END
***** end of SSEGFOV

***** ------------------------------------------------------------------
***** SUB SSEGLOV
***** ------------------------------------------------------------------
C---- 
C---- NAME : SSEGLOV
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Mar,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The measures related to segments are computed.   *
*     input variables:NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES          *
*     ----------------DSSPCHAR,PREDCHAR                                *
*     output variab.: NUMSEGOVERL                                      *
*     called by:      SEVALPRED (in lib-prot.f)                        *
*     SBRs calling:  from lib-comp.f:                                 *
*     --------------     SISTZ1,                                       *
*                     from lib-unix.f:                                 *
*                        SCHECKPASS                                    *
*                     from lib-prot.f:                                 *
*                        SSEGBEGIN, STABLESEG                          *
*     procedure:      overlapp NUMSEGOVERL(3/4,ITSEC) ,3=%OBS, 4=%PRED *
*     ----------         for H, E, T                                   *
*                            if overlapp > L/2                         *
*                        for L:                                        *
*                            if overlapp > 2                           *
*                     correct: NUMSEGOVERL(5,ITSEC)                    *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*----------------------------------------------------------------------*
      SUBROUTINE SSEGLOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,
     +     MAXSEGPASS,CHVP1,DSSPCHAR,PREDCHAR,CHVP2,LENSEGDSSP,
     +     LENSEGPRED,BEGSEGDSSP,ENDSEGDSSP,BEGSEGPRED,ENDSEGPRED,
     +     CHVP3,NUMSEGOVERL,QSEGLOV,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER       MAXNSECEL,NSECELLIB,MAXNUMRES,NUMRESLIB,MAXSEGPASS,
     +     CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3,
     +     NUMSEGOVERL(1:9,1:(MAXNSECEL+1)),
     +     BEGSEGDSSP(1:MAXSEGPASS),ENDSEGDSSP(1:MAXSEGPASS),
     +     LENSEGDSSP(1:MAXSEGPASS),LENSEGPRED(1:MAXSEGPASS),
     +     BEGSEGPRED(1:MAXSEGPASS),ENDSEGPRED(1:MAXSEGPASS)
      REAL            QSEGLOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*1     DSSPCHAR(1:MAXNUMRES),PREDCHAR(1:MAXNUMRES)
C---- local variables                                                  *
      INTEGER         MUE,ITSEC,ITOVER,ITER,IHELP,BEGMUE,ENDMUE
      LOGICAL         LERRCHVP,LOVER
******------------------------------*-----------------------------******
*     MUE,ITER        serve as iteration variables                     *
*     NUMSEGOVERL(i,j) number of overlapping/correct segments          *
*        i=1          number of DSSP segments in secondary structure   *
*                     j, for last: sum over all                        *
*        i=2          number of residues in structure i, summed up over*
*                     all segments (in i=1)                            *
*        i=3          number of predicted segments in class j          *
*        i=4          number of residues in structure i, summed up over*
*                     all segments (in i=1) for the prediction         *
*        i=5          number of overlapping predicted segments related *
*                     to those being observed.  Correct means:         *
*                     overlap >= length of segment / 2, for H, E, T    *
*                     and for loop: at least loop of 2, resp 1, if  the*
*                     DSSP loop is 1.                                  *
*        i=6          number of overlapping segments multiplied by     *
*                     length: related to %observed                     *
*        i=7          same as 5, but other way round: %pred!           *
*        i=8          same as 6, but other way round: %pred!           *
*                     j, for last: sum over all                        *
*        i=9          number of correct segments:                      *
*                     L +/- 1, and shift by 1, if L<=5                 *
*                     L +/- 1, and shift by 2, if 5<L<=10              *
*                     L +/- 2, and shift by 3, if L>10                 *
*                     noted: j, for last: sum over all                 *
*     QSEGLOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the loose overlap (half length overlap)    *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     LWRITE          variable used, to call subroutines such that the *
*                     result they compute is written out (true) or not *
******------------------------------*-----------------------------******
C---- initial check of variables passed
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SSEGLOV: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SSEGLOV: variables passed not correct !'
      END IF
C---- check variables passed
      IF (NUMRESLIB.GT.MAXNUMRES) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SSEGLOV'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current length: ',NUMRESLIB,'  allocated:',MAXNUMRES
         WRITE(6,'(T5,A)')' Stopped in SSEGLOV 12-11-92:3'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +       'The number of secondary structures passed to SBR SSEGLOV'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SSEGLOV 12-11-92:4'
         STOP
      END IF
C---- set zero
C     -----------
      CALL SRSTZ2(QSEGLOV,2,(MAXNSECEL+1))
C     -----------
      DO ITER=1,4
         DO ITSEC=1,(NSECELLIB+1)
            NUMSEGOVERL((4+ITER),ITSEC)=0
         END DO
      END DO

C--------------------------------------------------
C---- overlapp % observed                     -----
C--------------------------------------------------
      DO MUE=1,NUMSEGOVERL(1,(NSECELLIB+1))
         BEGMUE=BEGSEGDSSP(MUE)
         ENDMUE=ENDSEGDSSP(MUE)

C----------------------------------------
C------- helix, strand, turn     --------
C----------------------------------------
         IF ((DSSPCHAR(BEGMUE).EQ.'H').OR.(DSSPCHAR(BEGMUE).EQ.'E').OR.
     +        (DSSPCHAR(BEGMUE).EQ.'T')) THEN
            LOVER=.TRUE.
            IF (INT(LENSEGDSSP(MUE)).LT.2) THEN
               WRITE(6,'(T2,A,T10,A,T40,A1,A,T60,I3)')'***',
     +              'segment too short for:',DSSPCHAR(MUE),
     +              'seg no:',MUE
               LOVER=.FALSE.
            ELSEIF (INT(LENSEGDSSP(MUE)).GT.NUMRESLIB) THEN
               WRITE(6,'(T2,A,T10,A,T40,A1,A,T60,I3)')'***',
     +              'segment too long for:',DSSPCHAR(MUE),
     +              'seg no:',MUE
               LOVER=.FALSE.
            ELSE
C------------- overlapp with N-terminal end of segment?
               DO ITOVER=1,INT(LENSEGDSSP(MUE)/2.)
                  IHELP=BEGMUE+ITOVER-1
                  IF (LOVER.AND.
     +                 (PREDCHAR(IHELP).NE.DSSPCHAR(IHELP))) THEN
                     LOVER=.FALSE.

C------------------- allow H H -> HHH
                     IF ((IHELP.GT.1).AND.(IHELP.LT.NUMRESLIB)) THEN
                        IF ((PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +                     (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP))) THEN
                           LOVER=.TRUE.
                        END IF
                     END IF
                  END IF
               END DO
            END IF

C---------- overlapp with C-terminal end of segment?
            IF ((LOVER.EQV. .FALSE.).AND.(LENSEGDSSP(MUE).GT.2).AND.
     +           (LENSEGDSSP(MUE).LT.NUMRESLIB)) THEN
               LOVER=.TRUE.
               DO ITOVER=1,INT(LENSEGDSSP(MUE)/2.)
                  IHELP=ENDMUE-INT(LENSEGDSSP(MUE)/2.)+ITOVER-1
                  IF (IHELP.LT.0) THEN
                     LOVER=.FALSE.
                  ELSE IF (LOVER.EQV. .TRUE. .AND.(PREDCHAR(IHELP).NE.
     +                 DSSPCHAR(IHELP))) THEN
                     LOVER=.FALSE.

C------------------- allow H H -> HHH
                     IF ((IHELP.GT.1).AND.(IHELP.LT.NUMRESLIB)) THEN
                        IF ((PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +                      (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP))) THEN
                           LOVER=.TRUE.
                        END IF
                     END IF
                  END IF
               END DO
            END IF
C---------- overlapped?
            IF (LOVER .EQV. .TRUE.) THEN
               IF (DSSPCHAR(BEGMUE).EQ.'H') THEN
                  NUMSEGOVERL(5,1)=NUMSEGOVERL(5,1)+1
                  NUMSEGOVERL(6,1)=NUMSEGOVERL(6,1)+LENSEGDSSP(MUE)
                  QSEGLOV(1,1)=QSEGLOV(1,1)+LENSEGDSSP(MUE)
               ELSEIF (DSSPCHAR(BEGMUE).EQ.'E') THEN
                  NUMSEGOVERL(5,2)=NUMSEGOVERL(5,2)+1
                  NUMSEGOVERL(6,2)=NUMSEGOVERL(6,2)+LENSEGDSSP(MUE)
                  QSEGLOV(1,2)=QSEGLOV(1,2)+LENSEGDSSP(MUE)
               ELSEIF (DSSPCHAR(BEGMUE).EQ.'T') THEN
                  NUMSEGOVERL(5,3)=NUMSEGOVERL(5,3)+1
                  NUMSEGOVERL(6,3)=NUMSEGOVERL(6,3)+LENSEGDSSP(MUE)
                  QSEGLOV(1,3)=QSEGLOV(1,3)+LENSEGDSSP(MUE)
               END IF
            END IF
C----------------------------------------
C------- loop overlap > 2        --------
C----------------------------------------
         ELSEIF (DSSPCHAR(BEGMUE).EQ.'L') THEN
            LOVER=.FALSE.
C---------- overlapp with loop anywhere > 2?
            IF (LENSEGDSSP(MUE).EQ.1) THEN
               IF (PREDCHAR(BEGMUE).EQ.'L') THEN
                  LOVER=.TRUE.
               END IF
            ELSEIF (LENSEGDSSP(MUE).EQ.2) THEN
               IF (PREDCHAR(BEGMUE).EQ.'L') THEN
                  LOVER=.TRUE.
               ELSEIF ((BEGMUE.LT.NUMRESLIB).AND.
     +                 (PREDCHAR(BEGMUE+1).EQ.'L')) THEN
                  LOVER=.TRUE.
               END IF
            ELSEIF (LENSEGDSSP(MUE).GT.NUMRESLIB) THEN
               LOVER=.FALSE.
            ELSEIF (ENDMUE.GT.NUMRESLIB) THEN
               LOVER=.FALSE.
            ELSE
               DO ITOVER=BEGMUE,ENDMUE
                  IF ((LOVER.EQV. .FALSE.).AND.
     +                 (PREDCHAR(ITOVER).EQ.'L')) THEN
                     IF (ITOVER.GT.BEGMUE) THEN
                        IF (PREDCHAR(ITOVER-1).EQ.'L') THEN
                           LOVER=.TRUE.
                        END IF
                     ELSEIF (ITOVER.LT.ENDMUE) THEN
                        IF (PREDCHAR(ITOVER+1).EQ.'L') THEN
                           LOVER=.TRUE.
                        END IF
                     END IF
                  END IF
               END DO
            END IF
C---------- overlapped?
            IF (LOVER .EQV. .TRUE.) THEN
               NUMSEGOVERL(5,NSECELLIB)=NUMSEGOVERL(5,NSECELLIB)+1
               NUMSEGOVERL(6,NSECELLIB)=NUMSEGOVERL(6,NSECELLIB)
     +              +LENSEGDSSP(MUE)
               QSEGLOV(1,NSECELLIB)=QSEGLOV(1,NSECELLIB)+LENSEGDSSP(MUE)
            END IF
         END IF
      END DO
C---- compute number of all overlapping segments
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(5,(NSECELLIB+1))=NUMSEGOVERL(5,(NSECELLIB+1))
     +        +NUMSEGOVERL(5,ITSEC)
         NUMSEGOVERL(6,(NSECELLIB+1))=NUMSEGOVERL(6,(NSECELLIB+1))
     +        +NUMSEGOVERL(6,ITSEC)
         QSEGLOV(1,(NSECELLIB+1))=QSEGLOV(1,(NSECELLIB+1))
     +        +QSEGLOV(1,ITSEC)
      END DO
C--------------------------------------------------
C---- overlapp % predicted                    -----
C--------------------------------------------------
      DO MUE=1,NUMSEGOVERL(3,(NSECELLIB+1))
         BEGMUE=BEGSEGPRED(MUE)
         ENDMUE=ENDSEGPRED(MUE)

C----------------------------------------
C------- helix, strand, turn     --------
C----------------------------------------
         IF ((PREDCHAR(BEGMUE).EQ.'H').OR.
     +        (PREDCHAR(BEGMUE).EQ.'E').OR.
     +        (PREDCHAR(BEGMUE).EQ.'T')) THEN

            LOVER=.TRUE.
            IF (INT(LENSEGPRED(MUE)).LT.2) THEN
               WRITE(6,'(T2,A,T10,A,T40,A1,A,T60,I3)')'***',
     +              'pred seg too short for:',PREDCHAR(MUE),
     +              'seg no:',MUE
               LOVER=.FALSE.
            ELSEIF (INT(LENSEGPRED(MUE)).GT.NUMRESLIB) THEN
               WRITE(6,'(T2,A,T10,A,T40,A1,A,T60,I3)')'***',
     +              'pred seg too long for:',PREDCHAR(MUE),
     +              'seg no:',MUE
               LOVER=.FALSE.
            ELSE
C------------- overlapp with N-terminal end of segment?
               DO ITOVER=1,INT(LENSEGPRED(MUE)/2.)
                  IHELP=BEGMUE+ITOVER-1
                  IF (LOVER.EQV. .TRUE. .AND.(IHELP.LT.NUMRESLIB).AND.
     +                 (DSSPCHAR(IHELP).NE.PREDCHAR(IHELP))) THEN
                     LOVER=.FALSE.
C------------------- allow H H -> HHH
                     IF ((IHELP.GT.1).AND.(IHELP.LT.NUMRESLIB)) THEN
                        IF ((DSSPCHAR(IHELP-1).EQ.PREDCHAR(IHELP)).AND.
     +                     (DSSPCHAR(IHELP+1).EQ.PREDCHAR(IHELP))) THEN
                           LOVER=.TRUE.
                        END IF
                     END IF
                  END IF
               END DO
            END IF
C---------- overlapp with C-terminal end of segment?
            IF ((LOVER.EQV. .FALSE.).AND.(LENSEGPRED(MUE).GT.2).AND.
     +           (LENSEGPRED(MUE).LT.NUMRESLIB)) THEN
               LOVER=.TRUE.
               DO ITOVER=1,INT(LENSEGPRED(MUE)/2.)
                  IHELP=ENDMUE-INT(LENSEGPRED(MUE)/2.)+ITOVER-1
C---------------- hack 2-06-97
                  IF ((IHELP.GT.0).AND.(IHELP.LE.NUMRESLIB)) THEN
                     IF (LOVER.EQV. .TRUE. .AND.(IHELP.GE.1).AND.
     +                    (IHELP.LE.NUMRESLIB).AND.
     +                    (PREDCHAR(IHELP).NE.DSSPCHAR(IHELP))) THEN
                        LOVER=.FALSE.

C------------------- allow H H -> HHH
                        IF ((IHELP.GT.1).AND.(IHELP.LT.NUMRESLIB)) THEN
                           IF ((DSSPCHAR(IHELP-1).EQ.
     +                          PREDCHAR(IHELP)).AND.
     +                          (DSSPCHAR(IHELP+1).EQ.
     +                          PREDCHAR(IHELP))) THEN
                              LOVER=.TRUE.
                           END IF
                        END IF
                     ELSE
                        LOVER=.FALSE.
                     END IF
C---------------- hack 2-06-97
                  ELSE
                     LOVER=.FALSE.
                  END IF
               END DO
            ELSE
               LOVER=.FALSE.
            END IF

C---------- overlapped?
            IF (LOVER .EQV. .TRUE.) THEN
               IF (PREDCHAR(BEGMUE).EQ.'H') THEN
                  NUMSEGOVERL(7,1)=NUMSEGOVERL(7,1)+1
                  NUMSEGOVERL(8,1)=NUMSEGOVERL(8,1)+LENSEGPRED(MUE)
                  QSEGLOV(2,1)=QSEGLOV(2,1)+LENSEGPRED(MUE)
               ELSEIF (PREDCHAR(BEGMUE).EQ.'E') THEN
                  NUMSEGOVERL(7,2)=NUMSEGOVERL(7,2)+1
                  NUMSEGOVERL(8,2)=NUMSEGOVERL(8,2)+LENSEGPRED(MUE)
                  QSEGLOV(2,2)=QSEGLOV(2,2)+LENSEGPRED(MUE)
               ELSEIF (PREDCHAR(BEGMUE).EQ.'T') THEN
                  NUMSEGOVERL(7,3)=NUMSEGOVERL(7,3)+1
                  NUMSEGOVERL(8,3)=NUMSEGOVERL(8,3)+LENSEGPRED(MUE)
                  QSEGLOV(2,3)=QSEGLOV(2,3)+LENSEGPRED(MUE)
               END IF
            END IF
C----------------------------------------
C------- loop overlap > 2        --------
C----------------------------------------
         ELSEIF (PREDCHAR(BEGMUE).EQ.'L') THEN

            LOVER=.FALSE.
C---------- overlapp with loop anywhere > 2?
            IF (LENSEGPRED(MUE).EQ.1) THEN
               IF (DSSPCHAR(BEGMUE).EQ.'L') THEN
                  LOVER=.TRUE.
               END IF
            ELSEIF (LENSEGPRED(MUE).EQ.2) THEN
               IF (DSSPCHAR(BEGMUE).EQ.'L') THEN
                  LOVER=.TRUE.
               ELSEIF ((BEGMUE.LT.NUMRESLIB).AND.
     +                 (DSSPCHAR(BEGMUE+1).EQ.'L')) THEN
                  LOVER=.TRUE.
               END IF
            ELSEIF (LENSEGPRED(MUE).GT.NUMRESLIB) THEN
               LOVER=.FALSE.
            ELSE
               DO ITOVER=BEGMUE,ENDMUE
                  IF ((LOVER.EQV. .FALSE.).AND.
     +                 (PREDCHAR(ITOVER).EQ.'L')) THEN
                     IF (ITOVER.GT.BEGMUE) THEN
                        IF (DSSPCHAR(ITOVER-1).EQ.'L') THEN
                           LOVER=.TRUE.
                        END IF
                     ELSEIF (ITOVER.LT.ENDMUE) THEN
                        IF (DSSPCHAR(ITOVER+1).EQ.'L') THEN
                           LOVER=.TRUE.
                        END IF
                     END IF
                  END IF
               END DO
            END IF
C---------- overlapped?
            IF (LOVER .EQV. .TRUE.) THEN
               NUMSEGOVERL(7,NSECELLIB)=NUMSEGOVERL(7,NSECELLIB)+1
               NUMSEGOVERL(8,NSECELLIB)=NUMSEGOVERL(8,NSECELLIB)
     +              +LENSEGPRED(MUE)
               QSEGLOV(2,NSECELLIB)=
     +              QSEGLOV(2,NSECELLIB)+LENSEGPRED(MUE)
            END IF
         END IF
      END DO
C---- compute number of all overlapping segments
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(7,(NSECELLIB+1))=NUMSEGOVERL(7,(NSECELLIB+1))
     +        +NUMSEGOVERL(4,ITSEC)
         NUMSEGOVERL(8,(NSECELLIB+1))=NUMSEGOVERL(8,(NSECELLIB+1))
     +        +NUMSEGOVERL(8,ITSEC)
         QSEGLOV(2,(NSECELLIB+1))=QSEGLOV(2,(NSECELLIB+1))
     +        +QSEGLOV(2,ITSEC)
      END DO
C--------------------------------------------------
C---- end of scanning PRED segments           -----
C--------------------------------------------------
      END
***** end of SSEGLOV

***** ------------------------------------------------------------------
***** SUB SSEGSOV
***** ------------------------------------------------------------------
C---- 
C---- NAME : SSEGSOV
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The number of correctly predicted elements is    *
*     --------        computed.                                        *
*     input variables:NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,         *
*     ----------------DSSPCHAR,PREDCHAR,SSCHAR,LENSEGDSSP,LENSEGPRED,  *
*                     POINTPRED,BEGSEGDSSP,COUNTSEG                    *
*     output variab.: NUMCOR                                           *
*     called by:      SEVALSEG (in lib-prot.f)                         *
*     SBRs calling:  from lib-comp.f:                                 *
*     --------------     SISTZ1,                                       *
*                     from lib-unix.f:                                 *
*                        SCHECKPASS                                    *
*     procedure:      correct: NUMSEGOVERL(5,ITSEC)                    *
*     ----------         for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*----------------------------------------------------------------------*
      SUBROUTINE SSEGSOV(NSECELLIB,MAXNSECEL,NUMRESLIB,MAXNUMRES,
     +     MAXSEGPASS,CHVP1,DSSPCHAR,PREDCHAR,SSCHAR,CHVP2,
     +     LENSEGDSSP,LENSEGPRED,POINTDSSP,POINTPRED,
     +     BEGSEGDSSP,BEGSEGPRED,CHVP3,
     +     NUMCOR,COUNTSEGDSSP,COUNTSEGPRED,QSEGSOV,CHVPL)

      IMPLICIT        NONE

C---- variables passed
      INTEGER         MAXNSECEL,NSECELLIB,MAXNUMRES,NUMRESLIB,
     +     MAXSEGPASS,CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3,
     +     NUMCOR(1:2,1:8),COUNTSEGDSSP,COUNTSEGPRED,
     +     POINTDSSP(1:MAXNUMRES),POINTPRED(1:MAXNUMRES),
     +     BEGSEGDSSP(1:MAXSEGPASS),BEGSEGPRED(1:MAXSEGPASS),
     +     LENSEGDSSP(1:MAXSEGPASS),LENSEGPRED(1:MAXSEGPASS)
      REAL            QSEGSOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*1     DSSPCHAR(1:MAXNUMRES),PREDCHAR(1:MAXNUMRES)
      CHARACTER*1     SSCHAR(1:8)

C---- local variables                                                  *
      INTEGER         MUE,ITSEC,IHELP
      LOGICAL         LERRCHVP,LOVER
******------------------------------*-----------------------------******
*     MUE,ITER        serve as iteration variables                     *
*     QSEGSOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the strict overlap                         *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     LWRITE          variable used, to call subr such that the *
*                     result they compute is written out (true) or not *
******------------------------------*-----------------------------******
C---- initial check of variables passed
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SSEGSOV: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SSEGSOV: variables passed not correct !'
      END IF
C---- check variables passed
      IF (NUMRESLIB.GT.MAXNUMRES) THEN
         WRITE(6,'(T5,A)')
     +        ' The length of the protein passed to SBR SSEGSOV'
         WRITE(6,'(T5,A)')
     +        ' exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current length: ',NUMRESLIB,'  allocated:',MAXNUMRES
         WRITE(6,'(T5,A)')' Stopped in SSEGSOV 12-2-92:1'
         STOP
      END IF
      IF (NSECELLIB.GT.MAXNSECEL) THEN
         WRITE(6,'(T5,A)')
     +       'The number of secondary structures passed to SBR SSEGSOV'
         WRITE(6,'(T5,A)')
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECELLIB,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped in SSEGSOV 12-11-92:2'
         STOP
      END IF
C---- set zero
C     -----------
      CALL SISTZ2(NUMCOR,2,8)
C     -----------
      CALL SRSTZ2(QSEGSOV,2,(MAXNSECEL+1))
C     -----------

C----------------------------------------------------------------------
C---- computing correct elements for %observed                    -----
C----------------------------------------------------------------------
      DO MUE=1,COUNTSEGDSSP
         LOVER=.FALSE.
         IF (LENSEGDSSP(MUE).LE.2) THEN
            IHELP=BEGSEGDSSP(MUE)
            IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
               LOVER=.TRUE.
            END IF
         ELSEIF (LENSEGDSSP(MUE).LE.5) THEN
            IHELP=BEGSEGDSSP(MUE)
            IF ((PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).LE.
     +                          (LENSEGDSSP(MUE)+1)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).GE.
     +           (LENSEGDSSP(MUE)-1))) THEN
               LOVER=.TRUE.
            ELSEIF (IHELP.LT.0) THEN
               LOVER=.FALSE.
            ELSEIF (IHELP.EQ.1) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            ELSEIF (IHELP.EQ.NUMRESLIB) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ( (PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).LE.
     +                                (LENSEGDSSP(MUE)+1)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).GE.
     +                                (LENSEGDSSP(MUE)-1))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).LE.
     +                                (LENSEGDSSP(MUE)+1)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).GE.
     +                                (LENSEGDSSP(MUE)-1))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
         ELSEIF (LENSEGDSSP(MUE).LE.10) THEN
            IHELP=BEGSEGDSSP(MUE)
            IF ( (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).LE.
     +                          (LENSEGDSSP(MUE)+2)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).GE.
     +                          (LENSEGDSSP(MUE)-2))) THEN
               LOVER=.TRUE.
            ELSEIF (IHELP.LT.0) THEN
               LOVER=.FALSE.
            ELSEIF (IHELP.EQ.1) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ((PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).LE.
     +                                (LENSEGDSSP(MUE)+2)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).GE.
     +                                (LENSEGDSSP(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.2)) THEN
               IF ( (PREDCHAR(IHELP-2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-2)).LE.
     +                                (LENSEGDSSP(MUE)+2)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-2)).GE.
     +                                (LENSEGDSSP(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).LE.
     +                                (LENSEGDSSP(MUE)+2)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).GE.
     +                                (LENSEGDSSP(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-1))) THEN
               IF ( (PREDCHAR(IHELP+2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+2)).LE.
     +                                (LENSEGDSSP(MUE)+2)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+2)).GE.
     +                                (LENSEGDSSP(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
         ELSEIF (LENSEGDSSP(MUE).GT.NUMRESLIB) THEN
            LOVER=.FALSE.
         ELSEIF (LENSEGDSSP(MUE).GT.10) THEN
            IHELP=BEGSEGDSSP(MUE)
            IF ((PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).LE.
     +                          (LENSEGDSSP(MUE)+3)).AND.
     +           (LENSEGPRED(POINTPRED(IHELP)).GE.
     +                          (LENSEGDSSP(MUE)-3))) THEN
               LOVER=.TRUE.
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ( (PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-1)).GE.
     +                               (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.2)) THEN
               IF ( (PREDCHAR(IHELP-2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-2)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-2)).GE.
     +                                (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.3)) THEN
               IF ( (PREDCHAR(IHELP-3).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-3)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP-3)).GE.
     +                                (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+1)).GE.
     +                                (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-1))) THEN
               IF ( (PREDCHAR(IHELP+2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+2)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+2)).GE.
     +                                (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-2))) THEN
               IF ( (PREDCHAR(IHELP+3).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+3)).LE.
     +                                (LENSEGDSSP(MUE)+3)).AND.
     +              (LENSEGPRED(POINTPRED(IHELP+3)).GE.
     +                                (LENSEGDSSP(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
         END IF

C------- count number of correct elements
         IF (LOVER .EQV. .TRUE.) THEN
            DO ITSEC=1,NSECELLIB
               IF (DSSPCHAR(BEGSEGDSSP(MUE)).EQ.SSCHAR(ITSEC)) THEN
                  NUMCOR(1,ITSEC)=NUMCOR(1,ITSEC)+1
                  QSEGSOV(1,ITSEC)=QSEGSOV(1,ITSEC)+LENSEGDSSP(MUE)
               END IF
            END DO
         END IF
      END DO
C---- end of loop over observed segments
C--------------------------------------------------

C--------------------------------------------------
C---- compute SOV for NSECELLIB+1 (i.e. all states)
C--------------------------------------------------
      DO ITSEC=1,NSECELLIB
         QSEGSOV(1,(NSECELLIB+1))=QSEGSOV(1,(NSECELLIB+1))
     +        +QSEGSOV(1,ITSEC)
         NUMCOR(1,(NSECELLIB+1))=NUMCOR(1,(NSECELLIB+1))
     +        +NUMCOR(1,ITSEC)
      END DO

C---- end of computation for correct elements as % of observed    -----
C----------------------------------------------------------------------

C----------------------------------------------------------------------
C---- computing correct elements for %predicted                   -----
C----------------------------------------------------------------------
      DO MUE=1,COUNTSEGPRED
         LOVER=.FALSE.
         IF (LENSEGPRED(MUE).LE.5) THEN
            IHELP=BEGSEGPRED(MUE)
            IF ((PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).LE.
     +                          (LENSEGPRED(MUE)+1)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).GE.
     +           (LENSEGPRED(MUE)-1))) THEN
               LOVER=.TRUE.
            ELSEIF (IHELP.EQ.1) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ( (PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).LE.
     +                                (LENSEGPRED(MUE)+1)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).GE.
     +                                (LENSEGPRED(MUE)-1))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).LE.
     +                                (LENSEGPRED(MUE)+1)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).GE.
     +                                (LENSEGPRED(MUE)-1))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
C---------- correction for short helices/strands
            IF ((PREDCHAR(IHELP).EQ.'H').OR.
     +           (PREDCHAR(IHELP).EQ.'E')) THEN
               LOVER=.FALSE.
            END IF
         ELSEIF (LENSEGPRED(MUE).LE.10) THEN
            IHELP=BEGSEGPRED(MUE)
            IF ((PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).LE.
     +                          (LENSEGPRED(MUE)+2)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).GE.
     +                          (LENSEGPRED(MUE)-2))) THEN
               LOVER=.TRUE.
            ELSEIF (IHELP.EQ.1) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ( (PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).LE.
     +                                (LENSEGPRED(MUE)+2)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).GE.
     +                                (LENSEGPRED(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.2)) THEN
               IF ( (PREDCHAR(IHELP-2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-2)).LE.
     +                                (LENSEGPRED(MUE)+2)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-2)).GE.
     +                                (LENSEGPRED(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).LE.
     +                                (LENSEGPRED(MUE)+2)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).GE.
     +                                (LENSEGPRED(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-1))) THEN
               IF ( (PREDCHAR(IHELP+2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+2)).LE.
     +                                (LENSEGPRED(MUE)+2)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+2)).GE.
     +                                (LENSEGPRED(MUE)-2))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
         ELSEIF (LENSEGPRED(MUE).GT.10) THEN
            IHELP=BEGSEGPRED(MUE)
            IF ((PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).LE.
     +                          (LENSEGPRED(MUE)+3)).AND.
     +           (LENSEGDSSP(POINTDSSP(IHELP)).GE.
     +                          (LENSEGPRED(MUE)-3))) THEN
               LOVER=.TRUE.
            ELSEIF (IHELP.EQ.1) THEN
               IF (PREDCHAR(IHELP).EQ.DSSPCHAR(IHELP)) THEN
                  LOVER=.TRUE.
               ELSE
                  LOVER=.FALSE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.1)) THEN
               IF ( (PREDCHAR(IHELP-1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-1)).GE.
     +                               (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.2)) THEN
               IF ( (PREDCHAR(IHELP-2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-2)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-2)).GE.
     +                                (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.GT.3)) THEN
               IF ( (PREDCHAR(IHELP-3).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-3)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP-3)).GE.
     +                                (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.NUMRESLIB)) THEN
               IF ( (PREDCHAR(IHELP+1).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+1)).GE.
     +                                (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-1))) THEN
               IF ( (PREDCHAR(IHELP+2).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+2)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+2)).GE.
     +                                (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
            IF ((LOVER.EQV. .FALSE.).AND.(IHELP.LT.(NUMRESLIB-2))) THEN
               IF ( (PREDCHAR(IHELP+3).EQ.DSSPCHAR(IHELP)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+3)).LE.
     +                                (LENSEGPRED(MUE)+3)).AND.
     +              (LENSEGDSSP(POINTDSSP(IHELP+3)).GE.
     +                                (LENSEGPRED(MUE)-3))) THEN
                  LOVER=.TRUE.
               END IF
            END IF
         END IF
C---- count number of correct elements
         IF (LOVER .EQV. .TRUE.) THEN
            DO ITSEC=1,NSECELLIB
               IF (DSSPCHAR(BEGSEGPRED(MUE)).EQ.SSCHAR(ITSEC)) THEN
                  NUMCOR(2,ITSEC)=NUMCOR(2,ITSEC)+1
                  QSEGSOV(2,ITSEC)=QSEGSOV(2,ITSEC)+LENSEGPRED(MUE)
               END IF
            END DO
         END IF
      END DO
C---- end of loop over predicted segments
C--------------------------------------------------

C--------------------------------------------------
C---- compute SOV for NSECELLIB+1 (i.e. all states)
C--------------------------------------------------
      DO ITSEC=1,NSECELLIB
         QSEGSOV(2,(NSECELLIB+1))=QSEGSOV(2,(NSECELLIB+1))
     +        +QSEGSOV(2,ITSEC)
         NUMCOR(2,(NSECELLIB+1))=NUMCOR(2,(NSECELLIB+1))
     +        +NUMCOR(2,ITSEC)
      END DO
      END
***** end of SSEGSOV

***** ------------------------------------------------------------------
***** SUB STABLEPOLENFILE
***** ------------------------------------------------------------------
C---- 
C---- NAME : STABLEPOLENFILE
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-offs of a certain prediction are written *
*     --------        as a table onto unit KOUT                        *
*     input variables:NSECELLIB, PROTNAMELIB, NUMRESLIB, TITLE         *
*     ----------------MATNUM, MATQOFDSSP, MATQOFPRED, INTERQ3, INTERSQ *
*                     INTERCORR, MATLEN                                *
*     called by:      SEVALSEC (in lib-prot.f)                         *
*     calling:        SEVALQUO (in lib-prot.f) (if q3,sq=0, mat not)   *
*----------------------------------------------------------------------*
      SUBROUTINE STABLEPOLENFILE(KUNIT,NSECELLIB,MAXNSECELLIB,
     +     PROTNAMELIB,NUMRESLIB,TITLE,MATNUM,MATQOFDSSP,MATQOFPRED,
     +     INTERQ3,INTERSQ,INTERCORR,MATLEN)
      IMPLICIT        NONE
C---- local variables                                                  *
      INTEGER         MAXNSECELLIB
      INTEGER         ITER,ITSEC,NUMRESLIB,NSECELLIB,IHELP,KUNIT,ICOUNT
      INTEGER         MATNUM(1:(MAXNSECELLIB+1),1:(MAXNSECELLIB+1))
      INTEGER         MATLEN(1:(MAXNSECELLIB+1),1:4),IBEG,IEND
      REAL            MATQOFDSSP(1:MAXNSECELLIB,1:MAXNSECELLIB)
      REAL            MATQOFPRED(1:MAXNSECELLIB,1:MAXNSECELLIB)
      REAL            INTERCORR(1:MAXNSECELLIB),INTERQ3,INTERSQ
      REAL            INTERAVLENGTH(1:8,1:2)
      CHARACTER*1     INTERTITLE(1:40)
      CHARACTER*222   PROTNAMELIB,INTERNAME
      CHARACTER*10    INTERDSSP(1:8)
      CHARACTER       TITLE*(*)
      LOGICAL         LFLAG
******------------------------------*-----------------------------******
*     ITSEC,ITER      iteration variables                              *
*     INTERAVLENGTH(i,k) intermediately store average length of elements
*                     (required to avoid division by zero)             *
*     INTERCORR(i)    correlation for class i, (Mathews)               *
*     INTERQ3         =properly predicted for all classes/all residues *
*     INTERSQ         first divide predicted/DSSP in each class then   *
*                     sum all classes and divide by e.g. 3             *
*     INTERTITLE      intermediate variable storing a character 40 with*
*                     the title (potentially truncated)                *
*     KUNIT           =6, or 10, according to whether the table is to  *
*                     be written onto printer or into a file (FILETABLE)
*     MATLEN(i,j)     matrix with the lengths of the elements:         *
*                     i=1,4 => H,E,C,all                               *
*                     j=1   => number of elements DSSP                 *
*                     j=2   => number of elements PRED                 *
*                     j=2   => summed length of all elements for DSSP  *
*                     j=2   => summed length of all elements for PRED  *
*     MATNUM(i,j)     the number of residues in a certain secondary    *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix, MATNUM(1,2) those DSSP helices predicted*
*                     as strands and MATNUM(1,4) all DSSP helices, resp.
*                     MATNUM(4,4) all residues predicted.              *
*     MATOFDSSP(i,j)  stores according to the same scheme as MATNUM the*
*                     percentages of residues predicted divided by the *
*                     numbers of DSSP (note there is no element (4,4) )*
*     MATOFPRED(i,j)  same as previous but now percentages of prediction
*     MAXNSECELLIB    maximal number of secondary structures allowed   *
*     NSECELLIB       currently read number of secondary structures    *
*     NUMRESLIB       number of residues of protein for current table  *
*     PROTNAMELIB     the name of the protein for current table        *
*     TITLE           title of job which generated the prediction      *
******------------------------------*-----------------------------******
      IHELP=          0
C----------------------------------------------------------------------
C---- check the size of the quantities passed:                    -----
C---- if Q3=SQ=0 and MATNUM(i,i) not 0 --> compute newly          -----
C----------------------------------------------------------------------

      LFLAG=.TRUE.
      IF ((INTERQ3.EQ.0.).AND.(INTERSQ.EQ.0.)) THEN
         DO ITSEC=1,NSECELLIB
            IF (LFLAG.EQV. .TRUE. .AND.(MATNUM(ITSEC,ITSEC).NE.0)) THEN
               LFLAG=.FALSE.
            END IF
         END DO
         IF (LFLAG.EQV. .FALSE.) THEN
C           =============
            CALL SEVALQUO(NSECELLIB,MAXNSECELLIB,MATNUM,MATQOFDSSP,
     +           MATQOFPRED,INTERQ3,INTERSQ,INTERCORR)
C           =============
         END IF
      END IF

C----------------------------------------------------------------------
C---- TABLE                                                       -----
C----------------------------------------------------------------------

C      KUNIT=6

C---- count length of string 'TITLE'
      LFLAG=.TRUE.
      DO ITER=1,50
         IF (LFLAG .EQV. .TRUE.) THEN
            IF (TITLE(ITER:ITER).EQ.' ') THEN
               LFLAG=.FALSE.
               IHELP=ITER
            END IF
         END IF
      END DO
      DO ITER=1,IHELP
         INTERTITLE(ITER)=TITLE(ITER:ITER)
      END DO
      DO ITER=(IHELP+1),40
         INTERTITLE(ITER)=' '
      END DO
C---- length of protein name ok?
      CALL SILEN_STRING(PROTNAMELIB,IBEG,IEND)
      ICOUNT=IEND-IBEG+1
      IF ((ICOUNT+IBEG).LT.7) THEN
         INTERNAME(1:ICOUNT+IBEG)=PROTNAMELIB(1:ICOUNT+IBEG)
         INTERNAME((ICOUNT+IBEG+1):7)=' '
      ELSE
         INTERNAME(1:7)=PROTNAMELIB(1:7)
      END IF
C---- avoid division by zero!
      DO ITER=1,2
         DO ITSEC=1,(NSECELLIB+1)
            IF (MATLEN(ITSEC,ITER).NE.0) THEN
               INTERAVLENGTH(ITSEC,ITER)=
     +              MATLEN(ITSEC,(2+ITER))/REAL(MATLEN(ITSEC,ITER))
            ELSE
               INTERAVLENGTH(ITSEC,ITER)=0
            END IF
         END DO
      END DO
C--------------------------------------------------
C---- write all numbers into a table          -----
C--------------------------------------------------
C----------------------------------------
C---- 3 secondary structures        -----
C----------------------------------------
      IF (NSECELLIB.EQ.3) THEN
C------- header
         WRITE(KUNIT,'(T2,A1,16A1,A1,49A1,A1,T70,A)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,49),'+',
     +        '-------------------+'
         WRITE(KUNIT,'(T2,A2,A7,A1,I5,T19,A9,40A1,T69,A1,T70,A14,T89,
     +        A1)')'| ',INTERNAME,':',NUMRESLIB,'| method:',
     +        (INTERTITLE(ITER),ITER=1,40),'|','      segments','|'
         WRITE(KUNIT,'(T2,A1,16A1,A1,19A1,A1,17A1,A1,11A1,A1,T70,2A10)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,19),'+',('-',ITER=1,
     +        17),'+',('-',ITER=1,11),'+',('---------+',ITER=1,2)
         WRITE(KUNIT,'(T2,A,T40,A,T57,A1,A,T69,A1,T70,2A10)')
     +        '|    number of residues with H,E,C   |',
     +        '  % of DSSP','|','  % of Net','|',' number  |',
     +        'av length|'
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECELLIB+1)),
     +        ('-----+',ITER=1,NSECELLIB),('---+',ITER=1,NSECELLIB),
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A10,4A7,3A6,3A4,T70,4A5)')
     +        '|        |','net H |','net E |','net C |','sum DS|',
     +        '  H  |','  E  |','  C  |',' H |',' E |',' C |',
     +        'DSSP|',' Net|','DSSP|',' Net|'
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECELLIB+1)),
     +        ('-----+',ITER=1,NSECELLIB),('---+',ITER=1,NSECELLIB),
     +        ('----+',ITER=1,4)
C------- number for all secondary elements
         INTERDSSP(1)='| DSSP H |'
         INTERDSSP(2)='| DSSP E |'
         INTERDSSP(3)='| DSSP C |'
         DO ITSEC=1,NSECELLIB
            WRITE(KUNIT,'(T2,A10,4(I5,A2),3(F5.1,A1),3(I3,A1),
     +           T70,2(I4,A1),2(F4.1,A1))')
     +           INTERDSSP(ITSEC),(MATNUM(ITSEC,ITER),' |',ITER=1,
     +           (NSECELLIB+1)),(MATQOFDSSP(ITSEC,ITER),'|',ITER=1,
     +           NSECELLIB),(INT(MATQOFPRED(ITSEC,ITER)),'|',ITER=1,
     +           NSECELLIB),(MATLEN(ITSEC,ITER),'|',ITER=1,2),
     +           (INTERAVLENGTH(ITSEC,ITER),'|',ITER=1,2)
         END DO
C------- sums
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECELLIB+1)),
     +        ('-----+',ITER=1,NSECELLIB),'---+','-+-+','---+',
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A10,4(I5,A2),3A6,2A6,T70,2(I4,A1),2(F4.1,
     +        A1))')'| sum Net|',(MATNUM((NSECELLIB+1),ITER),' |',
     +        ITER=1,(NSECELLIB+1)),'corH |','corE |','corC |',' Q3  |',
     +        '  SQ |',(MATLEN((NSECELLIB+1),ITER),'|',ITER=1,2),
     +        (INTERAVLENGTH((NSECELLIB+1),ITER),'|',ITER=1,2)
         WRITE(KUNIT,'(T2,A1,36A1,A1,3(F4.2,A2),2(F5.1,A1),T89,A1)')
     +        '|',(' ',ITER=1,36),'|',(INTERCORR(ITSEC),' |',ITSEC=1,
     +        NSECELLIB),INTERQ3,'|',INTERSQ,'|','|'
         WRITE(KUNIT,'(T2,A1,36A1,A1,3A6,2A6,T70,A)')
     +        '+',('-',ITER=1,36),'+',('-----+',ITER=1,NSECELLIB),
     +        ('*****+',ITER=1,2),'-------------------+'
C----------------------------------------
C---- 4 secondary structures        -----
C----------------------------------------
      ELSEIF (NSECELLIB.EQ.4) THEN
C------- header
         WRITE(KUNIT,'(T2,A1,16A1,A1,50A1,A1,T71,A)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,50),'+',
     +        '-------------------+'
         WRITE(KUNIT,'(T2,A2,A7,A1,I5,T19,A9,40A1,T70,A1,T71,A14,T90,
     +        A1)')'| ',INTERNAME,':',NUMRESLIB,'| method:',
     +        (INTERTITLE(ITER),ITER=1,40),'|','      segments','|'
         WRITE(KUNIT,'(T2,A1,16A1,A1,18A1,A2,14A1,A1,14A1,A2,T71,2A10)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,18),'+-',('-',ITER=1,
     +        14),'+',('-',ITER=1,14),'-+',('---------+',ITER=1,2)
         WRITE(KUNIT,'(T2,A,T39,A,T54,A1,A,T70,A1,2A10)')
     +        '|   number of residues with H,E,T,L |',
     +        '  % of DSSP','|','  % of Net','|',' number  |',
     +        'av length|'
         
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECELLIB+1)),('---+',ITER=1,2*NSECELLIB),
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')
     +        '|     |','net H|','net E|','net T|','net L|','sum o|',
     +        ' H |','E |',' T |',' L |',' H |',' E |',' T |',' L |',
     +        'DSSP|',' Net|','DSSP|',' Net|'
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECELLIB+1)),('---+',ITER=1,2*NSECELLIB),
     +        ('----+',ITER=1,4)
C------- number for all secondary elements
         INTERDSSP(1)='|obs H|'
         INTERDSSP(2)='|obs E|'
         INTERDSSP(3)='|obs T|'
         INTERDSSP(4)='|obs L|'
         DO ITSEC=1,NSECELLIB
            WRITE(KUNIT,'(T2,A7,5(I5,A1),8(I3,A1),
     +           T71,2(I4,A1),2(F4.1,A1))')
     +           INTERDSSP(ITSEC),(MATNUM(ITSEC,ITER),'|',ITER=1,
     +           (NSECELLIB+1)),(INT(MATQOFDSSP(ITSEC,ITER)),'|',ITER=1,
     +           NSECELLIB),(INT(MATQOFPRED(ITSEC,ITER)),'|',ITER=1,
     +           NSECELLIB),(MATLEN(ITSEC,ITER),'|',ITER=1,2),
     +           (INTERAVLENGTH(ITSEC,ITER),'|',ITER=1,2)
         END DO
C------- sums
         WRITE(KUNIT,'(T2,A7,5A6,A32,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECELLIB+1)),'---+--++---++--+--++---++--+---+',
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A7,5(I5,A1),A1,4A6,A7,
     +        T71,2(I4,A1),2(F4.1,A1))')
     +        '|sum N|',(MATNUM((NSECELLIB+1),ITER),'|',ITER=1,
     +        (NSECELLIB+1)),' ','cor H|','cor E|','cor T|','cor L|',
     +        '  Q3  |',(MATLEN((NSECELLIB+1),ITER),'|',ITER=1,2),
     +        (INTERAVLENGTH((NSECELLIB+1),ITER),'|',ITER=1,2)
         WRITE(KUNIT,'(T2,A1,35A1,A2,4(F5.2,A1),F6.1,A1,T90,A1)')
     +        '|',(' ',ITER=1,35),'| ',(INTERCORR(ITSEC),'|',ITSEC=1,
     +        NSECELLIB),INTERQ3,'|','|'
         WRITE(KUNIT,'(T2,A1,35A1,A2,4A6,A7,T71,A)')
     +        '+',('-',ITER=1,35),'+-',('-----+',ITER=1,NSECELLIB),
     +        '******+','-------------------+'
      END IF
      END
***** end of STABLEPOLENFILE

***** ------------------------------------------------------------------
***** SUB STABLESEG
***** ------------------------------------------------------------------
C---- 
C---- NAME : STABLESEG
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Nov,        1992        version 0.1    *
*                      changed: Mar,        1993        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The results of the analysis of overlapping, resp.*
*     --------        correct segments is written onto unit KUNIT      *
*     input variables:KUNIT, NSECELLIB, MAXNSECEL, NUMSEGOVERL         *
*     called by:      SEVALSEG (in lib-prot.f)                         *
*----------------------------------------------------------------------*
      SUBROUTINE STABLESEG(KUNIT,NSECELLIB,MAXNSECEL,
     +     PROTNAME,NUMSEGOVERL,QSEGLOV,QSEGSOV,QSEGFOV,DEVNOM)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         MAXNSECEL,NSECELLIB,KUNIT,
     +     NUMSEGOVERL(1:9,1:(MAXNSECEL+1)),DEVNOM
      REAL            QSEGLOV(1:2,1:(MAXNSECEL+1)),
     +     QSEGSOV(1:2,1:(MAXNSECEL+1)),QSEGFOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*222   PROTNAME
C---- local variables                                                  *
      INTEGER         ITER,ITSEC,ICOUNT,IBEG,IEND
      REAL*4          ROVERL(1:2,1:10)
      CHARACTER*10    TXT1(1:5)
      CHARACTER*11    TXTFOV
      CHARACTER*7     INTERNAME
******------------------------------*-----------------------------******
*     MUE,ITER        serve as iteration variables                     *
*     NUMSEGOVERL(i,j) number of overlapping/correct segments          *
*        i=1          number of DSSP segments in secondary structure   *
*                     j, for last: sum over all                        *
*        i=2          number of residues in structure i, summed up over*
*                     all segments (in i=1)                            *
*        i=3          number of predicted segments in class j          *
*        i=4          number of residues in structure i, summed up over*
*                     all segments (in i=1) for the prediction         *
*        i=5          number of overlapping predicted segments related *
*                     to those being observed.  Correct means:         *
*                     overlap >= length of segment / 2, for H, E, T    *
*                     and for loop: at least loop of 2, resp 1, if  the*
*                     DSSP loop is 1.                                  *
*        i=6          number of overlapping segments multiplied by     *
*                     length: related to %observed                     *
*        i=7          same as 5, but other way round: %pred!           *
*        i=8          same as 6, but other way round: %pred!           *
*                     j, for last: sum over all                        *
*        i=9          number of correct segments:                      *
*                     L +/- 1, and shift by 1, if L<=5                 *
*                     L +/- 1, and shift by 2, if 5<L<=10              *
*                     L +/- 2, and shift by 3, if L>10                 *
*                     noted: j, for last: sum over all                 *
*     QSEGLOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the loose overlap (half length overlap)    *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     QSEGSOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the strict overlap                         *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     QSEGFOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the fractional overlap:                    *
*                                  overlapping length                  *
*                     as given by: ------------------                  *
*                                  common length                       *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
******------------------------------*-----------------------------******
C--------------------------------------------------
C---- writing the overlapping results         -----
C--------------------------------------------------
      TXT1(1)='% H'
      TXT1(2)='% E'
      TXT1(NSECELLIB)='% L'
      IF (NSECELLIB.EQ.4) THEN
         TXT1(3)='% T'
      END IF
      TXT1(NSECELLIB+1)='sum'
      IF (DEVNOM.EQ.0) THEN
         TXTFOV='| Fov0    |'
      ELSEIF (DEVNOM.EQ.1) THEN
         TXTFOV='| Fov1    |'
      ELSEIF (DEVNOM.EQ.2) THEN
         TXTFOV='| Fov2    |'
      ELSEIF (DEVNOM.EQ.3) THEN
         TXTFOV='| Fov3    |'
      ELSEIF (DEVNOM.EQ.4) THEN
         TXTFOV='| Fov4    |'
      ELSEIF (DEVNOM.EQ.50) THEN
         TXTFOV='| Fov50%  |'
      ELSEIF (DEVNOM.EQ.100) THEN
         TXTFOV='| Fov100% |'
      ELSE
         TXTFOV='| frac ov |'
      END IF
      CALL SILEN_STRING(PROTNAME,IBEG,IEND)
      ICOUNT=IEND-IBEG+1
      IF ((ICOUNT+IBEG).LT.7) THEN
         INTERNAME(1:ICOUNT+IBEG)=PROTNAME(1:ICOUNT+IBEG)
         INTERNAME((ICOUNT+IBEG+1):7)=' '
      ELSE
         INTERNAME(1:7)=PROTNAME(1:7)
      END IF
C---- take care about divisions by 0, non weighted measures
      DO ITSEC=1,(NSECELLIB+1)
         IF (NUMSEGOVERL(1,ITSEC).EQ.0) THEN
            ROVERL(1,ITSEC)=0
         ELSE
            ROVERL(1,ITSEC)=
     +           NUMSEGOVERL(5,ITSEC)/REAL(NUMSEGOVERL(1,ITSEC))
         END IF
         IF (NUMSEGOVERL(3,ITSEC).EQ.0) THEN
            ROVERL(2,ITSEC)=0
         ELSE
            ROVERL(2,ITSEC)=
     +           NUMSEGOVERL(7,ITSEC)/REAL(NUMSEGOVERL(3,ITSEC))
         END IF
      END DO
C--------------------------------------------------
C---- starting a hack!
C---- hack since the %predicted for NSECEL+1 (roverl(2,4)) seems to be
C---- wrong and I dont want to locate the bug:
      NUMSEGOVERL(7,(NSECELLIB+1))=0
      NUMSEGOVERL(3,(NSECELLIB+1))=0
      DO ITSEC=1,NSECELLIB
         NUMSEGOVERL(7,(NSECELLIB+1))=NUMSEGOVERL(7,(NSECELLIB+1))+
     +        NUMSEGOVERL(7,ITSEC)
         NUMSEGOVERL(3,(NSECELLIB+1))=NUMSEGOVERL(3,(NSECELLIB+1))+
     +        NUMSEGOVERL(3,ITSEC)
      END DO
      IF (NUMSEGOVERL(3,(NSECELLIB+1)).EQ.0) THEN
         ROVERL(2,ITSEC)=0
      ELSE
         ROVERL(2,(NSECELLIB+1))=
     +        NUMSEGOVERL(7,ITSEC)/REAL(NUMSEGOVERL(3,ITSEC))
      END IF
C---- end of hack!
C--------------------------------------------------
      WRITE(KUNIT,'(T2,A)')'---'
      IF (NSECELLIB.EQ.3) THEN
         WRITE(KUNIT,'(T2,A,T10,A11,2A36)')'---','+---------+',
     +        ('-----------------------------------+',ITER=1,2)
         WRITE(KUNIT,'(T2,A,T10,A2,A7,A2,2A36)')'---',
     +        '| ',INTERNAME,' |',
     +        '   % of observed overlapping seg.  |',
     +        '   % of predicted overlapping seg. |'
         WRITE(KUNIT,'(T2,A,T10,A11,8(A3,A1,A4,A1))')
     +        '---','+---------+',
     +        ('-----','+','----','+',ITSEC=1,2*(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(A3,A1,A4,A1))')
     +        '---','|         |',
     +        (TXT1(ITSEC),'|','Nobs','|',ITSEC=1,(NSECELLIB+1)),
     +        (TXT1(ITSEC),'|','Nprd','|',ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(A3,A1,A4,A1))')
     +        '---','+---------+',
     +        ('-----','+','----','+',ITSEC=1,2*(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(I3,A1,I4,A1))')
     +        '---','| no ov s |',
     +        (INT(100*ROVERL(1,ITSEC)),'|',NUMSEGOVERL(1,ITSEC),'|',
     +        ITSEC=1,(NSECELLIB+1)),
     +        (INT(100*ROVERL(2,ITSEC)),'|',NUMSEGOVERL(3,ITSEC),'|',
     +        ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(I3,A1,I4,A1))')
     +        '---','| Lov     |',
     +        (INT(QSEGLOV(1,ITSEC)),
     +        '|',NUMSEGOVERL(2,ITSEC),'|',ITSEC=1,(NSECELLIB+1)),
     +        (INT(QSEGLOV(2,ITSEC)),
     +        '|',NUMSEGOVERL(4,ITSEC),'|',ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(A3,A1,A4,A1))')
     +        '---','+---------+',
     +        ('-----','+','----','+',ITSEC=1,2*(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,2(4(A3,A3,A3)))')
     +        '---','|         |',
     +        (('   ',TXT1(ITSEC),'  |',ITSEC=1,(NSECELLIB+1)),ITER=1,2)
         WRITE(KUNIT,'(T2,A,T10,A11,8(A7,A2))')'---',
     +        '+---------+',('-------','-+',ITSEC=1,2*(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(F7.2,A2))')
     +        '---','| Sov     |',
     +        (QSEGSOV(1,ITSEC),' |',ITSEC=1,(NSECELLIB+1)),
     +        (QSEGSOV(2,ITSEC),' |',ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,T10,A11,8(F7.2,A2))')'---',TXTFOV,
     +        ((QSEGFOV(ITER,ITSEC),' |',ITSEC=1,(NSECELLIB+1)),
     +        ITER=1,2)
         WRITE(KUNIT,'(T2,A,T10,A11,8(A7,A2))')'---','+---------+',
     +        ('-------','-+',ITSEC=1,2*(NSECELLIB+1))
      ELSEIF (NSECELLIB.EQ.4) THEN
         WRITE(KUNIT,'(T2,A,T10,2A40,T90,A1)')'---',
     +        ('+---------------------------------------',ITER=1,2),'+'
         WRITE(KUNIT,'(T2,A,T10,A,T50,A,T90,A1)')'---',
     +        '|  % of observed overlapping seg.',
     +        '|  % of predicted overlapping seg.','|'
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(A3,A1,A4,A1),A3,A1))')'---',
     +        '+',(('-----','+','----','+',ITSEC=1,NSECELLIB),'---','+',
     +        ITER=1,2)
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(A3,A1,A4,A1),A3,A1))')'---',
     +        '|',(TXT1(ITSEC),'|','Nobs','|',ITSEC=1,NSECELLIB),
     +        TXT1(NSECELLIB+1),'|',
     +        (TXT1(ITSEC),'|','Nprd','|',ITSEC=1,NSECELLIB),
     +        TXT1(NSECELLIB+1),'|'
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(A3,A1,A4,A1),A3,A1))')'---',
     +        '+',(('-----','+','----','+',ITSEC=1,NSECELLIB),'---','+',
     +        ITER=1,2)
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(I3,A1,I4,A1),I3,A1),A4)')
     +        '---','|',(INT(100*ROVERL(1,ITSEC)),
     +        '|',NUMSEGOVERL(1,ITSEC),'|',ITSEC=1,NSECELLIB),
     +        INT(100*ROVERL(1,NSECELLIB)),'|',
     +        (INT(100*ROVERL(2,ITSEC)),
     +        '|',NUMSEGOVERL(3,ITSEC),'|',ITSEC=1,NSECELLIB),
     +        INT(100*ROVERL(2,NSECELLIB)),'|',' no '
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(I3,A1,I4,A1),I3,A1),A4)')
     +        '---','|',(INT(QSEGLOV(1,ITSEC)),
     +        '|',NUMSEGOVERL(2,ITSEC),'|',ITSEC=1,NSECELLIB),
     +        INT(QSEGLOV(1,(NSECELLIB+1))),'|',
     +        (INT(QSEGLOV(2,ITSEC)),
     +        '|',NUMSEGOVERL(4,ITSEC),'|',ITSEC=1,NSECELLIB),
     +        INT(QSEGLOV(2,(NSECELLIB+1))),'|',' Lov'
         WRITE(KUNIT,'(T2,A,T10,A1,2(4(A3,A1,A4,A1),A3,A1))')'---',
     +        '+',(('-----','+','----','+',ITSEC=1,NSECELLIB),'---','+',
     +        ITER=1,2)
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----------------------------+'//
     +        '+-----------------------------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '|     | % H | % E | % T | % L | sum |'//
     +        '| % H | % E | % T | % L | sum |'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----+-----+-----+-----+-----+'//
     +        '+-----+-----+-----+-----+-----+'
         WRITE(KUNIT,'(T2,A7,5(F5.1,A1),A1,5(F5.1,A1))')'|Sov  |',
     +        (QSEGSOV(1,ITSEC),'|',ITSEC=1,(NSECELLIB+1)),'|',
     +        (QSEGSOV(2,ITSEC),'|',ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A1,A6,A1,5(F5.1,A1),A1,5(F5.1,A1))')
     +        '|',TXTFOV(2:7),
     +        '|',(QSEGFOV(1,ITSEC),'|',ITSEC=1,(NSECELLIB+1)),'|',
     +        (QSEGFOV(2,ITSEC),'|',ITSEC=1,(NSECELLIB+1))
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----+-----+-----+-----+-----+'//
     +        '+-----+-----+-----+-----+-----+'
      END IF
      END
***** end of STABLESEG

***** ------------------------------------------------------------------
***** SUB STABLE_EXPNOINBIN
***** ------------------------------------------------------------------
C---- 
C---- NAME : STABLE_EXPNOINBIN
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1993        version 0.1    *
*                      changed: Feb,        1994        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        For a set of proteins the average relative expo- *
*     --------        sure per residue is computed (possibly for DSSP  *
*                     and PRED).                                       *
*     in variables:   NUMPROT,NUMRES                                   *
*     -------------   RESNAME,EXPDSSP,EXPPRED                          *
*     output variab.: EXPDSSP_AV,EXPPRED_AV,EXPDSSP_AVSIGMA,EXPPRED_AVSI
*----------------------------------------------------------------------*
      SUBROUTINE STABLE_EXPNOINBIN(KUNIT,CHVP1,EXP_NOINBIN,TXT,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         KUNIT,EXP_NOINBIN(0:9,0:9),
     +     CHVPM(1:50),CHVPL,CHVP1
      CHARACTER*222   TXT
C---- local variables
      INTEGER         IT1,IT2,SUMDSSP(0:9),SUMPRED(0:9),SUM
      REAL            RTMP
      LOGICAL         LERRCHVP
******------------------------------*-----------------------------******
C---- default
C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.1) THEN
         WRITE(6,'(T2,A,T10,A)')'***','WARNING: STABLE_EXPNOINBIN: '//
     +        'number of passed variables not fitting'
         WRITE(6,'(T2,A,T10,A,T40,4I5)')'***',
     +     'they are: CHVPL: 1,2,3:',CHVPL,CHVP1
         STOP
      END IF
      CHVPM(1)=CHVP1
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C      CALL SCHECKPASS(LERRCHVP,CHVPL,CHVPM)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +    'ERROR for STABLE_EXPNOINBIN: variables passed not correct !'
      END IF
C---- check variables passed
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C--------------------------------------------------
C---- computing percentages/sums
C--------------------------------------------------

      SUM=0
      DO IT1=0,9
         SUMDSSP(IT1)=0
         SUMPRED(IT1)=0
         DO IT2=0,9
            SUMDSSP(IT1)=SUMDSSP(IT1)+EXP_NOINBIN(IT1,IT2)
            SUMPRED(IT1)=SUMPRED(IT1)+EXP_NOINBIN(IT2,IT1)
         END DO
         SUM=SUM+SUMDSSP(IT1)
      END DO
C--------------------------------------------------
C---- writing
C--------------------------------------------------

      WRITE(KUNIT,'(T2,A)')'---'
      WRITE(KUNIT,'(T2,A,T10,A,T40,A20)')'---','numbers per bin for:',
     +     TXT
      WRITE(KUNIT,'(T2,A)')'---'
      WRITE(KUNIT,'(T2,A)')'no in bin'
      WRITE(KUNIT,'(T2,A,T11,A1,T13,10I5,A4,A6,A7)')'pred:',
     +     '|',(IT2,IT2=0,9),'  | ','   SUM','  %DSSP'
      WRITE(KUNIT,'(T2,9A1,A1,53A1,A1,16A1)')('-',IT1=1,9),'+',
     +     ('-',IT1=1,53),'+',('-',IT1=1,16)
      DO IT1=0,9
         IF (SUM.NE.0) THEN
            RTMP=100*SUMDSSP(IT1)/REAL(SUM)
         ELSE
            RTMP=0
         END IF
         WRITE(KUNIT,'(T2,A,T8,I1,T11,A1,T13,10I5,A4,I6,F7.1)')
     +        'DSSP ',IT1,'|',
     +        (EXP_NOINBIN(IT1,IT2),IT2=0,9),
     +        '  | ',SUMDSSP(IT1),RTMP
      END DO
      WRITE(KUNIT,'(T2,9A1,A1,53A1,A1,16A1)')('-',IT1=1,9),'+',
     +     ('-',IT1=1,53),'+',('-',IT1=1,16)
      WRITE(KUNIT,'(T2,A,T11,A1,T13,10I5,A4)')
     +     'PRED SUM','|',(SUMPRED(IT2),IT2=0,9),'  | '
      WRITE(KUNIT,'(T2,A,T11,A1,T13,10F5.1,A4)')
     +     'PRED  %','|',(100*SUMPRED(IT2)/REAL(MIN(1,SUM)),IT2=0,9),
     +     '  | '
      WRITE(KUNIT,'(T2,9A1,A1,53A1,A1,16A1)')('-',IT1=1,9),'+',
     +     ('-',IT1=1,53),'+',('-',IT1=1,16)
      WRITE(KUNIT,'(T2,A)')'---'
      END
***** end of STABLE_EXPNOINBIN

***** ------------------------------------------------------------------
***** SUB STABLE_EXPSTATES
***** ------------------------------------------------------------------
C---- 
C---- NAME : STABLE_EXPSTATES
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Mar,        1994        version 0.1    *
*                      changed: Mar,        1994        version 0.2    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The percentages of 2state, 3state, 10state       *
*     --------        exposure accuracy are written for one protein.   *
*----------------------------------------------------------------------*
      SUBROUTINE STABLE_EXPSTATES(KUNIT,LTOPLINE,LBOTTOMLINE,
     +     PROTNAME,CHVP1,EXP_NOINBIN,T2,T3A,T3B,CHVP2,EXP_NO2ST,
     +     EXP_NO3ST,CHVP3,EXP_NO10ST,EXP_CORR,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         KUNIT,EXP_NO2ST(1:3),EXP_NO3ST(1:4),
     +     EXP_NO10ST(1:11),EXP_NOINBIN(0:9,0:9),T2,T3A,T3B,
     +     CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3
      REAL            EXP_CORR
      CHARACTER*222   PROTNAME
      LOGICAL         LTOPLINE,LBOTTOMLINE
C---- local functions
      INTEGER         FILEN_STRING,FIEXP_NOINBIN_SUM
      REAL            FRDIVIDE_SAFE
      CHARACTER*222   FCUT_SPACES
C---- local variables
      INTEGER         IT1,IT2,ILEN,
     +     NUMRES,TMPT2(1:3),TMPT3(1:4),INUM,ICHECK,ICOUNT
      REAL            RES2(1:5),RES3(1:7),RES10(1:20),RES10S(1:2)
      CHARACTER*222   NAME6
      CHARACTER*10    COBS,CPRED
      LOGICAL         LERRCHVP
******------------------------------*-----------------------------******
C---- default
C----------------------------------------------------------------------*
C---- initial check of variables passed                           -----*
C----------------------------------------------------------------------*
      IF (CHVPL.NE.3) THEN
         WRITE(6,'(T2,A,T10,A)')'***','WARNING: STABLE_EXPSTATES'//
     +        ': number of passed variables not fitting'
         WRITE(6,'(T2,A,T10,A,T40,4I5)')'***',
     +     'they are: CHVPL: 1,2,3:',CHVPL,CHVP1,CHVP2,CHVP3
         STOP
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C      CALL SCHECKPASS(LERRCHVP,CHVPL,CHVPM)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     + 'ERROR for STABLE_EXPSTATES: variables passed not correct !'
      END IF
C---- check variables passed
      IF ( (T2.LT.0).OR.(T2.GT.10).OR.
     +     (T3A.LT.0).OR.(T3A.GT.10).OR.
     +     (T3B.LT.0).OR.(T3B.GT.10) ) THEN
         WRITE(6,'(T2,A,T10,A)')'***','STABLE_EXPSTATES: '//
     +        'thresholds wrong! '
         WRITE(6,'(T2,A,T10,A,T20,i3,a,t30,i3,A,T40,I3)')
     +        '***','T2= ',T2,'  T3A = ',T3A,'  T3B = ',T3B
         STOP
      END IF
C----------------------------------------------------------------------*
C---- end of checking variables                                   -----*
C----------------------------------------------------------------------*
C---- defaults
      COBS='obs'
      CPRED='pred'
      TMPT2(1)=0
      TMPT2(2)=T2
      TMPT2(3)=10
      TMPT3(1)=0
      TMPT3(2)=T3A
      TMPT3(3)=T3B
      TMPT3(4)=10
C---- cut off spaces:
      ILEN=FILEN_STRING(PROTNAME)
      IF (ILEN.LT.6) THEN
         NAME6(1:ILEN)=FCUT_SPACES(PROTNAME)
         NAME6((ILEN+1):6)=' '
      ELSE
         NAME6(1:6)=FCUT_SPACES(PROTNAME)
      END IF
C---- headline
      IF (LTOPLINE .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A1,6A1,A2,21A1,A2,29A1,A2,8A1,A2,2(6A1,A2))')
     +        '+',('-',IT1=1,6),'-+',('-',IT1=1,21),'-+',
     +        ('-',IT1=1,29),'-+',('-',IT1=1,8),'-+',
     +        (('-',IT1=1,6),'-+',IT2=1,2)
         WRITE(KUNIT,'(A1,A,T9,A1,A5,4A4,A2,A5,6A4,A2,
     +        2A4,A2,A6,A2,A6,A2)')'|','name','|',
     +        ' Q2  ',' b%o','  %p',' e%o','  %p',' |',
     +        ' Q3  ',' b%o','  %p',' i%o','  %p ',' e%o',
     +        '  %p',' |','Q10o','  %p',' |',
     +        '  c10 ',' |','     N',' |'
         WRITE(KUNIT,'(A1,6A1,A2,21A1,A2,29A1,A2,8A1,A2,2(6A1,A2))')
     +        '+',('-',IT1=1,6),'-+',('-',IT1=1,21),'-+',
     +        ('-',IT1=1,29),'-+',('-',IT1=1,8),'-+',
     +        (('-',IT1=1,6),'-+',IT2=1,2)
      END IF
C--------------------------------------------------
C---- computing percentages
C--------------------------------------------------

      NUMRES=0
      ICHECK=0
      DO IT1=1,2
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,TMPT2(IT1),TMPT2(IT1+1),
     +                          COBS)
         NUMRES=NUMRES+INUM
         RES2((IT1-1)*2+1)=100*FRDIVIDE_SAFE(REAL(EXP_NO2ST(IT1)),
     +                                       REAL(INUM))
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,TMPT2(IT1),TMPT2(IT1+1),
     +                          CPRED)
         RES2((IT1-1)*2+2)=100*FRDIVIDE_SAFE(REAL(EXP_NO2ST(IT1)),
     +                                       REAL(INUM))
         ICHECK=ICHECK+INUM
      END DO
      RES2(5)=100*FRDIVIDE_SAFE(REAL(EXP_NO2ST(3)),REAL(NUMRES))

C---- consistency check
      IF (ICHECK.NE.NUMRES) THEN
         WRITE(6,'(T2,A,T10,A)')'***','STABLE_EXPSTATES  '//
     +        'ERROR not same number for observed and predicted!'
         WRITE(6,'(T2,A,T10,A,T20,I5,A,T40,I5)')'***',
     +        'NUMRES = ',NUMRES,'   ICHECK = ',ICHECK
         DO IT1=0,9
            WRITE(kunit,'(T10,10I5)')(EXP_NOINBIN(IT1,IT2),IT2=0,9)
         END DO
         STOP
      END IF

      DO IT1=1,3
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,TMPT3(IT1),TMPT3(IT1+1),
     +                          COBS)
         RES3((IT1-1)*2+1)=100*FRDIVIDE_SAFE(REAL(EXP_NO3ST(IT1)),
     +                                       REAL(INUM))
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,TMPT3(IT1),TMPT3(IT1+1),
     +                          CPRED)
         RES3((IT1-1)*2+2)=100*FRDIVIDE_SAFE(REAL(EXP_NO3ST(IT1)),
     +                                       REAL(INUM))
      END DO
      RES3(7)=100*FRDIVIDE_SAFE(REAL(EXP_NO3ST(4)),REAL(NUMRES))

      RES10S(1)=0
      RES10S(2)=0
      ICOUNT=0
      DO IT1=1,10
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,(IT1-1),IT1,COBS)
         RES10((IT1-1)*2+1)=100*FRDIVIDE_SAFE(REAL(EXP_NO10ST(IT1)),
     +                                        REAL(INUM))
         RES10S(1)=RES10S(1)+REAL(EXP_NO10ST(IT1))
         INUM=FIEXP_NOINBIN_SUM(EXP_NOINBIN,(IT1-1),IT1,CPRED)
         RES10((IT1-1)*2+2)=100*FRDIVIDE_SAFE(REAL(EXP_NO10ST(IT1)),
     +                                        REAL(INUM))
         RES10S(2)=RES10S(2)+RES10((IT1-1)*2+2)
         IF (INUM.GT.0) THEN
            ICOUNT=ICOUNT+1
         END IF
      END DO
      RES10S(1)=100*FRDIVIDE_SAFE(RES10S(1),REAL(NUMRES))
      RES10S(2)=FRDIVIDE_SAFE(RES10S(2),REAL(ICOUNT))
C--------------------------------------------------
C---- writing percentages
C--------------------------------------------------

      WRITE(KUNIT,'(A1,A,T9,A1,F5.1,4I4,A2,F5.1,6I4,A2,
     +     2I4,A2,F6.3,A2,I6,A2)')'|',NAME6,'|',
     +     RES2(5),(INT(RES2(IT1)),IT1=1,4),' |',
     +     RES3(7),(INT(RES3(IT1)),IT1=1,6),' |',
     +     (INT(RES10S(IT1)),IT1=1,2),' |',EXP_CORR,' |',NUMRES,' |'
C--------------------------------------------------
C---- write bottom line?
      IF (LBOTTOMLINE .EQV. .TRUE.) THEN
         WRITE(KUNIT,'(A1,6A1,A2,21A1,A2,29A1,A2,8A1,A2,2(6A1,A2))')
     +        '+',('-',IT1=1,6),'-+',('-',IT1=1,21),'-+',
     +        ('-',IT1=1,29),'-+',('-',IT1=1,8),'-+',
     +        (('-',IT1=1,6),'-+',IT2=1,2)
      END IF
      END
***** end of STABLE_EXPSTATES

***** ------------------------------------------------------------------
***** SUB STABLE_QILS
***** ------------------------------------------------------------------
C---- 
C---- NAME : STABLE_QILS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Apr,        1993        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        The pay-offs of a certain prediction are written *
*     --------        as a table onto unit KUNIT:                      *
*     in variables:   NSECEL, MAXNSECEL, KUNIT, NUMRES, PROTNAME, TITLE*
*     -------------   MATNUM, MATLEN, NUMSEGOVERL,                     *
*                     MATQOFDSSP,MATQOFPRED,Q3,SQ,CORR,INFO,INFO_INV,  *
*                     QSEGLOV,QSEGSOV,QSEGFOV,DEVNOM,CHVPL             *
*     called by:      typically SEVALSEG, resp. Xevalpred              *
*     SBRs calling:   from lib-prot.f:                                 *
*     --------------     SEVALQUO, SEVALINFOFILE                       *
*                     from lib-unix.f:                                 *
*                        SCHECKPASS                                    *
*                     from lib-comp.f:                                 *
*                        SISTZ1, SRSTZ2, SILEN_STRING                  *
*                     from lib-prot.f:                                 *
*----------------------------------------------------------------------*
      SUBROUTINE STABLE_QILS(KUNIT,NSECEL,MAXNSECEL,NUMRES,CHVP1,
     +     PROTNAME,TITLE,CHVP2,MATNUM,MATLEN,CHVP3,
     +     MATQOFDSSP,MATQOFPRED,Q3,SQ,CORR,INFO,INFO_INV,CHVP4,
     +     QSEGSOV,QSEGFOV,DEVNOM,CHVPL)
      IMPLICIT        NONE
C---- variables passed
      INTEGER         CHVPM(1:50),CHVPL,CHVP1,CHVP2,CHVP3,CHVP4,
     +     MAXNSECEL,NSECEL,NUMRES,KUNIT,
     +     MATNUM(1:(MAXNSECEL+1),1:(MAXNSECEL+1)),
     +     MATLEN(1:(MAXNSECEL+1),1:4),DEVNOM
      REAL            MATQOFDSSP(1:MAXNSECEL,1:MAXNSECEL),
     +     MATQOFPRED(1:MAXNSECEL,1:MAXNSECEL),
     +     CORR(1:MAXNSECEL),Q3,SQ,INFO,INFO_INV,
     +     QSEGSOV(1:2,1:(MAXNSECEL+1)),QSEGFOV(1:2,1:(MAXNSECEL+1))
      CHARACTER*222   TITLE,PROTNAME
C---- local variables                                                  *
      INTEGER         ITER,ITSEC,ILEN,IBEG,IEND,ICOUNT
      REAL            INTERAVLENGTH(1:8,1:2)
      CHARACTER*222   INTERTITLE
      CHARACTER*7     INTERNAME
      CHARACTER*10    INTERDSSP(1:8)
      CHARACTER*11    TXTFOV
      LOGICAL         LFLAG,LERRCHVP
******------------------------------*-----------------------------******
*---------------------                                                 *
*     passed variables                                                 *
*     KUNIT           number of unit to write the files upon           *
*     MAXNSECEL       maximal number of secondary structures for array *
*                     boundaries                                       *
*     NSECEL          number of secondary structure types used         *
*     PROTNAME        the name of the chain for which the class is     *
*                     determined                                       *
*---------------------                                                 *
*     local variables                                                  *
*     ITSEC,ITER      iteration variables                              *
*     INTERAVLENGTH(i,k) intermediately store average length of elements
*                     (required to avoid division by zero)             *
*     CORR(i)    correlation for class i, (Mathews)               *
*     Q3         =properly predicted for all classes/all residues *
*     SQ         first divide predicted/DSSP in each class then   *
*                     sum all classes and divide by e.g. 3             *
*     INTERTITLE      intermediate variable storing a character 40 with*
*                     the title (potentially truncated)                *
*     KUNIT           =6, or 10, according to whether the table is to  *
*                     be written onto printer or into a file (FILETABLE)
*     MATLEN(i,j)     matrix with the lengths of the elements:         *
*                     i=1,4 => H,E,C,all                               *
*                     j=1   => number of elements DSSP                 *
*                     j=2   => number of elements PRED                 *
*                     j=2   => summed length of all elements for DSSP  *
*                     j=2   => summed length of all elements for PRED  *
*     MATNUM(i,j)     the number of residues in a certain secondary    *
*                     structure, i labels DSSP assignment, i.e. all    *
*                     numbers with i=1 are according to DSSP helices,  *
*                     j labels the prediction.  That means, e.g.:      *
*                     MATNUM(1,1) are all DSSP helices predicted to be *
*                     a helix, MATNUM(1,2) those DSSP helices predicted*
*                     as strands and MATNUM(1,4) all DSSP helices, resp.
*                     MATNUM(4,4) all residues predicted.              *
*     MATOFDSSP(i,j)  stores according to the same scheme as MATNUM the*
*                     percentages of residues predicted divided by the *
*                     numbers of DSSP (note there is no element (4,4) )*
*     MATOFPRED(i,j)  same as previous but now percentages of prediction
*     MAXNSECEL    maximal number of secondary structures allowed   *
*     NSECEL       currently read number of secondary structures    *
*     NUMRES       number of residues of protein for current table  *
*     PROTNAME     the name of the protein for current table        *
*     QSEGSOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the strict overlap                         *
*                        for L < 5:   | L(OBS)-L(PRED) | <= 1          *
*                                     .AND. shift by 1 allowed         *
*                        for 5<=L<10: | L(OBS)-L(PRED) | <= 2          *
*                                     .AND. shift by 2 allowed         *
*                        for L >= 10: | L(OBS)-L(PRED) | <= 3          *
*                                     .AND. shift by 3 allowed         *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     QSEGFOV(i,j)    i=1: %observed                                   *
*                     i=2: %predicted                                  *
*                     j=1-4 (helix,strand,loop,3-states)               *
*                     gives the fractional overlap:                    *
*                                  overlapping length                  *
*                     as given by: ------------------                  *
*                                  common length                       *
*                     multiplication with length of segment, normali-  *
*                     sation will be done by calling SBR with N=number *
*                     of all residues in the data set                  *
*     TITLE           title of job which generated the prediction      *
******------------------------------*-----------------------------******
C----------------------------------------------------------------------
C---- check the size of the quantities passed:                    -----
C---- if Q3=SQ=0 and MATNUM(i,i) not 0 --> compute newly          -----
C----------------------------------------------------------------------

      IF (CHVPL.NE.4) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +     'WARNING: SEVALSEG: number of passed variables not fitting'
      END IF
      CHVPM(1)=CHVP1
      CHVPM(2)=CHVP2
      CHVPM(3)=CHVP3
      CHVPM(4)=CHVP4
C     ---------------
      CALL SCHECKPASS(CHVPM,CHVPL,LERRCHVP)
C     ---------------
      IF (LERRCHVP .EQV. .TRUE.) THEN
         WRITE(6,'(T2,A,T10,A)')'***',
     +        'ERROR for SEVALSEG: variables passed not correct !'
      END IF
C---- check variables passed
      IF (NSECEL.GT.MAXNSECEL) THEN
         WRITE(6,'(T2,A,T10,A,A)')'***','ERROR: The number of '//
     +        'secondary structures passed to SBR STABLE_QILS'
         WRITE(6,'(t2,A,T10,A)')'***',
     +        'exceeds the locally allocated array dimensions.'
         WRITE(6,'(T5,A,T25,I4,T30,A,T40,I4)')
     +        ' Current number: ',NSECEL,'  allocated:',MAXNSECEL
         WRITE(6,'(T5,A)')' Stopped at 31-3-93a'
         STOP
      END IF
C---- end of initially checking consistency of ----
C---- variables passed                        -----
C--------------------------------------------------
C--------------------------------------------------
C---- if the quotient Q3 passed 0, call SBR   -----
C--------------------------------------------------

      LFLAG=.TRUE.
      IF ((Q3.EQ.0.).AND.(SQ.EQ.0.)) THEN
         DO ITSEC=1,NSECEL
            IF (LFLAG.EQV. .TRUE. .AND.(MATNUM(ITSEC,ITSEC).NE.0)) THEN
               LFLAG=.FALSE.
            END IF
         END DO
         IF (LFLAG.EQV. .FALSE.) THEN
C           =============
            CALL SEVALQUO(NSECEL,MAXNSECEL,MATNUM,MATQOFDSSP,
     +           MATQOFPRED,Q3,SQ,CORR)
C           =============
            LFLAG=.FALSE.
C           ==============
            CALL SEVALINFOFILE(6,NSECEL,MAXNSECEL,
     +           MATNUM,INFO,INFO_INV,LFLAG)
C           ==============
         END IF
      END IF
C--------------------------------------------------
C---- check length of Protname and Title      -----
C--------------------------------------------------

C---- count length of string 'TITLE'
      CALL SILEN_STRING(TITLE,IBEG,IEND)
      ILEN=IEND-IBEG+1
      IF (ILEN.LT.40) THEN
         INTERTITLE(1:ILEN)=TITLE(IBEG:IEND)
         DO ITER=(ILEN+1),40
            INTERTITLE(ITER:ITER)=' '
         END DO
      ELSE
         INTERTITLE(1:40)=TITLE(IBEG:IBEG+39)
      END IF
C---- length of protein name ok?
      CALL SILEN_STRING(PROTNAME,IBEG,IEND)
      ICOUNT=IEND-IBEG+1
      IF ((ICOUNT+IBEG).LT.7) THEN
         INTERNAME(1:ICOUNT+IBEG)=PROTNAME(1:ICOUNT+IBEG)
         INTERNAME((ICOUNT+IBEG+1):7)=' '
      ELSE
         INTERNAME(1:7)=PROTNAME(1:7)
      END IF
C---- assign names for txt used in table
      IF (DEVNOM.EQ.0) THEN
         TXTFOV='| Fov0    |'
      ELSEIF (DEVNOM.EQ.1) THEN
         TXTFOV='| Fov1    |'
      ELSEIF (DEVNOM.EQ.2) THEN
         TXTFOV='| Fov2    |'
      ELSEIF (DEVNOM.EQ.3) THEN
         TXTFOV='| Fov3    |'
      ELSEIF (DEVNOM.EQ.4) THEN
         TXTFOV='| Fov4    |'
      ELSEIF (DEVNOM.EQ.50) THEN
         TXTFOV='| Fov50%  |'
C                              changed 1-8-95
         TXTFOV='| SOV     |'
      ELSEIF (DEVNOM.EQ.100) THEN
         TXTFOV='| Fov100% |'
      ELSE
         TXTFOV='| frac ov |'
      END IF
C--------------------------------------------------
C---- check length of Protname and Title      -----
C--------------------------------------------------

C---- avoid division by zero for average lengths!
      DO ITER=1,2
         DO ITSEC=1,(NSECEL+1)
            IF (MATLEN(ITSEC,ITER).NE.0) THEN
               INTERAVLENGTH(ITSEC,ITER)=
     +              MATLEN(ITSEC,(2+ITER))/REAL(MATLEN(ITSEC,ITER))
            ELSE
               INTERAVLENGTH(ITSEC,ITER)=0
            END IF
         END DO
      END DO
C----------------------------------------------------------------------
C---- TABLE                                                       -----
C----------------------------------------------------------------------
C--------------------------------------------------
C---- 3 secondary structures                  -----
C--------------------------------------------------
      IF (NSECEL.EQ.3) THEN
C----------------------------------------
C------- single residue quotients   -----
C----------------------------------------

C------- header
         WRITE(KUNIT,'(T2,A1,17A1,A1,48A1,A1,T70,A)')
     +        '+',('-',ITER=1,17),'+',('-',ITER=1,48),'+',
     +        '-------------------+'
         WRITE(KUNIT,'(T2,A2,A7,A1,I6,T20,A9,A39,T69,A1,T70,A14,T89,
     +        A1)')'| ',INTERNAME,':',NUMRES,'| method:',
     +        INTERTITLE,'|','      segments','|'
         WRITE(KUNIT,'(T2,A1,17A1,A1,48A1,A1,T70,A)')
     +        '+',('-',ITER=1,17),'+',('-',ITER=1,48),'+',
     +        '-------------------+'
         WRITE(KUNIT,'(T2,A,T40,A,T57,A1,A,T69,A1,T70,2A10)')
     +        '|    number of residues with H,E,C   |',
     +        '  % of DSSP','|','  % of Net','|',' number  |',
     +        'av length|'
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECEL+1)),
     +        ('-----+',ITER=1,NSECEL),('---+',ITER=1,NSECEL),
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A10,4A7,3A6,3A4,T70,4A5)')
     +        '|        |','net H |','net E |','net C |','sum DS|',
     +        '  H  |','  E  |','  C  |',' H |',' E |',' C |',
     +        'DSSP|',' Net|','DSSP|',' Net|'
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECEL+1)),
     +        ('-----+',ITER=1,NSECEL),('---+',ITER=1,NSECEL),
     +        ('----+',ITER=1,4)
C------- number for all secondary elements
         INTERDSSP(1)='| DSSP H |'
         INTERDSSP(2)='| DSSP E |'
         INTERDSSP(3)='| DSSP C |'
         DO ITSEC=1,NSECEL
            WRITE(KUNIT,'(T2,A10,4(I6,A1),3(F5.1,A1),3(I3,A1),
     +           T70,2(I4,A1),2(F4.1,A1))')
     +           INTERDSSP(ITSEC),
     +           (MATNUM(ITSEC,ITER),'|',ITER=1,(NSECEL+1)),
     +           (MATQOFDSSP(ITSEC,ITER),'|',ITER=1,NSECEL),
     +           (INT(MATQOFPRED(ITSEC,ITER)),'|',ITER=1,NSECEL),
     +           (MATLEN(ITSEC,ITER),'|',ITER=1,2),
     +           (INTERAVLENGTH(ITSEC,ITER),'|',ITER=1,2)
         END DO
C------- sums
         WRITE(KUNIT,'(T2,A1,A,T12,4A7,3A6,3A4,T70,4A5)')
     +        '+','--------+',('------+',ITER=1,(NSECEL+1)),
     +        ('-----+',ITER=1,NSECEL),'---+','-+-+','---+',
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A10,4(I6,A1),3A6,2A6,T70,2(I4,A1),2(F4.1,
     +        A1))')
     +        '| sum Net|',
     +        (MATNUM((NSECEL+1),ITER),'|',ITER=1,(NSECEL+1)),
     +        'corH |','corE |','corC |',' Q3  |',
     +        '  SQ |',(MATLEN((NSECEL+1),ITER),'|',ITER=1,2),
     +        (INTERAVLENGTH((NSECEL+1),ITER),'|',ITER=1,2)
         WRITE(KUNIT,'(T2,A1,36A1,A1,3(F4.2,A2),2(F5.1,A1),T89,A1)')
     +        '|',(' ',ITER=1,36),'|',(CORR(ITSEC),' |',ITSEC=1,
     +        NSECEL),Q3,'|',SQ,'|','|'
C         WRITE(KUNIT,'(T2,A1,36A1,A1,3A6,2A6,T70,A)')
C     +        '+',('-',ITER=1,36),'+',('-----+',ITER=1,NSECEL),
C     +        ('*****+',ITER=1,2),'-------------------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+---------+--------------------------++'//
     +        '+---+-----+-----++****+*****+-------------------+'
C------- information
C         WRITE(KUNIT,'(T57,A10,F6.3,A9,F6.3,A2)')
C     +        '| I %obs= ',INFO,', %pred= ',INFO_INV,' |'
C         WRITE(KUNIT,'(T57,A)')
C     +        '+-------------------------------+'
C         WRITE(KUNIT,'(T2,A)')'---'
C------- end of single residue stuff ----
C----------------------------------------
C----------------------------------------
C------- segment based measures     -----
C----------------------------------------

         WRITE(KUNIT,'(T2,A,A)')
     +        '| segments|      % of observed        |'//
     +        '|     % of predicted        |    information    |'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+---------+------+------+------+------+'//
     +        '+------+------+------+------+---------+---------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '| measure | % H  | % E  | % L  | sum  |'//
     +        '| % H  | % E  | % L  | sum  | I %obs  | I %pred |'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+---------+------+------+------+------+'//
     +        '+------+------+------+------+---------+---------+'
C                              commented out 1-8-95         
         if (1.eq.0) then
            WRITE(KUNIT,'(T2,A11,4(F5.1,A2),A1,4(F5.1,A2),
     +           T70,2(F6.3,A4))')'| Sov     |',
     +           (QSEGSOV(1,ITSEC),' |',ITSEC=1,(NSECEL+1)),'|',
     +           (QSEGSOV(2,ITSEC),' |',ITSEC=1,(NSECEL+1)),
     +           INFO,'   |',INFO_INV,'   |'
         endif
C         changed 5.8.95
C         WRITE(KUNIT,'(T2,A11,4(F5.1,A2),A1,4(F5.1,A2),
C     +        T70,2(A5,I3,A2))')TXTFOV,
C     +        (QSEGFOV(1,ITSEC),' |',ITSEC=1,(NSECEL+1)),'|',
C     +        (QSEGFOV(2,ITSEC),' |',ITSEC=1,(NSECEL+1)),
C     +        '     ',INT(100*INFO/0.62),'%|',
C     +        '     ',INT(100*INFO_INV/0.62),'%|'
         WRITE(KUNIT,'(T2,A11,4(F5.1,A2),A1,4(F5.1,A2),
     +        T70,2(F4.2,A1,I3,A2))')TXTFOV,
     +        (QSEGFOV(1,ITSEC),' |',ITSEC=1,(NSECEL+1)),'|',
     +        (QSEGFOV(2,ITSEC),' |',ITSEC=1,(NSECEL+1)),
     +        INFO,' ',INT(100*INFO/0.62),'%|',
     +        INFO_INV,' ',INT(100*INFO_INV/0.62),'%|'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+---------+------+------+------+------+'//
     +        '+------+------+------+------+---------+---------+'
C--------------------------------------------------
C---- end of 3 secondary structures           -----
C--------------------------------------------------
C----------------------------------------
C---- 4 secondary structures        -----
C----------------------------------------
      ELSEIF (NSECEL.EQ.4) THEN
C------- header
         WRITE(KUNIT,'(T2,A1,16A1,A1,50A1,A1,T71,A)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,50),'+',
     +        '-------------------+'
         WRITE(KUNIT,'(T2,A2,A7,A1,I5,T19,A9,A40,T70,A1,T71,A14,T90,
     +        A1)')'| ',INTERNAME,':',NUMRES,'| method:',
     +        INTERTITLE,'|','      segments','|'
         WRITE(KUNIT,'(T2,A1,16A1,A1,18A1,A2,14A1,A1,14A1,A2,T71,2A10)')
     +        '+',('-',ITER=1,16),'+',('-',ITER=1,18),'+-',('-',ITER=1,
     +        14),'+',('-',ITER=1,14),'-+',('---------+',ITER=1,2)
         WRITE(KUNIT,'(T2,A,T39,A,T54,A1,A,T70,A1,2A10)')
     +        '|   number of residues with H,E,T,L |',
     +        '  % of DSSP','|','  % of Net','|',' number  |',
     +        'av length|'
         
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECEL+1)),('---+',ITER=1,2*NSECEL),
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')
     +        '|     |','net H|','net E|','net T|','net L|','sum o|',
     +        ' H |','E |',' T |',' L |',' H |',' E |',' T |',' L |',
     +        'DSSP|',' Net|','DSSP|',' Net|'
         WRITE(KUNIT,'(T2,A7,5A6,8A4,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECEL+1)),('---+',ITER=1,2*NSECEL),
     +        ('----+',ITER=1,4)
C------- number for all secondary elements
         INTERDSSP(1)='|obs H|'
         INTERDSSP(2)='|obs E|'
         INTERDSSP(3)='|obs T|'
         INTERDSSP(4)='|obs L|'
         DO ITSEC=1,NSECEL
            WRITE(KUNIT,'(T2,A7,5(I5,A1),8(I3,A1),
     +           T71,2(I4,A1),2(F4.1,A1))')
     +           INTERDSSP(ITSEC),(MATNUM(ITSEC,ITER),'|',ITER=1,
     +           (NSECEL+1)),(INT(MATQOFDSSP(ITSEC,ITER)),'|',ITER=1,
     +           NSECEL),(INT(MATQOFPRED(ITSEC,ITER)),'|',ITER=1,
     +           NSECEL),(MATLEN(ITSEC,ITER),'|',ITER=1,2),
     +           (INTERAVLENGTH(ITSEC,ITER),'|',ITER=1,2)
         END DO
C------- sums
         WRITE(KUNIT,'(T2,A7,5A6,A32,T71,4A5)')'+-----+',('-----+',
     +        ITER=1,(NSECEL+1)),'---+--++---++--+--++---++--+---+',
     +        ('----+',ITER=1,4)
         WRITE(KUNIT,'(T2,A7,5(I5,A1),A1,4A6,A7,
     +        T71,2(I4,A1),2(F4.1,A1))')
     +        '|sum N|',(MATNUM((NSECEL+1),ITER),'|',ITER=1,
     +        (NSECEL+1)),' ','cor H|','cor E|','cor T|','cor L|',
     +        '  Q3  |',(MATLEN((NSECEL+1),ITER),'|',ITER=1,2),
     +        (INTERAVLENGTH((NSECEL+1),ITER),'|',ITER=1,2)
         WRITE(KUNIT,'(T2,A1,35A1,A2,4(F5.2,A1),F6.1,A1,T90,A1)')
     +        '|',(' ',ITER=1,35),'| ',(CORR(ITSEC),'|',ITSEC=1,
     +        NSECEL),Q3,'|','|'
C         WRITE(KUNIT,'(T2,A1,35A1,A2,4A6,A7,T71,A)')
C     +        '+',('-',ITER=1,35),'+-',('-----+',ITER=1,NSECEL),
C     +        '******+','-------------------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----------------------------+'//
     +        '+-----+-----+-----+-----+******+-------------------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '|seg  |        % of observed        |'//
     +        '|        % of predicted        |     information   |'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----------------------------+'//
     +        '+-----------------------------++---------+---------+'
         WRITE(KUNIT,'(T2,A,A)')
     +        '|     | % H | % E | % T | % L | sum |'//
     +        '| % H | % E | % T | % L | sum || I %obs  | I %pred |'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----+-----+-----+-----+-----+'//
     +        '+-----+-----+-----+-----+-----++---------+---------+'
         WRITE(KUNIT,'(T2,A7,5(F5.1,A1),A1,5(F5.1,A1),
     +        T70,A1,2(F6.3,A4))')'|Sov  |',
     +        (QSEGSOV(1,ITSEC),'|',ITSEC=1,(NSECEL+1)),'|',
     +        (QSEGSOV(2,ITSEC),'|',ITSEC=1,(NSECEL+1)),'|',
     +        INFO,'   |',INFO_INV,'   |'
         WRITE(KUNIT,'(T2,A1,A5,A1,5(F5.1,A1),A1,5(F5.1,A1),
     +        T70,A1,2(A5,I3,A2))')'|',TXTFOV(3:7),'|',
     +        (QSEGFOV(1,ITSEC),'|',ITSEC=1,(NSECEL+1)),'|',
     +        (QSEGFOV(2,ITSEC),'|',ITSEC=1,(NSECEL+1)),'|',
     +        '     ',INT(100*INFO/0.62),'%|',
     +        '     ',INT(100*INFO_INV/0.62),'%|'
         WRITE(KUNIT,'(T2,A,A)')
     +        '+-----+-----+-----+-----+-----+-----+'//
     +        '+-----+-----+-----+-----+-----++---------+---------+'
      END IF
      WRITE(KUNIT,*)
      END
***** end of STABLE_QILS

***** ------------------------------------------------------------------
***** SUB StrPos
***** ------------------------------------------------------------------
C---- 
C---- NAME : StrPos
C---- ARG  :  
C---- DES  : StrPos(Source,IStart,IStop): Finds the positions of the 
C---- DES  : first and last non-blank/non-TAB in Source. 
C---- DES  : IStart=IStop=0 for empty Source
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE StrPos(Source,IStart,IStop)

      CHARACTER*222   SOURCE
      INTEGER         ISTART,ISTOP

      ISTART=0
      ISTOP=0
      DO J=1,LEN(SOURCE)
         IF(SOURCE(J:J).NE.' ')THEN
            ISTART=J
            GOTO 20
         ENDIF
      ENDDO
      RETURN
 20   DO J=LEN(SOURCE),1,-1
         IF(SOURCE(J:J).NE.' ')THEN
            ISTOP=J
            RETURN
         ENDIF
      ENDDO
      ISTART=0
      ISTOP=0
      RETURN
      END
***** end of STRPOS

***** ------------------------------------------------------------------
***** SUB WRITELINES
***** ------------------------------------------------------------------
C---- 
C---- NAME : WRITELINES
C---- ARG  :  
C---- DES  : if 'cstring' contains '/n' (new line) this routine writes
C---- DES  : cstring line by line on screen; called by GETINT,GETREAL..
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE WRITELINES(CSTRING)

      CHARACTER*222   CSTRING
      INTEGER         ICUTBEGIN(10),ICUTEND(10)

      CALL StrPos(CSTRING,ISTART,ISTOP)
      ILINE=1
      ICUTBEGIN(ILINE)=1
      ICUTEND(ILINE)=ISTOP

      DO I=1,ISTOP-1
         IF(CSTRING(I:I+1).EQ.'/n')THEN
            ILINE=ILINE+1
            ICUTBEGIN(ILINE)=I+2
            ICUTEND(ILINE-1)=I-1
            ICUTEND(ILINE)=ISTOP
         ENDIF
      ENDDO
      DO I=1,ILINE
         WRITE(*,*)CSTRING(ICUTBEGIN(I):ICUTEND(I))
      ENDDO
      RETURN
      END
***** end of WRITELINES

C vim:et:ts=2:
