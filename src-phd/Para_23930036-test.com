*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          /home/rost/pub/phd/net/
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              1
 NUMNETSND=              1
 NUMNETJURY=             1
*****
 TRANS2FROM1(1:NUMNETSND) (20I4)
   1
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CICLD
*****
 MODEASSSTR(1:NUMNETSND) (row: no. A25)
         1          REAL-CCLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          F252x-15pcicld.DAT
 FILEASND(1:NUMNETSND) (row: no. A50)
         1          S252x-rccld-15pcicld.DAT
END
