*----------------------------------------------------------------------*
*     Burkhard Rost             May,        1998      version 0.1      *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Aug,        1998      version 0.2      *
*----------------------------------------------------------------------*

************************************************************************
*     This file contains the maximal parameters for executing multiple *
*     runs of secstron.  The specific parameter and variables for a    *
*     particular run are written in parsecstron resp parcollsec.       *
************************************************************************

      IMPLICIT NONE

*                                  ****                                *
****            *            ***PARAMETERS***           *            ***
*                               **********                             *
*                                   *                                  *

************************************************************************

C--------------------------------------------------
C---- numbers                                 -----
C--------------------------------------------------
C----                                         -----
C----                                         -----
C----                                         -----
C---- architecture of network                 -----
C----                                         -----
      INTEGER         NUMINMAX,NUMOUTMAX,NUMHIDMAX,
     +     NUMNEIGHMAX,NBIOLOBJMAX,NCODEUNTMAX,NHISTOMAX
C----                                         -----
C---- units (global)                          -----
C----                                         -----
      INTEGER         NUNITS_LENGTH,NUNITS_DISTCAPS
C----                                         -----
C---- reading databank                        -----
C----                                         -----
      INTEGER         NUMPROTMAX,NUMRESMAX
C----                                         -----
C---- jury decision                           -----
C----                                         -----
      INTEGER         NUMNETMAX,NUMNETFSTMAX,NUMNETSNDMAX
*                                                                      *
C----                                         -----
C---- data handling                           -----
C----                                         -----
      REAL            ABW
*                                                                      *
      LOGICAL         LDSSP
C---- protein (read hssp)
      INTEGER         MAXALIGNS,MAXCORE

*                                                                      *
*                                                                      *
C---- =================================================================*
*                                                                      *
C----                 ----------------------------------------
      PARAMETER      (NUMINMAX=                850)
      PARAMETER      (NUMOUTMAX=                10)
      PARAMETER      (NUMHIDMAX=                60)
      PARAMETER      (NUMNEIGHMAX=              21)
      PARAMETER      (NBIOLOBJMAX=              21)
      PARAMETER      (NCODEUNTMAX=              63)
      PARAMETER      (NHISTOMAX=                50)
C----                 -------------------------------
*                                                                      *
C----                 ------------------------------------
      PARAMETER      (NUNITS_LENGTH=             4)
      PARAMETER      (NUNITS_DISTCAPS=           4)
C----                 ------------------------------------
*                                                                      *
C----                 ---------------------------------------
      PARAMETER      (NUMPROTMAX=                2)
C     MAC core-dump for > 4200!
C     br 2003-08-23 fixed problem with core dump
C      PARAMETER      (NUMRESMAX=              4200)
C     exe normal
C      PARAMETER      (NUMRESMAX=              5000)
C     exe big
      PARAMETER      (NUMRESMAX=              9999)
C      PARAMETER      (NUMRESMAX=             10000)
C----                 ---------------------------------------
*                                                                      *
C----                 ---------------------------------------
C     for maxhom
C     
      PARAMETER      (MAXALIGNS=              5000)
C     br 2003-08-23: no longer relevant
      PARAMETER      (MAXCORE=               50000)
C      PARAMETER      (MAXCORE=             1000000)
C      PARAMETER      (MAXCORE=             3000000)
      INTEGER         NBLOCKSIZE
      PARAMETER      (NBLOCKSIZE=               70)
      INTEGER         MAXAA
      PARAMETER      (MAXAA=                    20)

      
C----                 ---------------------------------------
*                                                                      *
C----                 ---------------------------------------
      PARAMETER      (NUMNETMAX=                30)
      PARAMETER      (NUMNETFSTMAX=             15)
      PARAMETER      (NUMNETSNDMAX=             15)
C----                 ---------------------------------------
*                                                                      *
C----                 ---------------------------------------
      PARAMETER      (ABW=                       0.00001)
C----                 ---------------------------------------
*                                                                      *
*                                                                      *
C----                 ---------------------------------------
      PARAMETER      (LDSSP=.FALSE.)
C----                 ---------------------------------------
*                                                                      *
*                                                                      *
C---- =================================================================*

************************************************************************
*                                   *                                  *
****            *            ***VARIABLES***           *             ***
*                               *********                              *
*                                   *                                  *
*                                                                      *
************************************************************************

C--------------------------------------------------
C---- shared numbers                          -----
C--------------------------------------------------

C----                                         -----
C---- environment (path asf)                  -----
C----                                         -----
      CHARACTER*222   PATH_PARACOM,PATH_ARCH,PATH_WORK,PATH_PRED
      INTEGER         LENPATH_PARACOM
*                                                                      *
*                                                                      *
C----                                         -----
C---- hssp related variables                  -----
C----                                         -----
      CHARACTER*222   FILE_HSSP,FILE_HSSP_NOCHAIN
      CHARACTER*132   PROTNAME(1:NUMPROTMAX)
*                                                                      *
      CHARACTER*1     RESNAME(1:NUMRESMAX),
     +     RESSECSTR(1:NUMRESMAX),CONVSECSTR(1:NUMRESMAX),
     +     AACODE(1:NBIOLOBJMAX),SSCODE(1:7),EXPCODE(1:10)
      CHARACTER*24    AACODE24
      CHARACTER*26    AACODE_LOWC
*                                                                      *
C     br 2003-08-23: save space
C      INTEGER         RESVAR(1:NUMRESMAX)
      INTEGER         RESVAR(1:1)
C     br 2003-08-23: end save space
      INTEGER         RESACC(1:NUMRESMAX),
     +     RESPROF(1:NUMRESMAX,1:NBIOLOBJMAX),
     +     RESNDEL(1:NUMRESMAX),RESNINS(1:NUMRESMAX),
     +     NUMNALIGN(1:NUMPROTMAX)
*                                                                      *
      REAL*4          RESCONSWEIGHT(1:NUMRESMAX)
*                                                                      *
      INTEGER         POINTBEG(1:NUMPROTMAX),POINTEND(0:NUMPROTMAX),
     +     NUMRES,AABIT(1:NBIOLOBJMAX,1:NCODEUNTMAX)
*                                                                      *
      LOGICAL         LDSSPREAD
*                                                                      *
      COMMON /ENV1/PATH_PARACOM,PATH_ARCH,PATH_WORK,PATH_PRED
      COMMON /ENV2/LENPATH_PARACOM
      COMMON /DATAB1A/PROTNAME
      COMMON /DATAB1B/FILE_HSSP,FILE_HSSP_NOCHAIN
      COMMON /DATAB2/RESNAME,RESSECSTR,CONVSECSTR,AACODE,SSCODE,EXPCODE
      COMMON /DATAB2b/AACODE24
      COMMON /DATAB2c/AACODE_LOWC
      COMMON /DATAB3/RESACC,RESVAR,RESPROF,RESNDEL,RESNINS,NUMNALIGN,
     +     POINTBEG,POINTEND,NUMRES,AABIT
      COMMON /DATAB4/RESCONSWEIGHT
      COMMON /DATAB5/LDSSPREAD
*                                                                      *
*                                                                      *
C----                                         -----
C---- Exposure related variables              -----
C----                                         -----
      INTEGER         MAXEXP,T2,T3A,T3B
*                                                                      *
      REAL            THREXP2ST(1:3),THREXP3ST(1:4),THREXP10ST(1:10)
      INTEGER         THREXP2STI(1:3),THREXP3STI(1:4),THREXP10STI(1:10)
*                                                                      *
      COMMON /EXP1/MAXEXP,T2,T3A,T3B
      COMMON /EXP2/THREXP2ST,THREXP3ST,THREXP10ST
      COMMON /EXP3/THREXP2STI,THREXP3STI,THREXP10STI
*                                                                      *
C----                                         -----
C---- architecture                            -----
C----                                         -----
*                                                                      *
      INTEGER         NUMIN,NUMHID,NUMOUT,NUMNEIGH,NBIOLOBJ,NCODEUNT,
     +     NUMNETFST,NUMNETSND,NUMNETJURY,MAXVAR,
     +     MAXACC,PROFACC,CASCACC,NSECEL
      INTEGER        TRANS2FROM1(1:NUMNETSNDMAX)
*                                                                      *
      COMMON /COMPARA1/NUMIN,NUMHID,NUMOUT,NUMNEIGH,NBIOLOBJ,NCODEUNT,
     +     NUMNETFST,NUMNETSND,NUMNETJURY,MAXVAR,
     +     MAXACC,PROFACC,CASCACC,NSECEL
      COMMON /COMPARA2/TRANS2FROM1
*                                                                      *
C--------------------------------------------------
C---- characters/modes being shared           -----
C--------------------------------------------------
C----                                         -----
C---- different modes                         -----
C----                                         -----
*                                                                      *
      CHARACTER*222   MODEASSSTR(1:NUMNETSNDMAX),
     +     MODEASSCAS(1:NUMNETFSTMAX),MODESECSTRON
*                                                                      *
      CHARACTER*222   FILEPRED,FILEOUTPUT,ACTFILE,
     +     FILEARCHFST(1:NUMNETFSTMAX),FILEARCHSND(1:NUMNETSNDMAX),
     +     FILE_ARCHLIST,FILE_RDB,FILE_WHATIF,
     +     CHAR_ARG_READ(1:222)
*                                                                      *
      COMMON /COMPARA3/MODEASSSTR,MODEASSCAS,MODESECSTRON
      COMMON /COMPARA4/FILEPRED,FILEOUTPUT,ACTFILE,
     +     FILEARCHFST,FILEARCHSND,FILE_ARCHLIST,FILE_RDB,FILE_WHATIF,
     +     CHAR_ARG_READ

C--------------------------------------------------
C---- previous parameters for particular job  -----
C---- note: assigned by parset.f              -----
C--------------------------------------------------
*                                                                      *
C----                                         -----
C---- junctions, biases                       -----
C----                                         -----
*                                                                      *
      REAL            JUNCTION1ST(1:(NUMINMAX+NUMOUTMAX),1:NUMHIDMAX)
      REAL            LOCFIELD1(1:NUMHIDMAX)
      REAL            JUNCTION2ND(1:(NUMHIDMAX+NUMOUTMAX),1:NUMOUTMAX)
      REAL            LOCFIELD2(1:NUMOUTMAX)
*                                                                      *
      COMMON /TRIGGER3/JUNCTION1ST,JUNCTION2ND
      COMMON /TRIGGER4/LOCFIELD1,LOCFIELD2
*                                                                      *
C----                                         -----
C---- in/out vectors                          -----
C----                                         -----
*                                                                      *
      REAL*4          INPUT(1:(NUMINMAX+NUMOUTMAX),1:NUMRESMAX)
      REAL*4          OUTPUT(1:NUMOUTMAX,1:NUMRESMAX),
     +     OUTFIL(1:NUMOUTMAX,1:NUMRESMAX),
     +     OUTFST(1:NUMOUTMAX,1:NUMRESMAX,1:NUMNETFSTMAX),
     +     OUTSND(1:NUMOUTMAX,1:NUMRESMAX,1:NUMNETSNDMAX)
      REAL*4          OUTEXP(1:NUMRESMAX),DESEXP(1:NUMRESMAX),
     +     OUTEXPFIL(1:NUMRESMAX)
      INTEGER*4       OUTBIN(1:NUMOUTMAX,1:NUMRESMAX),
     +     OUTBINPROB(1:NUMOUTMAX,1:NUMRESMAX)
*                                                                      *
      CHARACTER*1    OUTBINCHAR(1:NUMRESMAX),OUTBINCHARFIL(1:NUMRESMAX)
*                                                                      *
*                                                                      *
      COMMON /EXTERN1/INPUT,OUTPUT,OUTFST,OUTSND,OUTEXP,DESEXP,
     +     OUTFIL,OUTEXPFIL
      COMMON /EXTERN2/OUTBIN,OUTBINPROB
      COMMON /EXTERN3/OUTBINCHAR,OUTBINCHARFIL
*                                                                      *
C----                                         -----
C---- reliability index                       -----
C----                                         -----
*                                                                      *
      INTEGER         NUMRELIND(0:9)
      INTEGER*2       RELIND(1:NUMRESMAX)
      INTEGER*2       DSSPVEC_I2(1:NUMRESMAX),PREDVEC_I2(1:NUMRESMAX)
*                                                                      *
      COMMON /EXTERN4a/NUMRELIND
      COMMON /EXTERN4b/RELIND,DSSPVEC_I2,PREDVEC_I2
*                                                                      *
*                                                                      *
C----                                         -----
C---- filtering stuff (exposure)              -----
C----                                         -----
*                                                                      *
      REAL            REDUCE_MINSIZE,REDUCE_STATE0,REDUCE_STATE1
      LOGICAL         LREDUCE_BURRIED
*                                                                      *
      COMMON /FILTER1/REDUCE_MINSIZE,REDUCE_STATE0,REDUCE_STATE1
      COMMON /FILTER2/LREDUCE_BURRIED
*                                                                      *
C----                                         -----
C---- run time variables                      -----
C----                                         -----
*                                                                      *
      CHARACTER*24    STARTDATE,ENDDATE,XDTE
      CHARACTER*8     STARTTIME,ENDTIME
*                                                                      *
      REAL            TIMEDIFF,TIMEARRAY,TIMESTART,TIMERUN,TIMEEND
*                                                                      *
      COMMON /CLOCK1/STARTDATE,ENDDATE,XDTE,STARTTIME,ENDTIME
      COMMON /CLOCK2/TIMEARRAY,TIMEDIFF,TIMESTART,TIMERUN,TIMEEND
*                                                                      *
*                                                                      *
C----                                         -----
C---- pay-off numbers                         -----
C----                                         -----
*                                                                      *
      INTEGER         MATNUM(1:(NUMOUTMAX+1),1:(NUMOUTMAX+1)),
     +     MATLEN(1:(NUMOUTMAX+1),1:4),
     +     MATLENDIS(1:NHISTOMAX,1:(2*NUMOUTMAX)),
     +     NUMSEGOVERL(1:9,1:(NUMOUTMAX+1)),
     +     COUNTSEGMAT(1:2,1:(NUMOUTMAX+1)),
     +     EXP_NOINBIN(0:9,0:9),
     +     EXP_NOIN2ST(1:3),EXP_NOIN3ST(1:4),EXP_NOIN10ST(1:11),
     +     OBS_NOIN2ST(1:3),OBS_NOIN3ST(1:4),OBS_NOIN10ST(1:11)
*                                                                      *
      REAL            Q3,SQ,CORR(1:NUMOUTMAX),INFO,INFO_INV,
     +   RMATQOFDSSP(1:NUMOUTMAX,1:NUMOUTMAX),
     +     RMATQOFPRED(1:NUMOUTMAX,1:NUMOUTMAX),
     +     QLOV(1:2,1:(NUMOUTMAX+1)),
     +     QSOV(1:2,1:(NUMOUTMAX+1)),QFOV(1:2,1:(NUMOUTMAX+1)),
     +     EXP_CORR
*                                                                      *
*                                                                      *
      CHARACTER*132   TITLE,VERSION_SEC,VERSION_EXP,VERSION_HTM
*                                                                      *
      COMMON /PAYOFF1/MATNUM,MATLEN,MATLENDIS,NUMSEGOVERL,COUNTSEGMAT,
     +     EXP_NOINBIN,EXP_NOIN2ST,EXP_NOIN3ST,EXP_NOIN10ST,
     +     OBS_NOIN2ST,OBS_NOIN3ST,OBS_NOIN10ST
      COMMON /PAYOFF2/Q3,SQ,CORR,RMATQOFDSSP,RMATQOFPRED,INFO,INFO_INV,
     +     QLOV,QSOV,QFOV,EXP_CORR
      COMMON /PAYOFF3/TITLE,VERSION_SEC,VERSION_EXP,VERSION_HTM
*                                                                      *
*                                                                      *
C----                                         -----
C---- content, asf.                           -----
C----                                         -----
*                                                                      *
      REAL     CONTPRED(1:NUMOUTMAX),CONTDSSP(1:NUMOUTMAX),CONTAA(1:24)
*                                                                      *
      COMMON /CONT/CONTPRED,CONTDSSP,CONTAA
*                                                                      *
C----                                         -----
C---- communication                           -----
C----                                         -----
*                                                                      *
      CHARACTER*1     ACTRESIDUE
      CHARACTER*15    CASEDISCR,CONTROLCHAR
      CHARACTER*25    ACTREGION
*                                                                      *
      INTEGER         ACTPOS,ACTSOLVADDBEG,ACTSOLVADDEND,
     +     ACTSOLVADDEND2,CODEVECPROF(1:NBIOLOBJMAX),PROFMAX,
     +     CODEVECINCASC(1:(NUMOUTMAX+1)),
     +     CODECOMPOSITION(1:NBIOLOBJMAX),
     +     ACTCOMPOSITION(1:NBIOLOBJMAX),
     +     ACTCHI,ACTITER,ACTSTART,ACTNDEL,ACTNINS,ACTNALIGN,ACTACC
*                                                                      *
      INTEGER         BEGUNITS_COMPOSITION,
     +     BEGUNITS_LENGTH,BEGUNITS_DISTCAPS,
     +     SPLIT_LENGTH(1:NUNITS_LENGTH),
     +     SPLIT_DISTCAPS(1:(NUNITS_LENGTH+1))
*                                                                      *
      REAL            CODEVECTOR(1:NCODEUNTMAX),PROFINTERV,CASCINTERV,
     +     ACTCONSWEIGHT,ACTINTERVALL
*                                                                      *
      LOGICAL         LOGI_COMPOSITION,LOGI_LENGTH,LOGI_DISTCAPS,
     +     LOGI_CONS,LOGI_INDEL,LOGI_REALINPUT
*                                                                      *
      COMMON /CODE1A/ACTRESIDUE
      COMMON /CODE1B/CASEDISCR,CONTROLCHAR
      COMMON /CODE1C/ACTREGION
      COMMON /CODE3/ACTPOS,ACTSOLVADDBEG,ACTSOLVADDEND,ACTSOLVADDEND2,
     +     CODEVECPROF,PROFMAX,CODEVECINCASC,
     +     CODECOMPOSITION,ACTCOMPOSITION,
     +     ACTCHI,ACTITER,ACTSTART,ACTNDEL,ACTNINS,ACTNALIGN,ACTACC
      COMMON /CODE4/CODEVECTOR,PROFINTERV,CASCINTERV,ACTCONSWEIGHT,
     +     ACTINTERVALL
      COMMON /CODE5/BEGUNITS_COMPOSITION,
     +     BEGUNITS_LENGTH,BEGUNITS_DISTCAPS,
     +     SPLIT_LENGTH,SPLIT_DISTCAPS
      COMMON /CODE6/LOGI_COMPOSITION,LOGI_LENGTH,LOGI_DISTCAPS,
     +     LOGI_CONS,LOGI_INDEL,LOGI_REALINPUT
*                                                                      *
C----                                         -----
C---- controls                                -----
C----                                         -----
*                                                                      *
      LOGICAL          LSERVER,LFILTER,LDEC,LOUTBINPROB,
     +     LMACHINE_READABLE,LWHATIF,LRDB
*                                                                      *
      COMMON /CONTROL/LSERVER,LFILTER,LDEC,LOUTBINPROB,
     +     LMACHINE_READABLE,LWHATIF,LRDB
*                                                                      *
*----------------------------------------------------------------------*
*     Parmaters and Common variables for PHD                           *
*----------------------------------------------------------------------*
