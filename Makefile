PACKAGE := profnet
VERSION := 1.0.22
DISTDIR := $(PACKAGE)-$(VERSION)

SRCDIR := src

NET := bval chop con isis md norsnet prof snapfun
NETDIR := $(addsuffix _dir,$(NET))
NETDIFF := $(addprefix diff-,$(NET))
NETPATCH := $(NET:%=%_dir/patch_stamp)
NETPATCHNEW := $(addsuffix .patch.new,$(NET))
NETDISTCLEAN := $(addprefix distclean-,$(NET))
NETINSTALL := $(addprefix install-,$(NET))

MAN1POD := profnet.pod
MAN1GZ := $(MAN1POD:%.pod=%.1.gz)  

all:	net man phd

net:	$(NET)

diff:	$(NETDIFF)

$(NETDIFF) : diff-% : %.patch.new

$(NETPATCHNEW): %.patch.new :
	if [ -d $*_dir ]; then \
		diff -ruN -x '.*.swp' -x 'profnet*' -x .svn src $*_dir > $@ || true; \
	fi

$(NETDIR) : %_dir :
	mkdir -p $@ && rsync -avC src/. ./$@/.

%_dir/patch_stamp: | %_dir
	patch -p 1 -E -d $*_dir < $*.patch && touch $@

patch: $(NETPATCH)

$(NET) : % : | %_dir/patch_stamp
	$(MAKE) -C $@_dir

$(NETINSTALL) : install-% :
	mkdir -p $(DESTDIR)$(prefix)/bin && \
		cp -a $*_dir/profnet_$* \
		$(DESTDIR)$(prefix)/bin/
	mkdir -p $(DESTDIR)$(prefix)/share/man/man1 && cp $(MAN1GZ) $(DESTDIR)$(prefix)/share/man/man1/profnet_$*.1.gz

phd:
	$(MAKE) -C src-phd

install-phd:
	$(MAKE) -C src-phd install
	mkdir -p $(DESTDIR)$(prefix)/share/man/man1 && cp $(MAN1GZ) $(DESTDIR)$(prefix)/share/man/man1/profphd_net.1.gz
	rm -f $(DESTDIR)$(prefix)/share/man/man1/phd1994.1.gz && ln -s profphd_net.1.gz $(DESTDIR)$(prefix)/share/man/man1/phd1994.1.gz

install: $(NETINSTALL) install-phd

man:    $(MAN1GZ) $(MAN3:%.pod=%.3.gz)   

$(MAN1GZ) : %.1.gz : %.pod 
	pod2man -c 'User Commands' -r "$(VERSION)-$(RGVERSION)" $< | gzip -c > $@


help:
	@echo "Rules:"
	@echo "all* - build all"
	@echo "net - make all network binaries"
	@echo "$(NET) -"
	@echo "  make a specific network binary"
	@echo "diff - prepare patch files for profnet varieties"
	@echo "$(NETDIFF) -"
	@echo "  make a specific network variety patch"
	@echo "  A diff is run between the src dir - the base network - and the"
	@echo "  variety given.  This prepares a patch file with .patch.new extension"
	@echo "  that can me renamed to .patch.  Network varieties are obtained by"
	@echo "  applying the appropriate .patch file to the base network in src."
	@echo "  Importantly only these patch files are distributed with this package,"
	@echo "  the network variety directories (e.g. snapfun_dir) are used only for"
	@echo "  building the binary.  Preserve your updates to the base version"
	@echo "  by preparing and updating the appropriate patch file."
	@echo "$(NETDISTCLEAN) -"
	@echo "  distclean a specific network variety"
	@echo "clean"
	@echo "dist"
	@echo "distclean"
	@echo "man - make all manual pages"
	@echo "install - install all"
	@echo "$(NETINSTALL) -"
	@echo "  install a specific variety to \$$DESTDIR\$$prefix"
	@echo "install-phd - install profphd_net"
	@echo "patch - patch and prepare all for compilation"
	@echo "phd - make profphd_net"
	@echo
	@echo "Variables:"
	@echo "  Meaning and purpose not clear any more."
	@echo "DESTDIR - install to DESTDIR"
	@echo "prefix - common installation prefix for all files"
	@echo "  use prefix=$$HOME to build for personal use"

distclean: clean $(NETDISTCLEAN)
	rm -rf\
		$(DISTDIR) \
		$(DISTDIR).tar.gz

$(NETDISTCLEAN) : distclean-% :
	rm -rf $*_dir

dist:	$(DISTDIR)
	tar -ch -f - "$(DISTDIR)" | gzip -c >$(DISTDIR).tar.gz
	rm -rf $(DISTDIR)

$(DISTDIR): distclean
	rm -rf $(DISTDIR) && mkdir -p $(DISTDIR) && \
	rsync -avC \
		--exclude /*-stamp \
		--exclude .*.swp \
		--exclude debian \
		AUTHORS \
		ChangeLog \
		COPYING \
		Makefile \
		$(PACKAGE).spec \
		src \
		src-phd \
		*.patch \
		*.pod \
		$(DISTDIR)/;


clean: 
	$(MAKE) -C src clean
	$(MAKE) -C src-phd clean
	rm -f *.[0123456789].gz 

.PHONY: all clean diff install-phd man net patch phd $(NET) $(NETDIFF) $(NETDISTCLEAN) $(NETPATCHNEW)

# vim:ai:
