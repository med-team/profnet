Summary: neural network architectures for Rost Lab prediction methods
Name: profnet
Version: 1.0.20
Release: 1
License: GPL
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: gcc-gfortran, rsync

%define common_desc Profnet is a component of the prediction methods that make up the \
 Predict Protein service by the lab of Burkhard Rost. It provides the neural \
 network component to a variety of predictors that perform protein feature \
 prediction directly from sequence.  This neural network implementation has \
 to be compiled for every different network architecture.

%description
 %{common_desc}

%package bval
Summary: neural network architecture for profbval
Group: Applications/Science

%description bval
 %{common_desc}
 .
 This package contains the neural network architecture for profbval.

%package chop
Summary: neural network architecture for profchop
Group: Applications/Science

%description chop
 %{common_desc}
 .
 This package contains the neural network architecture for profchop.

%package con
Summary: neural network architecture for profcon
Group: Applications/Science

%description con
 %{common_desc}
 .
 This package contains the neural network architecture for profcon.

%package isis
Summary: neural network architecture for profisis
Group: Applications/Science

%description isis
 %{common_desc}
 .
 This package contains the neural network architecture for profisis.

%package md
Summary: neural network architecture for metadisorder
Group: Applications/Science

%description md
 %{common_desc}
 .
 This package contains the neural network architecture for metadisorder.

%package norsnet
Summary: neural network architecture for norsnet
Group: Applications/Science

%description norsnet
 %{common_desc}
 .
 This package contains the neural network architecture for norsnet.

%package prof
Summary: neural network architecture for profacc
Group: Applications/Science

%description prof
 %{common_desc}
 .
 This package contains the neural network architecture for profacc.

%package snapfun
Summary: neural network architecture for snapfun
Group: Applications/Science

%description snapfun
 %{common_desc}
 .
 This package contains the neural network architecture for snapfun.

%package phdnet
Summary: neural network architecture for profphd
Group: Applications/Science

%description phdnet
 %{common_desc}
 .
 This package contains the neural network architecture for profphd.

%prep
%setup -q

%build
make prefix=%{_prefix}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} prefix=%{_prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING

%files bval
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_bval
%{_mandir}/*/profnet_bval*

%files chop
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_chop
%{_mandir}/*/profnet_chop*

%files con
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_con
%{_mandir}/*/profnet_con*

%files isis
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_isis
%{_mandir}/*/profnet_isis*

%files md
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_md
%{_mandir}/*/profnet_md*

%files norsnet
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_norsnet
%{_mandir}/*/profnet_norsnet*

%files prof
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_prof
%{_mandir}/*/profnet_prof*

%files snapfun
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profnet_snapfun
%{_mandir}/*/profnet_snapfun*

%files phdnet
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/profphd_net
%{_bindir}/phd1994
%{_mandir}/*/profphd_net*
%{_mandir}/*/phd1994*

%changelog
* Fri Jun 17 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.rg1-1
- First rpm package
