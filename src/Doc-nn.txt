* ----------------------------------------------------------------------* 
* 
* organisation and dependence of program, functions, and subroutines
* for nn.f
* 
* ------------------------------
* keywords:
* ------------------------------
* 
*   nn                   internal subroutines:
*   nn                   external subroutines:
*   lib-nn               internal subroutines:
*   lib-nn               external subroutines:
*   lib-SGI64            internal subroutines:
* 
* ------------------------------
* notations:
* ------------------------------
* 
*   '-> x'               means is calling the following
*   '.. self content'    means: no subroutine called
* 
* 
* ----------------------------------------------------------------------* 
*                           ---------------------
*      nn                   internal subroutines:
*                           ---------------------
* 
*      MAIN                 -> GET_ARG_NUMBER,GET_ARGUMENT,SRDTIME,
*                           -> SCFDATE,ININN,RDPAR,RDIN,RDOUT,RDSAM,
*                           -> RDJCT,
*                           -> INIJCT,
*                           -> INITHRUNT,
*                           -> NETOUT,
*                           -> WRTOUT,
*                           -> WRTJCT,
*                           -> TRAIN,
*                           -> WRTSCR,
*                           -> WRTERR,
*                           -> WRTYEAH
*      TRG1ST               .. self-content
*      TRG2ND               .. self-content
*      TRGNORM              .. self-content
*      ININN                -> INIPAR_CON,INIPAR_DEFAULT,INIPAR_ASK,
*                           -> INIPAR_SWITCH
*      INIPAR_ASK           -> GETCHAR,GETINT
*      INIPAR_CON           .. self-content
*      INIPAR_DEFAULT       .. self-content
*      INIPAR_SWITCH        -> SCHAR_TO_INT
*      INITHRUNT            .. self-content
*      RDJCT                -> SFILEOPEN,RDJCT_HEAD,RDJCT_JCT1,
*                           -> RDJCT_JCT2,RDJCT_WRT
*      RDJCT_HEAD           -> RDJCT_CHECK
*      RDJCT_JCT1           .. self-content
*      RDJCT_JCT2           .. self-content
*      RDJCT_CHECK          .. self-content
*      RDJCT_WRT            .. self-content
*      RDIN                 -> SFILEOPEN,RDIN_HEAD,RDIN_DATA,RDIN_WRT
*      RDIN_HEAD            .. self-content
*      RDIN_DATA            .. self-content
*      RDIN_WRT             .. self-content
*      RDOUT                -> SFILEOPEN,RDOUT_HEAD,RDOUT_DATA,
*                           -> RDOUT_WRT
*      RDOUT_HEAD           .. self-content
*      RDOUT_DATA           .. self-content
*      RDOUT_WRT            .. self-content
*      RDPAR                -> SFILEOPEN,RDPAR_I,RDPAR_F,RDPAR_A,
*                           -> RDPAR_WRT
*      RDPAR_I              -> RDPAR_ERR
*      RDPAR_F              .. self-content
*      RDPAR_A              .. self-content
*      RDPAR_ERR            .. self-content
*      RDPAR_WRT            .. self-content
*      RDSAM                -> SFILEOPEN
*      NETOUT               -> NETOUT_MUE,NETOUT_BIN,NETOUT_ERR
*      NETOUT_MUE           .. self-content
*      NETOUT_BIN           .. self-content
*      NETOUT_ERR           .. self-content
*      TRAIN                -> NETOUT,TRAIN_STOP,TRAIN_WRT,
*                           -> TRAIN_INISWP,TRAIN_INIMUE,
*                           -> TRAIN_BACKPROP,
*                           -> WRTOUT,
*                           -> WRTJCT
*      TRAIN_BACKPROP       .. self-content
*      TRAIN_INIMUE         -> SRSTE2,SRSTZ2,NETOUT_MUE
*      TRAIN_INISWP         -> SRSTZ2
*      TRAIN_STOP           .. self-content
*      TRAIN_WRT            .. self-content
*      WRTJCT               -> SFILEOPEN,WRTHEAD
*      WRTOUT               -> WRTHEAD,NETOUT_MUE
*      WRTERR               -> SFILEOPEN
*      WRTHEAD              -> WRTHEAD_GEN,WRTHEAD_JOB
*      WRTHEAD_GEN          .. self-content
*      WRTHEAD_JOB          .. self-content
*      WRTSCR               -> WRTERR
*      WRTYEAH              -> SFILEOPEN
* 
* ----------------------------------------------------------------------* 
*                        ---------------------
*   nn                   external subroutines:
*                        ---------------------
* 
*   call from missing:    
*                        GETCHAR
*                        GETINT
*                        GET_ARGUMENT
*                        GET_ARG_NUMBER
*                        INIJCT
*                        SCFDATE
*                        SCHAR_TO_INT
*                        SFILEOPEN
*                        SRDTIME
*                        SRSTE2
*                        SRSTZ2
# 
* 
* ----------------------------------------------------------------------* 
*                           ---------------------
*      lib-nn               internal subroutines:
*                           ---------------------
* 
*      EMPTYSTRING          .. self-content
*      FILEN_STRING         .. self-content
*      FILENSTRING          .. self-content
*      FILENSTRING_ALPHANUMSEN .. self-content
*      FRMAX1               .. self-content
*      FRMAX2               .. self-content
*      GETCHAR              -> WRITELINES
*      GETINT               -> WRITELINES,STRPOS,GETTOKEN,RIGHTADJUST
*      GETTOKEN             .. self-content
*      GET_ARGUMENT         -> GETARG
*      GET_ARG_NUMBER       .. self-content
*      RIGHTADJUST          .. self-content
*      SCFDATE              .. self-content
*      SFILEOPEN            .. self-content
*      SCHAR_TO_INT         -> SILEN_STRING
*      SILEN_STRING         .. self-content
*      SRSTE2               .. self-content
*      SRSTZ2               .. self-content
*      STRPOS               .. self-content
*      WRITELINES           -> STRPOS
* 
* ----------------------------------------------------------------------* 
*                        ---------------------
*   lib-nn               external subroutines:
*                        ---------------------
* 
*   call from missing:    
*                        GETARG
# 
* 
* ----------------------------------------------------------------------* 
*                           ---------------------
*      lib-SGI64            internal subroutines:
*                           ---------------------
* 
*      INIJCT               .. self-content
*      SRDTIME              .. self-content
* 
* ----------------------------------------------------------------------* 
# 
