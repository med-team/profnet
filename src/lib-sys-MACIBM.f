***** ------------------------------------------------------------------
***** FCT FCTIME_DATE
***** ------------------------------------------------------------------
C---- 
C---- NAME : FCTIME_DATE
C---- ARG  :  
C---- DES  :   
C---- OUT  : character*24 'YYYY_MM_DD - hh:mm:ss'
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Oct,        2003        version 1.0    *
*     EMBL/LION                 http://www.predictprotein.org/         *
*     D-69012 Heidelberg        rost@columbia.edu                      *
*                      changed: Aug,        2003        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        returns date                                     *
*     note:           machine type dependent:                          *
*                     SGI, UNIX, LINUX: absolute unix time             *
*                     IBM: char*8 YYYYMMDD                             *
*     input :         NOM, DEN                                         *
*----------------------------------------------------------------------*
      CHARACTER*24 FUNCTION FCTIME_DATE()
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      CHARACTER*24    CTEMP,CTEMP2
******------------------------------*-----------------------------******
*     execution of function                                            *
      CTEMP=' '
      CALL DATE_AND_TIME(CTEMP)

      FCTIME_DATE=       ' '
      FCTIME_DATE(1:4)=  CTEMP(1:4)
      FCTIME_DATE(5:5)=  '_'
      FCTIME_DATE(6:7)=  CTEMP(5:6)
      FCTIME_DATE(8:8)=  '_'
      FCTIME_DATE(9:10)= CTEMP(7:8)
      FCTIME_DATE(11:13)=' - '
      FCTIME_DATE(14:15)=CTEMP2(1:2)
      FCTIME_DATE(16:16)=':'
      FCTIME_DATE(17:18)=CTEMP2(3:4)
      FCTIME_DATE(19:19)=':'
      FCTIME_DATE(20:21)=CTEMP2(5:6)

      END
***** end of FCTIME_DATE

***** ------------------------------------------------------------------
***** FCT FRTIME_SECNDS
***** ------------------------------------------------------------------
C---- 
C---- NAME : FRTIME_SECNDS
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Oct,        2003        version 1.0    *
*     EMBL/LION                 http://www.predictprotein.org/         *
*     D-69012 Heidelberg        rost@columbia.edu                      *
*                      changed: Aug,        2003        version 1.0    *
*----------------------------------------------------------------------*
*     purpose:        returns CPU time seconds                         *
*     note:           machine type dependent:                          *
*                     SGI, UNIX, LINUX: absolute unix time             *
*                     IBM: cputime                                     *
*     input :         T1: time to start (from previous call for unix)  *
*----------------------------------------------------------------------*
      REAL FUNCTION FRTIME_SECNDS(T1)
      IMPLICIT        NONE
C---- variables passed from/to SBR calling
      REAL            T1
******------------------------------*-----------------------------******
*     execution of function                                            *
      CALL CPU_TIME(T1)
      FRTIME_SECNDS=T1
      END
***** end of FRTIME_SECNDS

***** ------------------------------------------------------------------
***** SUB INIJCT
***** ------------------------------------------------------------------
C---- 
C---- NAME : INIJCT
C---- ARG  : 
C---- DES  : Generation of the initial couplings and biases of
C---- DES  : the network. Options: 
C---- DES  :    RANDOM : locfield (i,j,TIMEST=0) = [-/+diceintervall]
C---- DES  :             with equal distribution
C---- IN  p: NUMSAM,NUMIN,NUMHID,NUMOUT, DICESEED
C---- IN  v: JCT1ST,JCT2ND,BIAS1ST,BIAS2ND 
C---- OUT  : setting of JCT1ST, JCT2ND 
C---- FROM : MAIN
C---- CALL2: 
C---- LIB  : RAN(SEED), creates random numbers between 0 and    *
C---- LIB+ :           1 (1 excluded, 0 included), it is called by com-*
C---- LIB+ :           piling with -lV77, each call initializes next   *
C---- LIB+ :           seed, according to: seed=6909*seed+mod(2**32)   *
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost		May,        1998      version 0.1      *
*     EMBL/LION			http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg	rost@embl-heidelberg.de                *
*	               changed:	June,       1998      version 0.2      *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
      SUBROUTINE INIJCT

C---- global parameters and variables
      INCLUDE       'profPar.f'
C---- local variables
      INTEGER       I,J,NUMHIDFIRST,NUMHIDLAST
Cunix
C      REAL          RAN,DICE1,DICE2,DICEINTERVX
Clinux
      REAL          RAND,DICE1,DICE2,DICEINTERVX
******------------------------------*-----------------------------******
*     I,J                serve as iteration variables                  *
*     DICE1,2            =RAN(DICESEED1),i.e. random number between 0,1*
*     NUMHIDFIRST        number of units in the first hidden layer     *
*     NUMHIDLAST         number of units in the last hidden layer      *
******------------------------------*-----------------------------******

      DICEINTERVX=DICEITRVL

C---- ------------------------------------------------------------------
C---- first layer junctions 
C---- ------------------------------------------------------------------
      DICESEED1=DICESEED+DICESEED_ADDJCT
      IF (NUMLAYERS.EQ.1) THEN
         NUMHIDFIRST=NUMOUT
         NUMHIDLAST=0
      ELSE
         NUMHIDFIRST=NUMHID
         NUMHIDLAST=NUMHID
      END IF
C---- loop over hidden units
      DO I=1,NUMHIDFIRST
C------- junctions (loop over input units)
         DO J=1,NUMIN
C---------- generating random numbers from [0,1)
Cunix
C            DICE1= RAN(DICESEED1)
C            DICE2= RAN(DICESEED1)
Clinux
C            DICE1= RAND(DICESEED1)
C            DICE2= RAND(DICESEED1)
Cibm
            CALL RANDOM_NUMBER(DICE1)
            CALL RANDOM_NUMBER(DICE2)
            IF (DICE1.LT.0.5) THEN
               JCT1ST(J,I)= (-1.)*DICEINTERVX*DICE2
            ELSE
               JCT1ST(J,I)= DICEINTERVX*DICE2
            END IF
         END DO
C------- thresholds (resp. biases)
C------- generating random numbers from [0,1)
Cunix
C         DICE1= RAN(DICESEED1)
C         DICE2= RAN(DICESEED1)
Clinux
C         DICE1= RAND(DICESEED1)
C         DICE2= RAND(DICESEED1)
Cibm
         CALL RANDOM_NUMBER(DICE1)
         CALL RANDOM_NUMBER(DICE2)
         IF (DICE1.LT.0.5) THEN
            JCT1ST((NUMIN+1),I)=
     +           (-1.)*DICEINTERVX*DICE2
         ELSE
            JCT1ST((NUMIN+1),I)=DICEINTERVX*DICE2
         END IF
      END DO
C---- ------------------------------------------------------------------
C---- last layer junctions
C---- ------------------------------------------------------------------
      DO I=1,NUMOUT
C------- junctions
         DO J=1,NUMHIDLAST
C---------- generating random numbers from [0,1)
Cunix
C            DICE1= RAN(DICESEED1)
C            DICE2= RAN(DICESEED1)
Clinux
C            DICE1= RAND(DICESEED1)
C            DICE2= RAND(DICESEED1)
Cibm
            CALL RANDOM_NUMBER(DICE1)
            CALL RANDOM_NUMBER(DICE2)
            IF (DICE1.LT.0.5) THEN
               JCT2ND(J,I)=(-1.)*DICEINTERVX*DICE2
            ELSE
               JCT2ND(J,I)=DICEINTERVX*DICE2
            END IF
         END DO
C------- thresholds (resp. biases)
C------- generating random numbers from [0,1)
Cunix
C         DICE1= RAN(DICESEED1)
C         DICE2= RAN(DICESEED1)
Clinux
C         DICE1= RAND(DICESEED1)
C         DICE2= RAND(DICESEED1)
Cibm
         CALL RANDOM_NUMBER(DICE1)
         CALL RANDOM_NUMBER(DICE2)
         IF (DICE1.LT.0.5) THEN
            JCT2ND((NUMHID+1),I)=(-1.)*DICEINTERVX*DICE2
         ELSE
            JCT2ND((NUMHID+1),I)=DICEINTERVX*DICE2
         END IF
      END DO
      END
***** end of INIJCT

***** ------------------------------------------------------------------
***** SUB SRDTIME
***** ------------------------------------------------------------------
C---- 
C---- NAME : SRDTIME
C---- ARG  :  
C---- DES  :   
C---- 
*----------------------------------------------------------------------*
*     Burkhard Rost             Aug,        1998        version 1.0    *
*     EMBL/LION                 http://www.embl-heidelberg.de/~rost/   *
*     D-69012 Heidelberg        rost@embl-heidelberg.de                *
*                      changed: Dec,        1991        version 0.1    *
*                      changed: Aug,        1998        version 1.0    *
*----------------------------------------------------------------------*
***                                                                  ***
***                                                                  ***
***   SUBROUTINE SRDTIME                                             ***
***                                                                  ***
***                                                                  ***
*----------------------------------------------------------------------*
      SUBROUTINE SRDTIME(LOGIWRITE)

      IMPLICIT         NONE
Cunix
C      REAL             TIMEARRAYM,TIMEDIFF,DTIME,TIME_TMP
Clinux
      REAL             TIMEARRAYM(1:2),TIMEDIFF,DTIME,TIME_TMP
      INTEGER          ITER
      LOGICAL          LOGIWRITE

Cunix
C      TIMEDIFF=DTIME(TIMEARRAYM,TIME_TMP)
Clinux
CC      TIMEDIFF= TIMEDIFFX(1)
C      TIMEDIFF=DTIME(TIMEARRAYM)
      
Cibm
      CALL CPU_TIME(TIMEDIFF)

      IF (LOGIWRITE) THEN
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
         WRITE (6,'(T10,A12,T25,F9.3,A5)')
     +        'total time: ',TIMEDIFF,'  sec'
         WRITE(6,*)
         WRITE(6,'(T10,7A5)')('-----',ITER=1,7)
         WRITE(6,*)
      END IF

      END
***** end of SRDTIME

